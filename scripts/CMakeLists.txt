find_package(Python3 COMPONENTS Interpreter REQUIRED)
# Idea from Mark Moll
# http://www.cmake.org/pipermail/cmake/2011-January/041666.html
function(find_python_module module)
    string(TOUPPER ${module} module_upper)
    execute_process(COMMAND "${Python3_EXECUTABLE}" "-c" "import ${module};"
            RESULT_VARIABLE RESULT
            OUTPUT_VARIABLE OUTPUT
            ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
    if(NOT RESULT) # If module found (return code 0)
        set(HAVE_PYTHON_${module_upper} TRUE)
        message("-- Found python module: ${module}")
    else()
        message( FATAL_ERROR "-- ERROR: Could not find python package ${module}")
    endif()
endfunction(find_python_module)

find_python_module(numpy)
find_python_module(h5py)
find_python_module(matplotlib)
find_python_module(re)
find_python_module(os)
find_python_module(optparse)
find_python_module(subprocess)

#INSTALLING PYTHON-SCRIPTS
set(PYTHONSITEDIR lib/python${Python3_VERSION_MAJOR}.${Python3_VERSION_MINOR}/site-packages/)
install(DIRECTORY .
    DESTINATION ${PYTHONSITEDIR}
    FILES_MATCHING PATTERN "*.py"
    PATTERN "__pycache__" EXCLUDE
    PERMISSIONS OWNER_READ OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
)

#INSTALLING EXECUTABLE PYTHON-SCRIPTS

#for each python-script, make an executable script
file(GLOB PYTHONSCRIPTS draw_*.py animate_*.py)
foreach(SCRIPTPATH ${PYTHONSCRIPTS})
    get_filename_component(SCRIPTNAME ${SCRIPTPATH} NAME_WE)
    configure_file("./python_wrapper.in" "./${SCRIPTNAME}")
    install(FILES ${CMAKE_BINARY_DIR}/scripts/${SCRIPTNAME}
        DESTINATION bin
        PERMISSIONS OWNER_READ OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )
endforeach()
