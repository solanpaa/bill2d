#!/usr/bin/env python3
#
# draw_bounce_map.py
#
# This file is part of bill2d.
#

#
# bill2d is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bill2d is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bill2d.  If not, see <http://www.gnu.org/licenses/>.


import h5py
from matplotlib import pyplot
from numpy import array, arange, loadtxt
from math import ceil, pi
from os.path import exists
import re
from optparse import OptionParser

parser = OptionParser(usage="%prog [options] \n This script draws the bounce map as a scatter plot. Input can be given as\n a hdf5-file or a path to txt-files as given to bill2d.")
parser.add_option("-f",'--file', action="store", type="string", dest="datafile", help="Set the datafile (hdf5) or txt-bouncemap path", default="data/bill2d.h5")
parser.add_option("--vt", action="store_true", dest="tang_vel", help="Plot the tangential velocity instead of the bounce angle", default=False)
args=parser.parse_args()[0]

filee=args.datafile
tang_vel=args.tang_vel
try:
    f = h5py.File(filee, 'r')
except:
    print("File not found or not an hdf5-file")
    exit(1)

if(tang_vel):
    index=2 #Index 1 = tangential velocity to boundary
else:
    index=3 #Index 2 = angle

global_attrs = f["/parameters"].attrs
inter_str=float(global_attrs["interaction_strength"])
shape_string=global_attrs["table_shape"]
s=0
# Rectangle
m = re.match(r"rectangle\((?P<width>[^,]+),(?P<height>[^,]+)\)", shape_string)
if(m!=None and s==0):
    s=[0,float(m.group("width"))]

# Circle
if(m==None):
    m = re.match(r"circle\((?P<cx>[^,]+),(?P<cy>[^,]+),(?P<r>[^,]+)\)", shape_string)
if(m!=None and s==0):
    s=[-pi*float(m.group("r")),pi*float(m.group("r"))]

# Ellipse
if(m==None):
    m = re.match(r"ellipse\((?P<a>[^,]+),(?P<b>[^,]+),(?P<max>[^,]+)\)", shape_string)
if(m!=None and s==0):
    s=[0,float(m.group("max"))]

# Stadium
if(m==None):
    m = re.match(r"stadium\((?P<R>[^,]+),(?P<L>[^,]+)\)", shape_string)
if(m!=None and s==0):
    s=[0,2*pi*float(m.group("R"))+2*float(m.group("L"))]

#Sinai
if(m==None):
   m = re.match(r"sinai\((?P<l>[^,]+),(?P<r>[^,]+)\)", shape_string)

if(m != None and s==0):
   s=[0,float(m.group("l"))]

#Mushroom
if(m==None):
    m = re.match(r"mushroom\((?P<r>[^,]+),(?P<fh>[^,]+),(?P<fw>[^,]+)\)", shape_string)
if(m != None and s==0):
    s = [0,(2+pi)*float(m.group("r"))+2*float(m.group("fh"))]

#NoTable
if(m==None):
    m = re.match(r"notableperiodic\(\)", shape_string)
if(m != None and s==0):
    L1=global_attrs["uc_L1"]
    L2=global_attrs["uc_L2"]
    s = [0,2*(L1+L2)]

#Lorentz-gas
if(m==None):
    m = re.match(r"lorentzgas\((?P<r>[^,]+)\)", shape_string)
if(m != None and s==0):
    radius=float(m.group('r'))
    L1=global_attrs["uc_L1"]
    L2=global_attrs["uc_L2"]
    s = [0,radius*(pi/2.-4)+L1+L2]


if(m==None):
    print("Not supported billiard table for bounce maps")
    #exit(1)


fig = pyplot.figure()
ax=fig.add_subplot(1,1,1)
for key in list(f["/bounce maps"].keys()):
        data=array(f["/bounce maps/"+key])

        ax.scatter(data[:,1],data[:,index],s=1);

        for loc, spine in ax.spines.items():
            if loc in ["top", "right"]:
                spine.set_color('none') # don't draw spine
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')

        fontsize=8
        for tick in ax.xaxis.get_major_ticks():
            tick.label1.set_fontsize(fontsize)

        for tick in ax.yaxis.get_major_ticks():
            tick.label1.set_fontsize(fontsize)

        ax.set_title("Particle "+re.match(r"particle(?P<N>[^,]+)",key).group("N"))
        ax.set_xlabel(r'$s$',fontsize=16)

        if(tang_vel):
            ax.set_ylabel(r'$v_{\parallel}$',fontsize=16)
        else:
            ax.set_ylabel(r'$\theta$',fontsize=16)
        if(index==3):
            ax.set_ylim((-3.14/2,3.14/2))
        else:
            ax.set_ylim((-2,2))
        #ax.set_xlim((s[0],s[1]))

pyplot.show()
