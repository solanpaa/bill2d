#!/usr/bin/env python3
#
# draw_speed_histogram.py
#
# This file is part of bill2d.
#


#
# bill2d is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bill2d is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bill2d.  If not, see <http://www.gnu.org/licenses/>.


import h5py
from matplotlib import pyplot
from numpy import array, float64
from math import ceil
from os.path import exists
from optparse import OptionParser
parser = OptionParser(usage="%prog [options] \n This script draws the speed histogram.")
parser.add_option("-f",'--file', action="store", type="string", dest="datafile", help="Set the datafile (hdf5) or txt-bouncemap path", default="data/bill2d.h5")

args=parser.parse_args()[0]
dfilee=args.datafile
if(not exists(dfilee)):
    print("Error: Datafile does not exist.")
    exit(1)
f = h5py.File(dfilee, 'r')
global_attrs = f["/parameters"].attrs
iterations = global_attrs["iterations"]
fig = pyplot.figure()
N=len(list(f["/speed_histograms"].keys()))
n=0
for key in list(f["/speed_histograms"].keys()):
    ax = fig.add_subplot(int(ceil(N/3)), min((N,3)) ,n+1)
    ax.set_title("Particle %d " % (n+1))
    ax.set_ylabel(r'P')
    ax.set_xlabel(r'speed')
    shist_data = f["/speed_histograms/"+key]
    shist_attrs = shist_data.attrs
    range_ends = array(shist_attrs["range"])
    xrange = array([ (range_ends[i]+range_ends[i+1])/2 for i in range(len(range_ends)-1) ])
    data=array(shist_data, dtype=float64)/iterations
    ax.plot(xrange, data)
    ax.set_ylim(0,max(data)*1.05)
    n=n+1
# Set limits
pyplot.show()
