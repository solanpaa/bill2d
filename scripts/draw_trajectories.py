#!/usr/bin/env python3
#
# draw_trajectories.py
#
# This file is part of bill2d.
#


#
# bill2d is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bill2d is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bill2d.  If not, see <http://www.gnu.org/licenses/>.


from math import ceil, floor,sin,tan, pi,sqrt,cos
import h5py
from matplotlib import pyplot, path, patches
from numpy import array, diff, where, abs,append, nan, insert, savez, loadtxt
from billiard_common import parse_table_shape
from os.path import exists
from optparse import OptionParser


parser = OptionParser(usage="%prog [options] \n This script draws the trajectories of the particles.")
parser.add_option("-f",'--file', action="store", type="string", dest="datafile", help="Set the datafile (hdf5) or txt-bouncemap path", default="data/bill2d.h5")
parser.add_option("--save", action="store_true",dest="save", help="Save the trajectory to trajectories.pdf", default=False)
parser.add_option("--separate", action="store_true",dest="separate", help="Plot each particle trajectory in own figure", default=True)
parser.add_option("--max_iter",action="store", type="int", dest="max_iter", help="The number of datapoints up to which the figure is plotted", default=0)
parser.add_option("--min_iter",action="store", type="int", dest="min_iter", help="The number of datapoints starting from which the figure is plotted", default=0)
parser.add_option("--disable-table", action="store_true",dest="disable_table", help="Do not plot the table.", default=False)
parser.add_option("--linewidth", action="store", dest="linewidth", help="Linewidth of the trajectories", default=0.5)


args=parser.parse_args()[0]
lw=args.linewidth
min_iter=args.min_iter
max_iter=args.max_iter
same_image = args.separate
save_figure = args.save
plot_table = not args.disable_table
dfilee=args.datafile
if(not exists(dfilee)):
    print("Error: Datafile does not exist.")
    exit(1)

f = h5py.File(dfilee, 'r')
global_attrs = f["/parameters"].attrs

# Plot data
colors = "bgrcmyk"
fig = pyplot.figure()

manual_limits=False
ax = fig.add_subplot(1,1,1)

n=0
xr=[0,1]
yr=[0,1]
minx=0
maxx=0
miny=0
maxy=0
for key in list(f["/trajectories"].keys()):
    traj_data=f["/trajectories/"+key]
    if(max_iter!=0):
        if(min_iter!=0):
            datax = [ d[0] for d in traj_data[min_iter:max_iter] ]
            datay = [ d[1] for d in traj_data[min_iter:max_iter] ]
        else:
            datax = [ d[0] for d in traj_data[:max_iter] ]
            datay = [ d[1] for d in traj_data[:max_iter] ]
    else:
        if(min_iter!=0):
            datax = [ d[0] for d in traj_data[min_iter:] ]
            datay = [ d[1] for d in traj_data[min_iter:] ]
        else:
            datax = [ d[0] for d in traj_data[:] ]
            datay = [ d[1] for d in traj_data[:] ]
    datax=array(datax)
    datay=array(datay)

    # Set up information on what kind of periodic potential we have (or have not)
    if(global_attrs["periodic"]==1):
        if(min(datax)<minx):
            minx=min(datax)
        if(max(datax)>maxx):
            maxx=max(datax)
        if(min(datay)<miny):
            miny=min(datay)
        if(max(datay)>maxy):
            maxy=max(datay)

        if global_attrs["soft-lorentz-gas"]:
            periodic_potential_info = { 'potentialtype' : 'soft-lorentz-gas',
                                        'radius'        : global_attrs["soft-lorentz-radius"],
                                        'sharpness'     : global_attrs["soft-lorentz-sharpness"]}
        elif global_attrs["soft-lieb-gas"]:
            periodic_potential_info = { 'potentialtype' : 'soft-lieb-gas',
                                        'radius'        : global_attrs["soft-lieb-radius"],
                                        'sharpness'     : global_attrs["soft-lieb-sharpness"]}
        else:
            periodic_potential_info = { 'potentialtype' : None}

        periodic_potential_info['trajectory_bbox'] = [minx, miny, maxx, maxy]
        periodic_potential_info['unit-cell-info'] = [global_attrs["uc_phi"],global_attrs["uc_L1"],global_attrs["uc_L2"]]
        periodic_potential_info['periodic'] = True

    else:
        periodic_potential_info = {'periodic' : False}

    # Obtain the shapes for the periodic potentials and tables that are drawn as vectorial patches
    boundaries, limits = parse_table_shape(global_attrs["table_shape"], periodic_potential_info)

    if limits is not None:
        ax.set_xlim(limits[0], limits[2])
        ax.set_ylim(limits[1], limits[3])
    else:
        manual_limits=True
    ax.set_autoscale_on(False)
    if(plot_table):
        for boundary in boundaries:
            if boundary != None:
                try:
                    ax.add_patch(boundary)
                except:
                    ax.add_patch(boundary[0])
    if(boundaries[0]==None):
        manual_limits=True
    color = colors[n % len(colors)]




    ax.plot(datax, datay, color=color, linewidth=lw)
    # Plot also starting and final positions
    ax.plot(datax[0], datay[0], 'o', color=color)
    ax.plot(datax[-1], datay[-1], 's', color=color)
    if(manual_limits):
        if(xr[0]>min(datax)):
            if(min(datax)<0):
                xr[0]=min(datax)*1.05
            else:
                xr[0]=min(datax)*0.95
        if(xr[1]<max(datax)):
            xr[1]=max(datax)*1.05
        if(yr[0]>min(datay)):
            if(min(datay)<0):
                yr[0]=min(datay)*1.05
            else:
                yr[0]=min(datay)*0.95
        if(yr[1]<max(datay)):
            yr[1]=max(datay)*1.05
    n=n+1
# Set limits
if(manual_limits):

    ax.set_xlim(xr)
    ax.set_ylim(yr)
ax.set_aspect(1)

if save_figure:
    pyplot.savefig("trajectories.pdf")
else:
    pyplot.show()
