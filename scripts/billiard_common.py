#!/usr/bin/env python3
#
# billiard_common.py
#
# This file is part of bill2d.
#


#
# bill2d is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bill2d is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bill2d.  If not, see <http://www.gnu.org/licenses/>.

import re
from math import sqrt, pi,cos,sin,tan, ceil, floor
from numpy import array, arange, sign, amax
from matplotlib import patches
from matplotlib.transforms import Bbox
import numpy as np

def parse_table_shape(shape_string, periodicity_info):
    """Takes a string given by bill2d describing the boundary shape of a 2d billiard
    system and outputs the pair (boundaries, limits) where boundaries is a list of
    matplotlib.patches.Patch instances (first of which is the outest boundary) and
    limits is a 4-element tuple (min_x, min_y, max_x, max_y) describing the axis
    limits that should be used for drawing."""
    marg = 0.05

    boundaries=[]
    limits=None

    shape_string = shape_string.decode('utf-8')

    # Rectangle
    m = re.match(r"rectangle\((?P<width>[^,]+),(?P<height>[^,]+)\)", shape_string)
    if m != None:
        w = float(m.group("width"))
        h = float(m.group("height"))
        limits = (-marg, -marg, w+marg, h+marg)
        boundaries = [patches.Rectangle((0,0), w, h, fill=False, linewidth=2)]

    # Open rectangle
    m = re.match(r"openrectangle\((?P<width>[^,]+),(?P<height>[^,]+),(?P<holewidth>[^,]+)\)", shape_string)
    if m != None:
        w = float(m.group("width"))
        h = float(m.group("height"))
        hw = float(m.group("holewidth"))

        limits = (-marg, -marg, w+marg, h+marg)
        boundaries = [patches.Polygon(array((((w-hw)/2.,0), (0, 0), (0,h), (w,h), (w,0), (w/2.+hw/2.,0))), closed=False, fill=False, linewidth=2)]#[patches.Rectangle((0,0), w, h, fill=False, linewidth=2)]

    # Triangle
    m = re.match(r"triangle\((?P<scalex>[^,]+),(?P<scaley>[^,]+)\)", shape_string)
    if m != None:
        x = float(m.group("scalex"))
        y = float(m.group("scaley"))
        vertices = array(((0,0), (x/2,sqrt(3)/2*y), (x,0)))
        limits = (-marg, -marg, x+marg, sqrt(3)/2*y+marg)
        boundaries = [patches.Polygon(vertices, fill=False, linewidth=2)]

    # Circle
    m = re.match(r"circle\((?P<cx>[^,]+),(?P<cy>[^,]+),(?P<r>[^,]+)\)", shape_string)
    if m != None:
        cx = float(m.group("cx"))
        cy = float(m.group("cy"))
        r = float(m.group("r"))
        limits = (cx-r-marg, cy-r-marg, cx+r+marg, cy+r+marg)
        boundaries = [patches.Circle((cx,cy), radius=r, fill=False, linewidth=2)]

    # Ellipse
    m = re.match(r"ellipse\((?P<a>[^,]+),(?P<b>[^,]+),(?P<max>[^,]+)\)", shape_string)
    if m != None:
        a = float(m.group("a"))
        b = float(m.group("b"))
        limits = (-a-marg, -b-marg, a+marg, b+marg)
        boundaries = [patches.Ellipse((0,0), width=2*a, height=2*b, fill=False, linewidth=2)]

    # Open Circle
    m = re.match(r"opencircle\((?P<cx>[^,]+),(?P<cy>[^,]+),(?P<r>[^,]+),(?P<holewidth>[^,]+)\)", shape_string)
    if m != None:
        cx = float(m.group("cx"))
        cy = float(m.group("cy"))
        r = float(m.group("r"))
        hw = float(m.group("holewidth"))
        limits = (cx-r-marg, cy-r-marg, cx+r+marg, cy+r+marg)
        boundaries = [patches.Arc((cx,cy), 2*r, 2*r, theta1=hw/r*180/pi, theta2=360, linewidth=2)]

    # Sinai
    m = re.match(r"sinai\((?P<l>[^,]+),(?P<r>[^,]+)\)", shape_string)
    if m != None:
        r = float(m.group("r"))
        l = float(m.group("l"))
        limits = (-marg, -marg, l+marg, l+marg)
        boundaries = [  patches.Rectangle((0,0), l, l, fill=False, linewidth=2),
                        patches.Circle((l/2,l/2), radius=r, fill=False, linewidth=2)    ]

    # Open Sinai
    m = re.match(r"opensinai\((?P<r>[^,]+),(?P<l>[^,]+),(?P<holewidth>[^,]+)\)", shape_string)
    if m != None:
        r = float(m.group("r"))
        l = float(m.group("l"))
        hw = float(m.group("holewidth"))
        limits = (-marg, -marg, l+marg, l+marg)
        boundaries = [patches.Polygon(array((((l-hw)/2.,0), (0, 0), (0,l), (l,l), (l,0), (l/2.+hw/2.,0))), closed=False, fill=False, linewidth=2),
                        patches.Circle((l/2,l/2), radius=r, fill=False, linewidth=2)    ]


    # Ring
    m = re.match(r"ring\((?P<ir>[^,]+),(?P<or>[^,]+)\)", shape_string)
    if m != None:
        inner_r = float(m.group("ir"))
        outer_r = float(m.group("or"))
        limits = (-marg, -marg, 2*outer_r+marg, 2*outer_r+marg)
        boundaries = [  patches.Circle((outer_r,outer_r), radius=outer_r, fill=False, linewidth=2),
                        patches.Circle((outer_r,outer_r), radius=inner_r, fill=False, linewidth=2)    ]

    # Stadium
    m = re.match(r"stadium\((?P<R>[^,]+),(?P<L>[^,]+)\)", shape_string)
    if m != None:
        R=float(m.group("R"))
        L=float(m.group("L"))
        limits = (-marg, -marg, 2*R+L+marg, 2*R+marg)
        boundaries = [  patches.Polygon(array(((R, 0), (R+L, 0))), closed=False, fill=False, linewidth=2),
                        patches.Polygon(array(((R, 2*R), (R+L, 2*R))), closed=False, fill=False, linewidth=2),
                        patches.Arc((R,R), 2*R, 2*R, theta1=90, theta2=270, linewidth=2),
                        patches.Arc((R+L,R), 2*R, 2*R, theta1=270, theta2=90, linewidth=2)   ]


    #Open Stadium
    m = re.match(r"openstadium\((?P<R>[^,]+),(?P<L>[^,]+),(?P<holewidth>[^,]+)\)", shape_string)
    if m != None:
        R=float(m.group("R"))
        L=float(m.group("L"))
        hw=float(m.group("holewidth"))
        limits = (-marg, -marg, 2*R+L+marg, 2*R+marg)
        boundaries = [  patches.Polygon(array(((R, 0), (R+L, 0))), closed=False, fill=False, linewidth=2),
                        patches.Polygon(array(((R, 2*R), (R+0.5*(L-hw),2*R))), closed=False, fill=False, linewidth=2),
                        patches.Polygon(array(((R+L/2+hw/2,2*R), (R+L, 2*R))), closed=False, fill=False, linewidth=2),
                        patches.Arc((R,R), 2*R, 2*R, theta1=90, theta2=270, linewidth=2),
                        patches.Arc((R+L,R), 2*R, 2*R, theta1=270, theta2=90, linewidth=2)   ]

    # Mushroom
    m = re.match(r"mushroom\((?P<r>[^,]+),(?P<fh>[^,]+),(?P<fw>[^,]+)\)", shape_string)
    if m != None:
        fw = float(m.group("fw"))
        fh = float(m.group("fh"))
        r = float(m.group("r"))
        limits = (-marg, -marg, 2*r+marg, fh+r+marg)
        foot_points = array(((0, fh), (r-0.5*fw, fh), (r-0.5*fw, 0), (r+0.5*fw, 0), (r+0.5*fw, fh), (2*r, fh)))
        boundaries = [  patches.Polygon(foot_points, closed=False, fill=False, linewidth=2),
                        patches.Arc((r,fh), 2*r, 2*r, theta1=0, theta2=180, linewidth=2) ]


    # RecurrenceMushroom
    m = re.match(r"openmushroom\((?P<r>[^,]+),(?P<fh>[^,]+),(?P<fw>[^,]+),(?P<holewidth>[^,]+)\)", shape_string)
    if m != None:
        fw = float(m.group("fw"))
        fh = float(m.group("fh"))
        r = float(m.group("r"))
        hw= float(m.group("holewidth"))
        th=hw/r*180/pi
        limits = (-marg, -marg, 2*r+marg, fh+r+marg)
        foot_points = array(((0, fh), (r-0.5*fw, fh), (r-0.5*fw, 0), (r+0.5*fw, 0), (r+0.5*fw, fh), (2*r, fh)))
        boundaries = [  patches.Polygon(foot_points, closed=False, fill=False, linewidth=2),
                        patches.Arc((r,fh), 2*r, 2*r, theta1=0, theta2=180/4.-th/2, linewidth=2),
                        patches.Arc((r,fh), 2*r, 2*r, theta1=180/4.+th/2, theta2=180, linewidth=2) ]


    # Gate
    m = re.match(r"gate\((?P<ir>[^,]+),(?P<or>[^,]+)\)", shape_string)
    if m != None:
        w = float(m.group("ir"))
        b=float(m.group("or"))

        limits = (-marg, -marg,b+marg,2*b+0.1+marg)
        boundaries = [patches.Rectangle((0,0), b, 2*b+0.1, fill=False, linewidth=2),
                      patches.Rectangle((0,b),(b/2-w/2) , 0.1, fill=True, linewidth=2, facecolor="none"),
                      patches.Rectangle((b/2+w/2,b),b-b/2-w/2 , 0.1, fill=True, linewidth=2, facecolor='none') ]

    if(periodicity_info['periodic']):
       if periodicity_info['potentialtype'] == 'soft-lieb-gas':
           R = periodicity_info['radius']
           limits=array(periodicity_info['trajectory_bbox'])
           R_rel = [[0.25, 0.25],[0.75, 0.25],[0.5, 0.5],[0.25, 0.75],[0.75, 0.75]]

           ang = periodicity_info['unit-cell-info'][0]
           a = periodicity_info['unit-cell-info'][1]
           b = periodicity_info['unit-cell-info'][2] * sin(ang)

           Nx = int(ceil((limits[2]-limits[0])/a)) + 3
           Ny = int(ceil((limits[3]-limits[1])/b)) + 3
           Nref = Nx * Ny
           R_ref = [[],[]]
           R_xy = [[],[]]

           for j in range(Ny):
               for i in range(Nx):
                   R_ref[0].append((i-1)*a)
                   R_ref[1].append((j-2)*b)

           for i in range(Nref):
               for j in range(5):
                   R_xy[0].append(R_ref[0][i] + a * R_rel[j][0])
                   R_xy[1].append(R_ref[1][i] + b * R_rel[j][1])

           for i in range(len(R_xy[0])):
               R_xy[0][i] = R_xy[0][i] + R_xy[1][i]*tan(pi/2 - ang)

           for i in range(len(R_xy[0])):
               boundaries.append([patches.Circle((R_xy[0][i], R_xy[1][i]), radius=R, fill=True, fc='orange', ec='orange', linewidth=0.01)])

           limits = limits + (-a, -b, a, b)

       if periodicity_info['potentialtype'] == 'soft-lorentz-gas':
           R = periodicity_info['radius']
           limits=array(periodicity_info['trajectory_bbox'])

           # Primitive lattice vectors
           ang = periodicity_info['unit-cell-info'][0]
           a1 = array([periodicity_info['unit-cell-info'][1], 0 ])
           a2 = array([cos(ang)*periodicity_info['unit-cell-info'][2],sin(ang)* periodicity_info['unit-cell-info'][2] ])

           minx, miny, maxx, maxy = limits 

           n1_a = -(a2[1]* minx-a2[0]*miny)/(a1[1]*a2[0]-a1[0]*a2[1])
           n1_b = -(a2[1]* minx-a2[0]*maxy)/(a1[1]*a2[0]-a1[0]*a2[1])
           n1_c = -(a2[1]* maxx-a2[0]*miny)/(a1[1]*a2[0]-a1[0]*a2[1])
           n1_d = -(a2[1]* maxx-a2[0]*maxy)/(a1[1]*a2[0]-a1[0]*a2[1])
           
           n1_max = np.ceil( np.max([n1_a,n1_b,n1_c,n1_d]) ) +1
           n1_min = np.floor( np.min([n1_a, n1_b, n1_c, n1_d]) ) -1
           
           n2_a = -(-a1[1]* minx+a1[0]*miny)/(a1[1]*a2[0]-a1[0]*a2[1])
           n2_b = -(-a1[1]* minx+a1[0]*maxy)/(a1[1]*a2[0]-a1[0]*a2[1])
           n2_c = -(-a1[1]* maxx+a1[0]*miny)/(a1[1]*a2[0]-a1[0]*a2[1])
           n2_d = -(-a1[1]* maxx+a1[0]*maxy)/(a1[1]*a2[0]-a1[0]*a2[1])
           
           n2_max = np.ceil( np.max([n2_a,n2_b,n2_c,n2_d]) ) +1
           n2_min = np.floor( np.min([n2_a, n2_b, n2_c, n2_d]) ) -1

           for i in arange(n1_min, n1_max):
               for j in arange(n2_min, n2_max):
                   boundaries.append([patches.Circle((i*a1[0]+j*a2[0], i*a1[1]+j*a2[1]), radius=R, fill=True, fc='orange', ec='orange', linewidth=0.01)])

           limits = limits

    if(len(boundaries)==0):
        boundaries=[None]

    return (boundaries, limits)
