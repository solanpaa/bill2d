#!/usr/bin/env python3
#
# animate_trajectories.py
#
# This file is part of bill2d.
#

#
# bill2d is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bill2d is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bill2d.  If not, see <http://www.gnu.org/licenses/>.

from math import ceil,fabs
import h5py
from matplotlib import pyplot, path, patches
from numpy import array, arange
from billiard_common import parse_table_shape
from os.path import exists
from optparse import OptionParser
import subprocess

def animate(filee,tail=0.2,savefile="output.avi"):
    f = h5py.File(filee, 'r')
    global_attrs = f["/parameters"].attrs
    sp=global_attrs["sparsesave"]
    dt=global_attrs["delta_t"]
    max_t=global_attrs["max_t"]
    # Plot data
    colors = "bgrcmyk"
    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    boundaries, limits = parse_table_shape(global_attrs["table_shape"])
    max_t=0
    for key in list(f["/trajectories"].keys()):
        traj_data=f["/trajectories/"+key]
        spawnt=traj_data.attrs["spawntime"]
        T=spawnt+sp*dt*len(traj_data)
        if(T>max_t):
            max_t=T

    trange=arange(0,max_t,0.02)
    frame=1
    for T in trange:
        print(( "%4d / %d" %(frame,len(trange)) ))
        n=0
        #ax.set_title(' T=%.2f '%T,horizontalalignment='center',va='baseline')
        if limits != None:
            ax.set_xlim(limits[0], limits[2])
            ax.set_ylim(limits[1], limits[3])
        else:
            ax.set_xlim(-32,32)
            ax.set_ylim(-32,32)
            ax.set_autoscale_on(False)
        for boundary in boundaries:
            if boundary != None:
                ax.add_patch(boundary)
        for loc, spine in ax.spines.items():
            spine.set_color('none') # don't draw spine
            ax.xaxis.set_ticks_position('none')
            ax.yaxis.set_ticks_position('none')
            ax.set_xticks(())
            ax.set_yticks(())
        for key in list(f["/trajectories"].keys()):

            traj_data=f["/trajectories/"+key]
            color = colors[n % len(colors)]
            spawnt=traj_data.attrs["spawntime"]

            if(T<1e-5):
                if(spawnt<1e-5):
                    ax.plot(traj_data[0][0], traj_data[0][1], 'o', color=color,markersize=10)
            else:
                if((T<spawnt)or(int((T-spawnt)/(sp*dt))>len(traj_data))):
                    pass
                else:
                    if(int((T-tail-spawnt)/(sp*dt))<0):
                        a=0
                    else:
                        a=int((T-tail-spawnt)/(sp*dt))
                    datax = [ d[0] for d in traj_data[a:int((T-spawnt)/(sp*dt))] ]
                    datay = [ d[1] for d in traj_data[a:int((T-spawnt)/(sp*dt))] ]




                    ax.plot(datax, datay, color=color, linewidth=3)
                    # Plot also starting and final positions
                    #ax.plot(datax[0], datay[0], 'o', color=color)
                    ax.plot(datax[-1], datay[-1], 'o', color=color,markersize=10)

            n=n+1
            ax.set_aspect('equal')


        pyplot.savefig("movie/frame%.6d.png"%frame,dpi=100,bbox_inches='tight')
        ax.cla()
        frame=frame+1

    command = ('mencoder',
               'mf://movie/*.png',
               '-mf',
               'type=png:w=1919:h=1034:fps=25',
               '-ovc',
               'lavc',
               '-lavcopts',
               'vcodec=mpeg4',
               '-oac',
               'copy',
               '-o',
               savefile)

    print(( "\n\nabout to execute:\n%s\n\n" % ' '.join(command) ))
    subprocess.check_call(command)

    print( "\n\n The movie was written to 'output.avi'" )

    print( "\n\n You may want to delete *.png now.\n\n")

if __name__ == "__main__":
    parser = OptionParser(usage="%prog [options] \n This script draws the trajectories of the particles.")
    parser.add_option("-f",'--file', action="store", type="string", dest="datafile", help="Set the datafile (hdf5) or txt-bouncemap path", default="data/bill2d.h5")
    parser.add_option("--tail",action="store", type="float", dest="tail", help="Length of the tail in time (program units)", default=0.2)
    parser.add_option('--savefile', action="store", type="string", dest="savefile", help="Set the save path", default="output.avi")

    args=parser.parse_args()[0]

    dfilee=args.datafile
    tail=args.tail
    savefile=args.savefile

    if(not exists(dfilee)):
        print( "Error: Datafile does not exist.")
        exit(1)

    animate(dfilee,tail,savefile)
