#!/usr/bin/env python3
#
# draw_difference.py
#
# This file is part of bill2d.
#

#
# bill2d is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bill2d is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bill2d.  If not, see <http://www.gnu.org/licenses/>.


from math import ceil
import h5py
from matplotlib import pyplot, path, patches
from numpy import array,zeros,power,sqrt,linspace
from os.path import exists
from optparse import OptionParser

parser = OptionParser(usage="%prog [options] \n This script calculates the phase space difference of two (system) trajectories as a function of time.")
parser.add_option("-f",'--file', action="store", type="string", dest="datafile", help="Set the datafile (hdf5) or txt-bouncemap path", default="data/bill2d.h5")
parser.add_option('--file2', action="store", type="string", dest="datafile2", help="Set the datafile (hdf5) or txt-bouncemap path", default="data/bill2d2.h5")
parser.add_option("--no-velocity", action="store_true",dest="novel", help="Do not take velocities into account", default=False)

args=parser.parse_args()[0]

novel=args.novel
dfilee=args.datafile
dfilee2=args.datafile2
if(not (exists(dfilee)) and (exists(dfilee2))):
    print("Error: Datafile does not exist.")
    exit(1)

f = h5py.File(dfilee, 'r')
f2 = h5py.File(dfilee2, 'r')
global_attrs = f["/parameters"].attrs

# Plot data
colors = "bgrcmyk"
fig = pyplot.figure()

ax = fig.add_subplot(1,1,1)
n=0
diff=array(zeros(len(f["/trajectories/particle1"])))
for key in list(f["/trajectories"].keys()):
    traj_data=f["/trajectories/"+key]
    datax = array([ d[0] for d in traj_data[:] ])
    datay = array([ d[1] for d in traj_data[:] ])
    traj_data2=f2["/trajectories/"+key]
    datax2 = array([ d[0] for d in traj_data2[:] ])
    datay2 = array([ d[1] for d in traj_data2[:] ])
    diff=diff+power(datax-datax2,2)+power(datay-datay2,2)
if(not novel):
    for key in list(f["/velocities"].keys()):
        traj_data=f["/velocities/"+key]
        datax = array([ d[0] for d in traj_data[:] ])
        datay = array([ d[1] for d in traj_data[:] ])
        traj_data2=f2["/velocities/"+key]
        datax2 = array([ d[0] for d in traj_data2[:] ])
        datay2 = array([ d[1] for d in traj_data2[:] ])
        diff=diff+power(datax-datax2,2)+power(datay-datay2,2)

diff=sqrt(diff)
time=linspace(0,global_attrs["time"],len(diff))
ax.plot(time,diff)
ax.set_xlabel(r'T')
ax.set_ylabel(r'$\Delta$')


pyplot.show()
