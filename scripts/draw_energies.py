#!/usr/bin/env python3
#
# draw_energies.py
#
# This file is part of bill2d.
#


#
# bill2d is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bill2d is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bill2d.  If not, see <http://www.gnu.org/licenses/>.


import h5py
from matplotlib import pyplot,rcParams
from numpy import array,arange
from os.path import exists
from optparse import OptionParser

rcParams['figure.subplot.hspace']=0.35


parser = OptionParser(usage="%prog [options] \n This script draws the energies.")
parser.add_option("-f",'--file', action="store", type="string", dest="datafile", help="Set the datafile (hdf5) or txt-bouncemap path", default="data/bill2d.h5")

args=parser.parse_args()[0]
dfilee=args.datafile
if(not exists(dfilee)):
    print("Error: Datafile does not exist.")
    exit(1)
f = h5py.File(dfilee, 'r')
global_attrs = f["/parameters"].attrs

delta_t = global_attrs["delta_t"]
ekin_data = array(f["/kinetic_energy"])
epot_data = array(f["/potential_energy"])
iterations=len(ekin_data)
sp=global_attrs["sparsesave"]
time = arange(0, (sp*iterations-1)*delta_t, sp*delta_t)
fig = pyplot.figure()
ax = fig.add_subplot(3,1,1)
ax.plot(time, ekin_data+epot_data, linewidth=2)
ax.set_xlabel('t',fontsize=14)
ax.set_ylabel('Total energy',fontsize=14)
ax=fig.add_subplot(3,1,2)
ax.plot(time,ekin_data,linewidth=2)
ax.set_xlabel('t',fontsize=14)
ax.set_ylabel('Kinetic energy', fontsize=14)

ax=fig.add_subplot(3,1,3)
ax.plot(time,epot_data,linewidth=2)
ax.set_xlabel('t',fontsize=14)
ax.set_ylabel('Potential energy', fontsize=14)


pyplot.show()
