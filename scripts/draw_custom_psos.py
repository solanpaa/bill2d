#!/usr/bin/env python3
#
# draw_bounce_map.py
#
# This file is part of bill2d.
#

#
# bill2d is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bill2d is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bill2d.  If not, see <http://www.gnu.org/licenses/>.


import h5py
from matplotlib import pyplot
from numpy import array, arange, loadtxt
from math import ceil, pi
from os.path import exists
import re
from optparse import OptionParser

parser = OptionParser(usage="%prog [options] \n This script draws the custom Poincare section as a scatter plot.")
parser.add_option("-f",'--file', action="store", type="string", dest="datafile", help="Set the datafile (hdf5)", default="data/bill2d.h5")
parser.add_option("-d",'--directory', action="store", type="string", dest="directory", help="Set the datafile (hdf5)", default="")
parser.add_option("--angle", action="store_true", dest="angle", help="Plot the crossing angle instead of the tangential velocity", default=False)
args=parser.parse_args()[0]
filee=args.datafile
angle_plot=args.angle
directory = args.directory

# If not directory
if(angle_plot):
    index=3 #Index 2 = angle
else:
    index=2 #Index 1 = tangential velocity

if(directory == ""):
    try:
        f = h5py.File(filee, 'r')
    except:
        print("File not found or not an hdf5-file")
        exit(1)



    try:
        data = f['custom_psos'][:]
    except:
        print("No Poincare section saved!")
        exit(1)

    fig = pyplot.figure()
    ax=fig.add_subplot(1,1,1)


    ax.scatter(data[:,1],data[:,index],s=1);

    for loc, spine in ax.spines.items():
        if loc in ["top", "right"]:
            spine.set_color('none') # don't draw spine
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    fontsize=8
    for tick in ax.xaxis.get_major_ticks():
        tick.label1.set_fontsize(fontsize)

    for tick in ax.yaxis.get_major_ticks():
        tick.label1.set_fontsize(fontsize)

    ax.set_xlabel(r'$s$',fontsize=16)

    if(angle_plot):
        ax.set_ylabel(r'$\theta$',fontsize=16)
    else:
        ax.set_ylabel(r'$v_{\parallel}$',fontsize=16)


    pyplot.show()
else:
    from glob import glob
    files = glob(directory+"/*.h5")
    fig = pyplot.figure()
    ax=fig.add_subplot(1,1,1)
    if(len(files)==0):
        print("No hdf5-files found in the directory")
        exit(1)
    print(len(files))
    for filee in files:
        try:
            f = h5py.File(filee, 'r')
            data = f['custom_psos'][:]
            ax.scatter(data[:,1],data[:,index],s=1, lw=0, facecolor='k');
            f.close()
        except:
            continue



    fontsize=8
    for tick in ax.xaxis.get_major_ticks():
        tick.label1.set_fontsize(fontsize)

    for tick in ax.yaxis.get_major_ticks():
        tick.label1.set_fontsize(fontsize)

    ax.set_xlabel(r'$s$',fontsize=16)

    if(angle_plot):
        ax.set_ylabel(r'$\theta$',fontsize=16)
    else:
        ax.set_ylabel(r'$v_{\parallel}$',fontsize=16)

    pyplot.show()





