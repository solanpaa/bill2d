#!/usr/bin/env python3
#
#
# This file is part of bill2d.
#

#
# bill2d is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# bill2d is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bill2d.  If not, see <http://www.gnu.org/licenses/>.


from matplotlib import pyplot
from mpl_toolkits.axes_grid1 import make_axes_locatable
from numpy import array, loadtxt, mgrid, complex, sqrt
from scipy.interpolate import griddata
from optparse import OptionParser

parser = OptionParser(usage="%prog [options] \n This script draws the custom Poincare section as a scatter plot.")
parser.add_option("-f",'--file', action="store", type="string", dest="datafile", help="Set the datafile (txt)", default="phase_space_regularity.txt")
parser.add_option("-p",'--points', action="store", type="float", dest="points", help="Set plotpoints", default=100)
parser.add_option("-v",'--vmax', action="store", type="float", dest="vmax", help="Set vmax", default=None)
parser.add_option("-s",'--smoothing',action="store", type="string", dest="interpolation", help="Smoothing type (e.g., cubic, sinc, lanczos, hanning, none)", default="none")
args=parser.parse_args()[0]
datafile = args.datafile
ppoints = complex(0,args.points)
vmax = args.vmax
interpolation = args.interpolation

data = loadtxt(datafile)
points = data[:,1:3]
values = data[:,0]

fig = pyplot.figure()
ax = fig.add_subplot(111)
ax.set_autoscale_on(True)
S, VPARALLEL = mgrid[points[:,0].min():points[:,0].max():ppoints, points[:,1].min():points[:,1].max():ppoints]
Z = griddata(points, values, (S,VPARALLEL), method='linear')
im = ax.imshow(Z.T, origin='lower', vmin=0, extent=[points[:,0].min(),points[:,0].max(),points[:,1].min(),points[:,1].max()],interpolation=interpolation, cmap='magma_r', vmax=vmax)
ax.set_xlabel(r'$s$',fontsize=16)
ax.set_ylabel(r'$v_{\parallel}$',fontsize=16)


divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
pyplot.colorbar(im, cax=cax)
pyplot.show()
