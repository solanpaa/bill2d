Overview
========

The installation produces several binaries:
  
  *  `bill2d`,
  *  `bill2d_parallel`
  *  `bill2d_diffusion_coefficient`,
  *  `bill2d_escape`,
  *  `bill2d_phase_space_regularity`, and
  *  `bill2d_phase_space_regularity_checker`.

Some of these may not be built depending on the installation configuration and available libraries.

`bill2d` is used for time propagation of a single initial condition

`bill2d_diffusion_coefficient` is used solely for the calculation of diffusion
coefficients in periodic systems.

`bill2d_parallel` is the "big brother" of `bill2d_diffusion_coefficient`, allowing parallel
simulations with single-file customizable output.

`bill2d_escape` is a binary used to calculate escape rates/survival
probabilities of open billiards.

`bill2d_phase_space_regularity` and `bill2d_phase_space_regularity_checker` binaries
are used for analyzing the phase space.

All binaries obey the same arguments, all of which can be printed out with

```
    $ bill2d --help
```


Passing arguments
=================

Arguments can be passed via command line interface, e.g.
```
   $ bill2d --table 1
```
where argument `1` tells the binary to use a rectangular table with
default parameters.

Arguments can also be passed via configuration file, e.g.
```
   contents of the ascii file "input"
   ----------------------------------
   table = 1
```

The configuration file must be passed via the `--config` command line flag
to the binary, i.e.
```
   $ bill2d --config input
```

List of arguments with explanations can be obtained with the flag
`--help`. 

Examples of input files can be found in the directory `examples/`.

Arguments can be passed also as a combination of input file
and command line arguments, e.g.,
```
   $ bill2d --config /here/is/my/input --delta-t 1e-6
```

If a given argument, e.g. `delta-t` in the above example, exists also in
the configuration file, the value of the command-line argument is used.


Interrupting the simulation
===========================

A simple SIGINT (Ctrl+C) will tell the program to finish up as soon as possible.
This works for all binaries. The data gathered so far is still saved.



First simulation
================

To run a simple billiard simulation for a single (random) initial condition, try e.g.
```
$ bill2d --table 1 --billtype 2 --energy 0.5 \
         --magnetic-field 1.43 --poincare-section --savepath data.h5
$ draw_trajectories -f data.h5 &
$ draw_poincare_section -f data.h5
```

`draw_trajectories` plots the particle trajectories.

`draw_poincare_section` plots the bounce map / collision map / Poincare surface of section
which was saved with `--poincare-section`.

See also the example input files
 
 - `examples/magnetic_square_billiards.config`
 - `examples/interacting_mushroom.config`



Accessing data
==================

The data saved to the HDF5-file can be easily accessed either using
Python or Matlab.


Python
------
In python (examples for python 2), you need to use the h5py-package:
```
>> import h5py
>> file = h5py.File('data/bill2d.h5', 'r') #IMPORTANT TO USE 'r' to not to overwrite
```

All elements of the root level can be seen with command
```
>> list(file.keys())
```
Possible output: [u'kinetic_energy', u'parameters', u'potential_energy', u'trajectories']

If we wish list the datasets in the "trajectories"-group:
```
>> list(file["trajectories"].keys())
```

E.g., trajectory of particle nbr 1 can be obtained with:
```
>> trajectory = file["trajectories/particle1"][:]
```

The parameters are saved to the group "parameters" as attributes. You can
obtain the list of parameters with
```
>> file["parameters"].attrs.keys()
```

And to check the value of, say, attribute 'delta_t',
```
>> file["parameters"].attrs["delta_t"]
```

Finally, let's close the file:
```
>> file.close()
```

For more details on Python+HDF5, check h5py documentation.


Matlab
------
Load root group info:
```
>> info = h5info('data/bill2d.h5')
```

Check groups of root level:
```
>> info.Groups.Name
```

Check the parameters:
```
>> h5disp('data/bill2d.h5','/parameters')
```

List the datasets in the ``trajectories''-group (here trajectories was the second element from ``info'')
```
>> info.Groups(2).Datasets
```

Load, e.g., the trajectory of particle number 1:
```
>> trajectory = h5read('data/bill2d.h5', '/trajectories/particle1');
```

For more details on Matlab+HDF5, check Matlab documentation.


Defining the system
===================

The simulations binaries have several different options to choose from. Below you will find
a general overview of the options.

Basic properties
----------------

Throughout the program we use atomic units and have included the speed of light in the magnetic field strength.

Number of particles, interaction types, hard-wall or periodic poundaries, potentials, 
and magnetic field define the system under investigation. These can be defined using the following options.


* `--billtype` defines the interactions within the billiard simulation. 
     There are four possible options:
     1. Non-interacting particles without magnetic field
     2. Non-interacting particles in a magnetic field
     3. Interacting (particle-particle or external potential) particles
        without magnetic field
     4. Interacting (particle-particle or external potential) particles
        in a magnetic field
     Example: `--billtype 1`
* `--magnetic-field <float>` sets the magnetic field
* `--num-particles <int>` sets the number of particles in the simulation. 
* `--energy <float>` initial total energy of the entire system.

Periodic systems
----------------

For periodic systems you need to set the information on periodicity (this section) and either a periodic potential or a periodic hard-wall table (next section). Currently we implement only single-particle simulations for periodic systems.

*  `--periodic` sets periodic boundary conditions
*  `--unit-cell <float>` defines a parallelogram unit cell with unit length sides and angle given by the argument between bottom and left side of the parallellogram.
*  `--unit-cell <float> <float>` defines a parallelogram unit cell with side length defined by the second argument
    and angle given by the first argument between bottom and left side of the parallellogram.
*  `--unit-cell <float> <float> <float>` defines a parallellogram unit cell. First argument defines the angle between the bottom and the left side. Second argument defines the length of bottom and top sides. Last argument defines the length of left and right sides.

Particle-particle interactions
------------------------------

To activate interparticle interactions, you need to set `--billtype` to `3` or `4`. In addition, you should use more than one particle ;) We currently implement only coulombic interactions.

* `--interaction-strength <float>` sets the Coulomb coefficient in atomic units. If set to zero, particle-particle interactions are disabled.

Potentials
----------

Potentials define an external force acting on the particle(s). Any combination of these potentials can typically be activated simultaneously without any extra arguments.

### Gaussian bumps (noise potential)

* `--gaussian-bumps-manual` activates gaussian noise potential with manually set bump configuration. Each bump is of form 
$${V(r)=A\cdot \exp[-(r-r_c)/(2\sigma^2)]}$$ 
* `--gaussian-bumps-manual-locations` sets the locations of the gaussian bumps.
    - Example: `--gaussian-bumps-manual-locations 1.4 2 5.4 3` would produce two
      gaussian bumps at locations $(1.4,2)$ and $(5.4, 3)$.
*  `--gaussian-bumps-manual-widths` sets the widths ($\sigma$) of the gaussian bumps.
    - Example: `--gaussian-bumps-manual-widths 2.1 3` sets widths 2.1 and 3 for two bumps
*  `--gaussian-bumps-manual-amplitudes` sets the amplitudes of the gaussian bumps
    - Example: `--gaussian-bumps-manual-amplitudes 5.3 1e-3` sets amplitudes of two bumps to be 5.3 and $1e-3$.


### Gaussian bumps from itp2d

Gaussian bumps from itp2d savefiles.

* `--gaussian-bumps-fast-from-itp2d` activates fast calculation of gaussian noise
     and reads the noisy bump data from a savefile of itp2d-code.
* `--gaussian-bumps-itp2d-file` sets the itp2d savefile for reading noise data
    - Example: `--gaussian-bumps-itp2d-file here/is/my/itp2d.h5`


### Heat bath (gaussian random forces)

Forces are sampled from the distribution
$$P(x)= \frac{1}{\sqrt{2 \pi}\sigma} \exp\left[\frac{1}{2} \left(\frac{x}{\sigma}\right)^2\right].$$

*  `--gaussian-bath` activates random forces from a Gaussian distrubtion at each timestep. This method is not rigorously implemented (i.e., it essentially breaks the propagation algorithms). 
*  `--temperature` sets the temperature ($\sigma$) of the heat bath
    - Example: `--temperature 0.1`

### Soft stadium

$$ V(\mathbf r) = \min\limits_{\gamma} \left( \mathbf r - \mathbf r_\text{line}(\gamma) \right)^{-n},$$
where
$$\mathbf r_\text{line}(\gamma) = (R + \gamma, R), 0 \leq \gamma \leq L.$$

*  `--soft-stadium-potential` activates the potential for soft stadium 
*  `--soft-stadium-power <float>` sets $n$ in the above formulae.
*  `--geometry <float> <float>` sets $R$ and $L$ in the above formulae, respectively.

### Harmonic potential

$$V(\mathbf r) = \frac{k}{2}\Vert \mathbf r - \mathbf c\Vert^2$$

* `--harmonic-potential` activates the harmonic potential 
* `--centerpoint` defines the centerpoint ($\mathbf c$ above)
    - Example: `--centerpoint 0.25 0.55`
* `--potential-strength <float>` sets $k$. And no, it's not squared in the potential expression.

### Soft Lorentz Gas

In soft lorentz gas we have a periodic array of scatterers forming a triangular lattice. Each scatterer is equipped with a potential of form

$$ V(r) = \frac{1}{1+\exp(\frac{r-R}{\sigma})}, $$

where r is distance from the lattice site of the scatterer.

Note that Soft Lorentz Gas needs periodic boundary conditions (see Sec. Periodic systems).

*  `--soft-lorentz-gas` Activates soft lorentz gas.
*  `--soft-lorentz-radius <float>` sets the radii $R$ of the scatterers
*  `--soft-lorentz-sharpness <float>` sets $\sigma$
*  `--soft-lorentz-inverted` inverts the potentials, creating a periodic array of "attracting" scatterers. With this option, the potential is calculated as $V_{tot} = \sum [-V(r)] + 1$, so the potential should still be between 0 and 1.
*  `--soft-lorentz-lattice-sum-truncation <int>` determines how many nearest neighbour unit cells we use when calculating the total force and potential energy due to the scatteres. $n=1$ gives only a single unit cell, $n=2$ calculates also second and third nearest neighbours, $n=3$ calculates fourth and fifth nearest neighbours etc.


### Soft Lieb Gas

Soft Lieb Gas is similar to soft lorentz gas, but the scatterers are arranged in a Lieb-lattice formation instead of triangular lattice.

Each scatterer is equipped with a potential of form

$$ V(r) = \frac{1}{1+\exp(\frac{r-R}{\sigma})}, $$

where r is distance from the lattice site of the scatterer.


*  `--soft-lieb-gas` activates soft lieb gas.
*  `--soft-lieb-radius <float>` sets the radii $R$ of the scatterers
*  `--soft-lieb-sharpness <float>` sets $\sigma$
*  `--soft-lieb-lattice-sum-truncation <int>` determines how many nearest neighbour unit cells we use when calculating the total force and potential energy due to the scatteres. $n=1$ gives only a single unit cell, $n=2$ calculates also second and third nearest neighbours, $n=3$ calculates fourth and fifth nearest neighbours etc.

### Soft circular billiard

The soft circular billiard uses either an inverted Fermi pole or the error function (erf) to define the soft border of the billiard.
The Fermi pole has the form
$$ V(r) = -\frac{1}{1+\exp(\frac{r-R}{\sigma})} + 1, $$
where $\sigma$ is a softness parameter, $r$ the distance between the particle and the center of the pole and the radius $R$ is set to 1.
The error function potential has the form
$$ V(x, y) = \frac{\mathrm{erf}\left[h\left(x^2 + \frac{y^2}{b^2} - 1\right)\right] + 1}{2},$$
where $h$ is a hardness parameter and $b$ is defined through the relation $b = \sqrt{1 - e^2}$, where $e$ is the eccentricity of the ellipse, i.e., with the error function potential, it is possible to simulate an elliptical billiard.
The error function potential is also normalized between 0 and 1.
For more details on the soft elliptical billiard and its dynamical properties, see e.g. https://doi.org/10.1103/PhysRevE.94.022218.

* `--soft-circle` activates the soft circular billiard.
* `--soft-circle-type` chooses between the Fermi pole and the error function potential types.
* `--soft-circle-softness` sets the softness ($\sigma$) and the hardness ($h$) parameters of the potential.
* `--soft-circle-eccentricity` sets the eccentricity $e$ of the circular billiard if the error function potential is used.

### Constant bias voltage

This implementation is not ready. Only a skeleton has been implemented for future needs.

$$V(r) = C \mathbf n$$

*  `--bias-voltage` activates bias.
*  `--bias-strength <float>` C in above formula.
*  `--bias-direction <float> <float>` defines the cartesian components of $\mathbf n$ in the above formula. It's normalized by the program.

## Hard wall tables

Hard wall tables are set with the option

*  `--table <int>` where the argument defines the table type, given by `arg=` in the following subsection.

### No table, `arg=0`

No hard wall tables.

### Rectangle, `arg=1`

Rectangle with bottom left corner at the origin.

*  `--geometry <float> <float>` sets bottom/top and left/right side lengths.

### Triangle, `arg=2`

Upwards facing scaled equilateral triangular table with bottom left corner at the origin.

*  `--geometry <float> <float>` sets x- and y-scales

### Circle, `arg=3`

Circular table centered at $(R,R)$.

*  `--geometry <float> <float>` sets the radius $R$ with the first argument. Second arg is ignored, but must be given.

### Sinai, `arg=4`

Sinai table with square side length $L$ and circle radius $L$.

* `--geometry <float> <float>` 1st arg sets $L$ and 2nd $R$.

### Ring, `arg=5`

Ring shaped table with inner radius $R_1$ and outer radius $R_2$.

* `--geometry <float> <float>` 1st arg sets $R_1$ and second $R_2$.

### Stadium, `arg=6`

Stadium shaped table with straight track length $L$ and circular end caps of radii $R$.

* `--geometry <float> <float>` 1st arg $R$, 2nd $L$

### Mushroom, `arg=7`

Mushroom table with equal foot height and cap radius $R$, and foot width $L$.

* `--geometry <float> <float>` 1st arg $L$, 2nd $R$

### Isolated point contact, `arg=8`

Two-container system with sides of the square containers $L$, channel length $0.1$ and gate width $w$.

* `--geometry <float> <float>` 1st arg $L$, 2nd $w$

### Ellipse, `arg=9`

Elliptical table with horizontal and vertical axis lengths $a$ and $b$.

* `--geometry <float> <float>` 1st arg $a$, 2nd $b$

### Lorentz gas (hard wall), `arg=10`

Regular Lorentz gas with scatterer radius $R$. Remember to set also periodicity options.

* `--geometry <float> <float>` 1st arg $R$, 2nd ignored but mandatory.

Propagation, saving, and initial states
=======================================

Propagation algorithms
----------------------

Timestep can be adjusted with

*  `--delta-t <float>` 

Arguments for the option `--propagator` bolded and in quotes.

### Without interactions and without magnetic field (`--billtype 1`):

- Euler method

### Without interactions, with magnetic field (`--billtype 2`):

- 2nd order scheme from
  Q. Spreiter and M. Walter, J. Comp. Phys \textbf{152}, 102--119 (1999)

### With interactions, without magnetic field (`--billtype 3`):

- 2nd order scheme (velocity Verlet): "**2**"
  M. P. Allen and D. J. Tildesley, Computer Simulation of Liquids,
  Oxford University Press, USA, 1989
- 2nd order scheme with optimal coefficients: "**2opt**"
  R. I. McLachlan, SIAM J. Sci. Comput. 16(1), 151–168 (1995)
  (On page 14, type S, m=2)
- 3rd order scheme: "**3**"
  M. Suzuki, J. Phys. Soc. Jpn 61, 3015--3019 (1992) (page 3018, case r=5)
- 4th order scheme: "**4**"
  H. Yoshida, Phys. Lett. A 150, 262--268 (1990);
- 4th order scheme with optimal coefficients: "**4opt**"
  R. I. McLachlan, SIAM J. Sci. Comput. 16(1), 151–168 (1995)
  (On page 14, type $SB^3A$, m=4)
- 6th order scheme with optimal coefficients: "**6opt**"
  R. I. McLachlan, SIAM J. Sci. Comput. 16(1), 151–168 (1995)
  (On page 15, type SB^3A, m=7)


### With interactions, with magnetic field (`--billtype 4`):

- 4th order Runge Kutta (for testing purposes): "**rk4**"
- 2nd order scheme: "**2**"
  Q. Spreiter and M. Walter, J. Comp. Phys 152, 102--119 (1999)
- 2nd order Boris method: "**boris**"
  J. Boris, in Proceedings of the Fourth Conference on Numerical
  Simulation of Plasmas (Naval Research Laboratory, Washington DC,
  1970), p. 3.
- 2nd order method: "**yhe2**"
  Y. He et al., J. Comp. Phys 281, 135-147 (2015)
- 2nd order Scovel method: "**scovel**"
  B. Leimkuhler and S. Reich, Simulating Hamiltonian Dynamics,
  Cambridge University Press (2004)
- 2nd order Chin's methods: "**chin\_2a**" (should be essentially same as
  the Spreiter&Walter scheme) and "**chin\_2b**":
  S. A: Chin, PRE 77, 066401 (2008)
- 4th order Yoshida ("**yoshida4**") and Suzuki ("**suzuki4**") decompositions,
  based on Chin's paper: Explicit formulas, e.g., in
  Y. He et al., Journal of Computational Physics 305, 172-184 (2016)
- 4th and 6th order Partitioned Runge Kutta methods ("**prk4**" and "**prk6**")
  based on Chin's explicit solution to partial flows and operator
  splittings from:
  S. Blanes & P.C. Moan, J. Comp. and Appl. Math 142, 313-330 (2002)
- 6th and 8th order Partitioned Runge Kutta methods ("**ss6\_7**", "**ss6\_9**",
  "**ss8\_15**", "**ss8\_17**") based on Chin's explicit solution to partial flows
  and operator splittings from:
  R. I. McLachlan, SIAM J. Sci. Comput. 16(1), 151–168 (1995)


Saving
------

`bill2d` and many of the other executables save their data to a HDF5 file.
You can control what is saved by the following arguments:

*  `--output <int>` determines how much information is printed to stdout (0-2)
*  `--savepath <string>` sets the path where savefile is placed. E.g., `--savepath data/myfile.h5`
*  `--save-nothing` disables saving of simulation related data.
*  `--save-trajectory` saves particle trajectories
*  `--save-velocity` saves particle velocities
*  `--save-energies` saves kinetic and potential energy
*  `--save-histograms` saves position, velocity, and speed histograms
*  `--save-particlenum` saves the number of particles as a function of time to file (useful if you have a source or drain in the system)
*  `--histogram-bins <int>' number of bins in histograms
*  `--sparsesave <int>` saves data every nth timestep to file
*  `--poincare-map` saves information on collisions with hard wall boundaries
*  `--custom-psos` activates recording of crossings over a line in coordinate space. See the section 'studying phase space properties' for a description of what is saved.
*  `--custom-psos-line <float> <float> <float>` Defines the line: $a_1 x+a_2 y+a_3 = 0$

Controlling the duration of simulation
*  `--max-t <float>` maximum time in simulation units to be propagated
*  `--max-collisions <int>` stops simulation after n collisions with hard wall boundaries
*  `--min-t <float>` sets the buffer length for datasets. Data is flushed to file only when the buffer is full.


Initial state
-------------

Total energy of the system is fixed with

* `--energy <float>`

By default, the initial state of the system is randomized as follows:

We set the spawning area for the particles with 

*  `--spawn-area <float xmin> <float ymin> <float xmax> <float ymax>`

if this is not set, we use the defaults: with closed hard-wall boundaries, we spawn the particles inside the table. For periodic systems, we spawn them inside the first unit cell. The particle positions are randomized from a uniform distribution in the coordinate space, but we neglect any position configuration which gives too high potential energy (compared to the total energy of the system) or which is not fully inside the billiard table.

Next, we distribute the rest of the energy as kinetic energy evenly for each particle and randomize the launch angle from a uniform distribution in $[0, 2\pi]$.

You can also set the initial configuration manually by writing x, y, $v_x$, and $v_y$ coordinates separated by a space to a text file. One line per particle, i.e.,

```
input.txt
---------
2.3 4.3 1.2 5.4
2.5 4.2 1.2 -3.2
```

Location of this file is passed to `bill2d` with the option

* `--initial-positions path/to/input.txt`

If you have more particles in the simulation than lines in the file, the rest of the particles get random initial configuration. By default, the speeds are normalized so that the total energy is fixed to value given to `--energy` option. In case you want to use precisely what you have inputted, you should pass `--ignore-E` option to disable this normalization of speeds.

The initial configuration you have inputted can be perturbed slightly. Each coordinate and velocity component is perturbed by a random number sampled from a normal distribution. These are set with the options

*  `--perturbate` to activate perturbations
*  `--perturbation-std <float>` to set the standard deviation of the distribution.


In addition, you can read the initial configuration from a savefile (HDF5) if you have saved both the trajectories and velocities. Just supply the filepath to `--initial-positions` and make sure that the filename ends with `.h5`. By default, we take the **initial** configuration, but in case you want to continue the simulation, use the flag `--continue-run`.

Escape rates of open billiards
==============================

To calculate survival or escape probability distribution as a function of time for open tables, we use the `bill2d_escape_rate` binary.
It sets a single hole on the boundary of the table, and the ratio of the hole length to the total boundary length is given by 

*  `--holeratio <float>` 

Note that this is the only binary that uses open billiard tables (same tables implemented as for closed billiards)

What the program does: it creates $N$ copies of the system of interest, propagates each of them until one of the particles escapes through
the hole, and records the time and total number of collisions with the boundaries (for all particles). 

To set the ensemble size `N`, use

*  `--ensemble-size <int>` 

As usual, you can set the datafile location with `--savepath`. 

If you **do not** supply the option

*  `--save-nothing`

then trajectories of each simulation are saved in `<savepath>_trajectories/bill2d_<number>.h5`.
These can be plotted with `draw_trajectories` as usual. **When saving trajectories, the simulations 
do not run in parallel!**

The escape event datasets are in the file as `escape_events/escape_times` and
`escape_events/escape_hits`. The normalized escape rates can be easily
calculated as histograms of either of the datasets. Similarly,
the survival probability is given by $$S(T) = 1-\int\limits_0^T dt P_\text{escape}(t)$$
where $P_\text{escape}(t$) is the probability for a particle hitting a hole at time
$t$.

See also the example input file:

* `examples/magnetic_square_billiard_escape.config`



Diffusion coefficients in periodic systems
======================================================

Calculation of the diffusion coefficient
$$D = \lim\limits_{t\to\infty} \frac{\langle \Vert \mathbf r(t) - \mathbf r(0) \Vert^2 \rangle}{4t}$$
or more generally, the diffusive behaviour
$$ \langle \Vert \mathbf r(t) - \mathbf r(0) \Vert^2 \rangle \sim f(t) $$
typically requires numerical simulation and averaging over the phase space.

With `bill2d_diffusion_coefficient` we can propagate multiple copies of a periodic single-particle system and calculate the diffusion properties efficiently. For details on setting up a periodic system, see previous chapters. The initial states are randomized by first setting up the positions in energetically allowed region of the position space by uniform sampling of coordinates (which fixes the kinetic energy), and the direction of velocity is sampled from a uniform distribution.

The size of the ensemble over which the average $\langle \cdot \rangle$ is calculated is controlled by

*  `--ensemble-size <int>` 

If squared displacements of each individual trajectory of the ensemble are required,
they can be written to a txt-file using

*  `--individual-data`

and the filename with

*  `--savepath`.

Output (to stdout) is time, the ensemble average of the square
displacement, and the standard error of the average.
The diffusion coefficient can be obtained by a linear fit to this data using.

To check the system calculated by `bill2d_diffusion_coefficient`, pass the
configuration file (or command line options) to `bill2d`, and use the option
`--save-trajectory` via e.g. command line to make sure that the
trajectory is saved.

```
$ bill2d --config <diffusion_config_file> --save-trajectory
$ draw_trajectories
```

Notice that the drawing script does not always plot all the scatterers
(a bug), but they are there in the calculation.

See also the example input files:

*  `examples/diffusion_coefficient_hard_lorentz_gas.config` 
*  `examples/diffusion_coefficient_soft_lorentz_gas.config`

Parallel simulations
====================

With `bill2d_parallel`, several simulations can be run efficiently in parallel with OpenMP threading.
Compared to `bill2d_diffusion_coefficient`, this executable is more versatile as it allows more customization.
In addition, this executable saves everything in a single HDF5 file whose path is given with the `--savepath` argument, in contrast to `.txt` files which are saved by `bill2d_diffusion_coefficient`.

The diffusion coefficient calculation can be replicated with this executable by giving the `--save-individual-dist-sqr` argument.
The ensemble size (number of particles and billiard simulations) is given with the `--ensemble-size` argument, similar to `bill2d_diffusion_coefficient`.
If saving squared differences is enabled, they are saved into a dataset named `sd` in the root of the HDF5 file.
The saving frequency of the squared differences cannot currently be controlled, and it will happen once every simulation second.
The resulting dataset will have a shape of `(max_t, enseble_size)`, that is, it will have a column for each simulated particle, containing its squared difference to initial position with regards to time.

This executable was originally born as a means of efficiently saving custom Poincaré sections, which is why references to `bill2d_poincare` can be seen in the Git history.
Saving custom PSoS is still possible by giving the `--custom-psos 1` argument and defining the `--custom-psos-line`.
The datasets containing info about Poincaré section crossings will be saved under the `custom_psos` group, with particle index being the dataset name (for example, `custom_psos/42` would contain the PSoS data for a particle with ID 42).

Trajectories and velocities can also be saved with this executable, when `--save-trajectory 1` and `--save-velocity 1` arguments are given, respectively.
These trajectories will be saved to the HDF5 file in a fashion comparable to the other executables, that is, under the group `trajectories` with `particle<NUM>` being the dataset name for particle with ID `NUM`.
As an example, the particle with ID 42 would have its trajectory saved as `trajectories/particle42` and velocity `velocities/particle42`.
In hindsight, this system would also have been logical for storing the custom PSoS.
However, I (Esko) was not familiar enough with Bill2D when I originally created the `bill2d_poincare` executable, and all my analysis scripts expect the naming scheme described above, so for now I have not changed it.
I'm not against the change, so if you want to change the file structure from `custom_psos/0` to `custom_psos/particle0`, feel free to do the modification.

Initial positions for the particles can also be provided via the `--initial-positions` argument, which should be a path to a file containing these positions.
If the initial position file is not given, the initial positions are generated randomly with Bill2D's default generation methods.
Currently, only text files are supported.
The format is as follows: each line contains four space-separated floating point numbers.
These numbers are, in order, x coordinate, y coordinate, velocity component parallel to the x axis, velocity component parallel to the y axis.
Examples of these lines are given below:

```
1.108823529411764763 1.108823529411764763 0.153230951806890670 0.988190404430418612
1.108863701086587383 1.108863701086587383 0.153230951806890670 0.988190404430418612
```

In general, it is recommended to write 17-18 significant digits to make sure the number is moved to memory accurately.
This text file -based system is not optimal, as the floating-point numbers are not necessarily bit-by-bit accurate compared to the original initial positions.
A more proper way would be to save these into binary files, such as HDF5.
This is another feature that has not yet been developed, so if anyone  wants to implement support for giving initial positions in HDF5 files, feel free.
Regardless of whether the initial positions are given as a parameter or not, they will be saved into the HDF5 file root in a dataset named `initial_positions`.
This dataset will have a shape of `(ensemble_size, 4)`, so each row represents one particle.
Please note that there are currently no proper checks present in the code for the length of this file, so if the number of initial conditions in the file is smaller than ensemble size, the executable might crash or undefined behavior may occur.

The executable currently supports only single-particle simulations, that is, for each ensemble member a new billiard simulation is created.
Other usual parameters work, and the configuration can be given in a config file with the `--config` parameter.
It is recommended to use `--output 1` to suppress the "Using x billiard" messages printing from each of the billiard simulations.

The number of parallel threads run by this executable can be controlled with the standard `OMP_NUM_THREADS` environment variable.
As the simulations are completely independent of each other, the scaling of this executable is practically infinite (assuming enough free computing cores are available, doubling the number of threads will double the performance).
However, note that `min_t` should be set to a relatively high number to minimize file system access, as the memory buffers are written to the HDF5 file at every multiple of `min_t` simulation time.

Studying phase space properties
===============================

The binary `bill2d_phase_space_regularity` and `bill2d_phase_space_regularity_checker` takes an ensemble of random initial conditions on a Poincare section. The trajectories are propagated up to some time or number of crossings with the Poincare section.

The ensemble size is given by 

* `--ensemble-size <int>` 

and the Poincare section (a line) is defined with

* `--custom-psos` to activate it, and
* `--custom-psos-line <float> <float> <float>` which defines the parameters $a$, $b$, and $c$ of the line $a x + b y +c = 0$.

Each trajectory is simulated for `--max-t` time, and the minimum phase space
distance between the initial point on the Poincare section and the following
crossings is saved.

The data is saved to a file `phase_space_regularity.txt`,
which is organized as:

$\min\limits_{t\in \textrm{crossings}} \Vert \mathbf r(t) - \mathbf r(0) \Vert$ | arc length param | $v_\parallel$ | $\theta_\textrm{initial}$ | $x_0$ | $y_0$ | $v_{x0}$ | $v_{y0}$ | ...

On the other hand, the binary `bill2d_phase_space_regularity_checker` saves the data 

$\min\limits_{t_1,t_2\in \textrm{crossings}} \Vert \mathbf r(t_1) - \mathbf r(t_2) \Vert$ | arc length param | $v    _\parallel$ | $\theta_\textrm{initial}$ | $x_0$ | $y_0$ | $v_{x0}$ | $v_{y0}$ | ...

In addition to randomly sampling the initial coordinates on the Poincare section, we also start minimization algorithm whenever we find a trajectory with small enough recurrence distance. This threshold is set with

*  `--regularity-analysis-local-optimization-threshold <float>`


Scripts
=======

By default, `bill2d` installs a few python scripts to help you get started with analysing your simulations.

*  `draw_collision_map` plots Poincare section / collision map from collisions with hard wall boundaries.
*  `draw_custom_psos` plots Poincare section from crossings with the line defined by `--custom-psos`
*  `draw_difference` plots the phase space difference as a function of time for two different savefiles
*  `draw_energies` plots kinetic and potential energy as a function of time
*  `draw_particle_number` plots the particle number
*  `draw_position_histogram`
*  `draw_regularity_analysis` plots the results from `bill2d_phase_space_regularity(_checker)` simulations
*  `draw_speed_histogram`
*  `draw_trajectories`
*  `draw_velocity_histogram`

For details on a script, query `$ <script_name> --help`
