# A simple wrapper around check_cxx_source_compiles that takes a file path as input instead of the source code

if(__check_cxx_file_compiles)
    return()
endif()
set(__check_cxx_file_compiles YES)

include(CheckCXXSourceCompiles)

function(check_cxx_file_compiles _path _var)
    file(READ "${_path}" _snippet)
    check_cxx_source_compiles("${_snippet}" ${_var})
endfunction()
