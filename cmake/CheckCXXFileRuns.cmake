# A simple wrapper around check_cxx_source_runs that takes a file path as input instead of the source code

if(__check_cxx_file_runs)
    return()
endif()
set(__check_cxx_file_runs YES)

include(CheckCXXSourceRuns)

function(check_cxx_file_runs _path _var)
    file(READ "${_path}" _snippet)
    check_cxx_source_runs("${_snippet}" ${_var})
endfunction()
