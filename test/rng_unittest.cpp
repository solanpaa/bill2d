/*
 * rngtest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rng.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"

namespace bill2d {

TEST(RNG, default_constructor) {
    RNG* rng;
    RNG* rng2;
    for (unsigned i = 0; i < 1000; i++) {
        rng = new RNG();
        rng2 = new RNG();
        // Default constructor should produce different seeds
        EXPECT_NE(rng->uniform_rand(0, 1), rng2->uniform_rand(0, 1));
        delete rng;
        delete rng2;
    }
}

TEST(RNG, custom_constructor) {
    RNG rng(23487);
    RNG rng2(23487);
    EXPECT_EQ(rng.uniform_rand(0, 1), rng2.uniform_rand(0, 1));  // Same seed, should be the same
}
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
