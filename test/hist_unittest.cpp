/*
 * hist_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hist.hpp"
#include "rng.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"
#include <cmath>
#include <gsl/gsl_histogram.h>
#define EXPECT_FEQ(A, B) EXPECT_NEAR(A, B, 1e-10)

#define EXPECT_FNE(A, B) EXPECT_TRUE(fabsl((A) - (B)) > 1e-10)

namespace bill2d {

TEST(Hist, add) {
    Hist histogram(10, 0, 10);
    histogram.add(0.5);
    histogram.add(5.6);
    histogram.add(5.6);
    histogram.add(5.6);
    for (unsigned i = 0; i < 10; i++) {
        if (i == 0)
            EXPECT_FEQ(1, histogram.data()[i]);
        else if (i == 5)
            EXPECT_FEQ(3, histogram.data()[i]);
        else
            EXPECT_FEQ(0, histogram.data()[i]);
    }
}

TEST(Hist, get) {
    Hist histogram(10, 0, 10);
    RNG rng;
    for (unsigned i = 0; i < 1000; i++) histogram.add(rng.uniform_rand(-10, 20));

    for (unsigned i = 0; i < 10; i++) {
        EXPECT_EQ(histogram.data()[i], histogram.get(i));
    }
}

TEST(Hist, set) {
    Hist histogram(10, 0, 10);
    histogram.set(5.5, 20);
    EXPECT_FEQ(20, histogram.get(5));
}

TEST(Hist, range) {
    Hist histogram(10, 0, 10);
    for (unsigned i = 0; i < 11; i++) EXPECT_FEQ(i, histogram.range()[i]);
}

TEST(Hist, sum) {
    Hist histogram(10, 0, 10);
    RNG rng;
    for (unsigned i = 0; i < 1000; i++) histogram.add(rng.uniform_rand(0, 10));

    EXPECT_FEQ(1000, histogram.sum());
}
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
