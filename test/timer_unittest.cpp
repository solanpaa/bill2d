/*
 * timer_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"
#include <cmath>

#include "timer.hpp"

namespace bill2d {

TEST(Timer, start_stop) {
    Timer t;
    t.start();
    double a = t.stop();
    for (unsigned i = 0; i < 10000; i++) t.stop();
    double b = t.stop();
    EXPECT_TRUE(b > a);
}
}  // namespace bill2d
int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
