/*
 * hist_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "parameterlist.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"

#define EXPECT_FEQ(A, B) EXPECT_NEAR(A, B, 1e-10)

#define EXPECT_FNE(A, B) EXPECT_TRUE(fabsl((A) - (B)) > 1e-10)

namespace bill2d {

TEST(ParameterList, calculate_buffersizes_zero_modulus) {
    ParameterList plist;
    plist.calculate_buffersizes();
    EXPECT_EQ(0, plist.minimum_number_of_iterations % plist.spsave);
    EXPECT_TRUE(0 < plist.buffersize * plist.spsave - plist.minimum_number_of_iterations);
    EXPECT_TRUE(plist.spsave >= plist.buffersize * plist.spsave - plist.minimum_number_of_iterations);
}

TEST(ParameterList, calculate_buffersizes_nonzero_modulus) {
    ParameterList plist;
    plist.output_level = 1;
    plist.min_t = 0.15234;
    plist.delta_t = 1e-5;
    plist.spsave = 50;
    plist.calculate_buffersizes();
    EXPECT_EQ(0, plist.minimum_number_of_iterations % plist.spsave);
    EXPECT_EQ(15250, plist.minimum_number_of_iterations);

    plist.min_t = 1.2353;
    plist.delta_t = 0.243245234;
    plist.spsave = 3;
    plist.calculate_buffersizes();
    EXPECT_EQ(0, plist.minimum_number_of_iterations % plist.spsave);
    EXPECT_EQ(6, plist.minimum_number_of_iterations);
}
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
