#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Run a few test for the program

from __future__ import division
import h5py
from numpy import array, arange, loadtxt, fabs
from os import system

HEADER = '\033[95m'
BLUE = '\033[94m'
GREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'


def equalh5(filename1, filename2):
    isequal=True
    f1 = h5py.File(filename1, 'r')
    global_attrs1 = f1["/parameters"].attrs

    f2 = h5py.File(filename2, 'r')
    global_attrs2 = f2["/parameters"].attrs

    traj_data1 = f1["/trajectories"]
    traj_data2 = f2["/trajectories"]
    vel_data1=f1["/velocities"]
    vel_data2=f2["/velocities"]

    ekin_data1 = array(f1["/kinetic_energy"])
    epot_data1 = array(f1["/potential_energy"])

    ekin_data2 = array(f2["/kinetic_energy"])
    epot_data2 = array(f2["/potential_energy"])

    ts1=global_attrs1["table_shape"]
    ts2=global_attrs2["table_shape"]
    if(ts1!=ts2):
        f1.close()
        f2.close()
        print FAIL+"Different shape names :",
        return False

    if(fabs(ekin_data1-ekin_data2).max()>1e-6):
        f1.close()
        f2.close()
        print FAIL+"Too high difference in kinetic energy :", fabs(ekin_data1-ekin_data2).max()
        return False

    if(fabs(epot_data1-epot_data2).max()>1e-6):
        f1.close()
        f2.close()
        print FAIL+"Too high difference in potential energy :",fabs(epot_data1-epot_data2).max()
        return False

    for n in [1,2,3]:

        datax2 = array([ d[0] for d in traj_data2["particle"+str(n)][:] ])
        datay2 = array([ d[1] for d in traj_data2["particle"+str(n)][:] ])

        datax1 = array([ d[0] for d in traj_data1["particle"+str(n)][:] ])
        datay1 = array([ d[1] for d in traj_data1["particle"+str(n)][:] ])


        if(fabs(datax1-datax2).max()>1e-6):
            f1.close()
            f2.close()
            print FAIL+"Too high difference in x-coordinate :",fabs(datax1-datax2).max()

            return False
        if(fabs(datay1-datay2).max()>1e-6):
            f1.close()
            f2.close()
            print FAIL+"Too high difference in y-coordinate :",fabs(datay1-datay2).max()
            return False

    f1.close()
    f2.close()
    return True



def test1():
    system("bin/bill2d --save-trajectory --save-velocity --min-t 0.1 --max-t 0.9 -N 3 --billtype 1 --table 0 -E 10 --initial-positions test/testdata/lahtopaikat --output 0")
    if(equalh5("test/testdata/normal.h5","data/bill2d.h5")):
        print GREEN + "2nd order propagator without interactions" + ENDC
    else:
        print FAIL + "2nd order propagator without interactions failed" + ENDC


def test2():
    system("bin/bill2d --save-trajectory --save-velocity --min-t 0.1 --max-t 1 -N 3 -E 10 --billtype 2 -B 4 --table 0 --initial-positions test/testdata/lahtopaikat --output 0")
    if(equalh5("test/testdata/mfield.h5","data/bill2d.h5")):
        print GREEN + "2nd order propagator without interactions in a mag. field" + ENDC
    else:
        print FAIL + "2nd order propagator without interactions in a mag. field failed" + ENDC


def test3():
    system("bin/bill2d --save-trajectory --save-velocity --min-t 0.1 --max-t 1 -N 3 --billtype 3 --table 0 -E 6 --initial-positions test/testdata/lahtopaikat --propagator 2 --delta-t 1e-4 --sparsesave 10 --output 0")
    if(equalh5("test/testdata/propagator2_ia.h5","data/bill2d.h5")):
        print GREEN + "2nd order propagator with interactions" + ENDC
    else:
        print FAIL + "2nd order propagator with interactions failed" + ENDC


def test4():
    system("bin/bill2d --save-trajectory --save-velocity -E 10 --min-t 0.1 --max-t 1 -N 3 --billtype 4 -B 4 --table 0 --initial-positions test/testdata/lahtopaikat --inter-str 1 --output 0 --delta-t 1e-5 --sparsesave 100")
    if(equalh5("test/testdata/inter_ma.h5","data/bill2d.h5")):
        print GREEN + "2nd order propagator with interactions and mag. field" + ENDC
    else:
        print FAIL + "2nd order propagator with interactions and mag. field failed" + ENDC


def test5():
    system("bin/bill2d --save-trajectory --save-velocity --min-t 0.1 --max-t 1 -N 3 --billtype 3 --table 0 -E 6 --initial-positions test/testdata/lahtopaikat --propagator 2sym --delta-t 1e-3 --sparsesave 1 --output 0")

    if(equalh5("test/testdata/propagator2_ia.h5","data/bill2d.h5")):
        print GREEN + "2nd order symmetric propagator" + ENDC
    else:
        print FAIL + "2nd order symmetric propagator failed" + ENDC


def test6():
    system("bin/bill2d --save-trajectory --save-velocity --min-t 0.1 --max-t 1 -N 3 --billtype 3 --table 0 -E 6 --initial-positions test/testdata/lahtopaikat --propagator 3 --delta-t 1e-3 --sparsesave 1 --output 0")

    if(equalh5("test/testdata/propagator2_ia.h5","data/bill2d.h5")):
        print GREEN + "3rd order propagator" + ENDC
    else:
        print FAIL + "3rd order propagator failed" + ENDC


def test7():
    system("bin/bill2d --save-trajectory --save-velocity --min-t 0.1 --max-t 1 -N 3 --billtype 3 --table 0 -E 6 --initial-positions test/testdata/lahtopaikat --propagator 4 --delta-t 1e-3 --sparsesave 1 --output 0")

    if(equalh5("test/testdata/propagator2_ia.h5","data/bill2d.h5")):
        print GREEN + "4th order propagator" + ENDC
    else:
        print FAIL + "4th order propagator failed" + ENDC


def test8():
    system("bin/bill2d --save-trajectory --save-velocity --min-t 0.1 --max-t 1 -N 3 --billtype 3 --table 0 -E 6 --initial-positions test/testdata/lahtopaikat --propagator 4sym --delta-t 1e-3 --sparsesave 1 --output 0")

    if(equalh5("test/testdata/propagator2_ia.h5","data/bill2d.h5")):
        print GREEN + "4th order symmetric propagator" + ENDC
    else:
        print FAIL + "4th order symmetric propagator failed" + ENDC


def test9():
    system("bin/bill2d --save-trajectory --save-velocity --min-t 0.1 --max-t 1 -N 3 --billtype 3 --table 0 -E 6 --initial-positions test/testdata/lahtopaikat --propagator 6sym --delta-t 1e-3 --sparsesave 1 --output 0")

    if(equalh5("test/testdata/propagator2_ia.h5","data/bill2d.h5")):
        print GREEN + "6th order symmetric propagator" + ENDC
    else:
        print FAIL + "6th order symmetric propagator failed" + ENDC


def removeTMPfiles():
    system("rm data/bill2d.h5")

#Test different propagators against reference data from older version of bill2d
test1()
test2()
test3()
test4()
test5()
test6()
test7()
test8()
test9()

#removeTMPfiles()
