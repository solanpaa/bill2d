/*
 * timer_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "custom_psos.hpp"
#include "datatypes.hpp"
#include "parameterlist.hpp"
#include "particle.hpp"
#include "vec2d.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"

namespace bill2d {
class custom_psos_tests : public ::testing::Test {
protected:
    void particleset_to_vectors() {
        particleset particles;
        Particle p;
        p.d_r = {0.0, 1.0e-9};
        p.d_v = {0.0, 1.0};
        p.d_F = {0.0, 0.0};
        particles.emplace_back(std::move(p));
        p.d_r = {7.0, 3.0e-9};
        p.d_v = {-5.0, 7.0};
        p.d_F = {5.0, 7.0};
        particles.emplace_back(std::move(p));
        std::vector<double> x0, forces;
        CustomPSOS::particleset_to_vectors(particles, x0, forces);

        EXPECT_EQ(0.0, x0[0]);
        EXPECT_EQ(1.0e-9, x0[1]);
        EXPECT_EQ(0.0, x0[2]);
        EXPECT_EQ(1.0, x0[3]);
        EXPECT_EQ(7.0, x0[4]);
        EXPECT_EQ(3.0e-9, x0[5]);
        EXPECT_EQ(-5.0, x0[6]);
        EXPECT_EQ(7.0, x0[7]);

        EXPECT_EQ(0.0, forces[0]);
        EXPECT_EQ(0.0, forces[1]);
        EXPECT_EQ(5.0, forces[2]);
        EXPECT_EQ(7.0, forces[3]);
    }

    void handle_crossing1() {
        particleset particles;
        Particle p;
        p.d_r = {0.0, 1.0e-9};
        p.d_v = {0.0, 1.0};
        p.d_F = {0.0, 0.0};
        particles.emplace_back(std::move(p));

        auto S = [](const particleset& pset) {
            return pset.at(0).d_r[1];  // y_1
        };

        auto Sgrad = [](const std::vector<double>&) {
            std::vector<double> grad(4, 0.0);
            grad.at(1) = 1.0;
            return grad;
        };

        auto ph_to_psos = [](const std::vector<double>& Z) {
            std::vector<double> ret = {Z.at(0), Z.at(1), Z.at(2), Z.at(3), Z.at(4)};
            return ret;
        };

        CustomPSOS psos(S, Sgrad, ph_to_psos);
        psos.PSOS_value = -1e-9;

        psos.evaluate(particles, 1.0);

        // Check for the data

        EXPECT_NEAR(0.0, psos.PSOS_data.at(0)[0], 1e-6);
        EXPECT_NEAR(0.0, psos.PSOS_data.at(0)[1], 1e-6);
        EXPECT_NEAR(0.0, psos.PSOS_data.at(0)[2], 1e-6);
        EXPECT_NEAR(1.0, psos.PSOS_data.at(0)[3], 1e-6);
        EXPECT_NEAR(1.0 - 1e-9, psos.PSOS_data.at(0)[4], 1e-6);
    }

    void handle_crossing2() {
        particleset particles;
        Particle p;
        p.d_r = {-0.1, 0.1};
        p.d_v = {0.0, 1.0};
        p.d_F = {0.0, 0.0};
        particles.emplace_back(std::move(p));

        auto S = [](const particleset& pset) {
            return -pset.at(0).d_r[0] + pset.at(0).d_r[1];  // S = y-x
        };

        auto Sgrad = [](const std::vector<double>&) {
            std::vector<double> grad(4, 0.0);
            grad.at(0) = -1.0;
            grad.at(1) = 1.0;
            return grad;
        };

        auto ph_to_psos = [](const std::vector<double>& Z) {
            std::vector<double> ret = {Z.at(0), Z.at(1), Z.at(2), Z.at(3), Z.at(4)};
            return ret;
        };

        CustomPSOS psos(S, Sgrad, ph_to_psos);
        psos.PSOS_value = -1e-9;

        psos.evaluate(particles, 1.0);

        // Check for the data

        EXPECT_NEAR(-0.1, psos.PSOS_data.at(0)[0], 1e-6);
        EXPECT_NEAR(-0.1, psos.PSOS_data.at(0)[1], 1e-6);
        EXPECT_NEAR(0.0, psos.PSOS_data.at(0)[2], 1e-6);
        EXPECT_NEAR(1.0, psos.PSOS_data.at(0)[3], 1e-6);
        EXPECT_NEAR(1.0 - 0.2, psos.PSOS_data.at(0)[4], 1e-6);
    }

    void handle_crossing3() {
        particleset particles;
        Particle p;
        p.d_r = {-0.1, 0.1};
        p.d_v = {-2.0, 1.0};
        p.d_F = {0.0, 0.0};
        particles.emplace_back(std::move(p));

        auto S = [](const particleset& pset) {
            return -pset.at(0).d_r[0] + pset.at(0).d_r[1];  // S = y-x
        };

        auto Sgrad = [](const std::vector<double>&) {
            std::vector<double> grad(4, 0.0);
            grad.at(0) = -1.0;
            grad.at(1) = 1.0;
            return grad;
        };

        auto ph_to_psos = [](const std::vector<double>& Z) {
            std::vector<double> ret = {Z.at(0), Z.at(1), Z.at(2), Z.at(3), Z.at(4)};
            return ret;
        };

        CustomPSOS psos(S, Sgrad, ph_to_psos);
        psos.PSOS_value = -1e-9;

        psos.evaluate(particles, 1.0);

        // Check for the data

        EXPECT_NEAR(0.1 / 3.0, psos.PSOS_data.at(0)[0], 1e-5);
        EXPECT_NEAR(0.1 / 3.0, psos.PSOS_data.at(0)[1], 1e-5);
        EXPECT_NEAR(-2.0, psos.PSOS_data.at(0)[2], 1e-5);
        EXPECT_NEAR(1.0, psos.PSOS_data.at(0)[3], 1e-5);
        EXPECT_NEAR(1.0 - 0.2 / 3, psos.PSOS_data.at(0)[4], 1e-5);
    }

    void handle_crossing4() {  // Curved PSOS, TODO
        particleset particles;
        Particle p;
        p.d_r = {-0.1, 0.1};
        p.d_v = {-2.0, 1.0};
        p.d_F = {0.0, 0.0};
        particles.emplace_back(std::move(p));

        auto S = [](const particleset& pset) {
            return -pset.at(0).d_r[0] + pset.at(0).d_r[1];  // S = y-x
        };

        auto Sgrad = [](const std::vector<double>&) {
            std::vector<double> grad(4, 0.0);
            grad.at(0) = -1.0;
            grad.at(1) = 1.0;
            return grad;
        };

        auto ph_to_psos = [](const std::vector<double>& Z) {
            std::vector<double> ret = {Z.at(0), Z.at(1), Z.at(2), Z.at(3), Z.at(4)};
            return ret;
        };

        CustomPSOS psos(S, Sgrad, ph_to_psos);
        psos.PSOS_value = -1e-9;

        psos.evaluate(particles, 1.0);

        // Check for the data

        EXPECT_NEAR(0.1 / 3.0, psos.PSOS_data.at(0)[0], 1e-5);
        EXPECT_NEAR(0.1 / 3.0, psos.PSOS_data.at(0)[1], 1e-5);
        EXPECT_NEAR(-2.0, psos.PSOS_data.at(0)[2], 1e-5);
        EXPECT_NEAR(1.0, psos.PSOS_data.at(0)[3], 1e-5);
        EXPECT_NEAR(1.0 - 0.2 / 3, psos.PSOS_data.at(0)[4], 1e-5);
    }

    void handle_crossing5() {  // Straight line PSOS, with forces
        particleset particles;
        Particle p;
        p.d_r = {-0.1, 0.1};
        p.d_v = {-2.0, 1.0};
        p.d_F = {-1.0, 1.0};
        particles.emplace_back(std::move(p));

        auto S = [](const particleset& pset) {
            return -pset.at(0).d_r[0] + pset.at(0).d_r[1];  // S = y-x
        };

        auto Sgrad = [](const std::vector<double>&) {
            std::vector<double> grad(4, 0.0);
            grad.at(0) = -1.0;
            grad.at(1) = 1.0;
            return grad;
        };

        auto ph_to_psos = [](const std::vector<double>& Z) {
            std::vector<double> ret = {Z.at(0), Z.at(1), Z.at(2), Z.at(3), Z.at(4)};
            return ret;
        };

        CustomPSOS psos(S, Sgrad, ph_to_psos);
        psos.PSOS_value = -1e-9;

        psos.evaluate(particles, 0.0);

        // Exact results with Mathematica

        double tc = -0.06821789367236468455603987689662579662032;

        // Check for the data

        EXPECT_NEAR(0.0341089468361823422780199384483128983102, psos.PSOS_data.at(0)[0], 1e-5);
        EXPECT_NEAR(0.03410894683618234227801993844831289831016, psos.PSOS_data.at(0)[1], 1e-5);
        EXPECT_NEAR(-1.93178210632763531544396012310337420337968, psos.PSOS_data.at(0)[2], 1e-5);
        EXPECT_NEAR(0.93178210632763531544396012310337420337968, psos.PSOS_data.at(0)[3], 1e-5);
        EXPECT_NEAR(tc, psos.PSOS_data.at(0)[4], 1e-5);
    }

    void handle_crossing6() {  // y = sqrt(x) PSOS:
        particleset particles;
        Particle p;
        p.d_r = {-0.05, 0.05};
        p.d_v = {-2.0, 1.0};
        p.d_F = {-1.0, 1.0};
        particles.emplace_back(std::move(p));

        auto S = [](const particleset& pset) {
            return -pset.at(0).d_r[0] + pow(pset.at(0).d_r[1], 2.0);  // S = -x+y^2
        };

        auto Sgrad = [](const std::vector<double>& X) {
            std::vector<double> grad(X.size(), 0.0);
            grad.at(0) = -1.0;
            grad.at(1) = 2 * X[1];
            return grad;
        };

        auto ph_to_psos = [](const std::vector<double>& Z) {
            std::vector<double> ret = {Z.at(0), Z.at(1), Z.at(2), Z.at(3), Z.at(4)};
            return ret;
        };

        CustomPSOS psos(S, Sgrad, ph_to_psos);
        psos.PSOS_value = -1e-9;

        psos.evaluate(particles, 0.0);

        // Exact results with Mathematica

        double tc = -0.0254710379205955394978020363367;

        // Check for the data

        EXPECT_NEAR(0.0006176889548148678, psos.PSOS_data.at(0)[0], 1e-3);
        EXPECT_NEAR(0.02485334896578067, psos.PSOS_data.at(0)[1], 1e-3);
        EXPECT_NEAR(-1.974528962079404, psos.PSOS_data.at(0)[2], 1e-3);
        EXPECT_NEAR(0.97452896207940446050219796, psos.PSOS_data.at(0)[3], 1e-3);
        EXPECT_NEAR(tc, psos.PSOS_data.at(0)[4], 1e-3);
    }
    void linePSOS_to_position_test() {
        triplet<double> line{-1, 2, 0};
        Vec2d r = CustomPSOS::linePSOS_to_position(3, line);

        EXPECT_NEAR(2.683281572999748, r[0], 1e-6);
        EXPECT_NEAR(1.34164078649987, r[1], 1e-6);

        line = {1, 2, 0};
        r = CustomPSOS::linePSOS_to_position(3, line);

        EXPECT_NEAR(2.683281572999748, r[0], 1e-6);
        EXPECT_NEAR(-1.34164078649987, r[1], 1e-6);

        line = {-1, 2, 0};
        r = CustomPSOS::linePSOS_to_position(-3, line);

        EXPECT_NEAR(-2.683281572999748, r[0], 1e-6);
        EXPECT_NEAR(-1.34164078649987, r[1], 1e-6);

        line = {1, 2, 0};
        r = CustomPSOS::linePSOS_to_position(-3, line);

        EXPECT_NEAR(-2.683281572999748, r[0], 1e-6);
        EXPECT_NEAR(1.34164078649987, r[1], 1e-6);

        line = {1, 0, 3};
        r = CustomPSOS::linePSOS_to_position(2.0, line);
        EXPECT_NEAR(-3.0, r[0], 1e-6);
        EXPECT_NEAR(2.0, r[1], 1e-6);

        line = {1, 0, 3};
        r = CustomPSOS::linePSOS_to_position(-2.0, line);
        EXPECT_NEAR(-3.0, r[0], 1e-6);
        EXPECT_NEAR(-2.0, r[1], 1e-6);

        line = {0, 1, 3};
        r = CustomPSOS::linePSOS_to_position(2.0, line);
        EXPECT_NEAR(2.0, r[0], 1e-6);
        EXPECT_NEAR(-3.0, r[1], 1e-6);

        line = {0, 1, 3};
        r = CustomPSOS::linePSOS_to_position(-32.0, line);
        EXPECT_NEAR(-32.0, r[0], 1e-6);
        EXPECT_NEAR(-3.0, r[1], 1e-6);
    }

    void linePSOS_to_velocity_test() {
        triplet<double> line{-1, 1, 0};
        Vec2d v = CustomPSOS::linePSOS_to_velocity(0, line, sqrt(2));
        EXPECT_NEAR(1, v[0], 1e-6);
        EXPECT_NEAR(1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(M_PI / 2.0, line, sqrt(2));
        EXPECT_NEAR(-1, v[0], 1e-6);
        EXPECT_NEAR(1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(M_PI, line, sqrt(2));
        EXPECT_NEAR(-1, v[0], 1e-6);
        EXPECT_NEAR(-1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(-M_PI, line, sqrt(2));
        EXPECT_NEAR(-1, v[0], 1e-6);
        EXPECT_NEAR(-1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(-M_PI / 2.0, line, sqrt(2));
        EXPECT_NEAR(1, v[0], 1e-6);
        EXPECT_NEAR(-1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(-M_PI / 4.0, line, sqrt(2));
        EXPECT_NEAR(sqrt(2), v[0], 1e-6);
        EXPECT_NEAR(0, v[1], 1e-6);

        line = {1, 1, 1};
        v = CustomPSOS::linePSOS_to_velocity(0, line, sqrt(2));
        EXPECT_NEAR(1, v[0], 1e-6);
        EXPECT_NEAR(-1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(M_PI / 2.0, line, sqrt(2));
        EXPECT_NEAR(1, v[0], 1e-6);
        EXPECT_NEAR(1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(M_PI, line, sqrt(2));
        EXPECT_NEAR(-1, v[0], 1e-6);
        EXPECT_NEAR(1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(-M_PI, line, sqrt(2));
        EXPECT_NEAR(-1, v[0], 1e-6);
        EXPECT_NEAR(1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(-M_PI / 2.0, line, sqrt(2));
        EXPECT_NEAR(-1, v[0], 1e-6);
        EXPECT_NEAR(-1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(-M_PI / 4.0, line, sqrt(2));
        EXPECT_NEAR(0, v[0], 1e-6);
        EXPECT_NEAR(-sqrt(2), v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(M_PI + M_PI / 2.0, line, sqrt(2));
        EXPECT_NEAR(-1, v[0], 1e-6);
        EXPECT_NEAR(-1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(-M_PI - M_PI / 2.0, line, sqrt(2));
        EXPECT_NEAR(1, v[0], 1e-6);
        EXPECT_NEAR(1, v[1], 1e-6);

        line = {0, 1, 0};
        v = CustomPSOS::linePSOS_to_velocity(0, line, sqrt(2));
        EXPECT_NEAR(sqrt(2), v[0], 1e-6);
        EXPECT_NEAR(0, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(M_PI / 4.0, line, sqrt(2));
        EXPECT_NEAR(1, v[0], 1e-6);
        EXPECT_NEAR(1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(-M_PI / 4.0, line, sqrt(2));
        EXPECT_NEAR(1, v[0], 1e-6);
        EXPECT_NEAR(-1, v[1], 1e-6);

        line = {1, 0, 0};
        v = CustomPSOS::linePSOS_to_velocity(0, line, sqrt(2));
        EXPECT_NEAR(0, v[0], 1e-6);
        EXPECT_NEAR(sqrt(2), v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(M_PI / 4.0, line, sqrt(2));
        EXPECT_NEAR(-1, v[0], 1e-6);
        EXPECT_NEAR(1, v[1], 1e-6);

        v = CustomPSOS::linePSOS_to_velocity(-M_PI / 4.0, line, sqrt(2));
        EXPECT_NEAR(1, v[0], 1e-6);
        EXPECT_NEAR(1, v[1], 1e-6);
    }
};

TEST_F(custom_psos_tests, particleset_to_vectors) { particleset_to_vectors(); }

TEST_F(custom_psos_tests, handle_crossing1) { handle_crossing1(); }

TEST_F(custom_psos_tests, handle_crossing2) { handle_crossing2(); }

TEST_F(custom_psos_tests, handle_crossing3) { handle_crossing3(); }

TEST_F(custom_psos_tests, handle_crossing4) { handle_crossing4(); }

TEST_F(custom_psos_tests, handle_crossing5) { handle_crossing5(); }

TEST_F(custom_psos_tests, handle_crossing6) { handle_crossing6(); }

TEST_F(custom_psos_tests, linePSOS_to_position) { linePSOS_to_position_test(); }

TEST_F(custom_psos_tests, linePSOS_to_velocity) { linePSOS_to_velocity_test(); }
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
