/*
 * bill2d_system_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "billiard.hpp"
#include "billiard_factory.hpp"
#include "interactions.hpp"
#include "parameterlist.hpp"
#include "potentials.hpp"
#include "table.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"
#include <type_traits>
#include <typeinfo>
namespace bill2d {

class openbilliardfactory_tests : public ::testing::Test {
protected:
    void constructor_no_potentials() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_EQ(nullptr, system.d_potential.get());
    }

    void constructor_only_harmonic_potential() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.harmonic_potential = true;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const HarmonicPotential*>(system.d_potential.get()));
    }

    void constructor_only_soft_stadium_potential() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.soft_stadium_potential = true;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const SoftStadium*>(system.d_potential.get()));
    }

    void constructor_only_gaussian_bath() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.gaussian_bath = true;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const GaussianBath*>(system.d_potential.get()));
    }

    void constructor_only_soft_lorentz_gas() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.soft_lorentz_gas = true;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const SoftLorentzGas*>(system.d_potential.get()));
    }

    void constructor_two_potentials() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.soft_lorentz_gas = true;
        plist.harmonic_potential = true;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Potential_cont*>(system.d_potential.get()));
    }

    void constructor_table_rectangle_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::RECTANGLE;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const OpenRectangle*>(system.d_table.get()));
    }

    void constructor_table_notable_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::NOTABLE;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_notable_periodic_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::NOTABLE;
        plist.periodic = true;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_triangle_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::TRIANGLE;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_circle_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::CIRCLE;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const OpenCircle*>(system.d_table.get()));
    }

    void constructor_table_ellipse_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::ELLIPSE;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_sinai_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::SINAI;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const OpenSinai*>(system.d_table.get()));
    }

    void constructor_table_ring_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::RING;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_stadium_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::STADIUM;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const OpenStadium*>(system.d_table.get()));
    }

    void constructor_table_mushroom_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::MUSHROOM;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const OpenMushroom*>(system.d_table.get()));
    }

    void constructor_table_gate_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::GATE;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_lorentzgas_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::LORENTZGAS;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_rectangle() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::RECTANGLE;
        plist.default_geometry = false;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const OpenRectangle*>(system.d_table.get()));
    }

    void constructor_table_notable() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.default_geometry = false;
        plist.tabletype = ParameterList::TableType::NOTABLE;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_notable_periodic() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.default_geometry = false;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::NOTABLE;
        plist.periodic = true;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_triangle() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.default_geometry = false;
        plist.tabletype = ParameterList::TableType::TRIANGLE;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_circle() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.default_geometry = false;
        plist.tabletype = ParameterList::TableType::CIRCLE;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const OpenCircle*>(system.d_table.get()));
    }

    void constructor_table_ellipse() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.default_geometry = false;
        plist.tabletype = ParameterList::TableType::ELLIPSE;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_sinai() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::SINAI;
        plist.default_geometry = false;
        plist.geometry = pair<double>(1, 0.1);
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const OpenSinai*>(system.d_table.get()));
    }

    void constructor_table_ring() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::RING;
        plist.default_geometry = false;
        plist.geometry = pair<double>(0.1, 1);
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_stadium() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::STADIUM;
        plist.default_geometry = false;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const OpenStadium*>(system.d_table.get()));
    }

    void constructor_table_mushroom() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::MUSHROOM;
        plist.default_geometry = false;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const OpenMushroom*>(system.d_table.get()));
    }

    void constructor_table_gate() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::GATE;
        plist.default_geometry = false;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_table_lorentzgas() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.default_geometry = false;
        plist.geometry = pair<double>(0.1, 0.1);
        plist.tabletype = ParameterList::TableType::LORENTZGAS;
        OpenBilliardFactory system(plist);
        EXPECT_DEATH(system.create_Billiard(), "");
    }

    void constructor_coulomb_interaction() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::COULOMB;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const CoulombInteraction*>(system.d_interaction.get()));
    }

    void constructor_no_interaction() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::NONE;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_EQ(nullptr, system.d_interaction.get());
    }
    void constructor_billiard() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::NONE;
        plist.billtype = ParameterList::BilliardType::BILLIARD;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        Billiard* bill = system.d_bill.release();
        EXPECT_FALSE(dynamic_cast<MagneticBilliard*>(bill));
        EXPECT_FALSE(dynamic_cast<InteractingMagneticBilliard*>(bill));
        EXPECT_FALSE(dynamic_cast<InteractingBilliard*>(bill));
        delete bill;
    }

    void constructor_magneticbilliard() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::NONE;
        plist.billtype = ParameterList::BilliardType::MAGNETICBILLIARD;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<MagneticBilliard*>(system.d_bill.release()));
    }

    void constructor_interactingbilliard() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::COULOMB;
        plist.billtype = ParameterList::BilliardType::INTERACTINGBILLIARD;
        plist.propagator = ParameterList::PropagatorType::SECOND;

        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<InteractingBilliard*>(system.d_bill.release()));
    }

    void constructor_interactingmagneticbilliard() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::COULOMB;
        plist.billtype = ParameterList::BilliardType::INTERACTINGMAGNETICBILLIARD;
        plist.propagator = ParameterList::PropagatorType::SECOND;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        EXPECT_TRUE(dynamic_cast<InteractingMagneticBilliard*>(system.d_bill.release()));
    }

    void constructor_holeratio_should_result_in_nondefault_table_configuration() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.holeratio = 0.001;
        OpenBilliardFactory system(plist);
        system.create_Billiard();
        auto shape_name = system.d_table->shape_name();
        std::cout << shape_name << std::endl;
        EXPECT_EQ(std::string("openrectangle(1,1,0.004)"), shape_name);
    }
};

TEST_F(openbilliardfactory_tests, constructor_no_potentials) { constructor_no_potentials(); }

TEST_F(openbilliardfactory_tests, constructor_only_harmonic_potential) { constructor_only_harmonic_potential(); }

TEST_F(openbilliardfactory_tests, constructor_only_soft_stadium_potential) {
    constructor_only_soft_stadium_potential();
}

TEST_F(openbilliardfactory_tests, constructor_only_gaussian_bath) { constructor_only_gaussian_bath(); }

TEST_F(openbilliardfactory_tests, constructor_only_soft_lorentz_gas) { constructor_only_soft_lorentz_gas(); }

TEST_F(openbilliardfactory_tests, constructor_two_potentials) { constructor_two_potentials(); }

TEST_F(openbilliardfactory_tests, constructor_table_rectangle_default) { constructor_table_rectangle_default(); }

TEST_F(openbilliardfactory_tests, constructor_table_notable_default) { constructor_table_notable_default(); }

TEST_F(openbilliardfactory_tests, constructor_table_notable_periodic_default) {
    constructor_table_notable_periodic_default();
}

TEST_F(openbilliardfactory_tests, constructor_table_triangle_default) { constructor_table_triangle_default(); }

TEST_F(openbilliardfactory_tests, constructor_table_circle_default) { constructor_table_circle_default(); }

TEST_F(openbilliardfactory_tests, constructor_table_ellipse_default) { constructor_table_ellipse_default(); }

TEST_F(openbilliardfactory_tests, constructor_table_sinai_default) { constructor_table_sinai_default(); }

TEST_F(openbilliardfactory_tests, constructor_table_ring_default) { constructor_table_ring_default(); }

TEST_F(openbilliardfactory_tests, constructor_table_mushroom_default) { constructor_table_mushroom_default(); }

TEST_F(openbilliardfactory_tests, constructor_table_lorentzgas_default) { constructor_table_lorentzgas_default(); }

TEST_F(openbilliardfactory_tests, constructor_table_gate_default) { constructor_table_gate_default(); }

TEST_F(openbilliardfactory_tests, constructor_table_rectangle) { constructor_table_rectangle(); }

TEST_F(openbilliardfactory_tests, constructor_table_notable) { constructor_table_notable(); }

TEST_F(openbilliardfactory_tests, constructor_table_notable_periodic) { constructor_table_notable_periodic(); }

TEST_F(openbilliardfactory_tests, constructor_table_triangle) { constructor_table_triangle(); }

TEST_F(openbilliardfactory_tests, constructor_table_circle) { constructor_table_circle(); }

TEST_F(openbilliardfactory_tests, constructor_table_ellipse) { constructor_table_ellipse(); }

TEST_F(openbilliardfactory_tests, constructor_table_sinai) { constructor_table_sinai(); }

TEST_F(openbilliardfactory_tests, constructor_table_ring) { constructor_table_ring(); }

TEST_F(openbilliardfactory_tests, constructor_table_mushroom) { constructor_table_mushroom(); }

TEST_F(openbilliardfactory_tests, constructor_table_lorentzgas) { constructor_table_lorentzgas(); }

TEST_F(openbilliardfactory_tests, constructor_table_gate) { constructor_table_gate_default(); }

TEST_F(openbilliardfactory_tests, constructor_no_interaction) { constructor_no_interaction(); }

TEST_F(openbilliardfactory_tests, constructor_coulomb_interaction) { constructor_coulomb_interaction(); }

TEST_F(openbilliardfactory_tests, constructor_billiard) { constructor_billiard(); }

TEST_F(openbilliardfactory_tests, constructor_magneticbilliard) { constructor_magneticbilliard(); }

TEST_F(openbilliardfactory_tests, constructor_interactingmagneticbilliard) {
    constructor_interactingmagneticbilliard();
}

TEST_F(openbilliardfactory_tests, constructor_interactingbilliard) { constructor_interactingbilliard(); }

TEST_F(openbilliardfactory_tests, constructor_holeratio_should_result_in_nondefault_table_configuration) {
    constructor_holeratio_should_result_in_nondefault_table_configuration();
}

}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    testing::FLAGS_gtest_death_test_style = "threadsafe";
    return RUN_ALL_TESTS();
}
