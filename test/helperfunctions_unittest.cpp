/*
 * helperfunctions_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bill2d_config.h"
#include "exceptions.hpp"
#include "helperfunctions.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"
#include <cmath>
#include <string>
#include <vector>

#define EXPECT_FEQ5(A, B) EXPECT_NEAR(A, B, 1e-5)

#define EXPECT_FEQ(A, B) EXPECT_NEAR(A, B, 1e-10)

#define EXPECT_FNE(A, B) EXPECT_TRUE(fabsl((A) - (B)) > 1e-10)

std::string test_datapath;

namespace bill2d {

TEST(FileExists, file_exists) {
    EXPECT_TRUE(FileExists(test_datapath + "testvector.txt"));  // env variable
}

TEST(FileExists, file_does_not_exist) { EXPECT_FALSE(FileExists("me_really_tired.cpp")); }

TEST(FileExists, directory_exists) { EXPECT_TRUE(FileExists(test_datapath + "helperfunctions_unittest.cpp")); }

TEST(file2vector2d, file_doesnt_exist) {
    std::vector<std::vector<double>> tmp;
    EXPECT_THROW(file2vector2d("my_nonexistent_file.txt", tmp), FileDoesNotExist);  // env variable
}

TEST(file2vector2d, read) {
    std::vector<std::vector<double>> vec;
    file2vector2d(test_datapath + "testvector.txt", vec);
    std::vector<double> tmp1 = {1, 2, -3, 2.2};
    std::vector<double> tmp2 = {1, 0, 3};
    std::vector<double> tmp3 = {-2};

    EXPECT_EQ(tmp1, vec[0]);
    EXPECT_EQ(tmp2, vec[1]);
    EXPECT_EQ(tmp3, vec[2]);
}

TEST(vector2d2file, readtest) {
    std::vector<std::vector<double>> myvec;
    myvec.push_back({1, 3.5, 2});
    myvec.push_back({-4.5, 2.3, 1.2, -553.4});
    vector2d2file("testfile.txt", myvec);

    std::vector<std::vector<double>> readvec;
    file2vector2d("testfile.txt", readvec);

    EXPECT_EQ(myvec, readvec);

    rm_file("testfile.txt");
}

TEST(vector2file, readtest) {
    std::vector<double> myvec = {1, 5, 7, 3};

    vector2file("testfile.txt", myvec);

    std::vector<std::vector<double>> readvec;
    file2vector2d("testfile.txt", readvec);

    EXPECT_EQ(myvec, readvec.at(0));

    rm_file("testfile.txt");
}

TEST(hdf52vector2d_inpos, read_initial) {
    std::vector<std::vector<double>> myvector;
    hdf52vector2d_inpos(test_datapath + "testfile_helper.h5", 2, myvector, Position::FIRST);
    const std::vector<std::vector<double>> refvec = {
        {0.19106912, 0.26013204, -1.11931263, 1.32179394}, {0.76591192, 0.046688, -1.73198217, -0.01541954}
    };
    for (unsigned i = 0; i < 2; i++) {
        for (unsigned j = 0; j < 4; j++) {
            EXPECT_FEQ5(refvec.at(i).at(j), myvector.at(i).at(j));
        }
    }
}

TEST(hdf52vector2d_inpos, read_final) {
    std::vector<std::vector<double>> myvector;
    hdf52vector2d_inpos(test_datapath + "testfile_helper.h5", 2, myvector, Position::LAST);
    const std::vector<std::vector<double>> refvec = {
        {-0.92768385, 1.58126509, -1.11931263, 1.32179394}, {-0.96520426, 0.03127617, -1.73198217, -0.01541954}
    };
    for (unsigned i = 0; i < 2; i++) {
        for (unsigned j = 0; j < 4; j++) {
            EXPECT_FEQ5(refvec.at(i).at(j), myvector.at(i).at(j));
        }
    }
}

TEST(round_up, predefined_value_test) {
    EXPECT_EQ(24500, round_up(24500, 50));
    EXPECT_EQ(24862, round_up(24500, 401));
    EXPECT_EQ(1537, round_up(1513, 53));
    EXPECT_EQ(53, round_up(23, 53));
}
}  // namespace bill2d

int main(int argc, char** argv) {
    test_datapath = DATADIR;

    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
