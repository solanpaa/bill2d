/*
 * billiard.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "parser.hpp"
#include "gtest/gtest.h"
#include <functional>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

namespace bill2d {

TEST(CommandLineParser, test1) {
    int argc = 40;
    string cmd =
        "empty -s --billtype 1 --table 2 -N 3 -B 5 -E 4.7564 --geometry 3 5 --centerpoint 1.4 5.4 "
        "--ignore_E --bins 123 --delta_t 0.1 --min_t 25 --max_t 50 --sparsesave 1 --spawnarea 0 3 1 2 "
        "--save_trajectory --save_velocity --savepath data/home.h5 --output 1";
    vector<string> vect;
    split(cmd, ' ', vect);

    vector<char*> vectchar;
    for (vector<string>::iterator it = vect.begin(); it < vect.end(); it++) {
        vectchar.push_back(const_cast<char*>((it->c_str())));
    }
    char** argv = new char*[argc];
    for (unsigned i = 0; i < vectchar.size(); i++) {
        argv[i] = (vectchar.at(i));
    }
    Parser parser(argc, argv);
    parser.parse();

    ParameterList params = parser.getParameterList();
    EXPECT_EQ(SINGLE, params.simulationtype);
    EXPECT_EQ(BILLIARD, params.billtype);
    EXPECT_EQ(TRIANGLE, params.tabletype);
    EXPECT_EQ(3, params.num_particles);
    EXPECT_EQ(0, params.B);  // 0, since billtype doesn't include M.FIELD!
    EXPECT_EQ(4.7564, params.energy);

    EXPECT_EQ(3, params.geometry.x);
    EXPECT_EQ(5, params.geometry.y);
    EXPECT_EQ(1.4, params.centerpoint.x);
    EXPECT_EQ(5.4, params.centerpoint.y);

    EXPECT_TRUE(params.ignore_energy);
    EXPECT_EQ(123, params.bins);

    EXPECT_EQ(0.1, params.delta_t);
    EXPECT_EQ(25, params.min_t);
    EXPECT_EQ(50, params.max_t);
    EXPECT_EQ(1, params.spsave);

    EXPECT_EQ(0, params.spawnarea.lleft.x);
    EXPECT_EQ(3, params.spawnarea.lleft.y);
    EXPECT_EQ(1, params.spawnarea.uright.x);
    EXPECT_EQ(2, params.spawnarea.uright.y);

    EXPECT_TRUE(params.save_full_trajectory);
    EXPECT_TRUE(params.save_velocity);

    EXPECT_EQ("data/home.h5", params.savefilepath);
    EXPECT_EQ(1, params.output_level);
    EXPECT_EQ(NONE, params.interactiontype);
    EXPECT_EQ(0, params.interaction_strength);
    EXPECT_EQ(0, params.potential_strength);
}

TEST(CommandLineParser, test2) {
    int argc = 8;
    string cmd = "empty -N 4 --bounce_map --billtype 2 -B 4";
    vector<string> vect;
    split(cmd, ' ', vect);

    vector<char*> vectchar;
    for (vector<string>::iterator it = vect.begin(); it < vect.end(); it++) {
        vectchar.push_back(const_cast<char*>((it->c_str())));
    }
    char** argv = new char*[argc];
    for (unsigned i = 0; i < vectchar.size(); i++) {
        argv[i] = (vectchar.at(i));
    }

    Parser parser(argc, argv);
    parser.parse();

    ParameterList params = parser.getParameterList();
    EXPECT_EQ(4, params.num_particles);
    EXPECT_TRUE(params.save_bouncemap);
    EXPECT_EQ(MAGNETICBILLIARD, params.billtype);
    EXPECT_EQ(4, params.B);
    EXPECT_EQ(NONE, params.interactiontype);
    EXPECT_EQ(0, params.interaction_strength);
    EXPECT_EQ(0, params.potential_strength);
}

TEST(CommandLineParser, test3) {
    int argc = 9;
    string cmd = "empty --bounce_map --billtype 3 -B 4 --inter_str 1.1 --harmonic_potential";
    vector<string> vect;
    split(cmd, ' ', vect);

    vector<char*> vectchar;
    for (vector<string>::iterator it = vect.begin(); it < vect.end(); it++) {
        vectchar.push_back(const_cast<char*>((it->c_str())));
    }
    char** argv = new char*[argc];
    for (unsigned i = 0; i < vectchar.size(); i++) {
        argv[i] = (vectchar.at(i));
    }

    Parser parser(argc, argv);
    parser.parse();

    ParameterList params = parser.getParameterList();
    EXPECT_TRUE(params.save_bouncemap);
    EXPECT_EQ(INTERACTINGBILLIARD, params.billtype);
    EXPECT_EQ(0, params.B);
    EXPECT_EQ(COULOMB, params.interactiontype);
    EXPECT_EQ(1.1, params.interaction_strength);
    EXPECT_TRUE(params.harmonic_potential);
    EXPECT_EQ(1, params.potential_strength);
}

TEST(CommandLineParser, test4) {
    int argc = 11;
    string cmd = "empty --bounce_map --billtype 3 -B 4 --inter_str 1.1 --harmonic_potential --potential_strength 1.1";
    vector<string> vect;
    split(cmd, ' ', vect);

    vector<char*> vectchar;
    for (vector<string>::iterator it = vect.begin(); it < vect.end(); it++) {
        vectchar.push_back(const_cast<char*>((it->c_str())));
    }
    char** argv = new char*[argc];
    for (unsigned i = 0; i < vectchar.size(); i++) {
        argv[i] = (vectchar.at(i));
    }

    Parser parser(argc, argv);
    parser.parse();

    ParameterList params = parser.getParameterList();
    EXPECT_TRUE(params.save_bouncemap);
    EXPECT_EQ(INTERACTINGBILLIARD, params.billtype);
    EXPECT_EQ(0, params.B);
    EXPECT_EQ(COULOMB, params.interactiontype);
    EXPECT_EQ(1.1, params.interaction_strength);
    EXPECT_TRUE(params.harmonic_potential);
    EXPECT_EQ(1.1, params.potential_strength);
}

TEST(CommandLineParser, test5) {
    int argc = 8;
    string cmd = "empty --bounce_map --billtype 4 -B 4 --inter_str 1";
    vector<string> vect;
    split(cmd, ' ', vect);

    vector<char*> vectchar;
    for (vector<string>::iterator it = vect.begin(); it < vect.end(); it++) {
        vectchar.push_back(const_cast<char*>((it->c_str())));
    }
    char** argv = new char*[argc];
    for (unsigned i = 0; i < vectchar.size(); i++) {
        argv[i] = (vectchar.at(i));
    }

    Parser parser(argc, argv);
    parser.parse();

    ParameterList params = parser.getParameterList();
    EXPECT_TRUE(params.save_bouncemap);
    EXPECT_EQ(INTERACTINGMAGNETICBILLIARD, params.billtype);
    EXPECT_EQ(4, params.B);
    EXPECT_EQ(COULOMB, params.interactiontype);
    EXPECT_EQ(1, params.interaction_strength);
    EXPECT_EQ(0, params.potential_strength);
}

TEST(CommandLineParser, test6) {
    int argc = 8;
    string cmd = "empty --bounce_map --billtype 2 -B 4 --inter_str 1";
    vector<string> vect;
    split(cmd, ' ', vect);

    vector<char*> vectchar;
    for (vector<string>::iterator it = vect.begin(); it < vect.end(); it++) {
        vectchar.push_back(const_cast<char*>((it->c_str())));
    }
    char** argv = new char*[argc];
    for (unsigned i = 0; i < vectchar.size(); i++) {
        argv[i] = (vectchar.at(i));
    }

    Parser parser(argc, argv);
    parser.parse();

    ParameterList params = parser.getParameterList();
    EXPECT_TRUE(params.save_bouncemap);
    EXPECT_EQ(MAGNETICBILLIARD, params.billtype);
    EXPECT_EQ(4, params.B);
    EXPECT_EQ(NONE, params.interactiontype);
    EXPECT_EQ(0, params.interaction_strength);
    EXPECT_EQ(0, params.potential_strength);
}

TEST(ConfigFileParser, test1) {
    int argc = 3;
    string cmd = "empty --config test/test.config";
    vector<string> vect;
    split(cmd, ' ', vect);

    vector<char*> vectchar;
    for (vector<string>::iterator it = vect.begin(); it < vect.end(); it++) {
        vectchar.push_back(const_cast<char*>((it->c_str())));
    }
    char** argv = new char*[argc];
    for (unsigned i = 0; i < vectchar.size(); i++) {
        argv[i] = (vectchar.at(i));
    }

    Parser parser(argc, argv);
    parser.parse();

    ParameterList params = parser.getParameterList();
    EXPECT_EQ(SINGLE, params.simulationtype);
    EXPECT_EQ(INTERACTINGMAGNETICBILLIARD, params.billtype);
    EXPECT_EQ(2, params.B);
    EXPECT_EQ(1, params.geometry[0]);
    EXPECT_EQ(2, params.geometry[1]);
    EXPECT_EQ(COULOMB, params.interactiontype);
    EXPECT_EQ(1, params.interaction_strength);
    EXPECT_EQ(0, params.potential_strength);
    EXPECT_TRUE(params.save_full_trajectory);
    EXPECT_TRUE(params.save_velocity);
    EXPECT_EQ(-5.2, params.spawnarea.lleft.x);
    EXPECT_EQ(3.3, params.spawnarea.lleft.y);
    EXPECT_EQ(11.2, params.spawnarea.uright.x);
    EXPECT_EQ(1.2, params.spawnarea.uright.y);

    EXPECT_EQ(-5.2, params.spawnarea[0]);
    EXPECT_EQ(3.3, params.spawnarea[1]);
    EXPECT_EQ(11.2, params.spawnarea[2]);
    EXPECT_EQ(1.2, params.spawnarea[3]);
    EXPECT_TRUE(params.save_particlenum);
    EXPECT_FALSE(params.save_energies);
    EXPECT_FALSE(params.save_histograms);
}
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
