/*
 * table_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "table.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"

#include "bill2d_config.h"
#include "billiard.hpp"
#include "datafile.hpp"
#include "interactions.hpp"
#include "vec2d.hpp"
#include <memory>
#include <string>
#include <vector>
using ::testing::_;
using ::testing::AtLeast;
using ::testing::Return;
namespace bill2d {

class billiard_tests : public ::testing::Test {
protected:
    void test_billiard_contructor() {
        class MockTable : public Table {
        public:
            MOCK_CONST_METHOD1(point_is_inside, bool(const Vec2d&));
            MOCK_CONST_METHOD7(
                handle_collisions,
                bool(Vec2d&, Vec2d&, pair<int>&, std::vector<std::vector<double>>*, double, unsigned&, bool&)
            );
            MOCK_CONST_METHOD0(shape_name, const std::string());
        };
        MockTable* table = new MockTable();

        // Table methods shouldn't be called from the constructor of Billiard-classes
        EXPECT_CALL(*table, handle_collisions(_, _, _, _, _, _, _)).Times(0);
        EXPECT_CALL(*table, point_is_inside(_)).Times(0);
        EXPECT_CALL(*table, shape_name()).Times(0);

        ParameterList plist;
        plist.num_particles = 5;

        std::unique_ptr<RNG> rng = std::make_unique<RNG>();
        std::unique_ptr<Datafile> file;

        Billiard bill(std::unique_ptr<const Table>(table), std::move(rng), plist, std::move(file));

        EXPECT_EQ(5, bill.d_particles.size());  // Checking that all the particles

        // were created (but not yet initialized)
    }

    void test_billiard_prepare() {
        class MockTable : public Table {
        public:
            MOCK_CONST_METHOD7(
                handle_collisions,
                bool(Vec2d&, Vec2d&, pair<int>&, std::vector<std::vector<double>>*, double, unsigned&, bool&)
            );
            MOCK_CONST_METHOD1(point_is_inside, bool(const Vec2d&));
            MOCK_CONST_METHOD0(shape_name, const std::string());
            virtual ~MockTable() = default;
        };

        std::unique_ptr<MockTable> table = std::make_unique<MockTable>();

        // Table methods shouldn't be called from the constructor of Billiard-classes
        EXPECT_CALL(*table, handle_collisions(_, _, _, _, _, _, _)).Times(0);
        EXPECT_CALL(*table, point_is_inside(_)).Times(17).WillOnce(Return(false)).WillRepeatedly(Return(true));
        EXPECT_CALL(*table, shape_name()).Times(0);

        ParameterList plist;
        plist.num_particles = 16;
        plist.save_nothing = true;
        std::unique_ptr<RNG> rng = std::make_unique<RNG>();
        std::unique_ptr<Datafile> file;

        Billiard bill(std::move(table), std::move(rng), plist, std::move(file));

        bill.prepare(5);
        EXPECT_TRUE(::testing::Mock::VerifyAndClearExpectations(const_cast<Table*>(bill.d_table.get())));
    }

    void test_billiard_propagate() {
        // Let's check that boundary handling is called correctly.
        // Actual propagator methods are tested by comparing to reference data by 'test/propagator_tests.py'

        class MockTable : public Table {
        public:
            MOCK_CONST_METHOD7(
                handle_collisions,
                bool(Vec2d&, Vec2d&, pair<int>&, std::vector<std::vector<double>>*, double, unsigned&, bool&)
            );
            MOCK_CONST_METHOD1(point_is_inside, bool(const Vec2d&));
            MOCK_CONST_METHOD0(shape_name, const std::string());
        };
        MockTable* table = new MockTable();

        // Table methods shouldn't be called from the constructor of Billiard-classes
        EXPECT_CALL(*table, handle_collisions(_, _, _, _, _, _, _)).Times(1);

        EXPECT_CALL(*table, point_is_inside(_))
            .WillOnce(Return(true))   // Prepare
            .WillOnce(Return(true))   // Prepare
            .WillOnce(Return(false))  // First particle not inside
            .WillOnce(Return(true))
            .WillOnce(Return(true));

        EXPECT_CALL(*table, shape_name()).Times(0);
        ParameterList plist;
        plist.num_particles = 2;

        std::unique_ptr<RNG> rng = std::make_unique<RNG>();
        std::unique_ptr<Datafile> file;

        Billiard bill(std::unique_ptr<const Table>(table), std::move(rng), plist, std::move(file));
        bill.prepare(5);
        bill.propagate();
    }

    void test_billiard_remove_particle() {
        class MockTable : public Table {
        public:
            MOCK_CONST_METHOD7(
                handle_collisions,
                bool(Vec2d&, Vec2d&, pair<int>&, std::vector<std::vector<double>>*, double, unsigned&, bool&)
            );
            MOCK_CONST_METHOD1(point_is_inside, bool(const Vec2d&));
            MOCK_CONST_METHOD0(shape_name, const std::string());
        };
        MockTable* table = new MockTable();

        // Table methods shouldn't be called from the constructor of Billiard-classes
        EXPECT_CALL(*table, handle_collisions(_, _, _, _, _, _, _)).Times(0);
        EXPECT_CALL(*table, point_is_inside(_)).Times(AtLeast(3)).WillRepeatedly(Return(true));
        EXPECT_CALL(*table, shape_name()).Times(0);

        ParameterList plist;
        plist.save_bouncemap = false;
        plist.save_full_trajectory = false;
        plist.save_velocity = false;
        plist.save_histograms = false;
        plist.num_particles = 3;

        std::unique_ptr<RNG> rng = std::make_unique<RNG>();
        std::unique_ptr<Datafile> file;

        Billiard bill(std::unique_ptr<const Table>(table), std::move(rng), plist, std::move(file));

        std::vector<std::vector<double>> rv = {{0.1, 0.5, 0.3, 0.3}, {0.2, 0.2, 0.3, 0.3}, {0.21, 0.21, 0.3, 0.3}};

        bill.prepare_manual(rv, 0, true, false, 0);

        // Check that particle IDs are as expected
        unsigned n = 1;
        for (auto& p : bill.d_particles) {
            ASSERT_EQ(n, p.ID);
            n++;
        }

        // The function determining, which particle to remove
        auto lambda = [](Vec2d r, Vec2d) { return (r[1] < 0.205); };

        // Check that some particle was removed
        EXPECT_TRUE(bill.remove_particle(static_cast<bool (*)(Vec2d, Vec2d)>(lambda)));

        // Check that correct particle was removed
        n = 1;
        for (auto& p : bill.d_particles) {
            ASSERT_EQ(n, p.ID);
            n += 2;
        }
    }

    void test_interactingbilliard_remove_particle() {
        class MockTable : public Table {
        public:
            MOCK_CONST_METHOD7(
                handle_collisions,
                bool(Vec2d&, Vec2d&, pair<int>&, std::vector<std::vector<double>>*, double, unsigned&, bool&)
            );
            MOCK_CONST_METHOD1(point_is_inside, bool(const Vec2d&));
            MOCK_CONST_METHOD0(shape_name, const std::string());
        };
        MockTable* table = new MockTable();

        // Table methods shouldn't be called from the constructor of Billiard-classes
        EXPECT_CALL(*table, handle_collisions(_, _, _, _, _, _, _)).Times(0);
        EXPECT_CALL(*table, point_is_inside(_)).Times(AtLeast(3)).WillRepeatedly(Return(true));
        EXPECT_CALL(*table, shape_name()).Times(0);

        ParameterList plist;
        plist.save_bouncemap = false;
        plist.save_full_trajectory = false;
        plist.save_velocity = false;
        plist.save_histograms = false;
        plist.num_particles = 3;

        std::unique_ptr<RNG> rng = std::make_unique<RNG>();
        std::unique_ptr<Datafile> file;
        std::unique_ptr<Interaction> inter = std::make_unique<CoulombInteraction>();
        InteractingBilliard bill(
            std::move(inter),
            std::unique_ptr<Potential>(),
            std::unique_ptr<const Table>(table),
            std::move(rng),
            plist,
            std::move(file)
        );

        std::vector<std::vector<double>> rv = {{0.1, 0.5, 0.3, 0.3}, {0.2, 0.2, 0.3, 0.3}, {0.21, 0.21, 0.3, 0.3}};

        bill.prepare_manual(rv, 0, true, false, 0);

        // Check that particle IDs are as expected
        unsigned n = 1;
        for (auto& p : bill.d_particles) {
            ASSERT_EQ(n, p.ID);
            n++;
        }

        // The function determining, which particle to remove
        auto lambda = [](Vec2d r, Vec2d) {
            if (r[1] < 0.205) return true;
            return false;
        };

        // Check that some particle was removed
        EXPECT_TRUE(bill.remove_particle(static_cast<bool (*)(Vec2d, Vec2d)>(lambda)));

        // Check that correct particle was removed
        n = 1;
        for (auto& p : bill.d_particles) {
            ASSERT_EQ(n, p.ID);
            n += 2;
        }
    }
};

TEST_F(billiard_tests, billiard_contructor) { test_billiard_contructor(); }

TEST_F(billiard_tests, billiard_prepare) { test_billiard_prepare(); }

TEST_F(billiard_tests, billiard_propagate) { test_billiard_propagate(); }

TEST_F(billiard_tests, billiard_remove_particle) { test_billiard_remove_particle(); }

TEST_F(billiard_tests, interactingbilliard_remove_particle) { test_interactingbilliard_remove_particle(); }
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    testing::FLAGS_gtest_death_test_style = "threadsafe";

    return RUN_ALL_TESTS();
}
