/*
 * potentials_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "potentials.hpp"
#include "vec2d.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"
#include <cmath>
#include <vector>

#define EXPECT_FEQ5(A, B) EXPECT_NEAR(A, B, 1e-5)

#define EXPECT_FEQ(A, B) EXPECT_NEAR(A, B, 1e-10)

#define EXPECT_FNE(A, B) EXPECT_TRUE(fabsl((A) - (B)) > 1e-10)

namespace bill2d {

TEST(HarmonicPotential, force) {
    HarmonicPotential pot(1.5, {0.0, 0.0});
    EXPECT_FEQ(1.5 * -1.6, pot.force(Vec2d(1.6, -2.3))[0]);
    EXPECT_FEQ(1.5 * 2.3, pot.force(Vec2d(1.6, -2.3))[1]);
}

TEST(HarmonicPotential, energy) {
    HarmonicPotential pot(1.5, {0.0, 0.0});
    EXPECT_FEQ(0.5 * 1.5 * Vec2d(1.6, -2.3).normsqr(), pot.energy(Vec2d(1.6, -2.3)));
}

TEST(FermiPole, force) {
    FermiPole pot({0.5, 0.5}, 0.6, 0.1);
    Vec2d tmp = pot.force({0.4, 0.3});
    EXPECT_FEQ(
        -0.1 / (2 * sqrt(0.1 * 0.1 + 0.2 * 0.2) * 0.1 * (1 + cosh((0.6 - sqrt(0.1 * 0.1 + 0.2 * 0.2)) / 0.1))), tmp[0]
    );
    EXPECT_FEQ(
        -0.2 / (2 * sqrt(0.1 * 0.1 + 0.2 * 0.2) * 0.1 * (1 + cosh((0.6 - sqrt(0.1 * 0.1 + 0.2 * 0.2)) / 0.1))), tmp[1]
    );
}

TEST(FermiPole, energy) {
    FermiPole pot({0.5, 0.5}, 0.6, 0.1);
    EXPECT_FEQ(1 / (1 + exp(((sqrt(0.1 * 0.1 + 0.2 * 0.2) - 0.6) / 0.1))), pot.energy({0.4, 0.3}));
}

TEST(FermiPole, inverted_force) {
    FermiPole pot({0.5, 0.5}, 0.6, 0.1, true);
    Vec2d tmp = pot.force({0.4, 0.3});
    EXPECT_FEQ(
        0.1 / (2 * sqrt(0.1 * 0.1 + 0.2 * 0.2) * 0.1 * (1 + cosh((0.6 - sqrt(0.1 * 0.1 + 0.2 * 0.2)) / 0.1))), tmp[0]
    );
    EXPECT_FEQ(
        0.2 / (2 * sqrt(0.1 * 0.1 + 0.2 * 0.2) * 0.1 * (1 + cosh((0.6 - sqrt(0.1 * 0.1 + 0.2 * 0.2)) / 0.1))), tmp[1]
    );
}

TEST(FermiPole, inverted_energy) {
    FermiPole pot({0.5, 0.5}, 0.6, 0.1, true);
    EXPECT_FEQ(-1 / (1 + exp(((sqrt(0.1 * 0.1 + 0.2 * 0.2) - 0.6) / 0.1))), pot.energy({0.4, 0.3}));
}

TEST(Potential_cont, force) {
    Potential_cont pot;
    pot.add(new FermiPole({0.5, 0.5}, 0.6, 0.1));
    pot.add(new HarmonicPotential(1.5, {0.0, 0.0}));

    Vec2d tmp = pot.force({0.4, 0.3});
    EXPECT_FEQ(
        -1.5 * 0.4 -
            0.1 / (2 * sqrt(0.1 * 0.1 + 0.2 * 0.2) * 0.1 * (1 + cosh((0.6 - sqrt(0.1 * 0.1 + 0.2 * 0.2)) / 0.1))),
        tmp[0]
    );
    EXPECT_FEQ(
        -1.5 * 0.3 -
            0.2 / (2 * sqrt(0.1 * 0.1 + 0.2 * 0.2) * 0.1 * (1 + cosh((0.6 - sqrt(0.1 * 0.1 + 0.2 * 0.2)) / 0.1))),
        tmp[1]
    );
}

TEST(Potential_cont, energy) {
    Potential_cont pot;
    pot.add(new FermiPole({0.5, 0.5}, 0.6, 0.1));
    pot.add(new HarmonicPotential(1.5, {0.0, 0.0}));

    double tmp = pot.energy({0.4, 0.3});
    EXPECT_FEQ(0.5 * 1.5 * (0.4 * 0.4 + 0.3 * 0.3) + 1. / (1 + exp(((sqrt(0.1 * 0.1 + 0.2 * 0.2) - 0.6) / 0.1))), tmp);
}

TEST(SoftStadium, force) {
    SoftStadium pot(5, 0.5, 0.4);

    Vec2d r(0.2, 0.3);
}

TEST(SoftStadium, energy) {
    SoftStadium pot(5, 0.5, 0.4);
    EXPECT_FEQ(pow(1.5 - sqrt(0.3 * 0.3 + 0.2 * 0.2), -5), pot.energy({0.2, 0.3}));
    EXPECT_FEQ(pow(0.7, -5), pot.energy({0.7, 1.3}));
    EXPECT_FEQ(pow(1.5 - sqrt(0.2 * 0.2 + 0.2 * 0.2), -5), pot.energy({1.1, 0.7}));
}

TEST(SoftLorentzGas, force) {
    SoftLorentzGas pot({M_PI / 4., 1, 1}, 1., 0.1, 4);
    Potential_cont potc;
    int N = 4;
    double dx = 1;
    double dy = 1 / sqrt(2);
    double dhx = 1 / sqrt(2);
    for (int j = -N + 1; j <= N; j++) {
        for (int i = -N + 1; i <= N; i++) {
            potc.add(new FermiPole({i * dx + j * dhx, j * dy}, 1, 0.1));
        }
    }
    EXPECT_FEQ(potc.force({0.3, 0.2})[0], pot.force({0.3, 0.2})[0]);
    EXPECT_FEQ(potc.force({0.3, 0.2})[1], pot.force({0.3, 0.2})[1]);
}

TEST(SoftLorentzGas, energy) {
    SoftLorentzGas pot({M_PI / 4., 1, 1}, 1., 0.1, 4);
    Potential_cont potc;
    int N = 4;
    double dx = 1;
    double dy = 1 / sqrt(2);
    double dhx = 1 / sqrt(2);
    for (int j = -N + 1; j <= N; j++) {
        for (int i = -N + 1; i <= N; i++) {
            potc.add(new FermiPole({i * dx + j * dhx, j * dy}, 1, 0.1));
        }
    }
    EXPECT_FEQ(potc.energy({0.3, 0.2}), pot.energy({0.3, 0.2}));
}

TEST(SoftLorentzGas, inverted_force) {
    SoftLorentzGas pot({M_PI / 4., 1, 1}, 1., 0.1, 4, true);
    Potential_cont potc;
    int N = 4;
    double dx = 1;
    double dy = 1 / sqrt(2);
    double dhx = 1 / sqrt(2);
    for (int j = -N + 1; j <= N; j++) {
        for (int i = -N + 1; i <= N; i++) {
            potc.add(new FermiPole({i * dx + j * dhx, j * dy}, 1, 0.1, true));
        }
    }
    EXPECT_FEQ(potc.force({0.3, 0.2})[0], pot.force({0.3, 0.2})[0]);
    EXPECT_FEQ(potc.force({0.3, 0.2})[1], pot.force({0.3, 0.2})[1]);
}

TEST(SoftLorentzGas, inverted_energy) {
    SoftLorentzGas pot({M_PI / 4., 1, 1}, 1., 0.1, 4, true);
    Potential_cont potc;
    int N = 4;
    double dx = 1;
    double dy = 1 / sqrt(2);
    double dhx = 1 / sqrt(2);
    for (int j = -N + 1; j <= N; j++) {
        for (int i = -N + 1; i <= N; i++) {
            potc.add(new FermiPole({i * dx + j * dhx, j * dy}, 1, 0.1, true));
        }
    }
    // Soft Lorentz gas is supposed to reset the energy minimum to zero
    EXPECT_FEQ(potc.energy({0.3, 0.2}) + 1.0, pot.energy({0.3, 0.2}));
}

TEST(SoftCircle, energy_fermi) {
    SoftCircle pot(ParameterList::SoftCircleType::FERMI_POLE, 0.1, 0.0);
    FermiPole pole(Vec2d(0.0, 0.0), 1.0, 0.1, true);
    Vec2d r(0.3, 0.2);
    EXPECT_FEQ(pole.energy(r) + 1.0, pot.energy(r));
}

TEST(SoftCircle, force_fermi) {
    SoftCircle pot(ParameterList::SoftCircleType::FERMI_POLE, 0.1, 0.0);
    FermiPole pole(Vec2d(0.0, 0.0), 1.0, 0.1, true);
    Vec2d r(0.3, 0.2);
    EXPECT_FEQ(pole.force(r)[0], pot.force(r)[0]);
    EXPECT_FEQ(pole.force(r)[1], pot.force(r)[1]);
}

TEST(SoftCircle, energy_erf) {
    double softness = 0.1;
    double ecc = 0.3;
    SoftCircle pot(ParameterList::SoftCircleType::ERROR_FUNCTION, softness, ecc);
    Vec2d r(0.3, 0.2);
    double energy = (std::erf(softness * (r[0] * r[0] + r[1] * r[1] / (1.0 - ecc * ecc) - 1.0)) + 1.0) / 2.0;
    EXPECT_FEQ(energy, pot.energy(r));
}

TEST(SoftCircle, force_erf) {
    double softness = 0.1;
    double ecc = 0.3;
    SoftCircle pot(ParameterList::SoftCircleType::ERROR_FUNCTION, softness, ecc);
    Vec2d r(0.3, 0.2);

    double inner = r[0] * r[0] + r[1] * r[1] / (1 - ecc * ecc) - 1;
    double f_x = -2.0 * softness * r[0] / std::sqrt(M_PI) * std::exp(-softness * softness * inner * inner);
    double f_y =
        -2.0 * softness * r[1] / ((1 - ecc * ecc) * std::sqrt(M_PI)) * std::exp(-softness * softness * inner * inner);

    EXPECT_FEQ(f_x, pot.force(r)[0]);
    EXPECT_FEQ(f_y, pot.force(r)[1]);
}

TEST(GaussianBath, force) {
    GaussianBath pot(25);
    EXPECT_FNE(pot.force({0.0, 0.0})[0], pot.force({0.0, 0.0})[0]);
    EXPECT_FNE(pot.force({0.0, 0.0})[1], pot.force({0.0, 0.0})[1]);
}

TEST(GaussianBath, energy) {
    GaussianBath pot(25);
    EXPECT_EQ(0, pot.energy({5.0, 6.0}));
}
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
