/*
 * hist2d_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hist2d.hpp"
#include "rng.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"
#include <cmath>
#include <gsl/gsl_histogram.h>
#define EXPECT_FEQ(A, B) EXPECT_NEAR(A, B, 1e-10)

#define EXPECT_FNE(A, B) EXPECT_TRUE(fabsl((A) - (B)) > 1e-10)

namespace bill2d {

TEST(Hist2d, get) {
    Hist2d histogram(10, 11, 0, 10, 2, 4);
    RNG rng;
    for (unsigned i = 0; i < 1000; i++) histogram.add(rng.uniform_rand(-10, 20), rng.uniform_rand(-10, 20));

    for (unsigned i = 0; i < 10; i++) {
        for (unsigned j = 0; j < 11; j++) EXPECT_EQ(histogram.data()[i * 11 + j], histogram.get(i, j));
    }
}

TEST(Hist2d, add) {
    Hist2d histogram(10, 5, 0, 10, 2, 7);

    histogram.add(0.5, 3.4);
    histogram.add(5.6, 6.5);
    histogram.add(5.6, 6.5);
    histogram.add(5.6, 6.6);

    for (unsigned i = 0; i < 10; i++) {
        for (unsigned j = 0; j < 5; j++) {
            if ((i == 0) and (j == 1))
                EXPECT_FEQ(1, histogram.get(i, j));
            else if ((i == 5) and (j == 4))
                EXPECT_FEQ(3, histogram.get(i, j));
            else
                EXPECT_FEQ(0, histogram.get(i, j));
        }
    }
}

TEST(Hist2d, ranges) {
    Hist2d histogram(10, 5, 0, 10, 2, 7);
    for (unsigned i = 0; i < 11; i++) EXPECT_FEQ(i, histogram.xrange()[i]);

    for (unsigned i = 0; i < 6; i++) EXPECT_FEQ(2 + i, histogram.yrange()[i]);
}

TEST(Hist2d, sum) {
    Hist2d histogram(10, 5, 0, 10, 2, 7);
    RNG rng;
    for (unsigned i = 0; i < 1000; i++) histogram.add(rng.uniform_rand(0, 10), rng.uniform_rand(2, 7));

    EXPECT_FEQ(1000, histogram.sum());
}

TEST(Hist2d, set) {
    Hist2d histogram(10, 10, 0, 10, 0, 10);
    histogram.set(5.5, 6.5, 20);
    EXPECT_FEQ(20, histogram.get(5, 6));
}
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
