/*
 * billiardfactory_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "billiard.hpp"
#include "billiard_factory.hpp"
#include "custom_psos.hpp"
#include "interactions.hpp"
#include "parameterlist.hpp"
#include "particle.hpp"
#include "potentials.hpp"
#include "table.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"
#include <memory>
#include <type_traits>
#include <typeinfo>

#define EXPECT_FEQ(A, B) EXPECT_NEAR(A, B, 1e-10)

namespace bill2d {

class billiardfactory_tests : public ::testing::Test {
protected:
    void constructor_no_potentials() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_EQ(nullptr, factory.d_potential.get());
    }

    void constructor_only_harmonic_potential() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.harmonic_potential = true;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const HarmonicPotential*>(factory.d_potential.get()));
    }

    void constructor_only_soft_stadium_potential() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.soft_stadium_potential = true;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const SoftStadium*>(factory.d_potential.get()));
    }

    void constructor_only_gaussian_bath() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.gaussian_bath = true;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const GaussianBath*>(factory.d_potential.get()));
    }

    void constructor_only_soft_lorentz_gas() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.soft_lorentz_gas = true;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const SoftLorentzGas*>(factory.d_potential.get()));
    }

    void constructor_two_potentials() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.soft_lorentz_gas = true;
        plist.harmonic_potential = true;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Potential_cont*>(factory.d_potential.get()));
    }

    void constructor_table_rectangle_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::RECTANGLE;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Rectangle*>(factory.d_table.get()));
    }

    void constructor_table_notable_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::NOTABLE;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const NoTable*>(factory.d_table.get()));
    }

    void constructor_table_notable_periodic_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::NOTABLE;
        plist.periodic = true;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const NoTablePeriodic*>(factory.d_table.get()));
    }

    void constructor_table_triangle_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::TRIANGLE;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Triangle*>(factory.d_table.get()));
    }

    void constructor_table_circle_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::CIRCLE;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Circle*>(factory.d_table.get()));
    }

    void constructor_table_ellipse_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::ELLIPSE;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Ellipse*>(factory.d_table.get()));
    }

    void constructor_table_sinai_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::SINAI;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Sinai*>(factory.d_table.get()));
    }

    void constructor_table_ring_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::RING;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Ring*>(factory.d_table.get()));
    }

    void constructor_table_stadium_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::STADIUM;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Stadium*>(factory.d_table.get()));
    }

    void constructor_table_mushroom_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::MUSHROOM;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Mushroom*>(factory.d_table.get()));
    }

    void constructor_table_gate_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::GATE;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Gate*>(factory.d_table.get()));
    }

    void constructor_table_lorentzgas_default() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::LORENTZGAS;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const LorentzGas*>(factory.d_table.get()));
    }

    void constructor_table_rectangle() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::RECTANGLE;
        plist.default_geometry = false;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Rectangle*>(factory.d_table.get()));
    }

    void constructor_table_notable() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.default_geometry = false;
        plist.tabletype = ParameterList::TableType::NOTABLE;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const NoTable*>(factory.d_table.get()));
    }

    void constructor_table_notable_periodic() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.default_geometry = false;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::NOTABLE;
        plist.periodic = true;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const NoTablePeriodic*>(factory.d_table.get()));
    }

    void constructor_table_triangle() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.default_geometry = false;
        plist.tabletype = ParameterList::TableType::TRIANGLE;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Triangle*>(factory.d_table.get()));
    }

    void constructor_table_circle() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.default_geometry = false;
        plist.tabletype = ParameterList::TableType::CIRCLE;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Circle*>(factory.d_table.get()));
    }

    void constructor_table_ellipse() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.default_geometry = false;
        plist.tabletype = ParameterList::TableType::ELLIPSE;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Ellipse*>(factory.d_table.get()));
    }

    void constructor_table_sinai() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::SINAI;
        plist.default_geometry = false;
        plist.geometry = pair<double>(1, 0.1);
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Sinai*>(factory.d_table.get()));
    }

    void constructor_table_ring() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::RING;
        plist.default_geometry = false;
        plist.geometry = pair<double>(0.1, 1);
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Ring*>(factory.d_table.get()));
    }

    void constructor_table_stadium() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::STADIUM;
        plist.default_geometry = false;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Stadium*>(factory.d_table.get()));
    }

    void constructor_table_mushroom() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::MUSHROOM;
        plist.default_geometry = false;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Mushroom*>(factory.d_table.get()));
    }

    void constructor_table_gate() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.tabletype = ParameterList::TableType::GATE;
        plist.default_geometry = false;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const Gate*>(factory.d_table.get()));
    }

    void constructor_table_lorentzgas() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.default_geometry = false;
        plist.geometry = pair<double>(0.1, 0.1);
        plist.tabletype = ParameterList::TableType::LORENTZGAS;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const LorentzGas*>(factory.d_table.get()));
    }

    void constructor_coulomb_interaction() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::COULOMB;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<const CoulombInteraction*>(factory.d_interaction.get()));
    }

    void constructor_no_interaction() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::NONE;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_EQ(nullptr, factory.d_interaction.get());
    }

    void constructor_billiard() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::NONE;
        plist.billtype = ParameterList::BilliardType::BILLIARD;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        Billiard* bill = factory.d_bill.release();
        EXPECT_FALSE(dynamic_cast<MagneticBilliard*>(bill));
        EXPECT_FALSE(dynamic_cast<InteractingMagneticBilliard*>(bill));
        EXPECT_FALSE(dynamic_cast<InteractingBilliard*>(bill));
        delete bill;
    }

    void constructor_magneticbilliard() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::NONE;
        plist.billtype = ParameterList::BilliardType::MAGNETICBILLIARD;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<MagneticBilliard*>(factory.d_bill.get()));
    }

    void constructor_interactingbilliard() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::COULOMB;
        plist.billtype = ParameterList::BilliardType::INTERACTINGBILLIARD;
        plist.propagator = ParameterList::PropagatorType::SECOND;

        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<InteractingBilliard*>(factory.d_bill.get()));
    }

    void constructor_interactingmagneticbilliard() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.interactiontype = ParameterList::InteractionType::COULOMB;
        plist.billtype = ParameterList::BilliardType::INTERACTINGMAGNETICBILLIARD;
        plist.propagator = ParameterList::PropagatorType::SECOND;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        EXPECT_TRUE(dynamic_cast<InteractingMagneticBilliard*>(factory.d_bill.get()));
    }

    void bill_moved_out_from_billiardfactory() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        BilliardFactory factory(plist);

        auto b = factory.get_bill();

        // Check that factory's unique_ptr<Billiard> is empty
        EXPECT_TRUE(factory.d_bill.get() == nullptr);
    }

    void must_be_different_billptr_every_time() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        BilliardFactory factory(plist);
        auto bill1 = factory.get_bill();
        auto bill2 = factory.get_bill();
        EXPECT_FALSE(bill1.get() == bill2.get());
    }

    void test_create_custom_psos_linefun() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.calculate_custom_psos = true;
        const auto& line = plist.custom_psos_line;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        const auto& PSOSfun = factory.d_bill->get_custom_psos().PSOS;
        particleset particles;
        Particle p;
        p.d_r = {3, 6};
        particles.emplace_back(std::move(p));

        EXPECT_FEQ(line[0] * 3 + line[1] * 6 + line[2], PSOSfun(particles));

        particles.at(0).d_r = {-3.5, 5.3};

        EXPECT_FEQ(line[0] * particles.at(0).d_r[0] + line[1] * particles.at(0).d_r[1] + line[2], PSOSfun(particles));

        p.d_r = {1, 3};
        particles.emplace_back(std::move(p));

        EXPECT_FEQ(
            (line[0] * particles.at(0).d_r[0] + line[1] * particles.at(0).d_r[1] + line[2]) *
                (line[0] * particles.at(1).d_r[0] + line[1] * particles.at(1).d_r[1] + line[2]),
            PSOSfun(particles)
        );
    }

    void test_create_custom_psos_linefun_grad() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.calculate_custom_psos = true;
        const auto& line = plist.custom_psos_line;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        const auto& PSOS_gradient = factory.d_bill->get_custom_psos().PSOS_gradient;
        particleset particles;
        Particle p;
        p.d_r = {3, 6};
        particles.emplace_back(std::move(p));
        std::vector<double> Z;
        std::vector<double> forces;
        factory.d_custom_psos->particleset_to_vectors(particles, Z, forces);

        EXPECT_EQ(std::vector<double>({line[0], line[1], 0.0, 0.0}), PSOS_gradient(Z));
    }

    void test_create_custom_psos_ph_to_psos() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.calculate_custom_psos = true;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        const auto& PHS_to_PSOS = factory.d_bill->get_custom_psos().PHS_to_PSOS;
        const auto& PSOS = factory.d_bill->get_custom_psos().PSOS;
        particleset particles;
        Particle p;
        p.d_r = {1, 1};
        p.d_v = {2, 2};
        particles.emplace_back(std::move(p));
        std::vector<double> Z;
        std::vector<double> forces;
        EXPECT_FEQ(0, PSOS(particles));
        factory.d_bill->get_custom_psos().particleset_to_vectors(particles, Z, forces);
        auto result = PHS_to_PSOS(Z);
        EXPECT_FEQ(sqrt(2), result.at(1));
        EXPECT_FEQ(2 * sqrt(2), result.at(2));
        EXPECT_FEQ(0, result.at(3));

        Z.clear();
        result.clear();
        particles.at(0).d_r = {1.5, 1.5};
        particles.at(0).d_v = {2.5, -1.2};
        EXPECT_FEQ(0, PSOS(particles));
        factory.d_bill->get_custom_psos().particleset_to_vectors(particles, Z, forces);
        result = PHS_to_PSOS(Z);
        EXPECT_FEQ(sqrt(2 * 1.5 * 1.5), result.at(1));
        EXPECT_FEQ(1.3 / sqrt(2), result.at(2));
        EXPECT_FEQ(-acos(1.3 / sqrt(2) / particles[0].d_v.norm()), result.at(3));
    }

    void test_create_custom_psos_ph_to_psos_horizontal() {
        ParameterList plist;
        plist.save_nothing = true;
        plist.output_level = 0;
        plist.calculate_custom_psos = true;
        plist.custom_psos_line[0] = 0;
        BilliardFactory factory(plist);
        factory.create_Billiard();
        const auto& PHS_to_PSOS = factory.d_bill->get_custom_psos().PHS_to_PSOS;
        const auto& PSOS = factory.d_bill->get_custom_psos().PSOS;
        particleset particles;
        Particle p;
        p.d_r = {1.5, 0};
        p.d_v = {2, 2};
        particles.emplace_back(std::move(p));
        std::vector<double> Z;
        std::vector<double> forces;
        EXPECT_FEQ(0, PSOS(particles));
        factory.d_bill->get_custom_psos().particleset_to_vectors(particles, Z, forces);
        auto result = PHS_to_PSOS(Z);
        EXPECT_FEQ(1.5, result.at(1));
        EXPECT_FEQ(2, result.at(2));
        EXPECT_FEQ(M_PI / 4.0, result.at(3));
    }
};

TEST_F(billiardfactory_tests, constructor_no_potentials) { constructor_no_potentials(); }

TEST_F(billiardfactory_tests, constructor_only_harmonic_potential) { constructor_only_harmonic_potential(); }

TEST_F(billiardfactory_tests, constructor_only_soft_stadium_potential) { constructor_only_soft_stadium_potential(); }

TEST_F(billiardfactory_tests, constructor_only_gaussian_bath) { constructor_only_gaussian_bath(); }

TEST_F(billiardfactory_tests, constructor_only_soft_lorentz_gas) { constructor_only_soft_lorentz_gas(); }

TEST_F(billiardfactory_tests, constructor_two_potentials) { constructor_two_potentials(); }

TEST_F(billiardfactory_tests, constructor_table_rectangle_default) { constructor_table_rectangle_default(); }

TEST_F(billiardfactory_tests, constructor_table_notable_default) { constructor_table_notable_default(); }

TEST_F(billiardfactory_tests, constructor_table_notable_periodic_default) {
    constructor_table_notable_periodic_default();
}

TEST_F(billiardfactory_tests, constructor_table_triangle_default) { constructor_table_triangle_default(); }

TEST_F(billiardfactory_tests, constructor_table_circle_default) { constructor_table_circle_default(); }

TEST_F(billiardfactory_tests, constructor_table_ellipse_default) { constructor_table_ellipse_default(); }

TEST_F(billiardfactory_tests, constructor_table_sinai_default) { constructor_table_sinai_default(); }

TEST_F(billiardfactory_tests, constructor_table_ring_default) { constructor_table_ring_default(); }

TEST_F(billiardfactory_tests, constructor_table_mushroom_default) { constructor_table_mushroom_default(); }

TEST_F(billiardfactory_tests, constructor_table_lorentzgas_default) { constructor_table_lorentzgas_default(); }

TEST_F(billiardfactory_tests, constructor_table_gate_default) { constructor_table_gate_default(); }

TEST_F(billiardfactory_tests, constructor_table_rectangle) { constructor_table_rectangle(); }

TEST_F(billiardfactory_tests, constructor_table_notable) { constructor_table_notable(); }

TEST_F(billiardfactory_tests, constructor_table_notable_periodic) { constructor_table_notable_periodic(); }

TEST_F(billiardfactory_tests, constructor_table_triangle) { constructor_table_triangle(); }

TEST_F(billiardfactory_tests, constructor_table_circle) { constructor_table_circle(); }

TEST_F(billiardfactory_tests, constructor_table_ellipse) { constructor_table_ellipse(); }

TEST_F(billiardfactory_tests, constructor_table_sinai) { constructor_table_sinai(); }

TEST_F(billiardfactory_tests, constructor_table_ring) { constructor_table_ring(); }

TEST_F(billiardfactory_tests, constructor_table_mushroom) { constructor_table_mushroom(); }

TEST_F(billiardfactory_tests, constructor_table_lorentzgas) { constructor_table_lorentzgas(); }

TEST_F(billiardfactory_tests, constructor_table_gate) { constructor_table_gate_default(); }

TEST_F(billiardfactory_tests, constructor_no_interaction) { constructor_no_interaction(); }

TEST_F(billiardfactory_tests, constructor_coulomb_interaction) { constructor_coulomb_interaction(); }

TEST_F(billiardfactory_tests, constructor_billiard) { constructor_billiard(); }

TEST_F(billiardfactory_tests, constructor_magneticbilliard) { constructor_magneticbilliard(); }

TEST_F(billiardfactory_tests, constructor_interactingmagneticbilliard) { constructor_interactingmagneticbilliard(); }

TEST_F(billiardfactory_tests, constructor_interactingbilliard) { constructor_interactingbilliard(); }

TEST_F(billiardfactory_tests, bill_moved_out_from_billiardfactory) { bill_moved_out_from_billiardfactory(); }

TEST_F(billiardfactory_tests, must_be_different_billptr_every_time) { must_be_different_billptr_every_time(); }

TEST_F(billiardfactory_tests, create_custom_psos_linefun) { test_create_custom_psos_linefun(); }

TEST_F(billiardfactory_tests, create_custom_psos_grad) { test_create_custom_psos_linefun_grad(); }

TEST_F(billiardfactory_tests, create_custom_psos_data_extractor) { test_create_custom_psos_ph_to_psos(); }

TEST_F(billiardfactory_tests, create_custom_psos_data_extractor_horizontal_line) {
    test_create_custom_psos_ph_to_psos_horizontal();
}
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
