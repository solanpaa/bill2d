/*
 * datafile_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "H5Cpp.h"
#include "bill2d_config.h"
#include "datafile.hpp"
#include "datatypes.hpp"
#include "hist.hpp"
#include "hist2d.hpp"
#include "parameterlist.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"
#include <boost/filesystem.hpp>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
using namespace H5;
namespace bill2d {

TEST(DatasetInfo, contructor) {  // Finished

    // Open file&groups
    H5File* hfile = new H5File("testfile.h5", H5F_ACC_TRUNC);
    Group root_group = hfile->openGroup("/");

    root_group.createGroup("/mygroup");
    hsize_t dims[2] = {2, 10};
    hsize_t maxdims[2] = {2, H5S_UNLIMITED};
    unsigned rank = 2;
    unsigned rank_mem = 1;
    hsize_t memdims[1] = {20};

    DatasetInfo dinfo(
        *hfile, "/mygroup/mydata", rank, rank_mem, dims, maxdims, memdims, PredType::NATIVE_DOUBLE, true, true
    );
    delete hfile;

    // Check that the dataset was created
    hfile = new H5File("testfile.h5", H5F_ACC_RDONLY);
    Group mygroup = hfile->openGroup("/mygroup");
    hid_t loc_id = mygroup.getLocId();
    EXPECT_TRUE(H5Lexists(loc_id, "mydata", 0));
    delete hfile;
}

TEST(DatasetInfo, copycontructor) {  // Finished

    // Open file&groups
    H5File hfile("testfile.h5", H5F_ACC_TRUNC);
    Group root_group = hfile.openGroup("/");

    root_group.createGroup("/mygroup");
    hsize_t dims[2] = {2, 10};
    hsize_t maxdims[2] = {2, H5S_UNLIMITED};
    unsigned rank = 2;
    unsigned rank_mem = 1;
    hsize_t memdims[1] = {20};

    DatasetInfo* dinfo = new DatasetInfo(
        hfile, "/mygroup/mydata", rank, rank_mem, dims, maxdims, memdims, PredType::NATIVE_DOUBLE, true, true
    );
    DatasetInfo dinfo2(*dinfo);

    EXPECT_EQ(dinfo->d_name, dinfo2.d_name);
    dinfo->d_name = "kissa";

    EXPECT_NE(dinfo->d_name, dinfo2.d_name);

    // Pointers must point to different places
    EXPECT_NE(dinfo->offset, dinfo2.offset);
    EXPECT_NE(dinfo->chuncksize, dinfo2.chuncksize);
    EXPECT_NE(dinfo->dims, dinfo2.dims);
    EXPECT_NE(dinfo->maxdims, dinfo2.maxdims);
    EXPECT_NE(dinfo->memdims, dinfo2.memdims);

    // But values must be same (check only first dimension)
    EXPECT_EQ(dinfo->offset[0], dinfo2.offset[0]);
    EXPECT_EQ(dinfo->chuncksize[0], dinfo2.chuncksize[0]);
    EXPECT_EQ(dinfo->dims[0], dinfo2.dims[0]);
    EXPECT_EQ(dinfo->maxdims[0], dinfo2.maxdims[0]);
    EXPECT_EQ(dinfo->memdims[0], dinfo2.memdims[0]);

    EXPECT_EQ(dinfo->rank, dinfo2.rank);
    EXPECT_EQ(dinfo->rank_mem, dinfo2.rank_mem);
    EXPECT_EQ(dinfo->chuncked, dinfo2.chuncked);
    EXPECT_EQ(dinfo->compression, dinfo2.compression);

    delete dinfo;

    EXPECT_NE(dinfo2.d_name, "kissa");
}

TEST(DatasetInfo, writing_with_datasetinfo) {  // Finished

    // Open file&groups
    H5File* hfile = new H5File("testfile.h5", H5F_ACC_TRUNC);
    Group root_group = hfile->openGroup("/");

    root_group.createGroup("/mygroup");
    hsize_t dims[2] = {2, 10};
    hsize_t maxdims[2] = {2, H5S_UNLIMITED};
    unsigned rank = 2;
    unsigned rank_mem = 1;
    hsize_t memdims[1] = {20};

    DatasetInfo dinfo(
        *hfile, "/mygroup/mydata", rank, rank_mem, dims, maxdims, memdims, PredType::NATIVE_DOUBLE, true, true
    );

    double* buffer = new double[memdims[0]];
    for (unsigned i = 0; i < static_cast<unsigned>(memdims[0]); i++) buffer[i] = static_cast<double>(i);
    dinfo.fspace.selectHyperslab(H5S_SELECT_SET, dinfo.chuncksize.get(), dinfo.offset.get());
    dinfo.memspace.selectAll();
    dinfo.dset.write(buffer, dinfo.type, dinfo.memspace, dinfo.fspace);

    delete hfile;

    // Now read the dataset

    // Open file&groups
    H5File file("testfile.h5", H5F_ACC_RDONLY);
    root_group = file.openGroup("/");

    // A
    DataSet dset = root_group.openDataSet("/mygroup/mydata");
    DataSpace fspace = dset.getSpace();
    fspace.getSimpleExtentDims(dims, NULL);

    DataSpace memspace(2, dims);

    hsize_t offset[2] = {0, 0};
    hsize_t count[2] = {dims[0], dims[1]};
    double* buffer_from_file = new double[dims[0] * dims[1]];

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset.read(buffer_from_file, PredType::NATIVE_DOUBLE, memspace, fspace);

    for (unsigned i = 0; i < dims[0] * dims[1]; i++) EXPECT_EQ(buffer_from_file[i], buffer[i]);

    delete[] buffer_from_file;
    delete[] buffer;
}

TEST(Datafile, constructor) {  // Finished
    ParameterList plist;
    plist.savefilepath = "testfile.h5";

    Datafile file(plist);

    // Check if the file was created
    EXPECT_TRUE(boost::filesystem::exists("testfile.h5"));
}

TEST(Datafile, initialization) {  // Finished

    // Create parameterlist
    ParameterList plist;
    plist.save_full_trajectory = false;
    plist.save_velocity = false;
    plist.save_energies = false;
    plist.save_histograms = false;
    plist.save_particlenum = false;

    plist.savefilepath = "testfile.h5";
    plist.calculate_buffersizes();

    Datafile* file = new Datafile(plist);
    file->initialize(plist);

    // Check that the groups are not created
    hid_t loc_id = file->root_group.getLocId();
    EXPECT_FALSE(H5Lexists(loc_id, "velocities", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "trajectories", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "kinetic_energy", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "potential_energy", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "speed_histograms", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "velocity_histograms", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "position_histograms", 0));
    EXPECT_TRUE(H5Lexists(loc_id, "parameters", 0));

    delete file;

    plist.save_full_trajectory = true;
    plist.save_velocity = true;
    plist.save_energies = true;
    plist.save_histograms = true;
    plist.save_particlenum = true;
    plist.calculate_buffersizes();

    file = new Datafile(plist);
    file->initialize(plist);

    // Check that the groups are created
    loc_id = file->root_group.getLocId();
    EXPECT_TRUE(H5Lexists(loc_id, "velocities", 0));
    EXPECT_TRUE(H5Lexists(loc_id, "trajectories", 0));
    EXPECT_TRUE(H5Lexists(loc_id, "kinetic_energy", 0));
    EXPECT_TRUE(H5Lexists(loc_id, "potential_energy", 0));
    EXPECT_TRUE(H5Lexists(loc_id, "speed_histograms", 0));
    EXPECT_TRUE(H5Lexists(loc_id, "velocity_histograms", 0));
    EXPECT_TRUE(H5Lexists(loc_id, "position_histograms", 0));
    EXPECT_TRUE(H5Lexists(loc_id, "parameters", 0));

    delete file;
}

TEST(EscapeDatafile, initialization_for_escape_simulations) {  // Finished

    // Create parameterlist
    ParameterList plist;
    plist.savefilepath = "testfile.h5";
    plist.save_full_trajectory = false;
    plist.save_velocity = false;
    plist.save_energies = false;
    plist.save_histograms = false;
    plist.save_particlenum = false;
    plist.calculate_buffersizes();

    EscapeDatafile* file = new EscapeDatafile(plist);
    file->initialize(plist);

    hid_t loc_id = file->root_group.getLocId();
    EXPECT_FALSE(H5Lexists(loc_id, "velocities", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "trajectories", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "kinetic_energy", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "potential_energy", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "speed_histograms", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "velocity_histograms", 0));
    EXPECT_FALSE(H5Lexists(loc_id, "position_histograms", 0));
    EXPECT_TRUE(H5Lexists(loc_id, "parameters", 0));
    EXPECT_TRUE(H5Lexists(loc_id, "escape_events", 0));
    delete file;
}

TEST(Datafile, creating_new_dataset) {  // Finished

    // Create parameterlist
    ParameterList plist;
    plist.savefilepath = "testfile.h5";
    plist.save_full_trajectory = false;
    plist.save_velocity = false;
    plist.save_energies = false;
    plist.save_histograms = false;
    plist.save_particlenum = false;
    plist.calculate_buffersizes();
    Datafile file(plist);
    file.initialize(plist);

    double* buffer = new double[plist.buffersize_t];
    for (unsigned i = 0; i < plist.buffersize; i++) {
        buffer[i] = static_cast<double>(i);
    }

    // Should throw out_of_range exception since no dataset has been declared
    ASSERT_THROW(file.write_buffer("/my_fancy_dataset", buffer), std::out_of_range);

    hsize_t dims[] = {plist.buffersize_ht};
    DataType double_type = PredType::NATIVE_DOUBLE;

    file.create_dataset("/my_fancy_dataset", 1, 1, dims, dims, dims, double_type, false, true);

    ASSERT_NO_THROW(file.write_buffer("/my_fancy_dataset", buffer));

    // Throws exception if trying to create a dataset which already exists
    ASSERT_THROW(file.create_dataset("/my_fancy_dataset", 1, 1, dims, dims, dims, double_type, true, true), Exception);

    // Throws exception is chuncked dataset (implying several writings and appending) and not unlimited dataset in
    // some direction
    ASSERT_THROW(file.create_dataset("/my_fancy_dataset2", 1, 1, dims, dims, dims, double_type, true, true), Exception);
}

TEST(Datafile, writing_attributes) {  // Finished
    boost::filesystem::remove("testfile.h5");

    ParameterList plist;
    plist.savefilepath = "testfile.h5";
    plist.save_full_trajectory = true;
    plist.save_velocity = true;
    plist.save_energies = true;
    plist.save_histograms = true;
    plist.save_particlenum = true;
    plist.calculate_buffersizes();

    Datafile file(plist);
    file.initialize(plist);

    file.write_attribute("my_fancy_attribute_INT", (int)(-123));
    file.write_attribute("my_fancy_attribute_UINT", (unsigned)123);
    std::string str("Me hungry");
    file.write_attribute("my_fancy_attribute_STR", std::string("Me hungry"));
    file.write_attribute("my_fancy_attribute_DOUBLE", (double)12.3);

    file.write_attribute("my_fancy_attribute_SIZET", (size_t)5);

    file.write_attribute("my_fancy_attribute_BOOL", true);  // actually written as an integer: 1 or 0
    pair<double> mypair(1, 2);

    file.write_attribute("my_fancy_attribute_PAIR", mypair);

    twopairs<double> mytwopairs;
    mytwopairs[0] = 1;
    mytwopairs[1] = 2;
    mytwopairs[2] = -3.3;
    mytwopairs[3] = 4.1;
    file.write_attribute("my_fancy_attribute_twopairs", mytwopairs);
}

TEST(Datafile, writing_attributes_read) {
    // Now read the attributes and check
    H5File hfile("testfile.h5", H5F_ACC_RDONLY);
    Group pgroup = hfile.openGroup("/parameters");
    std::string str("Me hungry");

    Attribute attr = pgroup.openAttribute("my_fancy_attribute_INT");
    int iattr;
    attr.read(PredType::NATIVE_INT, &iattr);
    EXPECT_EQ(iattr, -123);

    attr = pgroup.openAttribute("my_fancy_attribute_UINT");
    unsigned uattr;
    attr.read(PredType::NATIVE_UINT, &uattr);
    EXPECT_EQ(uattr, 123);

    DataType string_type = StrType(0, str.size() + 1);
    attr = pgroup.openAttribute("my_fancy_attribute_STR");
    std::string out(str.size(), ' ');
    attr.read(string_type, &out.front());

    EXPECT_EQ(str, out);

    double dattr;
    attr = pgroup.openAttribute("my_fancy_attribute_DOUBLE");
    attr.read(PredType::NATIVE_DOUBLE, &dattr);
    EXPECT_EQ(dattr, 12.3);

    attr = pgroup.openAttribute("my_fancy_attribute_BOOL");
    int battr;
    attr.read(PredType::NATIVE_INT, &battr);
    EXPECT_EQ(battr, 1);

    attr = pgroup.openAttribute("my_fancy_attribute_SIZET");
    attr.read(PredType::NATIVE_UINT, &uattr);  // SIZE_T is written as NATIVE_UINT
    EXPECT_EQ(uattr, 5);

    pair<double> p;

    attr = pgroup.openAttribute("my_fancy_attribute_PAIR");
    const hsize_t pair_dims[1] = {2};
    ArrayType pair_type(PredType::NATIVE_DOUBLE, 1, pair_dims);
    attr.read(pair_type, &p);
    EXPECT_EQ(1, p[0]);
    EXPECT_EQ(2, p[1]);

    twopairs<double> tp;
    attr = pgroup.openAttribute("my_fancy_attribute_twopairs");
    const hsize_t twopair_dims[1] = {4};
    ArrayType twopair_type(PredType::NATIVE_DOUBLE, 1, twopair_dims);
    attr.read(twopair_type, &tp);
    EXPECT_EQ(1, tp[0]);
    EXPECT_EQ(2, tp[1]);
    EXPECT_EQ(-3.3, tp[2]);
    EXPECT_EQ(4.1, tp[3]);
}

TEST(Datafile, writing_double_buffers) {  // Finished

    ParameterList plist;
    plist.save_full_trajectory = true;
    plist.save_velocity = true;
    plist.save_energies = true;
    plist.save_histograms = true;
    plist.save_particlenum = true;
    plist.savefilepath = "testfile.h5";
    plist.calculate_buffersizes();

    // Initialize the file, i.e., create groups etc.
    Datafile* file = new Datafile(plist);
    file->initialize(plist);

    /*
     * Initialize some buffer and write all of it to file
     * Testing:
     * A: Writing a double buffer all at once
     * B: Writing a double buffer in two parts (append)
     */

    double* buffer = new double[plist.buffersize_t];
    double* buffer_from_file;
    for (unsigned i = 0; i < plist.buffersize; i++) {
        buffer[i] = static_cast<double>(i);
    }

    // A
    file->write_buffer("/kinetic_energy", buffer);

    // B
    file->write_buffer("/potential_energy", buffer);
    file->write_buffer("/potential_energy", buffer, plist.buffersize / 2);

    delete file;

    // Now read the buffer from file

    // Open file&groups
    H5File hfile("testfile.h5", H5F_ACC_RDONLY);
    Group root_group = hfile.openGroup("/");

    // A
    DataSet dset = root_group.openDataSet("/kinetic_energy");
    hsize_t dims[1];
    DataSpace fspace = dset.getSpace();
    fspace.getSimpleExtentDims(dims, NULL);

    DataSpace memspace(1, dims);

    hsize_t offset[] = {0};
    hsize_t count[1] = {dims[0]};
    buffer_from_file = new double[dims[0]];

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset.read(buffer_from_file, PredType::NATIVE_DOUBLE, memspace, fspace);

    for (unsigned i = 0; i < plist.buffersize; i++) EXPECT_EQ(buffer_from_file[i], buffer[i]);

    delete[] buffer_from_file;

    // B
    dset = root_group.openDataSet("/potential_energy");
    fspace = dset.getSpace();
    fspace.getSimpleExtentDims(dims, NULL);

    memspace = DataSpace(1, dims);

    count[0] = dims[0];
    buffer_from_file = new double[dims[0]];

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset.read(buffer_from_file, PredType::NATIVE_DOUBLE, memspace, fspace);

    for (unsigned i = 0; i < plist.buffersize; i++) EXPECT_EQ(buffer_from_file[i], buffer[i]);

    for (unsigned i = 0; i < plist.buffersize / 2; i++) EXPECT_EQ(buffer_from_file[plist.buffersize + i], buffer[i]);

    delete[] buffer_from_file;

    delete[] buffer;
}

TEST(Datafile, writing_unsigned_buffers) {  // Finished

    ParameterList plist;
    plist.save_full_trajectory = true;
    plist.save_velocity = true;
    plist.save_energies = true;
    plist.save_histograms = true;
    plist.save_particlenum = true;
    plist.calculate_buffersizes();
    plist.savefilepath = "testfile.h5";

    Datafile* file = new Datafile(plist);
    file->initialize(plist);

    /*
     * Initialize some buffer and write all of it to file
     * Testing:
     * A: Writing an unsigned buffer all at once
     * B: Writing an unsigned buffer in two parts (append)
     */

    unsigned* buffer = new unsigned[plist.buffersize_t];
    unsigned* buffer_from_file;
    for (unsigned i = 0; i < plist.buffersize; i++) {
        buffer[i] = i;
    }

    // A
    file->write_buffer("/particle_num", buffer);

    // B
    hsize_t dims[] = {plist.buffersize_ht};
    hsize_t maxdims[] = {H5S_UNLIMITED};
    DataType type = PredType::NATIVE_UINT;
    file->create_dataset("/particle_num2", 1, 1, dims, maxdims, dims, type, true, true);
    file->write_buffer("/particle_num2", buffer);
    file->write_buffer("/particle_num2", buffer, plist.buffersize / 2);

    delete file;

    // Now read the buffer from file

    // Open file&groups
    H5File hfile("testfile.h5", H5F_ACC_RDONLY);
    Group root_group = hfile.openGroup("/");

    // A
    DataSet dset = root_group.openDataSet("/particle_num");
    DataSpace fspace = dset.getSpace();
    fspace.getSimpleExtentDims(dims, NULL);

    DataSpace memspace(1, dims);

    hsize_t offset[] = {0};
    hsize_t count[1] = {dims[0]};
    buffer_from_file = new unsigned[dims[0]];

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset.read(buffer_from_file, PredType::NATIVE_UINT, memspace, fspace);

    for (unsigned i = 0; i < plist.buffersize; i++) EXPECT_EQ(buffer_from_file[i], buffer[i]);

    delete[] buffer_from_file;

    // B
    dset = root_group.openDataSet("/particle_num2");
    fspace = dset.getSpace();
    fspace.getSimpleExtentDims(dims, NULL);

    memspace = DataSpace(1, dims);

    count[0] = dims[0];
    buffer_from_file = new unsigned[dims[0]];

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset.read(buffer_from_file, PredType::NATIVE_UINT, memspace, fspace);

    for (unsigned i = 0; i < plist.buffersize; i++) EXPECT_EQ(buffer_from_file[i], buffer[i]);

    for (unsigned i = 0; i < plist.buffersize / 2; i++) EXPECT_EQ(buffer_from_file[plist.buffersize + i], buffer[i]);

    delete[] buffer_from_file;

    delete[] buffer;
}

TEST(Datafile, writing_trajectory_and_velocity) {  // Finished
    // Create parameterlist
    ParameterList plist;
    plist.save_full_trajectory = true;
    plist.save_velocity = true;
    plist.save_energies = false;
    plist.save_histograms = false;
    plist.save_particlenum = false;
    plist.calculate_buffersizes();
    plist.savefilepath = "testfile.h5";

    Datafile* file = new Datafile(plist);
    file->initialize(plist);
    double* buffer = new double[2 * plist.buffersize_t];
    for (unsigned i = 0; i < 2 * plist.buffersize; i++) {
        buffer[i] = i;
    }

    file->write_trajectory(1, buffer, plist.buffersize, 1);

    file->write_velocity(1, buffer, plist.buffersize, 1);

    delete file;

    // Open file&groups
    H5File hfile("testfile.h5", H5F_ACC_RDONLY);
    Group root_group = hfile.openGroup("/");

    // trajectory
    DataSet dset = root_group.openDataSet("/trajectories/particle1");
    DataSpace fspace = dset.getSpace();
    hsize_t dims[1];
    fspace.getSimpleExtentDims(dims, NULL);

    DataSpace memspace(1, dims);

    hsize_t offset[] = {0};
    hsize_t count[1] = {dims[0]};
    double* buffer_from_file = new double[2 * dims[0]];

    const hsize_t pair_dims[1] = {2};
    ArrayType pair_type(PredType::NATIVE_DOUBLE, 1, pair_dims);

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset.read(buffer_from_file, pair_type, memspace, fspace);

    for (unsigned i = 0; i < dims[0]; i++) EXPECT_EQ(buffer[i], buffer_from_file[i]);

    // velocity
    // trajectory
    delete[] buffer_from_file;
    buffer_from_file = new double[2 * dims[0]];
    dset = root_group.openDataSet("/velocities/particle1");
    fspace = dset.getSpace();
    fspace.getSimpleExtentDims(dims, NULL);

    memspace = DataSpace(1, dims);

    offset[0] = 0;
    count[0] = dims[0];

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset.read(buffer_from_file, pair_type, memspace, fspace);

    for (unsigned i = 0; i < dims[0]; i++) EXPECT_EQ(buffer[i], buffer_from_file[i]);

    delete[] buffer_from_file;
    delete[] buffer;
}

TEST(Datafile, writing_1D_histograms) {  // Finished

    // Create parameterlist
    ParameterList plist;
    plist.save_full_trajectory = false;
    plist.save_velocity = false;
    plist.save_energies = false;
    plist.save_histograms = true;
    plist.save_particlenum = false;
    plist.bins = 10;
    plist.calculate_buffersizes();
    plist.savefilepath = "testfile.h5";

    Datafile* file = new Datafile(plist);
    file->initialize(plist);

    Hist hist(10, 0, 10);
    hist.add(3);
    hist.add(4);
    hist.add(9.9);
    hist.add(9.9);
    file->write_shist(1, &hist);

    Hist hist2(15, 0, 16);
    hist2.add(3);
    hist2.add(4);
    hist2.add(9.9);
    hist2.add(9.9);
    hist2.add(3.4);
    hsize_t dims[1] = {10};
    DataType double_type = PredType::NATIVE_DOUBLE;
    file->create_dataset("myhist", 1, dims, double_type);
    file->write_buffer("myhist", &hist2);

    delete file;

    // Open file&groups
    H5File hfile("testfile.h5", H5F_ACC_RDONLY);
    Group root_group = hfile.openGroup("/");

    DataSet dset = root_group.openDataSet("/speed_histograms/particle1");
    DataSpace fspace = dset.getSpace();

    fspace.getSimpleExtentDims(dims, NULL);
    DataSpace memspace(1, dims);

    hsize_t offset[] = {0};
    hsize_t count[1] = {dims[0]};

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    double* buffer_from_file = new double[dims[0]];
    dset.read(buffer_from_file, PredType::NATIVE_DOUBLE, memspace, fspace);

    for (unsigned i = 0; i < static_cast<unsigned>(dims[0]); i++) {
        EXPECT_EQ(hist.data()[i], buffer_from_file[i]);
    }
    delete[] buffer_from_file;

    DataSet dset2 = root_group.openDataSet("/myhist");
    DataSpace fspace2 = dset2.getSpace();
    hsize_t dims2[1];
    fspace2.getSimpleExtentDims(dims2, NULL);
    DataSpace memspace2(1, dims2);
    buffer_from_file = new double[dims2[0]];
    offset[0] = 0;
    count[0] = dims2[0];
    fspace2.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace2.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset2.read(buffer_from_file, PredType::NATIVE_DOUBLE, memspace2, fspace2);
    for (unsigned i = 0; i < static_cast<unsigned>(dims[0]); i++) {
        EXPECT_EQ(hist2.data()[i], buffer_from_file[i]);
    }

    delete[] buffer_from_file;
}

TEST(Datafile, writing_2D_histograms) {  // Finished
    // Create parameterlist
    ParameterList plist;
    plist.save_full_trajectory = false;
    plist.save_velocity = false;
    plist.save_energies = false;
    plist.save_histograms = true;
    plist.save_particlenum = false;
    plist.bins = 10;
    plist.calculate_buffersizes();
    plist.savefilepath = "testfile.h5";

    Datafile* file = new Datafile(plist);
    file->initialize(plist);

    Hist2d hist(10, 10, 0, 10, 0, 15);
    hist.add(3, 4);
    hist.add(4, 1);
    hist.add(9.9, 2);
    hist.add(9.9, 2);
    file->write_rhist(1, &hist);

    Hist2d hist2(15, 16, 0, 10, 1, 7);
    hist2.add(3, 2);
    hist2.add(4, 1);
    hist2.add(9.9, 2);
    hist2.add(9.9, 2);
    hist2.add(3.4, 6);
    hsize_t dims[2] = {15, 16};
    DataType double_type = PredType::NATIVE_DOUBLE;
    file->create_dataset("myhist", 2, dims, double_type);
    file->write_buffer("myhist", &hist2);

    delete file;

    // Open file&groups
    H5File hfile("testfile.h5", H5F_ACC_RDONLY);
    Group root_group = hfile.openGroup("/");

    // A
    DataSet dset = root_group.openDataSet("/position_histograms/particle1");
    DataSpace fspace = dset.getSpace();

    fspace.getSimpleExtentDims(dims, NULL);
    DataSpace memspace(2, dims);

    hsize_t offset[2] = {0, 0};
    hsize_t count[2] = {dims[0], dims[1]};

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    double* buffer_from_file = new double[dims[0] * dims[1]];
    dset.read(buffer_from_file, PredType::NATIVE_DOUBLE, memspace, fspace);

    for (unsigned i = 0; i < static_cast<unsigned>(dims[0]); i++) {
        for (unsigned j = 0; j < static_cast<unsigned>(dims[1]); j++)
            EXPECT_EQ(hist.data()[i * dims[0] + j], buffer_from_file[i * dims[0] + j]);
    }
    delete[] buffer_from_file;

    DataSet dset2 = root_group.openDataSet("/myhist");
    DataSpace fspace2 = dset2.getSpace();
    hsize_t dims2[2];
    fspace2.getSimpleExtentDims(dims2, NULL);
    DataSpace memspace2(2, dims2);
    buffer_from_file = new double[dims2[0] * dims2[1]];
    count[0] = dims2[0];
    count[1] = dims2[1];
    fspace2.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace2.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset2.read(buffer_from_file, PredType::NATIVE_DOUBLE, memspace2, fspace2);

    for (unsigned i = 0; i < static_cast<unsigned>(dims[0]); i++) {
        for (unsigned j = 0; j < static_cast<unsigned>(dims[1]); j++)
            EXPECT_EQ(hist2.data()[i * dims[0] + j], buffer_from_file[i * dims[0] + j]);
    }

    delete[] buffer_from_file;
}

TEST(Datafile, writing_bouncing_maps) {  // Finished
    std::vector<std::vector<double>> bmap;
    std::vector<double> row;
    for (unsigned i = 0; i < 5; i++) {
        row.clear();
        row.push_back(1 + i);
        row.push_back(2 + i);
        row.push_back(3 + i);
        row.push_back(4 + i);
        bmap.push_back(row);
    }

    // Create parameterlist
    ParameterList plist;
    plist.save_full_trajectory = false;
    plist.save_velocity = false;
    plist.save_bouncemap = true;
    plist.save_energies = false;
    plist.save_histograms = false;
    plist.save_particlenum = false;
    plist.calculate_buffersizes();
    plist.savefilepath = "testfile.h5";

    Datafile* file = new Datafile(plist);
    file->initialize(plist);

    file->write_bmap(1, bmap);

    delete file;

    // Check by reading the bounce map from file

    // Open file&groups
    H5File hfile("testfile.h5", H5F_ACC_RDONLY);
    Group root_group = hfile.openGroup("/");

    // trajectory
    DataSet dset = root_group.openDataSet("/bounce maps/particle1");
    DataSpace fspace = dset.getSpace();
    hsize_t dims[2];
    fspace.getSimpleExtentDims(dims, NULL);

    DataSpace memspace(2, dims);

    hsize_t offset[2] = {0, 0};
    hsize_t count[2] = {dims[0], dims[1]};
    double buffer_from_file[5][4];

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset.read(buffer_from_file, PredType::NATIVE_DOUBLE, memspace, fspace);

    for (unsigned i = 0; i < 5; i++) {
        for (unsigned j = 0; j < 4; j++) EXPECT_EQ(buffer_from_file[i][j], bmap.at(i).at(j));
    }
}

TEST(EscapeDatafile, writing_escape_events) {  // Finished
    boost::filesystem::remove("testfile.h5");

    // Create parameterlist
    ParameterList plist;
    plist.save_full_trajectory = false;
    plist.save_velocity = false;
    plist.save_energies = false;
    plist.save_histograms = false;
    plist.save_particlenum = false;
    plist.calculate_buffersizes();
    plist.savefilepath = "testfile.h5";

    EscapeDatafile* file = new EscapeDatafile(plist);
    file->initialize(plist);

    // Writing
    double* buffer = new double[plist.simulations];
    for (unsigned i = 0; i < plist.simulations; i++) buffer[i] = static_cast<double>(i);

    file->write_escape_events("escape_times", buffer);
    file->write_escape_events("escape_collisions", buffer);

    delete file;

    // Check
    // Open file&groups
    H5File hfile("testfile.h5", H5F_ACC_RDONLY);
    Group root_group = hfile.openGroup("/");

    // A
    DataSet dset = root_group.openDataSet("/escape_events/escape_times");
    hsize_t dims[1];
    DataSpace fspace = dset.getSpace();
    fspace.getSimpleExtentDims(dims, NULL);

    DataSpace memspace(1, dims);

    hsize_t offset[] = {0};
    hsize_t count[1] = {dims[0]};
    double* buffer_from_file = new double[dims[0]];

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset.read(buffer_from_file, PredType::NATIVE_DOUBLE, memspace, fspace);

    for (unsigned i = 0; i < plist.simulations; i++) EXPECT_EQ(buffer_from_file[i], buffer[i]);

    delete[] buffer_from_file;
    delete[] buffer;
}

TEST(EscapeDatafile, writing_escape_events_append) {  // Finished

    // Create parameterlist
    ParameterList plist;
    plist.save_full_trajectory = false;
    plist.save_velocity = false;
    plist.save_energies = false;
    plist.save_histograms = false;
    plist.save_particlenum = false;
    plist.calculate_buffersizes();
    plist.savefilepath = "testfile.h5";

    EscapeDatafile* file = new EscapeDatafile(plist);
    file->initialize(plist);

    // Writing
    double* buffer = new double[plist.simulations];
    for (unsigned i = 0; i < plist.simulations; i++) buffer[i] = static_cast<double>(5 + i);

    file->write_escape_events("escape_times", buffer);
    file->write_escape_events("escape_collisions", buffer);

    delete file;

    // Check
    // Open file&groups
    H5File hfile("testfile.h5", H5F_ACC_RDONLY);
    Group root_group = hfile.openGroup("/");

    // A
    DataSet dset = root_group.openDataSet("/escape_events/escape_times");
    hsize_t dims[1];
    DataSpace fspace = dset.getSpace();
    fspace.getSimpleExtentDims(dims, NULL);

    DataSpace memspace(1, dims);

    hsize_t offset[] = {0};
    hsize_t count[1] = {dims[0]};
    double* buffer_from_file = new double[dims[0]];

    fspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    memspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset.read(buffer_from_file, PredType::NATIVE_DOUBLE, memspace, fspace);

    for (unsigned i = 0; i < plist.simulations; i++) EXPECT_EQ(buffer_from_file[i], buffer[i] - 5);
    for (unsigned i = 0; i < plist.simulations; i++) EXPECT_EQ(buffer_from_file[plist.simulations + i], buffer[i]);
    delete[] buffer_from_file;
    delete[] buffer;

    boost::filesystem::remove("testfile.h5");
}

TEST(Datafile, flushing_to_disk) {  // Finished

    ParameterList plist;
    plist.save_full_trajectory = true;
    plist.save_velocity = true;
    plist.save_energies = true;
    plist.save_histograms = true;
    plist.save_particlenum = true;
    plist.calculate_buffersizes();
    plist.savefilepath = "testfile.h5";

    Datafile* file = new Datafile(plist);
    file->initialize(plist);
    double* buffer = new double[plist.buffersize_t];
    for (unsigned i = 0; i < plist.buffersize; i++) {
        buffer[i] = static_cast<double>(i);
    }

    // A
    file->write_buffer("/kinetic_energy", buffer);

    auto filesize1 = boost::filesystem::file_size("testfile.h5");
    file->flush();
    auto filesize2 = boost::filesystem::file_size("testfile.h5");
    EXPECT_TRUE(filesize1 < filesize2);
    delete file;
    EXPECT_EQ(filesize2, boost::filesystem::file_size("testfile.h5"));

    boost::filesystem::remove("testfile.h5");
}
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
