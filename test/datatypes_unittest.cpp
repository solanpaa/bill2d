/*
 * datatypes_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "datatypes.hpp"
#include "exceptions.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"

#define EXPECT_FEQ5(A, B) EXPECT_NEAR(A, B, 1e-5)

#define EXPECT_FEQ(A, B) EXPECT_NEAR(A, B, 1e-10)

#define EXPECT_FNE(A, B) EXPECT_TRUE(fabsl((A) - (B)) > 1e-10)

namespace bill2d {
TEST(pair, constructor) {
    pair<double> p(1.5, 3.4);
    EXPECT_EQ(1.5, p[0]);
    EXPECT_EQ(3.4, p[1]);
}

TEST(pair, default_constructor) {
    pair<double> p;
    EXPECT_EQ(0, p[0]);
    EXPECT_EQ(0, p[1]);
}

TEST(pair, equality) {
    pair<double> p1(1.5, 2.3);
    pair<double> p2(1.5, 2.3);
    EXPECT_EQ(p1, p2);

    p2[1] = 1.4;
    EXPECT_NE(p1, p2);
}

TEST(pair, nomember) {
    pair<double> p;
    EXPECT_THROW(p[2], NoSuchMember);
}

TEST(triplet, constructor) {
    triplet<double> p(1.5, 3.4, -6.2);
    EXPECT_EQ(1.5, p[0]);
    EXPECT_EQ(3.4, p[1]);
    EXPECT_EQ(-6.2, p[2]);
}

TEST(triplet, default_constructor) {
    triplet<double> p;
    EXPECT_EQ(0, p[0]);
    EXPECT_EQ(0, p[1]);
    EXPECT_EQ(0, p[2]);
}

TEST(triplet, equality) {
    triplet<double> p1(1.5, 2.3, -6.2);
    triplet<double> p2(1.5, 2.3, -6.2);
    EXPECT_EQ(p1, p2);

    p2[1] = 1.4;
    EXPECT_NE(p1, p2);
}

TEST(triplet, nomember) {
    triplet<double> p;
    EXPECT_THROW(p[3], NoSuchMember);
}

TEST(twopairs, constructor) {
    twopairs<double> p(1.5, 3.4, -6.2, 6.7);
    EXPECT_EQ(1.5, p[0]);
    EXPECT_EQ(3.4, p[1]);
    EXPECT_EQ(-6.2, p[2]);
    EXPECT_EQ(6.7, p[3]);
}

TEST(twopairs, default_constructor) {
    twopairs<double> p;
    EXPECT_EQ(0, p[0]);
    EXPECT_EQ(0, p[1]);
    EXPECT_EQ(0, p[2]);
    EXPECT_EQ(0, p[3]);
}

TEST(twopairs, equality) {
    twopairs<double> p1(1.5, 2.3, -6.2, 6.7);
    twopairs<double> p2(1.5, 2.3, -6.2, 6.7);
    EXPECT_EQ(p1, p2);

    p2[1] = 1.4;
    EXPECT_NE(p1, p2);
}

TEST(twopairs, nomember) {
    twopairs<double> p;
    EXPECT_THROW(p[4], NoSuchMember);
}
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
