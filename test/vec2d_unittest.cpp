/*
 * vec2d_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"
#include <cmath>

#include "rng.hpp"
#include "vec2d.hpp"
#include <sstream>
#include <string>

#define EXPECT_FEQ(A, B) EXPECT_NEAR(A, B, 1e-10)

namespace bill2d {

TEST(Vec2d, constructor) {
    Vec2d r;
    EXPECT_EQ(r[0], 0);
    EXPECT_EQ(r[1], 0);

    Vec2d r2(-0.5, 3.2);
    EXPECT_EQ(r2[0], -0.5);
    EXPECT_EQ(r2[1], 3.2);
}

TEST(Vec2d, element_access) {
    Vec2d r(0.1, 0.1);
    r[0] += 2;
    r[1] *= 5.2;
    EXPECT_EQ(r[0], 2.1);
    EXPECT_EQ(r[1], 0.52);
#ifdef USE_SSE2_VEC2D
    EXPECT_EQ(&r[0], &r.d_vec.arr[0]);
    EXPECT_EQ(&r[1], &r.d_vec.arr[1]);
#else
    EXPECT_EQ(&r[0], &r.d_vec[0]);
    EXPECT_EQ(&r[1], &r.d_vec[1]);
#endif

    const double a = r[0];
    EXPECT_NE(&a, &r[0]);

    const double& b = r[0];
    EXPECT_EQ(&b, &r[0]);
}

TEST(Vec2d, copy_constructor) {
    Vec2d r2(-0.5, 3.2);

    Vec2d r3(r2);
    EXPECT_EQ(r2[0], r3[0]);
    EXPECT_EQ(r2[1], r3[1]);

    EXPECT_NE(&r2[0], &r3[0]);
    EXPECT_NE(&r2[1], &r3[1]);
}

TEST(Vec2d, stream) {
    std::stringstream strstream;
    Vec2d r(0.5, 0.2);
    strstream << r;
    EXPECT_EQ(std::string("(0.5, 0.2)"), strstream.str());
}

TEST(Vec2d, equal_to) {
    Vec2d r(0.5, 1.2);
    Vec2d r2(0.5, 1.2);

    EXPECT_EQ(r, r2);
}

TEST(Vec2d, basic_assignment) {
    Vec2d r;
    Vec2d r2(0.5, 1.2);
    r = r2;
    EXPECT_EQ(r, r2);

    EXPECT_NE(&r[0], &r2[0]);

    r = Vec2d(0.6, 0.3);
    EXPECT_EQ(r[0], 0.6);
    EXPECT_EQ(0.3, r[1]);
}

TEST(Vec2d, assignment_addition) {
    RNG rng;
    double a, b, c, d;
    Vec2d r, r2;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        c = rng.uniform_rand(-100, 100);
        d = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        r2 = Vec2d(c, d);
        r += r2;
        EXPECT_FEQ(r[0], a + c);
        EXPECT_FEQ(r[1], b + d);
    }
}

TEST(Vec2d, assignment_substraction) {
    RNG rng;
    double a, b, c, d;
    Vec2d r, r2;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        c = rng.uniform_rand(-100, 100);
        d = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        r2 = Vec2d(c, d);
        r -= r2;
        EXPECT_FEQ(r[0], a - c);
        EXPECT_FEQ(r[1], b - d);
    }
}

TEST(Vec2d, assignment_multiplication) {
    RNG rng;
    double a, b, c;
    Vec2d r;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        c = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        r *= c;
        EXPECT_FEQ(r[0], a * c);
        EXPECT_FEQ(r[1], b * c);
    }
}

TEST(Vec2d, assignment_division) {
    RNG rng;
    double a, b, c;
    Vec2d r;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        c = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        r /= c;
        EXPECT_FEQ(r[0], a / c);
        EXPECT_FEQ(r[1], b / c);
    }
}

TEST(Vec2d, addition) {
    RNG rng;
    double a, b, c, d;
    Vec2d r, r2, r3;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        c = rng.uniform_rand(-100, 100);
        d = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        r2 = Vec2d(c, d);
        r3 = r + r2;
        EXPECT_FEQ(r3[0], a + c);
        EXPECT_FEQ(r3[1], b + d);
    }
}

TEST(Vec2d, substraction) {
    RNG rng;
    double a, b, c, d;
    Vec2d r, r2, r3;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        c = rng.uniform_rand(-100, 100);
        d = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        r2 = Vec2d(c, d);
        r3 = r - r2;
        EXPECT_FEQ(r3[0], a - c);
        EXPECT_FEQ(r3[1], b - d);
    }
}

TEST(Vec2d, multiplication) {
    RNG rng;
    double a, b, c;
    Vec2d r, r2;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        c = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        r2 = r * c;
        EXPECT_FEQ(r2[0], a * c);
        EXPECT_FEQ(r2[1], b * c);
    }
}

TEST(Vec2d, multiplication2) {
    RNG rng;
    double a, b, c;
    Vec2d r, r2;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        c = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        r2 = c * r;
        EXPECT_FEQ(r2[0], a * c);
        EXPECT_FEQ(r2[1], b * c);
    }
}

TEST(Vec2d, division) {
    RNG rng;
    double a, b, c;
    Vec2d r, r2;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        c = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        r2 = r / c;
        EXPECT_FEQ(r2[0], a / c);
        EXPECT_FEQ(r2[1], b / c);
    }
}

TEST(Vec2d, norm) {
    RNG rng;
    double a, b;
    Vec2d r;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        EXPECT_FEQ(r.norm(), sqrt(a * a + b * b));
    }
}

TEST(Vec2d, normsqr) {
    RNG rng;
    double a, b;
    Vec2d r;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        EXPECT_FEQ(r.normsqr(), a * a + b * b);
    }
}

TEST(Vec2d, dot) {
    RNG rng;
    double a, b, c, d;
    Vec2d r, r2;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        c = rng.uniform_rand(-100, 100);
        d = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        r2 = Vec2d(c, d);
        EXPECT_FEQ(r.dot(r2), a * c + b * d);
    }
}

TEST(Vec2d, scale_to_unit) {
    RNG rng;
    double a, b;
    Vec2d r;
    for (unsigned i = 0; i < 1000; i++) {
        a = rng.uniform_rand(-100, 100);
        b = rng.uniform_rand(-100, 100);
        r = Vec2d(a, b);
        r.scale_to_unit();
        EXPECT_FEQ(r.norm(), 1);
    }
}

TEST(Vec2d, rot90cw) {
    RNG rng;
    for (unsigned i = 0; i < 1000; i++) {
        double a = rng.uniform_rand(-100, 100);
        double b = rng.uniform_rand(-100, 100);
        Vec2d r = Vec2d(a, b);
        Vec2d tmp = r.rotcw90();
        EXPECT_FEQ(b, tmp[0]);
        EXPECT_FEQ(-a, tmp[1]);
    }
}
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
