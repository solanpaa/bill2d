/*
 * table_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"
#include <gsl/gsl_sf_ellint.h>

#include "bill2d_config.h"
#include "datatypes.hpp"
#include "rng.hpp"
#include "table.hpp"
#include "vec2d.hpp"
#include <functional>
#include <iostream>
#include <string>
#include <vector>

#define EXPECT_AEQ(A, B) EXPECT_NEAR(A, B, 1e-3)

#define EXPECT_FEQ5(A, B) EXPECT_NEAR(A, B, 1e-5)

#define EXPECT_FEQ(A, B) EXPECT_NEAR(A, B, 1e-10)

#define EXPECT_FNE(A, B) EXPECT_TRUE(fabsl((A) - (B)) > 1e-10)

namespace bill2d {

class compoundshape_tests : public ::testing::Test {
protected:
    void boundaryline_constructor() {
        CompoundShape::BoundaryLine table(
            CompoundShape::BoundaryLine::ForbidUpper, 1.5, 0.5, 1.0, 3.2, Vec2d(0.0, -2.0)
        );

        EXPECT_DEATH(
            CompoundShape::BoundaryLine(CompoundShape::BoundaryLine::ForbidUpper, 1.5, 0.5, 1.0, 3.2, Vec2d(0.0, -3.0)),
            ""
        );
    }

    void boundaryline_point_is_inside() {
        CompoundShape::BoundaryLine table(
            CompoundShape::BoundaryLine::ForbidUpper, 1.5, 0.5, 1.0, 3.2, Vec2d(0.0, -2.0)
        );

        EXPECT_TRUE(table.point_is_inside({1.0, -5.1}));
        EXPECT_FALSE(table.point_is_inside({1.0, -4.9}));

        CompoundShape::BoundaryLine table2(
            CompoundShape::BoundaryLine::ForbidLower, -1.5, 0.5, 1.0, 3.2, Vec2d(0.0, -2.0)
        );

        EXPECT_FALSE(table2.point_is_inside({1.0, 0.9}));
        EXPECT_TRUE(table2.point_is_inside({1.0, 1.1}));

        CompoundShape::BoundaryLine table3(
            CompoundShape::BoundaryLine::ForbidLower, 1.5, 0.5, 1.0, 3.2, Vec2d(0.0, -2.0)
        );
        EXPECT_FALSE(table3.point_is_inside({1.0, -5.1}));
        EXPECT_TRUE(table3.point_is_inside({1.0, -4.9}));
    }

    void boundaryline_handle_crossing_forbidupper() {
        pair<int> cell;
        unsigned hits = 0;
        bool hit_flag = false;
        ;
        CompoundShape::BoundaryLine table(
            CompoundShape::BoundaryLine::ForbidUpper, 1.5, 0.5, 1.0, 3.2, Vec2d(0.0, -2.0)
        );

        Vec2d r(1.0, -5 + 1e-4);
        Vec2d v(1.0, 0.0);
        double theta = -2 * atan(3);
        Vec2d vref(cos(theta), sin(theta));
        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_EQ(1, hits);

        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-5, r[1]);

        EXPECT_FEQ(vref[0], v[0]);
        EXPECT_FEQ(vref[1], v[1]);

        v = {0, 1};
        r = {1, -5 + 1e-4};
        vref = {-sin(2 * atan(1 / 3.)), cos(2 * atan(1 / 3.))};
        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_EQ(2, hits);

        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-5, r[1]);

        EXPECT_FEQ(vref[0], v[0]);
        EXPECT_FEQ(vref[1], v[1]);
    }

    void boundaryline_handle_crossing_forbidlower() {
        Vec2d v = {0.0, -1.0};
        Vec2d r = {1.0, -5 - 1e-4};
        double theta = 2 * atan(1 / 3.);
        pair<int> cell;
        unsigned hits = 0;
        bool hit_flag = false;
        ;
        CompoundShape::BoundaryLine table(
            CompoundShape::BoundaryLine::ForbidLower, 1.5, 0.5, 1.0, 3.2, Vec2d(0.0, -2.0)
        );

        EXPECT_FALSE(table.point_is_inside(r));
        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_EQ(1, hits);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-5, r[1]);
        Vec2d vref = {sin(theta), -cos(theta)};
        EXPECT_FEQ(vref[0], v[0]);
        EXPECT_FEQ(vref[1], v[1]);

        v = {-1.0, 0.0};
        r = {1.0, -5 - 1e-4};
        theta = 2 * atan(-3.);
        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_EQ(2, hits);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-5, r[1]);
        vref = {-cos(theta), -sin(theta)};
        EXPECT_FEQ(vref[0], v[0]);
        EXPECT_FEQ(vref[1], v[1]);
    }

    void boundaryline_handle_crossing_bmap_forbidupper() {
        bool hit_flag = false;
        unsigned hits = 0;
        std::vector<std::vector<double>> bmap;
        CompoundShape::BoundaryLine table(
            CompoundShape::BoundaryLine::ForbidUpper, 1.5, 0.5, 1.0, 3.2, Vec2d(0.0, -2.0)
        );

        Vec2d r(1.0, -5 + 1e-4);
        Vec2d v(1.0, 0.0);
        double theta = -2 * atan(3);
        Vec2d vref(cos(theta), sin(theta));
        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_EQ(1, hits);

        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-5, r[1]);

        EXPECT_FEQ(vref[0], v[0]);
        EXPECT_FEQ(vref[1], v[1]);

        // Check bmap
        EXPECT_FEQ(3.2 - (Vec2d(0.0, -2.0) - r).norm(), bmap[0][1]);
        EXPECT_FEQ(-cos(atan(3.)), bmap[0][2]);
        EXPECT_FEQ(-(M_PI / 2 - atan(3.)), bmap[0][3]);

        v = {0.0, 1.0};
        r = {-1.0, 1 + 1e-4};
        vref = {-sin(2 * atan(1 / 3.)), cos(2 * atan(1 / 3.))};
        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_EQ(2, hits);

        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(1, r[1]);

        EXPECT_FEQ(vref[0], v[0]);
        EXPECT_FEQ(vref[1], v[1]);

        // Check bmap
        EXPECT_FEQ(3.2 + (Vec2d(0.0, -2.0) - r).norm(), bmap[1][1]);
        EXPECT_FEQ(cos(atan(1 / 3.)), bmap[1][2]);
        EXPECT_FEQ(M_PI / 2 - atan(1 / 3.), bmap[1][3]);
    }

    void boundaryline_handle_crossing_bmap_forbidlower() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        Vec2d v = {0.0, -1.0};
        Vec2d r = {1.0, -5 - 1e-4};
        double theta = 2 * atan(1 / 3.);
        unsigned hits = 0;
        CompoundShape::BoundaryLine table(
            CompoundShape::BoundaryLine::ForbidLower, 1.5, 0.5, 1.0, 3.2, Vec2d(0.0, -2.0)
        );

        EXPECT_FALSE(table.point_is_inside(r));
        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_EQ(1, hits);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-5, r[1]);
        Vec2d vref = {sin(theta), -cos(theta)};
        EXPECT_FEQ(vref[0], v[0]);
        EXPECT_FEQ(vref[1], v[1]);

        // Check bmap
        EXPECT_FEQ(3.2 + (Vec2d(0.0, -2.0) - r).norm(), bmap[0][1]);
        EXPECT_FEQ(cos(atan(1 / 3.)), bmap[0][2]);
        EXPECT_FEQ(M_PI / 2 - atan(1 / 3.), bmap[0][3]);

        v = {-1.0, 0.0};
        r = {-1.0, 1 - 1e-4};
        theta = 2 * atan(-3.);
        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_EQ(2, hits);
        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(1, r[1]);
        vref = {-cos(theta), -sin(theta)};
        EXPECT_FEQ(vref[0], v[0]);
        EXPECT_FEQ(vref[1], v[1]);

        // Check bmap
        EXPECT_FEQ(3.2 - (Vec2d(0.0, -2.0) - r).norm(), bmap[1][1]);
        EXPECT_FEQ(-cos(atan(3.)), bmap[1][2]);
        EXPECT_FEQ(-(M_PI / 2 - atan(3.)), bmap[1][3]);
    }

    void boundaryhorizontalline_point_is_inside() {
        CompoundShape::BoundaryHorizontalLine table(CompoundShape::BoundaryHorizontalLine::ForbidLower, 0.2, 0.3, 3);

        EXPECT_TRUE(table.point_is_inside({0.3, 0.4}));
        EXPECT_FALSE(table.point_is_inside({-4.3, 0.19}));
        EXPECT_TRUE(table.point_is_inside({5161.0, 153615.0}));
        EXPECT_FALSE(table.point_is_inside({0.0, -65465132.0}));

        CompoundShape::BoundaryHorizontalLine table2(CompoundShape::BoundaryHorizontalLine::ForbidUpper, 0.2, 0.3, 3);

        EXPECT_FALSE(table2.point_is_inside({0.3, 0.4}));
        EXPECT_TRUE(table2.point_is_inside({-4.3, 0.19}));
        EXPECT_FALSE(table2.point_is_inside({5161.0, 153615.0}));
        EXPECT_TRUE(table2.point_is_inside({0.0, -65465132.0}));
    }

    void boundaryhorizontalline_handle_crossing_forbidlower() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::BoundaryHorizontalLine table(CompoundShape::BoundaryHorizontalLine::ForbidLower, 0.2, 0.3, 3);

        Vec2d r(0.456, 0.2 - 1e-4);
        Vec2d v(2.0, -3.0);
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0.456, r[0]);
        EXPECT_AEQ(0.2, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(3, v[1]);
    }

    void boundaryhorizontalline_handle_crossing_forbidupper() {
        bool hit_flag = false;

        unsigned hits = 0;
        CompoundShape::BoundaryHorizontalLine table(CompoundShape::BoundaryHorizontalLine::ForbidUpper, 0.2, 0.3, 3);

        Vec2d r(0.456, 0.2 + 1e-4);
        Vec2d v(2.0, 3.0);
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0.456, r[0]);
        EXPECT_AEQ(0.2, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(-3, v[1]);
    }

    void boundaryhorizontalline_handle_crossing_bmap_forbidlower() {
        bool hit_flag = false;

        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryHorizontalLine table(CompoundShape::BoundaryHorizontalLine::ForbidLower, 0.2, 0.3, 3);

        Vec2d r(0.456, 0.2 - 1e-4);
        Vec2d v(2.0, -3.0);
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(0.456, r[0]);
        EXPECT_AEQ(0.2, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(3, v[1]);

        EXPECT_AEQ(0.3 - 3 + 0.456, bmap[0][1]);
        EXPECT_AEQ(2, bmap[0][2]);
        EXPECT_AEQ(M_PI / 2. - atan(3. / 2), bmap[0][3]);

        r = {5.0, 0.2 - 1e-4};
        v = {-2.0, -3.0};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(5, r[0]);
        EXPECT_AEQ(0.2, r[1]);

        EXPECT_FEQ(-2, v[0]);
        EXPECT_FEQ(3, v[1]);

        EXPECT_EQ(0.3 + 2, bmap[1][1]);
        EXPECT_EQ(-2, bmap[1][2]);
        EXPECT_EQ(-(M_PI / 2. - atan(3. / 2)), bmap[1][3]);
    }

    void boundaryhorizontalline_handle_crossing_bmap_forbidupper() {
        bool hit_flag = false;

        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryHorizontalLine table(CompoundShape::BoundaryHorizontalLine::ForbidUpper, 0.2, 0.3, 3);

        Vec2d r(0.456, 0.2 + 1e-4);
        Vec2d v(2.0, 3.0);
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(0.456, r[0]);
        EXPECT_AEQ(0.2, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(-3, v[1]);

        EXPECT_FEQ(0.3 + (3 - .456), bmap[0][1]);
        EXPECT_FEQ(-2, bmap[0][2]);
        EXPECT_FEQ(-atan(2. / 3), bmap[0][3]);

        r = {5.0, 0.2 + 1e-4};
        v = {-2.0, 3.0};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(5, r[0]);
        EXPECT_AEQ(0.2, r[1]);

        EXPECT_FEQ(-2, v[0]);
        EXPECT_FEQ(-3, v[1]);

        EXPECT_FEQ(0.3 - 2, bmap[1][1]);
        EXPECT_FEQ(2, bmap[1][2]);
        EXPECT_FEQ(atan(2 / 3.), bmap[1][3]);
    }

    void boundaryhorizontalray_point_is_inside() {
        CompoundShape::BoundaryHorizontalRay table1(
            CompoundShape::BoundaryHorizontalRay::ForbidUpper,
            CompoundShape::BoundaryHorizontalRay::Left,
            0.2,
            0.3,
            2.45
        );
        CompoundShape::BoundaryHorizontalRay table2(
            CompoundShape::BoundaryHorizontalRay::ForbidLower,
            CompoundShape::BoundaryHorizontalRay::Left,
            0.2,
            0.3,
            2.45
        );
        CompoundShape::BoundaryHorizontalRay table3(
            CompoundShape::BoundaryHorizontalRay::ForbidUpper,
            CompoundShape::BoundaryHorizontalRay::Right,
            0.2,
            0.3,
            2.45
        );
        CompoundShape::BoundaryHorizontalRay table4(
            CompoundShape::BoundaryHorizontalRay::ForbidLower,
            CompoundShape::BoundaryHorizontalRay::Right,
            0.2,
            0.3,
            2.45
        );

        EXPECT_TRUE(table1.point_is_inside({0.19, 0.25}));
        EXPECT_FALSE(table1.point_is_inside({0.19, 0.31}));
        EXPECT_TRUE(table1.point_is_inside({0.3, 5.0}));
        EXPECT_TRUE(table1.point_is_inside({5.0, -5.0}));

        EXPECT_TRUE(table2.point_is_inside({0.19, 0.31}));
        EXPECT_FALSE(table2.point_is_inside({-2.0, 0.29}));
        EXPECT_TRUE(table2.point_is_inside({0.3, 5.0}));
        EXPECT_TRUE(table2.point_is_inside({5.0, -5.0}));

        EXPECT_FALSE(table3.point_is_inside({0.21, 0.31}));
        EXPECT_TRUE(table3.point_is_inside({-2.0, 0.29}));
        EXPECT_TRUE(table3.point_is_inside({0.1, 5.0}));
        EXPECT_TRUE(table3.point_is_inside({5.0, -5.0}));

        EXPECT_FALSE(table4.point_is_inside({0.21, 0.29}));
        EXPECT_TRUE(table4.point_is_inside({-2.0, 0.29}));
        EXPECT_TRUE(table4.point_is_inside({0.1, -5.0}));
        EXPECT_TRUE(table4.point_is_inside({5.0, 5.0}));
    }

    void boundaryhorizontalray_handle_crossing_forbidupper() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::BoundaryHorizontalRay table(
            CompoundShape::BoundaryHorizontalRay::ForbidUpper,
            CompoundShape::BoundaryHorizontalRay::Left,
            0.2,
            0.3,
            2.45
        );

        Vec2d r(0.1, 0.3 + 1e-4);
        Vec2d v(2.0, 3.0);
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0.1, r[0]);
        EXPECT_AEQ(0.3, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(-3, v[1]);

        CompoundShape::BoundaryHorizontalRay table2(
            CompoundShape::BoundaryHorizontalRay::ForbidUpper,
            CompoundShape::BoundaryHorizontalRay::Right,
            0.2,
            0.3,
            2.45
        );

        r = {0.3, 0.3 + 1e-4};
        v = {2.0, 3.0};
        EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0.3, r[0]);
        EXPECT_AEQ(0.3, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(-3, v[1]);
    }

    void boundaryhorizontalray_handle_crossing_forbidlower() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::BoundaryHorizontalRay table(
            CompoundShape::BoundaryHorizontalRay::ForbidLower,
            CompoundShape::BoundaryHorizontalRay::Left,
            0.2,
            0.3,
            2.45
        );

        Vec2d r(0.1, 0.3 - 1e-4);
        Vec2d v(2.0, -3.0);
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0.1, r[0]);
        EXPECT_AEQ(0.3, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(3, v[1]);

        CompoundShape::BoundaryHorizontalRay table2(
            CompoundShape::BoundaryHorizontalRay::ForbidLower,
            CompoundShape::BoundaryHorizontalRay::Right,
            0.2,
            0.3,
            2.45
        );

        r = {0.3, 0.3 - 1e-4};
        v = {2.0, -3.0};
        EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0.3, r[0]);
        EXPECT_AEQ(0.3, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(3, v[1]);
    }

    void boundaryhorizontalray_handle_crossing_bmap_forbidupper() {
        bool hit_flag = false;

        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryHorizontalRay table(
            CompoundShape::BoundaryHorizontalRay::ForbidUpper,
            CompoundShape::BoundaryHorizontalRay::Left,
            0.2,
            0.3,
            2.45
        );

        Vec2d r(0.1, 0.3 + 1e-4);
        Vec2d v(2.0, 3.0);
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(0.1, r[0]);
        EXPECT_AEQ(0.3, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(-3, v[1]);

        EXPECT_FEQ(2.45 + 0.1, bmap.at(0)[1]);
        EXPECT_FEQ(-2, bmap.at(0)[2]);
        EXPECT_FEQ(-atan(2. / 3), bmap.at(0)[3]);

        CompoundShape::BoundaryHorizontalRay table2(
            CompoundShape::BoundaryHorizontalRay::ForbidUpper,
            CompoundShape::BoundaryHorizontalRay::Right,
            0.2,
            0.3,
            2.45
        );

        r = {0.3, 0.3 + 1e-4};
        v = {-2.0, 3.0};
        EXPECT_TRUE(table2.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(0.3, r[0]);
        EXPECT_AEQ(0.3, r[1]);

        EXPECT_FEQ(-2, v[0]);
        EXPECT_FEQ(-3, v[1]);

        EXPECT_FEQ(2.45 - 0.1, bmap.at(1)[1]);
        EXPECT_FEQ(2, bmap.at(1)[2]);
        EXPECT_FEQ(atan(2. / 3), bmap.at(1)[3]);
    }

    void boundaryhorizontalray_handle_crossing_bmap_forbidlower() {
        bool hit_flag = false;

        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryHorizontalRay table(
            CompoundShape::BoundaryHorizontalRay::ForbidLower,
            CompoundShape::BoundaryHorizontalRay::Left,
            0.2,
            0.3,
            2.45
        );

        Vec2d r(0.1, 0.3 - 1e-4);
        Vec2d v(2.0, -3.0);
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(0.1, r[0]);
        EXPECT_AEQ(0.3, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(3, v[1]);

        EXPECT_FEQ(2.45 - 0.1, bmap.at(0)[1]);
        EXPECT_FEQ(2, bmap.at(0)[2]);
        EXPECT_FEQ(atan(2. / 3), bmap.at(0)[3]);

        CompoundShape::BoundaryHorizontalRay table2(
            CompoundShape::BoundaryHorizontalRay::ForbidLower,
            CompoundShape::BoundaryHorizontalRay::Right,
            0.2,
            0.3,
            2.45
        );

        r = {0.3, 0.3 - 1e-4};
        v = {-2.0, -3.0};
        EXPECT_TRUE(table2.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(0.3, r[0]);
        EXPECT_AEQ(0.3, r[1]);

        EXPECT_FEQ(-2, v[0]);
        EXPECT_FEQ(3, v[1]);

        EXPECT_FEQ(2.45 + 0.1, bmap.at(1)[1]);
        EXPECT_FEQ(-2, bmap.at(1)[2]);
        EXPECT_FEQ(-atan(2. / 3), bmap.at(1)[3]);
    }

    void boundarycircle_point_is_inside() {
        CompoundShape::BoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0.0, 0.0},
            0.5,
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table.point_is_inside({0.2, 0.2}));
        EXPECT_FALSE(table.point_is_inside({0.6, 0.2}));

        CompoundShape::BoundaryCircle table2(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0.0, 0.0},
            0.5,
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_FALSE(table2.point_is_inside({0.2, 0.2}));
        EXPECT_TRUE(table2.point_is_inside({0.6, 0.2}));

        CompoundShape::BoundaryCircle table3(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0.0, 0.0},
            0.5,
            CompoundShape::BoundaryCircle::Left,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table3.point_is_inside({0.2, 0.2}));
        EXPECT_TRUE(table3.point_is_inside({0.6, 0.2}));

        EXPECT_TRUE(table3.point_is_inside({-0.3, 0.2}));
        EXPECT_FALSE(table3.point_is_inside({-.6, 0.0}));

        CompoundShape::BoundaryCircle table4(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0.0, 0.0},
            0.5,
            CompoundShape::BoundaryCircle::Left,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table4.point_is_inside({0.2, 0.2}));
        EXPECT_TRUE(table4.point_is_inside({0.6, 0.2}));

        EXPECT_FALSE(table4.point_is_inside({-0.3, 0.2}));
        EXPECT_TRUE(table4.point_is_inside({-.6, 0.0}));
        EXPECT_TRUE(table4.point_is_inside({0.4, 0.4}));

        CompoundShape::BoundaryCircle table5(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0.0, 0.0},
            0.5,
            CompoundShape::BoundaryCircle::Top,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_FALSE(table5.point_is_inside({0.2, 0.2}));
        EXPECT_TRUE(table5.point_is_inside({0.6, 0.2}));
        EXPECT_FALSE(table5.point_is_inside({0.3, 0.3}));
        EXPECT_FALSE(table5.point_is_inside({-0.3, 0.2}));
        EXPECT_TRUE(table5.point_is_inside({-.6, .1}));

        CompoundShape::BoundaryCircle table6(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0.0, 0.0},
            0.5,
            CompoundShape::BoundaryCircle::Top,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_FALSE(table6.point_is_inside({0.6, 0.1}));
        EXPECT_TRUE(table6.point_is_inside({0.3, 0.3}));
        EXPECT_TRUE(table6.point_is_inside({-1.0, -1.0}));

        CompoundShape::BoundaryCircle table7(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0.0, 0.0},
            0.5,
            CompoundShape::BoundaryCircle::Right,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_FALSE(table7.point_is_inside({0.6, 0.1}));
        EXPECT_TRUE(table7.point_is_inside({0.3, 0.3}));
        EXPECT_TRUE(table7.point_is_inside({-1.0, -1.0}));

        CompoundShape::BoundaryCircle table8(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0.0, 0.0},
            0.5,
            CompoundShape::BoundaryCircle::Right,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table8.point_is_inside({0.6, 0.1}));
        EXPECT_FALSE(table8.point_is_inside({0.3, 0.3}));
        EXPECT_FALSE(table8.point_is_inside({0.3, -0.3}));
        EXPECT_TRUE(table8.point_is_inside({-1.0, -1.0}));

        CompoundShape::BoundaryCircle table9(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0.0, 0.0},
            0.5,
            CompoundShape::BoundaryCircle::Bottom,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table9.point_is_inside({0.2, 0.2}));
        EXPECT_FALSE(table9.point_is_inside({-0.3, -0.3}));
        EXPECT_FALSE(table9.point_is_inside({0.3, -0.3}));
        EXPECT_TRUE(table9.point_is_inside({0.0, -5}));

        CompoundShape::BoundaryCircle table10(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0.0, 0.0},
            0.5,
            CompoundShape::BoundaryCircle::Bottom,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table10.point_is_inside({-0.3, -0.3}));
        EXPECT_TRUE(table10.point_is_inside({0.3, -0.3}));
        EXPECT_TRUE(table10.point_is_inside({0.2, 0.2}));
        EXPECT_FALSE(table10.point_is_inside({0.0, -5}));
    }

    void boundarycircle_handle_crossing_forbidinside() {
        unsigned hits = 0;
        bool hit_flag = false;

        CompoundShape::BoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0.0, 0.0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ADown
        );

        Vec2d r(1 - 1e-5, 1 - 1e-5);
        Vec2d v(-1.0, -1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);

        r = {-1 + 1e-5, -1 + 1e-5};
        v = {1.0, 1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(-1, r[1]);

        r = {-1 + 1e-5, 1 - 1e-5};
        v = {1.0, -1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(1, r[1]);

        r = {1 - 1e-5, -1 + 1e-5};
        v = {-1.0, 1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);

        r = {1 - 1e-5, -1 + 1e-5};
        v = {0.0, 1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(1, v[0]);
        EXPECT_AEQ(0, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);

        r = {1 - 1e-5, -1 + 1e-5};
        v = {-1.0, 0.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0, v[0]);
        EXPECT_AEQ(-1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);

        r = {1 - 1e-5, -1 + 1e-5};
        v = {0.5, 1};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(1, v[0]);
        EXPECT_AEQ(0.5, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);
    }

    void boundarycircle_handle_crossing_forbidoutside() {
        unsigned hits = 0;
        bool hit_flag = false;

        CompoundShape::BoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0.0, 0.0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ADown
        );

        Vec2d r(1 + 1e-5, 1 + 1e-5);
        Vec2d v(1.0, 1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);

        r = {-1 - 1e-5, -1 - 1e-5};
        v = {-1.0, -1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(-1, r[1]);

        r = {-1 - 1e-5, 1 + 1e-5};
        v = {-1.0, 1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(1, r[1]);

        r = {1 + 1e-5, -1 - 1e-5};
        v = {1.0, -1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);

        r = {1 + 1e-5, -1 - 1e-5};
        v = {0.0, -1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(-1, v[0]);
        EXPECT_AEQ(0, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);

        r = {1 + 1e-5, -1 - 1e-5};
        v = {1.0, 0.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0, v[0]);
        EXPECT_AEQ(1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);

        r = {1 + 1e-5, -1 - 1e-5};
        v = {0.5, -1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(-1, v[0]);
        EXPECT_AEQ(0.5, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);
    }

    void boundarycircle_handle_crossing_forbidinside_CCW_S0Right_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0.0, 0.0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ARight,
            CompoundShape::BoundaryCircle::COUNTERCLOCKWISE
        );

        Vec2d r(1.0, 1 - 1e-5);
        Vec2d v(-0.5, -1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);

        EXPECT_AEQ(M_PI / 4. * sqrt(2), bmap.at(0)[1]);
        EXPECT_AEQ(-(M_PI / 4 - atan(0.5)), bmap.at(0)[3]);
        EXPECT_AEQ(sin(-(M_PI / 4 - atan(0.5))) * sqrt(1.25), bmap.at(0)[2]);

        r = {-1.0, -1 + 1e-5};
        v = {0.3, 0.4};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ((M_PI + M_PI / 4.) * sqrt(2), bmap.at(1)[1]);
        EXPECT_AEQ(-(atan(0.4 / 0.3) - atan(1)), bmap.at(1)[3]);
        EXPECT_AEQ(sin(-(atan(0.4 / 0.3) - atan(1))) * sqrt(0.3 * 0.3 + 0.4 * 0.4), bmap.at(1)[2]);
    }

    void boundarycircle_handle_crossing_forbidoutside_CCW_S0Right_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0.0, 0.0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ARight,
            CompoundShape::BoundaryCircle::COUNTERCLOCKWISE
        );

        Vec2d r(1.0, 1 + 1e-5);
        Vec2d v(0.5, 1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);

        EXPECT_AEQ(M_PI / 4. * sqrt(2), bmap.at(0)[1]);
        EXPECT_AEQ((M_PI / 4 - atan(0.5)), bmap.at(0)[3]);
        EXPECT_AEQ(sin((M_PI / 4 - atan(0.5))) * sqrt(1.25), bmap.at(0)[2]);

        r = {-1.0, -1 - 1e-5};
        v = {-0.3, -0.4};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ((M_PI + M_PI / 4.) * sqrt(2), bmap.at(1)[1]);
        EXPECT_AEQ((atan(0.4 / 0.3) - atan(1)), bmap.at(1)[3]);
        EXPECT_AEQ(sin((atan(0.4 / 0.3) - atan(1))) * sqrt(0.3 * 0.3 + 0.4 * 0.4), bmap.at(1)[2]);
    }

    void boundarycircle_handle_crossing_forbidinside_CCW_S0Down_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0.0, 0.0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ADown,
            CompoundShape::BoundaryCircle::COUNTERCLOCKWISE
        );

        Vec2d r(1.0, 1 - 1e-5);
        Vec2d v(-0.5, -1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(1.0, r[0]);
        EXPECT_AEQ(1.0, r[1]);

        EXPECT_AEQ((3 * M_PI / 4.) * sqrt(2), bmap.at(0)[1]);
        EXPECT_AEQ(-(M_PI / 4 - atan(0.5)), bmap.at(0)[3]);
        EXPECT_AEQ(sin(-(M_PI / 4 - atan(0.5))) * sqrt(1.25), bmap.at(0)[2]);

        r = {-1.0, -1 + 1e-5};
        v = {0.3, 0.4};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ((-M_PI / 4.) * sqrt(2), bmap.at(1)[1]);
        EXPECT_AEQ(-(atan(0.4 / 0.3) - atan(1)), bmap.at(1)[3]);
        EXPECT_AEQ(sin(-(atan(0.4 / 0.3) - atan(1))) * sqrt(0.3 * 0.3 + 0.4 * 0.4), bmap.at(1)[2]);
    }

    void boundarycircle_handle_crossing_forbidoutside_CCW_S0Down_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ADown,
            CompoundShape::BoundaryCircle::COUNTERCLOCKWISE
        );

        Vec2d r(1.0, 1 + 1e-5);
        Vec2d v(0.5, 1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);

        EXPECT_AEQ((M_PI / 2 + M_PI / 4.) * sqrt(2), bmap.at(0)[1]);
        EXPECT_AEQ((M_PI / 4 - atan(0.5)), bmap.at(0)[3]);
        EXPECT_AEQ(sin((M_PI / 4 - atan(0.5))) * sqrt(1.25), bmap.at(0)[2]);

        r = {-1, -1 - 1e-5};
        v = {-0.3, -0.4};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ((-M_PI / 4.) * sqrt(2), bmap.at(1)[1]);
        EXPECT_AEQ((atan(0.4 / 0.3) - atan(1)), bmap.at(1)[3]);
        EXPECT_AEQ(sin((atan(0.4 / 0.3) - atan(1))) * sqrt(0.3 * 0.3 + 0.4 * 0.4), bmap.at(1)[2]);
    }

    void boundarycircle_handle_crossing_forbidinside_CW_S0Right_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ARight,
            CompoundShape::BoundaryCircle::CLOCKWISE
        );

        Vec2d r(1.0, 1 - 1e-5);
        Vec2d v(-0.5, -1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);

        EXPECT_AEQ((2 * M_PI - M_PI / 4.) * sqrt(2), bmap.at(0)[1]);
        EXPECT_AEQ((M_PI / 4 - atan(0.5)), bmap.at(0)[3]);
        EXPECT_AEQ(sin((M_PI / 4 - atan(0.5))) * sqrt(1.25), bmap.at(0)[2]);

        r = {-1.0, -1 + 1e-5};
        v = {0.3, 0.4};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ((3 * M_PI / 4.) * sqrt(2), bmap.at(1)[1]);
        EXPECT_AEQ((atan(0.4 / 0.3) - atan(1)), bmap.at(1)[3]);
        EXPECT_AEQ(sin((atan(0.4 / 0.3) - atan(1))) * sqrt(0.3 * 0.3 + 0.4 * 0.4), bmap.at(1)[2]);
    }

    void boundarycircle_handle_crossing_forbidoutside_CW_S0Right_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ARight,
            CompoundShape::BoundaryCircle::CLOCKWISE
        );

        Vec2d r(1.0, 1 + 1e-5);
        Vec2d v(0.5, 1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);

        EXPECT_AEQ((2 * M_PI - M_PI / 4.) * sqrt(2), bmap.at(0)[1]);
        EXPECT_AEQ(-(M_PI / 4 - atan(0.5)), bmap.at(0)[3]);
        EXPECT_AEQ(sin(-(M_PI / 4 - atan(0.5))) * sqrt(1.25), bmap.at(0)[2]);

        r = {-1.0, -1 - 1e-5};
        v = {-0.3, -0.4};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ((3 * M_PI / 4.) * sqrt(2), bmap.at(1)[1]);
        EXPECT_AEQ(-(atan(0.4 / 0.3) - atan(1)), bmap.at(1)[3]);
        EXPECT_AEQ(sin(-(atan(0.4 / 0.3) - atan(1))) * sqrt(0.3 * 0.3 + 0.4 * 0.4), bmap.at(1)[2]);
    }

    void boundarycircle_handle_crossing_forbidinside_CW_S0Down_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ADown,
            CompoundShape::BoundaryCircle::CLOCKWISE
        );

        Vec2d r(1.0, 1 - 1e-5);
        Vec2d v(-0.5, -1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);

        EXPECT_AEQ(-(3 * M_PI / 4.) * sqrt(2), bmap.at(0)[1]);
        EXPECT_AEQ((M_PI / 4 - atan(0.5)), bmap.at(0)[3]);
        EXPECT_AEQ(sin((M_PI / 4 - atan(0.5))) * sqrt(1.25), bmap.at(0)[2]);

        r = {-1.0, -1 + 1e-5};
        v = {0.3, 0.4};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ((M_PI / 4.) * sqrt(2), bmap.at(1)[1]);
        EXPECT_AEQ((atan(0.4 / 0.3) - atan(1)), bmap.at(1)[3]);
        EXPECT_AEQ(sin((atan(0.4 / 0.3) - atan(1))) * sqrt(0.3 * 0.3 + 0.4 * 0.4), bmap.at(1)[2]);
    }

    void boundarycircle_handle_crossing_forbidoutside_CW_S0Down_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            0,
            CompoundShape::BoundaryCircle::ADown,
            CompoundShape::BoundaryCircle::CLOCKWISE
        );

        Vec2d r(1.0, 1 + 1e-5);
        Vec2d v(0.5, 1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);

        EXPECT_AEQ(-(M_PI / 2 + M_PI / 4.) * sqrt(2), bmap.at(0)[1]);
        EXPECT_AEQ(-(M_PI / 4 - atan(0.5)), bmap.at(0)[3]);
        EXPECT_AEQ(sin(-(M_PI / 4 - atan(0.5))) * sqrt(1.25), bmap.at(0)[2]);

        r = {-1.0, -1 - 1e-5};
        v = {-0.3, -0.4};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ((M_PI / 4.) * sqrt(2), bmap.at(1)[1]);
        EXPECT_AEQ(-(atan(0.4 / 0.3) - atan(1)), bmap.at(1)[3]);
        EXPECT_AEQ(sin(-(atan(0.4 / 0.3) - atan(1))) * sqrt(0.3 * 0.3 + 0.4 * 0.4), bmap.at(1)[2]);
    }

    void boundaryellipse_point_is_inside() {
        CompoundShape::BoundaryEllipse table(4, 3, 0);

        EXPECT_TRUE(table.point_is_inside({0.0, 0.0}));
        EXPECT_FALSE(table.point_is_inside({0.0, 4.1}));
        EXPECT_TRUE(table.point_is_inside({3.5, 0.0}));
        EXPECT_TRUE(table.point_is_inside({-3.5, 0.0}));
        EXPECT_FALSE(table.point_is_inside({0.0, -4.1}));
        EXPECT_FALSE(table.point_is_inside({0.0, 3.99}));
        EXPECT_FALSE(table.point_is_inside({0.0, -3.99}));
        EXPECT_TRUE(table.point_is_inside({0.0, 2.99}));
        EXPECT_TRUE(table.point_is_inside({0.0, -2.99}));
        EXPECT_TRUE(table.point_is_inside({2, 2}));
        EXPECT_TRUE(table.point_is_inside({3, 1.5}));
        EXPECT_FALSE(table.point_is_inside({3, 3}));

        CompoundShape::BoundaryEllipse table2(3, 4, 0);

        EXPECT_TRUE(table2.point_is_inside({0.0, 0.0}));
        EXPECT_FALSE(table2.point_is_inside({4.1, 0.0}));
        EXPECT_TRUE(table2.point_is_inside({0.0, 3.5}));
        EXPECT_TRUE(table2.point_is_inside({0.0, 3.5}));
        EXPECT_TRUE(table2.point_is_inside({0.0, -3.5}));
        EXPECT_FALSE(table2.point_is_inside({-4.1, 0.0}));
        EXPECT_FALSE(table2.point_is_inside({3.99, 0.0}));
        EXPECT_FALSE(table2.point_is_inside({-3.99, 0.0}));
        EXPECT_TRUE(table2.point_is_inside({2.99, 0.0}));
        EXPECT_TRUE(table2.point_is_inside({-2.99, 0.0}));
        EXPECT_TRUE(table2.point_is_inside({2.0, 2.0}));
        EXPECT_TRUE(table2.point_is_inside({1.5, 3.0}));
        EXPECT_FALSE(table2.point_is_inside({3.0, 3.0}));
    }

    void boundaryellipse_handle_crossing() {
        double a = 4;
        double b = 3;
        CompoundShape::BoundaryEllipse table(4, 3, 0);
        unsigned hits = 0;
        bool hit_flag = false;

        Vec2d r = {1 + 1e-5, 3 * sqrt(1 - 1. / 16)};
        Vec2d v = {1.0, 1.0};
        EXPECT_FALSE(table.point_is_inside(r));

        Vec2d normal = {b / a * 1, a / b * 3 * sqrt(1 - 1. / 16)};
        normal.scale_to_unit();

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(3 * sqrt(1 - 1. / 16), r[1]);

        EXPECT_AEQ(1 - 2 * normal.dot({1, 1}) * normal[0], v[0]);
        EXPECT_AEQ(1 - 2 * normal.dot({1, 1}) * normal[1], v[1]);

        r = {1 + 1e-5, -3 * sqrt(1 - 1. / 16)};
        v = {1.0, -1.0};
        EXPECT_FALSE(table.point_is_inside(r));

        normal = {b / a * 1, -a / b * 3 * sqrt(1 - 1. / 16)};
        normal.scale_to_unit();

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-3 * sqrt(1 - 1. / 16), r[1]);

        EXPECT_AEQ(1 - 2 * normal.dot({1, -1}) * normal[0], v[0]);
        EXPECT_AEQ(-1 - 2 * normal.dot({1, -1}) * normal[1], v[1]);

        r = {-1 - 1e-5, -3 * sqrt(1 - 1. / 16)};
        v = {-1.0, -1.0};
        EXPECT_FALSE(table.point_is_inside(r));

        normal = {-b / a * 1, -a / b * 3 * sqrt(1 - 1. / 16)};
        normal.scale_to_unit();

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(-3 * sqrt(1 - 1. / 16), r[1]);

        EXPECT_AEQ(-1 - 2 * normal.dot({-1, -1}) * normal[0], v[0]);
        EXPECT_AEQ(-1 - 2 * normal.dot({-1, -1}) * normal[1], v[1]);

        r = {-1 - 1e-5, 3 * sqrt(1 - 1. / 16)};
        v = {-1.0, 1.0};
        EXPECT_FALSE(table.point_is_inside(r));

        normal = {-b / a * 1, a / b * 3 * sqrt(1 - 1. / 16)};
        normal.scale_to_unit();

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(3 * sqrt(1 - 1. / 16), r[1]);

        EXPECT_AEQ(-1 - 2 * normal.dot({-1, 1}) * normal[0], v[0]);
        EXPECT_AEQ(1 - 2 * normal.dot({-1, 1}) * normal[1], v[1]);

        EXPECT_EQ(4, hits);
    }

    void boundaryellipse_handle_crossing_bmap() {
        std::vector<std::vector<double>> bmap;
        bool hit_flag = false;
        double a = 4;
        double b = 3;
        CompoundShape::BoundaryEllipse table(4, 3, 0);
        unsigned hits = 0;

        Vec2d r = {1 + 1e-8, 3 * sqrt(1 - 1. / 16)};
        Vec2d v = {1.0, 1.0};

        Vec2d normal = {b / a * 1, a / b * 3 * sqrt(1 - 1. / 16)};
        normal.scale_to_unit();

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(3 * sqrt(1 - 1. / 16), r[1]);

        EXPECT_AEQ(1 - 2 * normal.dot({1, 1}) * normal[0], v[0]);
        EXPECT_AEQ(1 - 2 * normal.dot({1, 1}) * normal[1], v[1]);

        EXPECT_FEQ5(4.519817057685657, bmap.at(0)[1]);

        Vec2d tangent = {-a / b * 3 * sqrt(1 - 1. / 16), b / a * 1};
        tangent.scale_to_unit();

        EXPECT_AEQ(tangent.dot({1, 1}), bmap.at(0)[2]);

        EXPECT_AEQ(-acos(normal.dot({1, 1}) / sqrt(2)), bmap.at(0)[3]);

        r = {1 + 1e-5, -3 * sqrt(1 - 1. / 16)};
        v = {1.0, -1.0};

        normal = {b / a * 1, -a / b * 3 * sqrt(1 - 1. / 16)};
        normal.scale_to_unit();

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-3 * sqrt(1 - 1. / 16), r[1]);

        EXPECT_AEQ(1 - 2 * normal.dot({1, -1}) * normal[0], v[0]);
        EXPECT_AEQ(-1 - 2 * normal.dot({1, -1}) * normal[1], v[1]);

        tangent = {a / b * 3 * sqrt(1 - 1. / 16), b / a * 1};
        tangent.scale_to_unit();

        EXPECT_AEQ(tangent.dot({1, -1}), bmap.at(1)[2]);

        EXPECT_AEQ(acos(normal.dot({1, -1}) / sqrt(2)), bmap.at(1)[3]);

        EXPECT_FEQ5(17.58367510302409, bmap.at(1)[1]);

        r = {-1 - 1e-5, -3 * sqrt(1 - 1. / 16)};
        v = {-1.0, -1.0};

        normal = {-b / a * 1, -a / b * 3 * sqrt(1 - 1. / 16)};
        normal.scale_to_unit();

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(-3 * sqrt(1 - 1. / 16), r[1]);

        EXPECT_AEQ(-1 - 2 * normal.dot({-1, -1}) * normal[0], v[0]);
        EXPECT_AEQ(-1 - 2 * normal.dot({-1, -1}) * normal[1], v[1]);

        tangent = {a / b * 3 * sqrt(1 - 1. / 16), -b / a * 1};
        tangent.scale_to_unit();

        EXPECT_AEQ(tangent.dot({-1, -1}), bmap.at(2)[2]);

        EXPECT_AEQ(-acos(normal.dot({-1, -1}) / sqrt(2)), bmap.at(2)[3]);

        EXPECT_FEQ5(15.571563138040784, bmap.at(2)[1]);

        r = {-1 - 1e-5, 3 * sqrt(1 - 1. / 16)};
        v = {-1.0, 1.0};

        normal = {-b / a * 1, a / b * 3 * sqrt(1 - 1. / 16)};
        normal.scale_to_unit();

        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(3 * sqrt(1 - 1. / 16), r[1]);

        EXPECT_AEQ(-1 - 2 * normal.dot({-1, 1}) * normal[0], v[0]);
        EXPECT_AEQ(1 - 2 * normal.dot({-1, 1}) * normal[1], v[1]);

        tangent = {-a / b * 3 * sqrt(1 - 1. / 16), -b / a * 1};
        tangent.scale_to_unit();

        EXPECT_AEQ(tangent.dot({-1, 1}), bmap.at(3)[2]);

        EXPECT_AEQ(acos(normal.dot({-1, 1}) / sqrt(2)), bmap.at(3)[3]);

        EXPECT_FEQ5(6.53192902266258, bmap.at(3)[1]);

        EXPECT_EQ(4, hits);
    }

    void boundaryrectangle_point_is_inside() {
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidOutside, 0, 2, 0, 5, 0);
        Vec2d r(0.5, 0.5);
        ASSERT_TRUE(table.point_is_inside(r));
        r = {2.1, 0.5};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {-0.1, 0.5};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {0.5, -0.1};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {0.5, 5.5};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {0.99, 4.9};
        ASSERT_TRUE(table.point_is_inside(r));

        CompoundShape::BoundaryRectangle table2(CompoundShape::BoundaryRectangle::ForbidInside, 0, 2, 0, 5, 0);
        r = {0.5, 0.5};
        ASSERT_FALSE(table2.point_is_inside(r));
        r = {2.1, 0.5};
        EXPECT_TRUE(table2.point_is_inside(r));
        r = {-0.1, 0.5};
        EXPECT_TRUE(table2.point_is_inside(r));
        r = {0.5, -0.1};
        EXPECT_TRUE(table2.point_is_inside(r));
        r = {0.5, 5.5};
        EXPECT_TRUE(table2.point_is_inside(r));
        r = {0.99, 4.9};
        ASSERT_FALSE(table2.point_is_inside(r));
    }

    void boundaryrectangle_handle_crossing_left_forbidoutside() {
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0);
        unsigned hits = 0;
        bool hit_flag = false;

        Vec2d r(-1e-4, 0.5);
        Vec2d v(-1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.5, r[1]);
        EXPECT_EQ(1, hits);

        r = {-1e-8, 0.6};
        v = {-0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.6, r[1]);

        EXPECT_EQ(2, hits);
    }

    void boundaryrectangle_handle_crossing_bottom_forbidoutside() {
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0);
        unsigned hits = 0;

        bool hit_flag = false;
        Vec2d r(0.4, -1e-8);
        Vec2d v(-1.0, -1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0.4, r[0]);
        EXPECT_AEQ(0, r[1]);
        EXPECT_EQ(1, hits);

        r = {0.6, -1e-8};
        v = {0.8, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.8, v[0]);
        EXPECT_FEQ(0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(0, r[1]);

        EXPECT_EQ(2, hits);
    }

    void boundaryrectangle_handle_crossing_right_forbidoutside() {
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0);
        unsigned hits = 0;

        Vec2d r(1.2 + 1e-8, 15.0);
        Vec2d v(1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(15, r[1]);
        EXPECT_EQ(1, hits);

        r = {1.2 + 1e-8, 18.0};
        v = {0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(18, r[1]);

        EXPECT_EQ(2, hits);
    }

    void boundaryrectangle_handle_crossing_top_forbidoutside() {
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0);
        unsigned hits = 0;

        Vec2d r(0.5, 25 + 1e-8);
        Vec2d v(1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(0.5, r[0]);
        EXPECT_AEQ(25, r[1]);
        EXPECT_EQ(1, hits);

        r = {0.6, 25 + 1e-8};
        v = {-0.5, 0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(25, r[1]);

        EXPECT_EQ(2, hits);
    }

    void boundaryrectangle_handle_crossing_left_forbidinside() {
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0);
        unsigned hits = 0;

        Vec2d r(1e-4, 0.5);
        Vec2d v(1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.5, r[1]);
        EXPECT_EQ(1, hits);

        r = {1e-8, 0.6};
        v = {0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.6, r[1]);

        EXPECT_EQ(2, hits);
    }

    void boundaryrectangle_handle_crossing_bottom_forbidinside() {
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0);
        unsigned hits = 0;

        Vec2d r(0.4, 1e-8);
        Vec2d v(-1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(0.4, r[0]);
        EXPECT_AEQ(0, r[1]);
        EXPECT_EQ(1, hits);

        r = {0.6, 1e-8};
        v = {0.8, 0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.8, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(0, r[1]);

        EXPECT_EQ(2, hits);
    }

    void boundaryrectangle_handle_crossing_right_forbidinside() {
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0);
        unsigned hits = 0;

        Vec2d r(1.2 - 1e-8, 15.0);
        Vec2d v(-1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(15, r[1]);
        EXPECT_EQ(1, hits);

        r = {1.2 - 1e-8, 18.0};
        v = {-0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(18, r[1]);

        EXPECT_EQ(2, hits);
    }

    void boundaryrectangle_handle_crossing_top_forbidinside() {
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0);
        unsigned hits = 0;

        Vec2d r(0.5, 25 - 1e-8);
        Vec2d v(1.0, -1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0.5, r[0]);
        EXPECT_AEQ(25, r[1]);
        EXPECT_EQ(1, hits);

        r = {0.6, 25 - 1e-8};
        v = {-0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(25, r[1]);

        EXPECT_EQ(2, hits);
    }

    void boundaryrectangle_handle_crossing_left_forbidoutside_bmap() {
        std::vector<std::vector<double>> bmap;
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 9.87);
        unsigned hits = 0;

        Vec2d r(-1e-4, 0.5);
        Vec2d v(-1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.5, r[1]);
        EXPECT_EQ(1, hits);

        EXPECT_FEQ5(9.87 + 2.4 + 25 + 25 - 0.5, bmap.at(0)[1]);
        EXPECT_FEQ5(-1, bmap.at(0)[2]);
        EXPECT_FEQ5(-M_PI / 4., bmap.at(0)[3]);

        r = {-1e-8, 0.6};
        v = {-0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.6, r[1]);

        EXPECT_EQ(2, hits);

        EXPECT_FEQ5(9.87 + 2.4 + 25 + 25 - 0.6, bmap.at(1)[1]);
        EXPECT_FEQ5(0.1, bmap.at(1)[2]);
        EXPECT_FEQ5(atan(0.1 / 0.5), bmap.at(1)[3]);
    }

    void boundaryrectangle_handle_crossing_bottom_forbidoutside_bmap() {
        std::vector<std::vector<double>> bmap;
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidOutside, 0.1, 1.2, 0, 25, 9.87);
        unsigned hits = 0;

        Vec2d r(0.4, -1e-8);
        Vec2d v(-1.0, -1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0.4, r[0]);
        EXPECT_AEQ(0, r[1]);
        EXPECT_EQ(1, hits);

        EXPECT_FEQ5(9.87 + 0.4 - 0.1, bmap.at(0)[1]);
        EXPECT_FEQ5(-1, bmap.at(0)[2]);
        EXPECT_FEQ5(-M_PI / 4., bmap.at(0)[3]);

        r = {0.6, -1e-8};
        v = {0.8, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.8, v[0]);
        EXPECT_FEQ(0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(0, r[1]);

        EXPECT_EQ(2, hits);

        EXPECT_FEQ5(9.87 + 0.6 - 0.1, bmap.at(1)[1]);
        EXPECT_FEQ5(0.8, bmap.at(1)[2]);
        EXPECT_FEQ5(atan(0.8 / 0.1), bmap.at(1)[3]);
    }

    void boundaryrectangle_handle_crossing_right_forbidoutside_bmap() {
        std::vector<std::vector<double>> bmap;

        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 1, 25, 9.87);
        unsigned hits = 0;

        Vec2d r(1.2 + 1e-8, 15.0);
        Vec2d v(1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(15, r[1]);
        EXPECT_EQ(1, hits);

        EXPECT_FEQ5(9.87 + 1.2 + 14, bmap.at(0)[1]);
        EXPECT_FEQ5(1, bmap.at(0)[2]);
        EXPECT_FEQ5(M_PI / 4., bmap.at(0)[3]);

        r = {1.2 + 1e-8, 18.0};
        v = {0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(18, r[1]);

        EXPECT_EQ(2, hits);

        EXPECT_FEQ5(9.87 + 1.2 + 17, bmap.at(1)[1]);
        EXPECT_FEQ5(-0.1, bmap.at(1)[2]);
        EXPECT_FEQ5(-atan(0.1 / 0.5), bmap.at(1)[3]);
    }

    void boundaryrectangle_handle_crossing_top_forbidoutside_bmap() {
        std::vector<std::vector<double>> bmap;
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 9.87);
        unsigned hits = 0;

        Vec2d r(0.5, 25 + 1e-8);
        Vec2d v(1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(0.5, r[0]);
        EXPECT_AEQ(25, r[1]);
        EXPECT_EQ(1, hits);

        EXPECT_FEQ5(9.87 + 1.2 + 25 + 1.2 - 0.5, bmap.at(0)[1]);
        EXPECT_FEQ5(-1, bmap.at(0)[2]);
        EXPECT_FEQ5(-M_PI / 4., bmap.at(0)[3]);

        r = {0.6, 25 + 1e-8};
        v = {-0.5, 0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(25, r[1]);

        EXPECT_EQ(2, hits);

        EXPECT_FEQ5(9.87 + 1.2 + 25 + 1.2 - 0.6, bmap.at(1)[1]);
        EXPECT_FEQ5(0.5, bmap.at(1)[2]);
        EXPECT_FEQ5(atan(0.5 / 0.1), bmap.at(1)[3]);
    }

    void boundaryrectangle_handle_crossing_left_forbidinside_bmap() {
        std::vector<std::vector<double>> bmap;
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 9.87);
        unsigned hits = 0;

        Vec2d r(1e-4, 0.5);
        Vec2d v(1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.5, r[1]);
        EXPECT_EQ(1, hits);

        EXPECT_FEQ5(9.87 + 2 * 1.2 + 25 + 25 - 0.5, bmap.at(0)[1]);
        EXPECT_FEQ5(-1, bmap.at(0)[2]);
        EXPECT_FEQ5(-M_PI / 4., bmap.at(0)[3]);

        r = {1e-8, 0.6};
        v = {0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.6, r[1]);

        EXPECT_EQ(2, hits);

        EXPECT_FEQ5(9.87 + 2 * 1.2 + 25 + 25 - 0.6, bmap.at(1)[1]);
        EXPECT_FEQ5(0.1, bmap.at(1)[2]);
        EXPECT_FEQ5(atan(0.1 / 0.5), bmap.at(1)[3]);
    }

    void boundaryrectangle_handle_crossing_bottom_forbidinside_bmap() {
        std::vector<std::vector<double>> bmap;
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidInside, 0.2, 1.2, 0, 25, 9.87);
        unsigned hits = 0;

        Vec2d r(0.4, 1e-8);
        Vec2d v(-1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(0.4, r[0]);
        EXPECT_AEQ(0, r[1]);
        EXPECT_EQ(1, hits);

        EXPECT_FEQ5(9.87 + 0.2, bmap.at(0)[1]);
        EXPECT_FEQ5(-1, bmap.at(0)[2]);
        EXPECT_FEQ5(-M_PI / 4., bmap.at(0)[3]);

        r = {0.6, 1e-8};
        v = {0.8, 0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.8, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(0, r[1]);

        EXPECT_EQ(2, hits);

        EXPECT_FEQ5(9.87 + 0.4, bmap.at(1)[1]);
        EXPECT_FEQ5(0.8, bmap.at(1)[2]);
        EXPECT_FEQ5(atan(8), bmap.at(1)[3]);
    }

    void boundaryrectangle_handle_crossing_right_forbidinside_bmap() {
        std::vector<std::vector<double>> bmap;
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 9.87);
        unsigned hits = 0;

        Vec2d r(1.2 - 1e-8, 15.0);
        Vec2d v(-1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(15, r[1]);
        EXPECT_EQ(1, hits);

        EXPECT_FEQ5(9.87 + 1.2 + 15, bmap.at(0)[1]);
        EXPECT_FEQ5(1, bmap.at(0)[2]);
        EXPECT_FEQ5(M_PI / 4., bmap.at(0)[3]);

        r = {1.2 - 1e-8, 18.0};
        v = {-0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(18, r[1]);

        EXPECT_EQ(2, hits);

        EXPECT_FEQ5(9.87 + 1.2 + 18, bmap.at(1)[1]);
        EXPECT_FEQ5(-0.1, bmap.at(1)[2]);
        EXPECT_FEQ5(-atan(0.1 / 0.5), bmap.at(1)[3]);
    }

    void boundaryrectangle_handle_crossing_top_forbidinside_bmap() {
        std::vector<std::vector<double>> bmap;
        bool hit_flag = false;
        CompoundShape::BoundaryRectangle table(CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 9.87);
        unsigned hits = 0;

        pair<int> cell;

        Vec2d r(0.5, 25 - 1e-8);
        Vec2d v(1.0, -1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0.5, r[0]);
        EXPECT_AEQ(25, r[1]);
        EXPECT_EQ(1, hits);

        EXPECT_FEQ5(9.87 + 1.2 + 25 + 1.2 - 0.5, bmap.at(0)[1]);
        EXPECT_FEQ5(-1, bmap.at(0)[2]);
        EXPECT_FEQ5(-M_PI / 4., bmap.at(0)[3]);

        r = {0.6, 25 - 1e-8};
        v = {-0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(25, r[1]);

        EXPECT_EQ(2, hits);

        EXPECT_FEQ5(9.87 + 1.2 + 25 + 1.2 - 0.6, bmap.at(1)[1]);
        EXPECT_FEQ5(.5, bmap.at(1)[2]);
        EXPECT_FEQ5(atan(5), bmap.at(1)[3]);
    }

    void boundaryrectanglecorner_point_is_inside_top_right() {
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::TopRight, 1, 2, 0);
        EXPECT_TRUE(table.point_is_inside({2.0, 1.0}));
        EXPECT_TRUE(table.point_is_inside({2.0, 3.0}));
        EXPECT_TRUE(table.point_is_inside({0.5, 2.2}));
        EXPECT_FALSE(table.point_is_inside({0.5, 1.0}));
    }

    void boundaryrectanglecorner_point_is_inside_top_left() {
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::TopLeft, 1, 2, 0);
        EXPECT_FALSE(table.point_is_inside({2.0, 1.0}));
        EXPECT_TRUE(table.point_is_inside({2.0, 3.0}));
        EXPECT_TRUE(table.point_is_inside({0.5, 2.2}));
        EXPECT_TRUE(table.point_is_inside({0.5, 1.0}));
    }

    void boundaryrectanglecorner_point_is_inside_bottom_left() {
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::BottomLeft, 1, 2, 0);
        EXPECT_TRUE(table.point_is_inside({2.0, 1.0}));
        EXPECT_FALSE(table.point_is_inside({2.0, 3.0}));
        EXPECT_TRUE(table.point_is_inside({0.5, 2.2}));
        EXPECT_TRUE(table.point_is_inside({0.5, 1.0}));
    }

    void boundaryrectanglecorner_point_is_inside_bottom_right() {
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::BottomRight, 1, 2, 0);
        EXPECT_TRUE(table.point_is_inside({2.0, 1.0}));
        EXPECT_TRUE(table.point_is_inside({2.0, 3.0}));
        EXPECT_FALSE(table.point_is_inside({0.5, 2.2}));
        EXPECT_TRUE(table.point_is_inside({0.5, 1.0}));
    }

    void boundaryrectanglecorner_handle_crossing_top_right() {
        unsigned hits = 0;
        bool hit_flag = false;
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::TopRight, 1, 2, 0);

        Vec2d r = {0.5, 2 - 1e-8};
        Vec2d v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(0.5, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(3.2, v[1]);
        EXPECT_EQ(1, hits);

        r = {1 - 1e-8, 0};
        v = {-0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(0, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(2, hits);
    }

    void boundaryrectanglecorner_handle_crossing_top_left() {
        unsigned hits = 0;
        bool hit_flag = false;
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::TopLeft, 1, 2, 0);

        Vec2d r = {2, 2 - 1e-8};
        Vec2d v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(2, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(3.2, v[1]);
        EXPECT_EQ(1, hits);

        r = {1 + 1e-8, 0};
        v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(0, r[1]);
        EXPECT_FEQ5(-0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(2, hits);
    }

    void boundaryrectanglecorner_handle_crossing_bottom_left() {
        unsigned hits = 0;
        bool hit_flag = false;
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::BottomLeft, 1, 2, 0);

        Vec2d r = {1 + 1e-8, 3};
        Vec2d v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(3, r[1]);
        EXPECT_FEQ5(-0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(1, hits);

        r = {3, 2 + 1e-8};
        v = {0.2, 3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(3, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(2, hits);
    }

    void boundaryrectanglecorner_handle_crossing_bottom_right() {
        unsigned hits = 0;
        bool hit_flag = false;
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::BottomRight, 1, 2, 0);

        Vec2d r = {1 - 1e-8, 3};
        Vec2d v = {-0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(3, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(1, hits);

        r = {0.5, 2 + 1e-8};
        v = {0.2, 3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(0.5, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(2, hits);
    }

    void boundaryrectanglecorner_handle_crossing_top_right_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::TopRight, 1, 2, 9.87);

        Vec2d r = {0.5, 2 - 1e-8};
        Vec2d v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_FEQ5(0.5, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(3.2, v[1]);

        EXPECT_FEQ5(9.87 - 0.5, bmap.at(0)[1]);
        EXPECT_FEQ5(0.2, bmap.at(0)[2]);
        EXPECT_FEQ5(atan(0.2 / 3.2), bmap.at(0)[3]);
        EXPECT_EQ(1, hits);

        r = {1 - 1e-8, 0};
        v = {-0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(0, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);

        EXPECT_FEQ5(9.87 + 2, bmap.at(1)[1]);
        EXPECT_FEQ5(-3.2, bmap.at(1)[2]);
        EXPECT_FEQ5(atan(3.2 / 0.2), bmap.at(1)[3]);
        EXPECT_EQ(2, hits);
    }

    void boundaryrectanglecorner_handle_crossing_top_left_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::TopLeft, 1, 2, 9.87);

        Vec2d r = {2, 2 - 1e-8};
        Vec2d v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_FEQ5(2, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(3.2, v[1]);

        EXPECT_FEQ5(9.87 + 1, bmap.at(0)[1]);
        EXPECT_FEQ5(0.2, bmap.at(0)[2]);
        EXPECT_FEQ5(atan(0.2 / 3.2), bmap.at(0)[3]);
        EXPECT_EQ(1, hits);

        r = {1 + 1e-8, 0};
        v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(0, r[1]);
        EXPECT_FEQ5(-0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);

        EXPECT_FEQ5(9.87 - 2, bmap.at(1)[1]);
        EXPECT_FEQ5(-3.2, bmap.at(1)[2]);
        EXPECT_FEQ5(atan(-3.2 / 0.2), bmap.at(1)[3]);
        EXPECT_EQ(2, hits);
    }

    void boundaryrectanglecorner_handle_crossing_bottom_left_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::BottomLeft, 1, 2, 9.87);

        Vec2d r = {1 + 1e-8, 3.0};
        Vec2d v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(3, r[1]);
        EXPECT_FEQ5(-0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(1, hits);

        EXPECT_FEQ5(9.87 + 1, bmap.at(0)[1]);
        EXPECT_FEQ5(-3.2, bmap.at(0)[2]);
        EXPECT_FEQ5(atan(-3.2 / 0.2), bmap.at(0)[3]);

        r = {3, 2 + 1e-8};
        v = {0.2, 3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_FEQ5(3, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);

        EXPECT_FEQ5(9.87 - 2, bmap.at(1)[1]);
        EXPECT_FEQ5(-0.2, bmap.at(1)[2]);
        EXPECT_FEQ5(atan(-0.2 / 3.2), bmap.at(1)[3]);
        EXPECT_EQ(2, hits);
    }

    void boundaryrectanglecorner_handle_crossing_bottom_right_bmap() {
        bool hit_flag = false;
        std::vector<std::vector<double>> bmap;

        unsigned hits = 0;
        CompoundShape::BoundaryRectangleCorner table(CompoundShape::BoundaryRectangleCorner::BottomRight, 1, 2, 9.87);

        Vec2d r = {1 - 1e-8, 3.0};
        Vec2d v = {-0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(3, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);

        EXPECT_FEQ5(9.87 - 1, bmap.at(0)[1]);
        EXPECT_FEQ5(3.2, bmap.at(0)[2]);
        EXPECT_FEQ5(atan(3.2 / 0.2), bmap.at(0)[3]);
        EXPECT_EQ(1, hits);

        r = {0.5, 2 + 1e-8};
        v = {0.2, 3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, &bmap, 0, hits, hit_flag));

        EXPECT_FEQ5(0.5, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);

        EXPECT_FEQ5(9.87 + 0.5, bmap.at(1)[1]);
        EXPECT_FEQ5(-0.2, bmap.at(1)[2]);
        EXPECT_FEQ5(atan(-0.2 / 3.2), bmap.at(1)[3]);
        EXPECT_EQ(2, hits);
    }

    void openboundaryrectangle_point_is_inside() {
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 2, 0, 5, 0.1, 0.2, 0
        );

        Vec2d r(0.5, 0.5);
        ASSERT_TRUE(table.point_is_inside(r));
        r = {2.1, 0.5};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {-0.1, 0.5};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {0.5, -0.1};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {0.5, 5.5};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {0.99, 4.9};
        ASSERT_TRUE(table.point_is_inside(r));

        CompoundShape::OpenBoundaryRectangle table2(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 2, 0, 5, 0.1, 0.2, 0
        );
        r = {0.5, 0.5};
        ASSERT_FALSE(table2.point_is_inside(r));
        r = {2.1, 0.5};
        EXPECT_TRUE(table2.point_is_inside(r));
        r = {-0.1, 0.5};
        EXPECT_TRUE(table2.point_is_inside(r));
        r = {0.5, -0.1};
        EXPECT_TRUE(table2.point_is_inside(r));
        r = {0.5, 5.5};
        EXPECT_TRUE(table2.point_is_inside(r));
        r = {0.99, 4.9};
        ASSERT_FALSE(table2.point_is_inside(r));
    }

    void openboundaryrectangle_handle_crossing_left_forbidoutside() {
        bool hit_flag = false;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0.01, 60, 0
        );
        unsigned hits = 0;

        pair<int> cell;

        Vec2d r(-1e-4, 0.5);
        Vec2d v(-1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.5, r[1]);
        EXPECT_EQ(1, hits);

        r = {-1e-8, 0.6};
        v = {-0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.6, r[1]);

        EXPECT_EQ(2, hits);
    }

    void openboundaryrectangle_handle_crossing_bottom_forbidoutside() {
        bool hit_flag = false;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0.01, 60, 0
        );
        unsigned hits = 0;

        pair<int> cell;

        Vec2d r(0.4, -1e-8);
        Vec2d v(-1.0, -1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0.4, r[0]);
        EXPECT_AEQ(0, r[1]);
        EXPECT_EQ(1, hits);

        r = {0.6, -1e-8};
        v = {0.8, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.8, v[0]);
        EXPECT_FEQ(0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(0, r[1]);

        EXPECT_EQ(2, hits);
    }

    void openboundaryrectangle_handle_crossing_right_forbidoutside() {
        bool hit_flag = false;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0.01, 60, 0
        );
        unsigned hits = 0;

        pair<int> cell;

        Vec2d r(1.2 + 1e-8, 15.0);
        Vec2d v(1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(15, r[1]);
        EXPECT_EQ(1, hits);

        r = {1.2 + 1e-8, 18};
        v = {0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(18, r[1]);

        EXPECT_EQ(2, hits);
    }

    void openboundaryrectangle_handle_crossing_top_forbidoutside() {
        bool hit_flag = false;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0.01, 60, 0
        );
        unsigned hits = 0;

        pair<int> cell;

        Vec2d r(0.5, 25 + 1e-8);
        Vec2d v(1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(0.5, r[0]);
        EXPECT_AEQ(25, r[1]);
        EXPECT_EQ(1, hits);

        r = {0.6, 25 + 1e-8};
        v = {-0.5, 0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(25, r[1]);

        EXPECT_EQ(2, hits);
    }

    void openboundaryrectangle_handle_crossing_left_forbidinside() {
        bool hit_flag = false;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0.01, 60, 0
        );
        unsigned hits = 0;

        pair<int> cell;

        Vec2d r(1e-4, 0.5);
        Vec2d v(1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.5, r[1]);
        EXPECT_EQ(1, hits);

        r = {1e-8, 0.6};
        v = {0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0, r[0]);
        EXPECT_AEQ(0.6, r[1]);

        EXPECT_EQ(2, hits);
    }

    void openboundaryrectangle_handle_crossing_bottom_forbidinside() {
        bool hit_flag = false;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0.01, 60, 0
        );
        unsigned hits = 0;

        pair<int> cell;

        Vec2d r(0.4, 1e-8);
        Vec2d v(-1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(0.4, r[0]);
        EXPECT_AEQ(0, r[1]);
        EXPECT_EQ(1, hits);

        r = {0.6, 1e-8};
        v = {0.8, 0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.8, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(0, r[1]);

        EXPECT_EQ(2, hits);
    }

    void openboundaryrectangle_handle_crossing_right_forbidinside() {
        bool hit_flag = false;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0.01, 60, 0
        );
        unsigned hits = 0;

        pair<int> cell;

        Vec2d r(1.2 - 1e-8, 15.0);
        Vec2d v(-1.0, 1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(15, r[1]);
        EXPECT_EQ(1, hits);

        r = {1.2 - 1e-8, 18};
        v = {-0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(0.5, v[0]);
        EXPECT_FEQ(-0.1, v[1]);
        EXPECT_AEQ(1.2, r[0]);
        EXPECT_AEQ(18, r[1]);

        EXPECT_EQ(2, hits);
    }

    void openboundaryrectangle_handle_crossing_top_forbidinside() {
        bool hit_flag = false;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0.01, 60, 0
        );
        unsigned hits = 0;

        pair<int> cell;

        Vec2d r(0.5, 25 - 1e-8);
        Vec2d v(1.0, -1.0);

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(0.5, r[0]);
        EXPECT_AEQ(25, r[1]);
        EXPECT_EQ(1, hits);

        r = {0.6, 25 - 1e-8};
        v = {-0.5, -0.1};

        ASSERT_FALSE(table.point_is_inside(r));

        ASSERT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        ASSERT_TRUE(table.point_is_inside(r));

        EXPECT_FEQ(-0.5, v[0]);
        EXPECT_FEQ(0.1, v[1]);
        EXPECT_AEQ(0.6, r[0]);
        EXPECT_AEQ(25, r[1]);

        EXPECT_EQ(2, hits);
    }

    void openboundaryrectangle_handle_escape_forbidinside_single_hole() {
        bool hit_flag = false;
        // first we test only single hole
        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0.2, 52.4, 0
        );

        Vec2d r = {0.15, 1e-8};
        Vec2d v = {0.0, 1.0};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table
        hit_flag = false;

        // Only single hole in the lower left corner
        for (const double& x : {0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 1e-8};
            v = {2, 1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1.2 - 1e-8, y};
            v = {-1.0, 1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1e-8, y};
            v = {1.0, 0.2};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.05, 0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 - 1e-8};
            v = {1.0, -1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        hits = 0;
        CompoundShape::OpenBoundaryRectangle table2(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0.2, 52.4, -0.5
        );

        r = {0.6, 1e-8};
        v = {0.0, 1.0};
        EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table2
        hit_flag = false;

        // Only single hole in the lower left corner
        for (const double& x : {0.05, 0.15, 0.21, 0.31, 0.41, 0.49, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 1e-8};
            v = {2, 1};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1.2 - 1e-8, y};
            v = {-1.0, 1.0};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1e-8, y};
            v = {1.0, 0.2};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.05, 0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 - 1e-8};
            v = {1.0, -1.0};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        hits = 0;
        CompoundShape::OpenBoundaryRectangle table3(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0.2, 52.4, -2.3
        );

        r = {1.2 - 1e-8, 1.21};
        v = {-1.0, 0.1};
        EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table3
        hit_flag = false;

        // Only single hole in the middle of right side
        for (const double& x : {0.05, 0.15, 0.21, 0.31, 0.41, 0.49, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 1e-8};
            v = {2.0, 1.0};
            EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 1.1; y += 0.15) {
            r = {1.2 - 1e-8, y};
            v = {-1.0, 1.0};
            EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1.3 + 1e-8; y < 25; y += 0.15) {
            r = {1.2 - 1e-8, y};
            v = {-1.0, 1.0};
            EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1e-8, y};
            v = {1.0, 0.2};
            EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.05, 0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 - 1e-8};
            v = {1.0, -1.0};
            EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        hits = 0;
        CompoundShape::OpenBoundaryRectangle table4(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0.2, 52.4, -26.8
        );

        r = {0.5, 25 - 1e-8};
        v = {-1.0, -1.0};
        EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table4
        hit_flag = false;

        // Only single hole in the middle of right side
        for (const double& x : {0.05, 0.15, 0.21, 0.31, 0.41, 0.49, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 1e-8};
            v = {2.0, 1.0};
            EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 1.3; y += 0.15) {
            r = {1.2 - 1e-8, y};
            v = {-1.0, 1.0};
            EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1.3 + 1e-8; y < 25; y += 0.15) {
            r = {1.2 - 1e-8, y};
            v = {-1.0, 1.0};
            EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1e-8, y};
            v = {1.0, 0.2};
            EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.05, 0.21, 0.31, 0.39, 0.61, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 - 1e-8};
            v = {1.0, -1.0};
            EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        hits = 0;
        CompoundShape::OpenBoundaryRectangle table5(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0.2, 52.4, -42.4
        );

        hit_flag = false;
        r = {1e-8, 9.9};
        v = {1.0, -1.0};
        EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table5
        hit_flag = false;

        // Only single hole in the middle of right side
        for (const double& x : {0.05, 0.15, 0.21, 0.31, 0.41, 0.49, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 1e-8};
            v = {2.0, 1.0};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 1.3; y += 0.15) {
            r = {1.2 - 1e-8, y};
            v = {-1.0, 1.0};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1.3 + 1e-8; y < 25; y += 0.15) {
            r = {1.2 - 1e-8, y};
            v = {-1.0, 1.0};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 9.8; y += 0.15) {
            r = {1e-8, y};
            v = {1.0, 0.2};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }
        for (double y = 10 + 1e-8; y < 25; y += 0.15) {
            r = {1e-8, y};
            v = {1.0, 0.2};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }
        for (const double& x : {0.05, 0.21, 0.31, 0.41, 0.51, 0.61, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 - 1e-8};
            v = {1.0, -1.0};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }
    }

    void openboundaryrectangle_handle_escape_forbidinside_two_holes() {
        bool hit_flag = false;

        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0.2, 27.2, 0
        );

        Vec2d r = {0.15, 1e-8};
        Vec2d v = {0.0, 1.0};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table
        hit_flag = false;

        r = {0.15, 25 - 1e-8};
        v = {0.0, -1.0};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table
        hit_flag = false;

        for (const double& x : {0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 1e-8};
            v = {2.0, 1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1.2 - 1e-8, y};
            v = {-1.0, 1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1e-8, y};
            v = {1.0, 0.2};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 - 1e-8};
            v = {1.0, -1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        hits = 0;
        CompoundShape::OpenBoundaryRectangle table2(
            CompoundShape::BoundaryRectangle::ForbidInside, 0, 1.2, 0, 25, 0.2, 39.4, -0.5
        );

        r = {0.6, 1e-8};
        v = {0.0, 1.0};
        EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table2
        hit_flag = false;

        r = {1e-8, 12.4};
        v = {1.0, 1.0};
        EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table
        hit_flag = false;

        // Only single hole in the lower left corner
        for (const double& x : {0.05, 0.15, 0.21, 0.31, 0.41, 0.49, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 1e-8};
            v = {2.0, 1.0};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1.2 - 1e-8, y};
            v = {-1.0, 1.0};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 12.3; y += 0.15) {
            r = {1e-8, y};
            v = {1.0, 0.2};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 12.5 + 1e-8; y < 25; y += 0.15) {
            r = {1e-8, y};
            v = {1.0, 0.2};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.05, 0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 - 1e-8};
            v = {1.0, -1.0};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }
    }

    void openboundaryrectangle_handle_escape_forbidoutside_single_hole() {
        bool hit_flag = false;
        // first we test only single hole
        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0.2, 52.4, 0
        );

        Vec2d r = {0.15, -1e-8};
        Vec2d v = {0.0, -1.0};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table
        hit_flag = false;

        // Only single hole in the lower left corner
        for (const double& x : {0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, -1e-8};
            v = {2.0, -1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1.2 + 1e-8, y};
            v = {1.0, 1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {-1e-8, y};
            v = {-1.0, 0.2};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.05, 0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 + 1e-8};
            v = {1.0, 1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        hits = 0;
        CompoundShape::OpenBoundaryRectangle table2(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0.2, 52.4, -0.5
        );

        r = {0.6, -1e-8};
        v = {0.0, -1.0};
        EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table2
        hit_flag = false;

        // Only single hole in the lower left corner
        for (const double& x : {0.05, 0.15, 0.21, 0.31, 0.41, 0.49, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, -1e-8};
            v = {2.0, -1.0};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1.2 + 1e-8, y};
            v = {1.0, 1.0};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {-1e-8, y};
            v = {-1, 0.2};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.05, 0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 + 1e-8};
            v = {1.0, 1.0};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        hits = 0;
        CompoundShape::OpenBoundaryRectangle table3(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0.2, 52.4, -2.3
        );

        r = {1.2 + 1e-8, 1.21};
        v = {1, 0.1};
        EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table3
        hit_flag = false;

        // Only single hole in the middle of right side
        for (const double& x : {0.05, 0.15, 0.21, 0.31, 0.41, 0.49, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, -1e-8};
            v = {2.0, -1.0};
            EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 1.1; y += 0.15) {
            r = {1.2 + 1e-8, y};
            v = {1.0, 1.0};
            EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1.3 + 1e-8; y < 25; y += 0.15) {
            r = {1.2 + 1e-8, y};
            v = {1.0, 1.0};
            EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {-1e-8, y};
            v = {-1.0, 0.2};
            EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.05, 0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 + 1e-8};
            v = {1.0, 1.0};
            EXPECT_TRUE(table3.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        hits = 0;
        CompoundShape::OpenBoundaryRectangle table4(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0.2, 52.4, -26.8
        );

        r = {0.5, 25 + 1e-8};
        v = {-1.0, 1.0};
        EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table4
        hit_flag = false;

        // Only single hole in the middle of right side
        for (const double& x : {0.05, 0.15, 0.21, 0.31, 0.41, 0.49, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, -1e-8};
            v = {2.0, -1.0};
            EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 1.3; y += 0.15) {
            r = {1.2 + 1e-8, y};
            v = {1.0, 1.0};
            EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1.3 + 1e-8; y < 25; y += 0.15) {
            r = {1.2 + 1e-8, y};
            v = {1.0, 1.0};
            EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {-1e-8, y};
            v = {-1.0, 0.2};
            EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.05, 0.21, 0.31, 0.39, 0.61, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 + 1e-8};
            v = {1.0, 1.0};
            EXPECT_TRUE(table4.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        hits = 0;
        CompoundShape::OpenBoundaryRectangle table5(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0.2, 52.4, -42.4
        );

        hit_flag = false;
        r = {-1e-8, 9.9};
        v = {-1.0, -1.0};
        EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table5
        hit_flag = false;

        // Only single hole in the middle of right side
        for (const double& x : {0.05, 0.15, 0.21, 0.31, 0.41, 0.49, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, -1e-8};
            v = {2.0, -1.0};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 1.3; y += 0.15) {
            r = {1.2 + 1e-8, y};
            v = {1.0, 1.0};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1.3 + 1e-8; y < 25; y += 0.15) {
            r = {1.2 + 1e-8, y};
            v = {1.0, 1.0};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 9.8; y += 0.15) {
            r = {-1e-8, y};
            v = {-1.0, 0.2};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }
        for (double y = 10 + 1e-8; y < 25; y += 0.15) {
            r = {-1e-8, y};
            v = {-1.0, 0.2};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }
        for (const double& x : {0.05, 0.21, 0.31, 0.41, 0.51, 0.61, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 + 1e-8};
            v = {1.0, 1.0};
            EXPECT_TRUE(table5.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }
    }

    void openboundaryrectangle_handle_escape_forbidoutside_two_holes() {
        bool hit_flag = false;

        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangle table(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0.2, 27.2, 0
        );

        Vec2d r = {0.15, -1e-8};
        Vec2d v = {0.0, -1.0};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table
        hit_flag = false;

        r = {0.15, 25 + 1e-8};
        v = {0.0, 1.0};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table
        hit_flag = false;

        for (const double& x : {0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, -1e-8};
            v = {2.0, -1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1.2 + 1e-8, y};
            v = {1.0, 1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {-1e-8, y};
            v = {-1.0, 0.2};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 + 1e-8};
            v = {1.0, 1.0};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        hits = 0;
        CompoundShape::OpenBoundaryRectangle table2(
            CompoundShape::BoundaryRectangle::ForbidOutside, 0, 1.2, 0, 25, 0.2, 39.4, -0.5
        );

        r = {0.6, -1e-8};
        v = {0.0, -1.0};
        EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table
        hit_flag = false;

        r = {-1e-8, 12.4};
        v = {-1.0, 1.0};
        EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
        EXPECT_EQ(0, hits);  // It hit the hole, not the table
        hit_flag = false;

        for (const double& x : {0.05, 0.15, 0.21, 0.31, 0.41, 0.49, 0.71, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, -1e-8};
            v = {2.0, -1.0};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 25; y += 0.15) {
            r = {1.2 + 1e-8, y};
            v = {1.0, 1.0};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 1e-8; y < 12.3; y += 0.15) {
            r = {-1e-8, y};
            v = {-1.0, 0.2};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (double y = 12.5 + 1e-8; y < 25; y += 0.15) {
            r = {-1e-8, y};
            v = {-1.0, 0.2};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }

        for (const double& x : {0.05, 0.21, 0.31, 0.41, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.19}) {
            r = {x, 25 + 1e-8};
            v = {1.0, 1.0};
            EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
            hit_flag = false;
        }
    }

    void openboundarycircle_point_is_inside() {
        CompoundShape::OpenBoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            0.5,
            CompoundShape::BoundaryCircle::No,
            1,
            2,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table.point_is_inside({0.2, 0.2}));
        EXPECT_FALSE(table.point_is_inside({0.6, 0.2}));

        CompoundShape::OpenBoundaryCircle table2(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0, 0},
            0.5,
            CompoundShape::BoundaryCircle::No,
            1,
            2,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_FALSE(table2.point_is_inside({0.2, 0.2}));
        EXPECT_TRUE(table2.point_is_inside({0.6, 0.2}));

        CompoundShape::OpenBoundaryCircle table3(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            0.5,
            CompoundShape::BoundaryCircle::Left,
            1,
            2,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table3.point_is_inside({0.2, 0.2}));
        EXPECT_TRUE(table3.point_is_inside({0.6, 0.2}));

        EXPECT_TRUE(table3.point_is_inside({-0.3, 0.2}));
        EXPECT_FALSE(table3.point_is_inside({-.6, 0}));

        CompoundShape::OpenBoundaryCircle table4(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0, 0},
            0.5,
            CompoundShape::BoundaryCircle::Left,
            1,
            2,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table4.point_is_inside({0.2, 0.2}));
        EXPECT_TRUE(table4.point_is_inside({0.6, 0.2}));

        EXPECT_FALSE(table4.point_is_inside({-0.3, 0.2}));
        EXPECT_TRUE(table4.point_is_inside({-.6, 0}));
        EXPECT_TRUE(table4.point_is_inside({0.4, 0.4}));

        CompoundShape::OpenBoundaryCircle table5(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0, 0},
            0.5,
            CompoundShape::BoundaryCircle::Top,
            1,
            2,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_FALSE(table5.point_is_inside({0.2, 0.2}));
        EXPECT_TRUE(table5.point_is_inside({0.6, 0.2}));
        EXPECT_FALSE(table5.point_is_inside({0.3, 0.3}));
        EXPECT_FALSE(table5.point_is_inside({-0.3, 0.2}));
        EXPECT_TRUE(table5.point_is_inside({-.6, .1}));

        CompoundShape::OpenBoundaryCircle table6(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            0.5,
            CompoundShape::BoundaryCircle::Top,
            1,
            2,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_FALSE(table6.point_is_inside({0.6, 0.1}));
        EXPECT_TRUE(table6.point_is_inside({0.3, 0.3}));
        EXPECT_TRUE(table6.point_is_inside({-1, -1}));

        CompoundShape::OpenBoundaryCircle table7(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            0.5,
            CompoundShape::BoundaryCircle::Right,
            1,
            2,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_FALSE(table7.point_is_inside({0.6, 0.1}));
        EXPECT_TRUE(table7.point_is_inside({0.3, 0.3}));
        EXPECT_TRUE(table7.point_is_inside({-1, -1}));

        CompoundShape::OpenBoundaryCircle table8(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0, 0},
            0.5,
            CompoundShape::BoundaryCircle::Right,
            1,
            2,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table8.point_is_inside({0.6, 0.1}));
        EXPECT_FALSE(table8.point_is_inside({0.3, 0.3}));
        EXPECT_FALSE(table8.point_is_inside({0.3, -0.3}));
        EXPECT_TRUE(table8.point_is_inside({-1.0, -1.0}));

        CompoundShape::OpenBoundaryCircle table9(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0, 0},
            0.5,
            CompoundShape::BoundaryCircle::Bottom,
            1,
            2,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table9.point_is_inside({0.2, 0.2}));
        EXPECT_FALSE(table9.point_is_inside({-0.3, -0.3}));
        EXPECT_FALSE(table9.point_is_inside({0.3, -0.3}));
        EXPECT_TRUE(table9.point_is_inside({0, -5}));

        CompoundShape::OpenBoundaryCircle table10(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            0.5,
            CompoundShape::BoundaryCircle::Bottom,
            1,
            2,
            0,
            CompoundShape::BoundaryCircle::ADown
        );
        EXPECT_TRUE(table10.point_is_inside({-0.3, -0.3}));
        EXPECT_TRUE(table10.point_is_inside({0.3, -0.3}));
        EXPECT_TRUE(table10.point_is_inside({0.2, 0.2}));
        EXPECT_FALSE(table10.point_is_inside({0, -5}));
    }

    void openboundarycircle_handle_crossing_forbidinside() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            M_PI * sqrt(2.) / 2.,
            2 * M_PI * sqrt(2.),
            M_PI * sqrt(2.) / 2.,
            CompoundShape::BoundaryCircle::ADown
        );

        Vec2d r(1 - 1e-5, 1 - 1e-5);
        Vec2d v(-1.0, -1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);
        EXPECT_EQ(0, hits);

        r = {-1 + 1e-5, -1 + 1e-5};
        v = {1.0, 1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(-1, r[1]);
        EXPECT_EQ(1, hits);

        r = {-1 + 1e-5, 1 - 1e-5};
        v = {1.0, -1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(1, r[1]);
        EXPECT_EQ(2, hits);

        r = {1 - 1e-5, -1 + 1e-5};
        v = {-1.0, 1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);
        EXPECT_EQ(3, hits);

        r = {1 - 1e-5, -1 + 1e-5};
        v = {0.0, 1.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(1, v[0]);
        EXPECT_AEQ(0, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);
        EXPECT_EQ(4, hits);

        r = {1 - 1e-5, -1 + 1e-5};
        v = {-1.0, 0.0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0, v[0]);
        EXPECT_AEQ(-1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);
        EXPECT_EQ(5, hits);

        r = {1 - 1e-5, -1 + 1e-5};
        v = {0.5, 1};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(1, v[0]);
        EXPECT_AEQ(0.5, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);
        EXPECT_EQ(6, hits);
    }

    void openboundarycircle_handle_crossing_forbidoutside() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            M_PI * sqrt(2.) / 2.,
            2 * M_PI * sqrt(2.),
            0,
            CompoundShape::BoundaryCircle::ARight
        );

        Vec2d r(1 + 1e-5, 1 + 1e-5);
        Vec2d v(1.0, 1.0);

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);
        EXPECT_EQ(0, hits);

        r = {-1 - 1e-5, -1 - 1e-5};
        v = {-1, -1};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(-1, r[1]);
        EXPECT_EQ(1, hits);

        r = {-1 - 1e-5, 1 + 1e-5};
        v = {-1, 1};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(1, v[0]);
        EXPECT_FEQ(-1, v[1]);
        EXPECT_AEQ(-1, r[0]);
        EXPECT_AEQ(1, r[1]);
        EXPECT_EQ(2, hits);

        r = {1 + 1e-5, -1 - 1e-5};
        v = {1, -1};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FEQ(-1, v[0]);
        EXPECT_FEQ(1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);
        EXPECT_EQ(3, hits);

        r = {1 + 1e-5, -1 - 1e-5};
        v = {0, -1};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(-1, v[0]);
        EXPECT_AEQ(0, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);
        EXPECT_EQ(4, hits);

        r = {1 + 1e-5, -1 - 1e-5};
        v = {1, 0};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0, v[0]);
        EXPECT_AEQ(1, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);
        EXPECT_EQ(5, hits);

        r = {1 + 1e-5, -1 - 1e-5};
        v = {0.5, -1};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(-1, v[0]);
        EXPECT_AEQ(0.5, v[1]);
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(-1, r[1]);
        EXPECT_EQ(6, hits);
    }

    void openboundarycircle_handle_escape_forbidoutside1() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            M_PI * sqrt(2.) / 2.,
            2 * M_PI * sqrt(2.),
            0,
            CompoundShape::BoundaryCircle::ARight
        );

        EXPECT_FALSE(hit_flag);
        Vec2d r;
        Vec2d v;
        for (double theta = 0.01; theta < 2 * M_PI; theta += 0.1) {
            hit_flag = false;
            r = {cos(theta), sin(theta)};
            r *= sqrt(2.) + 1e-8;
            v = {cos(theta), sin(theta)};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            if (theta > M_PI / 2.) {
                EXPECT_FALSE(hit_flag);
            } else
                EXPECT_TRUE(hit_flag);
        }
    }

    void openboundarycircle_handle_escape_forbidoutside2() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            M_PI * sqrt(2.) / 2.,
            2 * M_PI * sqrt(2.),
            -M_PI * sqrt(2.) / 2.,
            CompoundShape::BoundaryCircle::ARight
        );

        EXPECT_FALSE(hit_flag);
        Vec2d r;
        Vec2d v;
        for (double theta = 0.01; theta < 2 * M_PI; theta += 0.1) {
            hit_flag = false;
            r = {cos(theta), sin(theta)};
            r *= sqrt(2.) + 1e-8;
            v = {cos(theta), sin(theta)};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            if (theta > M_PI or theta < M_PI / 2.) {
                EXPECT_FALSE(hit_flag);
            } else
                EXPECT_TRUE(hit_flag);
        }
    }

    void openboundarycircle_handle_escape_forbidoutside3() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidOutside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            M_PI * sqrt(2.) / 2.,
            M_PI * sqrt(2.),
            -M_PI * sqrt(2.) / 2.,
            CompoundShape::BoundaryCircle::ARight
        );

        EXPECT_FALSE(hit_flag);
        Vec2d r;
        Vec2d v;
        for (double theta = 0.01; theta < 2 * M_PI; theta += 0.1) {
            hit_flag = false;
            r = {cos(theta), sin(theta)};
            r *= sqrt(2.) + 1e-8;
            v = {cos(theta), sin(theta)};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            if (theta < M_PI / 2. or (theta > M_PI and theta < 3 * M_PI / 2)) {
                EXPECT_FALSE(hit_flag);
            } else
                EXPECT_TRUE(hit_flag);
        }
    }

    void openboundarycircle_handle_escape_forbidinside1() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            M_PI * sqrt(2.) / 2.,
            2 * M_PI * sqrt(2.),
            0,
            CompoundShape::BoundaryCircle::ARight
        );

        EXPECT_FALSE(hit_flag);
        Vec2d r;
        Vec2d v;
        for (double theta = 0.01; theta < 2 * M_PI; theta += 0.1) {
            hit_flag = false;
            r = {cos(theta), sin(theta)};
            r *= sqrt(2.) - 1e-8;
            v = {-cos(theta), -sin(theta)};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            if (theta > M_PI / 2.) {
                EXPECT_FALSE(hit_flag);
            } else
                EXPECT_TRUE(hit_flag);
        }
    }

    void openboundarycircle_handle_escape_forbidinside2() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            M_PI * sqrt(2.) / 2.,
            2 * M_PI * sqrt(2.),
            -M_PI * sqrt(2.) / 2.,
            CompoundShape::BoundaryCircle::ARight
        );

        EXPECT_FALSE(hit_flag);
        Vec2d r;
        Vec2d v;
        for (double theta = 0.01; theta < 2 * M_PI; theta += 0.1) {
            hit_flag = false;
            r = {cos(theta), sin(theta)};
            r *= sqrt(2.) - 1e-8;
            v = {-cos(theta), -sin(theta)};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            if (theta > M_PI or theta < M_PI / 2.) {
                EXPECT_FALSE(hit_flag);
            } else
                EXPECT_TRUE(hit_flag);
        }
    }

    void openboundarycircle_handle_escape_forbidinside3() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryCircle table(
            CompoundShape::BoundaryCircle::ForbidInside,
            {0, 0},
            sqrt(2.),
            CompoundShape::BoundaryCircle::No,
            M_PI * sqrt(2.) / 2.,
            M_PI * sqrt(2.),
            -M_PI * sqrt(2.) / 2.,
            CompoundShape::BoundaryCircle::ARight
        );

        EXPECT_FALSE(hit_flag);
        Vec2d r;
        Vec2d v;
        for (double theta = 0.01; theta < 2 * M_PI; theta += 0.1) {
            hit_flag = false;
            r = {cos(theta), sin(theta)};
            r *= sqrt(2.) - 1e-8;
            v = {-cos(theta), -sin(theta)};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            if (theta < M_PI / 2. or (theta > M_PI and theta < 3 * M_PI / 2)) {
                EXPECT_FALSE(hit_flag);
            } else
                EXPECT_TRUE(hit_flag);
        }
    }

    void openboundaryhorizontalline_point_is_inside() {
        CompoundShape::OpenBoundaryHorizontalLine table(
            CompoundShape::BoundaryHorizontalLine::ForbidLower, 0.2, 0.3, 3, 1, 2
        );

        EXPECT_TRUE(table.point_is_inside({0.3, 0.4}));
        EXPECT_FALSE(table.point_is_inside({-4.3, 0.19}));
        EXPECT_TRUE(table.point_is_inside({5161, 153615}));
        EXPECT_FALSE(table.point_is_inside({0, -65465132}));

        CompoundShape::OpenBoundaryHorizontalLine table2(
            CompoundShape::BoundaryHorizontalLine::ForbidUpper, 0.2, 0.3, 3, 1, 2
        );

        EXPECT_FALSE(table2.point_is_inside({0.3, 0.4}));
        EXPECT_TRUE(table2.point_is_inside({-4.3, 0.19}));
        EXPECT_FALSE(table2.point_is_inside({5161, 153615}));
        EXPECT_TRUE(table2.point_is_inside({0, -65465132}));
    }

    void openboundaryhorizontalline_handle_crossing_forbidlower() {
        bool hit_flag = false;

        unsigned hits = 0;
        CompoundShape::OpenBoundaryHorizontalLine table(
            CompoundShape::BoundaryHorizontalLine::ForbidLower, 0.2, 0.3, 3, 1, 2
        );

        Vec2d r(0.456, 0.2 - 1e-4);
        Vec2d v(2.0, -3.0);
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0.456, r[0]);
        EXPECT_AEQ(0.2, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(3, v[1]);
    }

    void openboundaryhorizontalline_handle_crossing_forbidupper() {
        bool hit_flag = false;

        unsigned hits = 0;
        CompoundShape::OpenBoundaryHorizontalLine table(
            CompoundShape::BoundaryHorizontalLine::ForbidUpper, 0.2, 0.3, 3, 1, 2
        );

        Vec2d r(0.456, 0.2 + 1e-4);
        Vec2d v(2.0, 3.0);
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_AEQ(0.456, r[0]);
        EXPECT_AEQ(0.2, r[1]);

        EXPECT_FEQ(2, v[0]);
        EXPECT_FEQ(-3, v[1]);
    }

    void openboundaryhorizontalline_handle_escape_forbidlower() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryHorizontalLine table(
            CompoundShape::BoundaryHorizontalLine::ForbidLower, 0.2, -0.3, 0.2, 0.2, 2
        );

        EXPECT_FALSE(hit_flag);

        Vec2d r(0.456, 0.2 - 1e-4);
        Vec2d v(2.0, -3.0);

        for (double x = 0.61; x < 20; x += 2) {
            v = {2.0, -3.0};
            hit_flag = false;
            r = {x, 0.2 - 1e-4};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_TRUE(hit_flag);
        }

        for (double x = 0.81; x < 20; x += 2) {
            v = {2.0, -3.0};
            hit_flag = false;
            r = {x, 0.2 - 1e-4};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
        }
    }

    void openboundaryhorizontalline_handle_escape_forbidupper() {
        unsigned hits = 0;
        bool hit_flag = false;
        CompoundShape::OpenBoundaryHorizontalLine table(
            CompoundShape::BoundaryHorizontalLine::ForbidUpper, 0.2, -0.3, 0.2, 0.2, 2
        );

        EXPECT_FALSE(hit_flag);

        Vec2d r(0.456, 0.2 + 1e-4);
        Vec2d v(2.0, 3.0);

        for (double x = -0.11; x > -20; x -= 2) {
            v = {2.0, 3.0};
            hit_flag = false;
            r = {x, 0.2 + 1e-4};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_TRUE(hit_flag);
        }

        for (double x = -0.31; x > -20; x -= 2) {
            v = {2, 3};
            hit_flag = false;
            r = {x, 0.2 + 1e-4};
            EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
            EXPECT_FALSE(hit_flag);
        }
    }

    void openboundaryrectanglecorner_point_is_inside_top_right() {
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::TopRight, 1, 2, 0, 1, 2
        );
        EXPECT_TRUE(table.point_is_inside({2, 1}));
        EXPECT_TRUE(table.point_is_inside({2, 3}));
        EXPECT_TRUE(table.point_is_inside({0.5, 2.2}));
        EXPECT_FALSE(table.point_is_inside({0.5, 1}));
    }

    void openboundaryrectanglecorner_point_is_inside_top_left() {
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::TopLeft, 1, 2, 0, 1, 2
        );
        EXPECT_FALSE(table.point_is_inside({2, 1}));
        EXPECT_TRUE(table.point_is_inside({2, 3}));
        EXPECT_TRUE(table.point_is_inside({0.5, 2.2}));
        EXPECT_TRUE(table.point_is_inside({0.5, 1}));
    }

    void openboundaryrectanglecorner_point_is_inside_bottom_left() {
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::BottomLeft, 1, 2, 0, 1, 2
        );
        EXPECT_TRUE(table.point_is_inside({2, 1}));
        EXPECT_FALSE(table.point_is_inside({2, 3}));
        EXPECT_TRUE(table.point_is_inside({0.5, 2.2}));
        EXPECT_TRUE(table.point_is_inside({0.5, 1}));
    }

    void openboundaryrectanglecorner_point_is_inside_bottom_right() {
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::BottomRight, 1, 2, 0, 1, 2
        );
        EXPECT_TRUE(table.point_is_inside({2, 1}));
        EXPECT_TRUE(table.point_is_inside({2, 3}));
        EXPECT_FALSE(table.point_is_inside({0.5, 2.2}));
        EXPECT_TRUE(table.point_is_inside({0.5, 1}));
    }

    void openboundaryrectanglecorner_handle_crossing_top_right() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::TopRight, 1, 2, 0, 1, 2
        );

        Vec2d r = {0.5, 2 - 1e-8};
        Vec2d v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(0.5, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(3.2, v[1]);
        EXPECT_EQ(1, hits);

        r = {1 - 1e-8, 0};
        v = {-0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(0, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(2, hits);
    }

    void openboundaryrectanglecorner_handle_crossing_top_left() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::TopLeft, 1, 2, 0, 1, 2
        );

        Vec2d r = {2, 2 - 1e-8};
        Vec2d v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(2, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(3.2, v[1]);
        EXPECT_EQ(1, hits);

        r = {1 + 1e-8, 0};
        v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(0, r[1]);
        EXPECT_FEQ5(-0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(2, hits);
    }

    void openboundaryrectanglecorner_handle_crossing_bottom_left() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::BottomLeft, 1, 2, 0, 1, 2
        );

        Vec2d r = {1 + 1e-8, 3};
        Vec2d v = {0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(3, r[1]);
        EXPECT_FEQ5(-0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(1, hits);

        r = {3, 2 + 1e-8};
        v = {0.2, 3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(3, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(2, hits);
    }

    void openboundaryrectanglecorner_handle_crossing_bottom_right() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::BottomRight, 1, 2, 0, 1, 2
        );

        Vec2d r = {1 - 1e-8, 3};
        Vec2d v = {-0.2, -3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(1, r[0]);
        EXPECT_FEQ5(3, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(1, hits);

        r = {0.5, 2 + 1e-8};
        v = {0.2, 3.2};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));

        EXPECT_FEQ5(0.5, r[0]);
        EXPECT_FEQ5(2, r[1]);
        EXPECT_FEQ5(0.2, v[0]);
        EXPECT_FEQ5(-3.2, v[1]);
        EXPECT_EQ(2, hits);
    }

    void openboundaryrectanglecorner_handle_escape_top_right() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::TopRight, 1, 2, -0.2, 1, 2
        );

        // Right side
        Vec2d r = {1 - 1e-8, 1.79};
        Vec2d v = {-0.2, 0.01};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        v = {-0.2, 0.01};
        r = {1 - 1e-8, 0.79};

        hit_flag = false;
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FALSE(hit_flag);

        // Top side
        hit_flag = false;
        r = {0.39, 2 - 1e-8};
        v = {0.2, -1};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FALSE(hit_flag);

        CompoundShape::OpenBoundaryRectangleCorner table2(
            CompoundShape::BoundaryRectangleCorner::TopRight, 1, 2, 0.62, 1, 2
        );

        hit_flag = false;
        r = {0.4, 2 - 1e-8};
        v = {0.2, -1};

        EXPECT_TRUE(table2.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);
    }

    void openboundaryrectanglecorner_handle_escape_top_left() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::TopLeft, 1, 2, 0.3, 1, 2
        );

        // Left side
        Vec2d r = {1 + 1e-8, 1.79};
        Vec2d v = {0.2, 0.01};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        v = {0.2, 0.01};
        r = {1 + 1e-8, 0.79};

        hit_flag = false;
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FALSE(hit_flag);

        // Top side
        hit_flag = false;
        r = {1.69, 2 - 1e-8};
        v = {0.2, -1};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        hit_flag = false;
        r = {1.71, 2 - 1e-8};
        v = {0.2, -1};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FALSE(hit_flag);
    }

    void openboundaryrectanglecorner_handle_escape_bottom_left() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::BottomLeft, 1, 2, 0.3, 1, 2
        );

        // Left side
        Vec2d r = {1 + 1e-8, 2.21};
        Vec2d v = {0.2, 0.01};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        v = {0.2, 0.01};
        r = {1 + 1e-8, 2.71};

        hit_flag = false;
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FALSE(hit_flag);

        // Bottom

        hit_flag = false;
        v = {0.2, 0.9};
        r = {1.29, 2 + 1e-8};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        hit_flag = false;
        v = {0.2, 0.9};
        r = {1.31, 2 + 1e-8};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FALSE(hit_flag);
    }

    void openboundaryrectanglecorner_handle_escape_bottom_right() {
        bool hit_flag = false;
        unsigned hits = 0;
        CompoundShape::OpenBoundaryRectangleCorner table(
            CompoundShape::BoundaryRectangleCorner::BottomRight, 1, 2, 0.3, 1, 2
        );

        // Left side
        Vec2d r = {1 - 1e-8, 2.21};
        Vec2d v = {-0.2, 0.01};

        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        v = {-0.2, 0.01};
        r = {1 - 1e-8, 2.71};

        hit_flag = false;
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FALSE(hit_flag);

        // Bottom

        hit_flag = false;
        v = {0.2, 0.9};
        r = {0.31, 2 + 1e-8};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        hit_flag = false;
        v = {0.2, 0.9};
        r = {0.29, 2 + 1e-8};
        EXPECT_TRUE(table.handle_crossing(r, v, nullptr, 0, hits, hit_flag));
        EXPECT_FALSE(hit_flag);
    }

    void compoundshapeperiodic_point_is_inside() {
        CompoundShapePeriodic table({M_PI / 3., 2, 1}, 0, 0);
        EXPECT_FALSE(table.point_is_inside({2.1, 0}));
        EXPECT_TRUE(table.point_is_inside({2.4, 0.85}));
        EXPECT_TRUE(table.point_is_inside({2.1, 0.2}));
        EXPECT_TRUE(table.point_is_inside({0.1, 0.15}));
        EXPECT_FALSE(table.point_is_inside({0.47, 0.86}));
        EXPECT_TRUE(table.point_is_inside({0.55, 0.86}));
        EXPECT_FALSE(table.point_is_inside({0.55, 0.9}));
        EXPECT_FALSE(table.point_is_inside({0.5, -0.01}));
        EXPECT_TRUE(table.point_is_inside({0.5, 0.001}));
    }

    void compoundshapeperiodic_handle_cell() {
        const double a = sqrt(1.2 * 1.2 - 1);
        bool hit_flag = false;
        Vec2d v = {0.0, 0.0};
        CompoundShapePeriodic table({asin(1. / 1.2), 2, 1.2}, 0, 0);
        Vec2d r = {0.5, -1e-8};
        pair<int> cell = {0, 0};
        EXPECT_TRUE(table.handle_cell(r, v, cell, nullptr, 0, hit_flag));

        EXPECT_FEQ5(0.5 + a, r[0]);
        EXPECT_FEQ5(1, r[1]);
        EXPECT_EQ(0, cell[0]);
        EXPECT_EQ(-1, cell[1]);

        r = {1.2, 1 + 1e-8};
        cell = {0, 0};
        EXPECT_TRUE(table.handle_cell(r, v, cell, nullptr, 0, hit_flag));
        EXPECT_FEQ5(1.2 - a, r[0]);
        EXPECT_FEQ5(0, r[1]);
        EXPECT_EQ(0, cell[0]);
        EXPECT_EQ(1, cell[1]);

        r = {0.5 * a, 0.5 + 1e-8};
        cell = {0, 0};
        EXPECT_TRUE(table.handle_cell(r, v, cell, nullptr, 0, hit_flag));
        EXPECT_FEQ5(2 + 0.5 * a, r[0]);
        EXPECT_FEQ5(0.5, r[1]);

        EXPECT_EQ(-1, cell[0]);
        EXPECT_EQ(0, cell[1]);

        r = {2 + 0.5 * a, 0.5 - 1e-8};
        cell = {0, 0};
        EXPECT_TRUE(table.handle_cell(r, v, cell, nullptr, 0, hit_flag));
        EXPECT_FEQ5(0.5 * a, r[0]);
        EXPECT_FEQ5(0.5, r[1]);

        EXPECT_EQ(1, cell[0]);
        EXPECT_EQ(0, cell[1]);
    }

    void compoundshapeperiodic_handle_cell_bmap() {
        std::vector<std::vector<double>> bmap;
        bool hit_flag = false;
        const double a = sqrt(1.2 * 1.2 - 1);
        CompoundShapePeriodic table({asin(1. / 1.2), 2, 1.2}, 0, 2);

        Vec2d r = {0.5, -1e-8};
        Vec2d v = {0.2, -1};
        pair<int> cell = {0, 0};
        EXPECT_TRUE(table.handle_cell(r, v, cell, &bmap, 0, hit_flag));

        EXPECT_FEQ5(0.5 + a, r[0]);
        EXPECT_FEQ5(1, r[1]);
        EXPECT_EQ(0, cell[0]);
        EXPECT_EQ(-1, cell[1]);

        EXPECT_FEQ(0.5, bmap.at(0)[1]);
        EXPECT_FEQ(0.2, bmap.at(0)[2]);
        EXPECT_FEQ(atan(0.2), bmap.at(0)[3]);

        r = {2 + 0.5 * a, 0.5 - 1e-8};
        v = {1, -0.1};
        cell = {0, 0};
        EXPECT_TRUE(table.handle_cell(r, v, cell, &bmap, 0, hit_flag));
        EXPECT_FEQ5(0.5 * a, r[0]);
        EXPECT_FEQ5(0.5, r[1]);

        EXPECT_EQ(1, cell[0]);
        EXPECT_EQ(0, cell[1]);

        EXPECT_AEQ(2 + 0.6, bmap.at(1)[1]);
        EXPECT_FEQ(v.norm() * sin(M_PI / 2. - asin(1. / 1.2) - atan(0.1 / 1.0)), bmap.at(1)[2]);

        EXPECT_AEQ(M_PI / 2. - asin(1. / 1.2) - atan(0.1 / 1.0), bmap.at(1)[3]);

        r = {2 + 0.5 * a, 0.5 - 1e-8};
        v = {1, 0.2};
        cell = {0, 0};
        EXPECT_TRUE(table.handle_cell(r, v, cell, &bmap, 0, hit_flag));
        EXPECT_FEQ5(0.5 * a, r[0]);
        EXPECT_FEQ5(0.5, r[1]);

        EXPECT_EQ(1, cell[0]);
        EXPECT_EQ(0, cell[1]);

        EXPECT_AEQ(2 + 0.6, bmap.at(2)[1]);
        EXPECT_FEQ(1.019803902718557 * sin(M_PI / 2. - asin(1. / 1.2) + atan(0.2 / 1.0)), bmap.at(2)[2]);

        EXPECT_AEQ(M_PI / 2. - asin(1. / 1.2) + atan(0.2 / 1.0), bmap.at(2)[3]);

        r = {2 + 0.5 * a, 0.5 - 1e-8};
        v = {0.2, -1};
        cell = {0, 0};
        EXPECT_TRUE(table.handle_cell(r, v, cell, &bmap, 0, hit_flag));
        EXPECT_FEQ5(0.5 * a, r[0]);
        EXPECT_FEQ5(0.5, r[1]);

        EXPECT_EQ(1, cell[0]);
        EXPECT_EQ(0, cell[1]);
        EXPECT_AEQ(2 + 0.6, bmap.at(3)[1]);
        EXPECT_FEQ(-sqrt(1 + 0.2 * 0.2) * sin(asin(1. / 1.2) - atan(0.2 / 1.0)), bmap.at(3)[2]);
        EXPECT_AEQ(-(asin(1. / 1.2) - atan(0.2 / 1.0)), bmap.at(3)[3]);

        r = {2 + 0.5 * a, 0.5 - 1e-8};
        v = {-0.2, -1};
        cell = {0, 0};
        EXPECT_TRUE(table.handle_cell(r, v, cell, &bmap, 0, hit_flag));
        EXPECT_FEQ5(0.5 * a, r[0]);
        EXPECT_FEQ5(0.5, r[1]);

        EXPECT_EQ(1, cell[0]);
        EXPECT_EQ(0, cell[1]);
        EXPECT_AEQ(2 + 0.6, bmap.at(4)[1]);
        EXPECT_FEQ(-sqrt(1 + 0.2 * 0.2) * sin(asin(1. / 1.2) + atan(0.2 / 1.0)), bmap.at(4)[2]);
        EXPECT_AEQ(-(asin(1. / 1.2) + atan(0.2 / 1.0)), bmap.at(4)[3]);

        r = {1.2, 1 + 1e-8};
        v = {0.2, 1};
        cell = {0, 0};
        EXPECT_TRUE(table.handle_cell(r, v, cell, &bmap, 0, hit_flag));
        EXPECT_FEQ5(1.2 - a, r[0]);
        EXPECT_FEQ5(0, r[1]);
        EXPECT_EQ(0, cell[0]);
        EXPECT_EQ(1, cell[1]);
        EXPECT_AEQ(2 + 1.2 + 2 + a - 1.2, bmap.at(5)[1]);
        EXPECT_FEQ(-0.2, bmap.at(5)[2]);
        EXPECT_AEQ(-atan(0.2), bmap.at(5)[3]);

        r = {1.2, 1 + 1e-8};
        v = {-0.2, 1};
        cell = {0, 0};
        EXPECT_TRUE(table.handle_cell(r, v, cell, &bmap, 0, hit_flag));
        EXPECT_FEQ5(1.2 - a, r[0]);
        EXPECT_FEQ5(0, r[1]);
        EXPECT_EQ(0, cell[0]);
        EXPECT_EQ(1, cell[1]);
        EXPECT_AEQ(2 + 1.2 + 2 + a - 1.2, bmap.at(6)[1]);
        EXPECT_FEQ(0.2, bmap.at(6)[2]);
        EXPECT_AEQ(atan(0.2), bmap.at(6)[3]);

        r = {0.5 * a, 0.5 + 1e-8};
        v = {-1, -0.1};
        cell = {0, 0};

        EXPECT_TRUE(table.handle_cell(r, v, cell, &bmap, 0, hit_flag));
        EXPECT_FEQ5(2 + 0.5 * a, r[0]);
        EXPECT_FEQ5(0.5, r[1]);

        EXPECT_EQ(-1, cell[0]);
        EXPECT_EQ(0, cell[1]);
        EXPECT_AEQ(2 + 2 + 1.2 + 0.6, bmap.at(7)[1]);
        EXPECT_FEQ(v.norm() * sin(M_PI / 2 - asin(1 / 1.2) + atan(0.1)), bmap.at(7)[2]);
        EXPECT_AEQ(M_PI / 2 - asin(1 / 1.2) + atan(0.1), bmap.at(7)[3]);

        r = {0.5 * a, 0.5 + 1e-8};
        v = {-1, 0.1};
        cell = {0, 0};

        EXPECT_TRUE(table.handle_cell(r, v, cell, &bmap, 0, hit_flag));
        EXPECT_FEQ5(2 + 0.5 * a, r[0]);
        EXPECT_FEQ5(0.5, r[1]);

        EXPECT_EQ(-1, cell[0]);
        EXPECT_EQ(0, cell[1]);
        EXPECT_AEQ(2 + 2 + 1.2 + 0.6, bmap.at(8)[1]);
        EXPECT_FEQ(v.norm() * sin(M_PI / 2 - asin(1 / 1.2) - atan(0.1)), bmap.at(8)[2]);
        EXPECT_AEQ(M_PI / 2 - asin(1 / 1.2) - atan(0.1), bmap.at(8)[3]);

        r = {0.5 * a, 0.5 + 1e-8};
        v = {-0.1, 1};
        cell = {0, 0};

        EXPECT_TRUE(table.handle_cell(r, v, cell, &bmap, 0, hit_flag));
        EXPECT_FEQ5(2 + 0.5 * a, r[0]);
        EXPECT_FEQ5(0.5, r[1]);

        EXPECT_EQ(-1, cell[0]);
        EXPECT_EQ(0, cell[1]);
        EXPECT_AEQ(2 + 2 + 1.2 + 0.6, bmap.at(9)[1]);
        EXPECT_FEQ(-v.norm() * sin(asin(1 / 1.2) - atan(0.1)), bmap.at(9)[2]);
        EXPECT_AEQ(-(asin(1 / 1.2) - atan(0.1)), bmap.at(9)[3]);

        r = {0.5 * a, 0.5 + 1e-8};
        v = {0.1, 1};
        cell = {0, 0};

        EXPECT_TRUE(table.handle_cell(r, v, cell, &bmap, 0, hit_flag));
        EXPECT_FEQ5(2 + 0.5 * a, r[0]);
        EXPECT_FEQ5(0.5, r[1]);

        EXPECT_EQ(-1, cell[0]);
        EXPECT_EQ(0, cell[1]);
        EXPECT_AEQ(2 + 2 + 1.2 + 0.6, bmap.at(10)[1]);
        EXPECT_FEQ(-v.norm() * sin(asin(1 / 1.2) + atan(0.1)), bmap.at(10)[2]);
        EXPECT_AEQ(-(asin(1 / 1.2) + atan(0.1)), bmap.at(10)[3]);
    }

    void triangle_point_is_inside() {
        Triangle table(2, 6);
        EXPECT_FALSE(table.point_is_inside({1.75, 2}));
        EXPECT_FALSE(table.point_is_inside({1.5, 3.5}));
        EXPECT_FALSE(table.point_is_inside({1.25, 5.05}));
        EXPECT_FALSE(table.point_is_inside({1, 6 + 1e-8}));
        EXPECT_FALSE(table.point_is_inside({0.75, 5.05}));
        EXPECT_FALSE(table.point_is_inside({0.5, 3.5}));
        EXPECT_FALSE(table.point_is_inside({0.25, 2}));

        EXPECT_TRUE(table.point_is_inside({1.75, 1}));
        EXPECT_TRUE(table.point_is_inside({1.5, 2.35}));
        EXPECT_TRUE(table.point_is_inside({1.25, 3.66}));
        EXPECT_TRUE(table.point_is_inside({1, 6 * sqrt(0.75) - 1e-8}));
        EXPECT_TRUE(table.point_is_inside({0.75, 3.66}));
        EXPECT_TRUE(table.point_is_inside({0.5, 2.35}));
        EXPECT_TRUE(table.point_is_inside({0.25, 1}));

        for (double x = 1e-8; x < 2; x += 0.1) {
            EXPECT_FALSE(table.point_is_inside({x, -1e-8}));
            EXPECT_TRUE(table.point_is_inside({x, 1e-8}));
        }
    }

    void triangle_handle_crossing_bmap() {
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        bool hit_flag = false;
        pair<int> cell;
        Triangle table(2, 6);

        // Hits to bottom
        Vec2d r = {0.7, -1e-8};
        Vec2d v = {1, -1};
        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(bmap.at(0).at(0), 0);
        EXPECT_AEQ(bmap.at(0).at(1), 0.7);
        EXPECT_AEQ(bmap.at(0).at(2), 1);
        EXPECT_AEQ(bmap.at(0).at(3), M_PI / 4.);

        r = {0.8, -1e-8};
        v = {-1, -2};
        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(bmap.at(1).at(0), 0);
        EXPECT_AEQ(bmap.at(1).at(1), 0.8);
        EXPECT_AEQ(bmap.at(1).at(2), -1);
        EXPECT_AEQ(bmap.at(1).at(3), -atan(1 / 2.));

        r = {1.5, 1.5 * sqrt(3) + 1e-8};
        v = {1, 4};
        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(bmap.at(2).at(0), 0);
        EXPECT_AEQ(bmap.at(2).at(1), 2 + 0.5 * sqrt(28));
        EXPECT_AEQ(bmap.at(2).at(2), sqrt(17) * sin(M_PI / 2 - atan(1 / 4.) - atan(1 / (3 * sqrt(3)))));
        EXPECT_AEQ(bmap.at(2).at(3), M_PI / 2 - atan(1 / 4.) - atan(1 / (3 * sqrt(3))));

        r = {1.5, 1.5 * sqrt(3) + 1e-8};
        v = {1, -4};
        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(bmap.at(3).at(0), 0);
        EXPECT_AEQ(bmap.at(3).at(1), 2 + 0.5 * sqrt(28));
        EXPECT_AEQ(bmap.at(3).at(2), -sqrt(17) * sin(atan(4.) + atan(1 / (3 * sqrt(3)))));
        EXPECT_AEQ(bmap.at(3).at(3), -(atan(4.) + atan(1 / (3 * sqrt(3)))));

        r = {0.5, 1.5 * sqrt(3) + 1e-8};
        v = {-1, 4};
        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(bmap.at(4).at(0), 0);
        EXPECT_AEQ(bmap.at(4).at(1), 2 + 1.5 * sqrt(28));
        EXPECT_AEQ(bmap.at(4).at(2), sqrt(17) * sin(-M_PI / 2 + atan(1 / 4.) + atan(1 / (3 * sqrt(3)))));
        EXPECT_AEQ(bmap.at(4).at(3), -M_PI / 2 + atan(1 / 4.) + atan(1 / (3 * sqrt(3))));

        r = {0.5, 1.5 * sqrt(3) + 1e-8};
        v = {-1, -0.1};
        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(bmap.at(5).at(0), 0);
        EXPECT_AEQ(bmap.at(5).at(1), 2 + 1.5 * sqrt(28));
        EXPECT_AEQ(bmap.at(5).at(2), sqrt(1.01) * sin(atan(0.1) + atan(1 / (3 * sqrt(3)))));
        EXPECT_AEQ(bmap.at(5).at(3), atan(0.1) + atan(1 / (3 * sqrt(3))));
    }

    void circle_point_is_inside() {
        Circle* table;
        RNG rng;
        Vec2d rtmp, ctmp;
        double Rtmp, thetatmp;
        for (unsigned i = 0; i < 100; i++) {
            Rtmp = rng.uniform_rand(0, 100);
            thetatmp = rng.uniform_rand(0, 2 * M_PI);
            ctmp = rng.uniform_rand(0, 100) * Vec2d(cos(thetatmp), sin(thetatmp));

            table = new Circle(ctmp, Rtmp);
            thetatmp = rng.uniform_rand(0, 2 * M_PI);
            rtmp = rng.uniform_rand(0, 100) * Vec2d(cos(thetatmp), sin(thetatmp));
            if ((rtmp - ctmp).norm() < Rtmp)
                EXPECT_TRUE(table->point_is_inside(rtmp));
            else
                EXPECT_FALSE(table->point_is_inside(rtmp));
            delete (table);
        }
    }

    void circle_handle_crossing_bmap() {
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        bool hit_flag = false;
        Circle table({0, 0}, sqrt(2));
        pair<int> cell;

        Vec2d r(1.0, 1 + 1e-5);
        Vec2d v(0.5, 1.0);

        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));
        EXPECT_AEQ(1, r[0]);
        EXPECT_AEQ(1, r[1]);

        EXPECT_AEQ((M_PI / 2 + M_PI / 4.) * sqrt(2), bmap.at(0)[1]);
        EXPECT_AEQ((M_PI / 4 - atan(0.5)), bmap.at(0)[3]);
        EXPECT_AEQ(sin((M_PI / 4 - atan(0.5))) * sqrt(1.25), bmap.at(0)[2]);

        r = {-1, -1 - 1e-5};
        v = {-0.3, -0.4};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ((-M_PI / 4.) * sqrt(2), bmap.at(1)[1]);
        EXPECT_AEQ((atan(0.4 / 0.3) - atan(1)), bmap.at(1)[3]);
        EXPECT_AEQ(sin((atan(0.4 / 0.3) - atan(1))) * sqrt(0.3 * 0.3 + 0.4 * 0.4), bmap.at(1)[2]);
    }

    void ellipse_point_is_inside() {
        Ellipse table(4, 3);

        EXPECT_TRUE(table.point_is_inside({0, 0}));
        EXPECT_FALSE(table.point_is_inside({0, 4.1}));
        EXPECT_TRUE(table.point_is_inside({3.5, 0}));
        EXPECT_TRUE(table.point_is_inside({-3.5, 0}));
        EXPECT_FALSE(table.point_is_inside({0, -4.1}));
        EXPECT_FALSE(table.point_is_inside({0, 3.99}));
        EXPECT_FALSE(table.point_is_inside({0, -3.99}));
        EXPECT_TRUE(table.point_is_inside({0, 2.99}));
        EXPECT_TRUE(table.point_is_inside({0, -2.99}));
        EXPECT_TRUE(table.point_is_inside({2, 2}));
        EXPECT_TRUE(table.point_is_inside({3, 1.5}));
        EXPECT_FALSE(table.point_is_inside({3, 3}));

        Ellipse table2(3, 4);

        EXPECT_TRUE(table2.point_is_inside({0, 0}));
        EXPECT_FALSE(table2.point_is_inside({4.1, 0}));
        EXPECT_TRUE(table2.point_is_inside({0, 3.5}));
        EXPECT_TRUE(table2.point_is_inside({0, 3.5}));
        EXPECT_TRUE(table2.point_is_inside({0, -3.5}));
        EXPECT_FALSE(table2.point_is_inside({-4.1, 0}));
        EXPECT_FALSE(table2.point_is_inside({3.99, 0}));
        EXPECT_FALSE(table2.point_is_inside({-3.99, 0}));
        EXPECT_TRUE(table2.point_is_inside({2.99, 0}));
        EXPECT_TRUE(table2.point_is_inside({-2.99, 0}));
        EXPECT_TRUE(table2.point_is_inside({2, 2}));
        EXPECT_TRUE(table2.point_is_inside({1.5, 3}));
        EXPECT_FALSE(table2.point_is_inside({3, 3}));
    }

    void sinai_point_is_inside() {
        Sinai table(2, 0.4);
        EXPECT_TRUE(table.point_is_inside({0.1, 0.1}));
        EXPECT_FALSE(table.point_is_inside({0.7, 0.9}));
        EXPECT_FALSE(table.point_is_inside({0.1, -1e-8}));
        EXPECT_FALSE(table.point_is_inside({1.9, -1e-8}));
        EXPECT_FALSE(table.point_is_inside({2 + 1e-8, 0.5}));
        EXPECT_FALSE(table.point_is_inside({0.1, 2 + 1e-8}));
        EXPECT_FALSE(table.point_is_inside({1.9, 2 + 1e-8}));
        EXPECT_FALSE(table.point_is_inside({-1e-8, 0.5}));

        EXPECT_TRUE(table.point_is_inside({1.4, 0.34}));
        /*
         * Ugh, this works. Just check the trajectories
         * if you don't believe me. It's late
         * and I just wanna go home.
         */
    }

    void ring_point_is_inside() {
        Ring* table;
        RNG rng;
        Vec2d rtmp, ctmp;
        double Rtmp1, Rtmp2, thetatmp;
        for (unsigned i = 0; i < 100; i++) {
            Rtmp1 = rng.uniform_rand(0, 100);
            Rtmp2 = rng.uniform_rand(0, Rtmp1);
            thetatmp = rng.uniform_rand(0, 2 * M_PI);
            ctmp = {Rtmp1, Rtmp1};

            table = new Ring(Rtmp2, Rtmp1);
            thetatmp = rng.uniform_rand(0, 2 * M_PI);
            rtmp = rng.uniform_rand(0, 100) * Vec2d(cos(thetatmp), sin(thetatmp));
            if ((rtmp - ctmp).norm() < Rtmp1 and (rtmp - ctmp).norm() > Rtmp2)
                EXPECT_TRUE(table->point_is_inside(rtmp));
            else
                EXPECT_FALSE(table->point_is_inside(rtmp));
            delete (table);
        }
    }

    void stadium_point_is_inside() {
        Stadium table(2, 5);
        EXPECT_TRUE(table.point_is_inside({0.071, 2.09}));
        EXPECT_TRUE(table.point_is_inside({0.83, 3.51}));
        EXPECT_TRUE(table.point_is_inside({7.85, 3.64}));
        EXPECT_TRUE(table.point_is_inside({8.07, 0.44}));
        EXPECT_TRUE(table.point_is_inside({5, 2}));

        EXPECT_FALSE(table.point_is_inside({5, -1e-8}));
        EXPECT_FALSE(table.point_is_inside({5, 4 + 1e-8}));
        EXPECT_FALSE(table.point_is_inside({0.4, 0.35}));
        EXPECT_FALSE(table.point_is_inside({8.5, 3.6}));
    }

    void stadium_handle_crossing_bmap() {
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        bool hit_flag = false;
        Stadium table(0.5, 1);
        pair<int> cell;

        // Bottom
        Vec2d r = {1, -1e-8};
        Vec2d v = {1, -1};

        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0, bmap.at(0)[0]);
        EXPECT_AEQ(0.5, bmap.at(0)[1]);
        EXPECT_AEQ(1, bmap.at(0)[2]);
        EXPECT_AEQ(M_PI / 4, bmap.at(0)[3]);

        r = {1, -1e-8};
        v = {-1, -1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0, bmap.at(1)[0]);
        EXPECT_AEQ(0.5, bmap.at(1)[1]);
        EXPECT_AEQ(-1, bmap.at(1)[2]);
        EXPECT_AEQ(-M_PI / 4, bmap.at(1)[3]);

        // Top
        r = {1, 1 + 1e-8};
        v = {-1, 1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0, bmap.at(2)[0]);
        EXPECT_AEQ(1.5 + M_PI * 0.5, bmap.at(2)[1]);
        EXPECT_AEQ(1, bmap.at(2)[2]);
        EXPECT_AEQ(M_PI / 4, bmap.at(2)[3]);

        r = {1, 1 + 1e-8};
        v = {1, 1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0, bmap.at(3)[0]);
        EXPECT_AEQ(1.5 + M_PI * 0.5, bmap.at(3)[1]);
        EXPECT_AEQ(-1, bmap.at(3)[2]);
        EXPECT_AEQ(-M_PI / 4, bmap.at(3)[3]);

        // Right
        r = {1.5 + 0.5 * cos(M_PI / 4), 0.5 - 0.5 * sin(M_PI / 4) - 1e-8};
        v = {1, -11};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0, bmap.at(4)[0]);
        EXPECT_AEQ(1 + M_PI * 0.5 * 0.25, bmap.at(4)[1]);
        EXPECT_TRUE(bmap.at(4)[2] < 0);
        EXPECT_TRUE(bmap.at(4)[3] < 0);

        r = {1.5 + 0.5 * cos(M_PI / 4), 0.5 - 0.5 * sin(M_PI / 4) - 1e-8};
        v = {1, 0.5};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0, bmap.at(5)[0]);
        EXPECT_AEQ(1 + M_PI * 0.5 * 0.25, bmap.at(5)[1]);
        EXPECT_TRUE(bmap.at(5)[2] > 0);
        EXPECT_TRUE(bmap.at(5)[3] > 0);

        // Left
        r = {.5 - 0.5 * cos(M_PI / 4), 0.5 - 0.5 * sin(M_PI / 4) - 1e-8};
        v = {-1, -11};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0, bmap.at(6)[0]);
        EXPECT_AEQ(2 + M_PI * 0.5 * 1.75, bmap.at(6)[1]);
        EXPECT_TRUE(bmap.at(6)[2] > 0);
        EXPECT_TRUE(bmap.at(6)[3] > 0);

        r = {.5 - 0.5 * cos(M_PI / 4), 0.5 - 0.5 * sin(M_PI / 4) - 1e-8};
        v = {-1, 0.5};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0, bmap.at(7)[0]);
        EXPECT_AEQ(2 + M_PI * 0.5 * 1.75, bmap.at(7)[1]);
        EXPECT_TRUE(bmap.at(7)[2] < 0);
        EXPECT_TRUE(bmap.at(7)[3] < 0);
    }

    void mushroom_point_is_inside() {
        Mushroom table(2, 5, 5);
        EXPECT_FALSE(table.point_is_inside({3.99, 4}));
        EXPECT_FALSE(table.point_is_inside({6.01, 4}));
        EXPECT_FALSE(table.point_is_inside({5, -1e-8}));
        EXPECT_FALSE(table.point_is_inside({5, 10 + 1e-8}));
        EXPECT_FALSE(table.point_is_inside({8, 5 - 1e-8}));

        EXPECT_TRUE(table.point_is_inside({1, 6}));
        EXPECT_TRUE(table.point_is_inside({5, 4}));
        EXPECT_TRUE(table.point_is_inside({5, 8}));
        EXPECT_TRUE(table.point_is_inside({8, 8}));
    }

    void mushroom_handle_crossing_bmap() {
        std::vector<std::vector<double>> bmap;
        unsigned hits = 0;
        bool hit_flag = false;
        Mushroom table(0.2, 0.5, 0.5);

        pair<int> cell;

        Vec2d r = {0.5, -1e-8};
        Vec2d v = {1, -1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0.1, bmap.at(0)[1]);
        EXPECT_AEQ(1, bmap.at(0)[2]);
        EXPECT_AEQ(M_PI / 4, bmap.at(0)[3]);

        r = {0.6 + 1e-8, 0.2};
        v = {1, 1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0.4, bmap.at(1)[1]);
        EXPECT_AEQ(1, bmap.at(1)[2]);
        EXPECT_AEQ(M_PI / 4, bmap.at(1)[3]);

        r = {0.6 + 1e-8, 0.2};
        v = {1, -1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0.4, bmap.at(2)[1]);
        EXPECT_AEQ(-1, bmap.at(2)[2]);
        EXPECT_AEQ(-M_PI / 4, bmap.at(2)[3]);

        r = {0.8, 0.5 - 1e-8};
        v = {1, -1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0.9, bmap.at(3)[1]);
        EXPECT_AEQ(1, bmap.at(3)[2]);
        EXPECT_AEQ(M_PI / 4, bmap.at(3)[3]);

        r = {0.8, 0.5 - 1e-8};
        v = {-1, -1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(0.9, bmap.at(4)[1]);
        EXPECT_AEQ(-1, bmap.at(4)[2]);
        EXPECT_AEQ(-M_PI / 4, bmap.at(4)[3]);

        r = {0.5 + 0.5 * cos(M_PI / 4), 0.5 + 0.5 * sin(M_PI / 4) + 1e-8};
        v = {1, 11};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(1.1 + M_PI * .5 * 0.25, bmap.at(5)[1]);
        EXPECT_TRUE(bmap.at(5)[2] > 0);
        EXPECT_TRUE(bmap.at(5)[3] > 0);

        r = {0.5 + 0.5 * cos(M_PI / 4), 0.5 + 0.5 * sin(M_PI / 4) + 1e-8};
        v = {11, -1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(1.1 + M_PI * .5 * 0.25, bmap.at(6)[1]);
        EXPECT_TRUE(bmap.at(6)[2] < 0);
        EXPECT_TRUE(bmap.at(6)[3] < 0);

        r = {0.2, 0.5 - 1e-8};
        v = {1, -1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(1.1 + M_PI * 0.5 + 0.2, bmap.at(7)[1]);
        EXPECT_AEQ(1, bmap.at(7)[2]);
        EXPECT_AEQ(M_PI / 4, bmap.at(7)[3]);

        r = {0.2, 0.5 - 1e-8};
        v = {-1, -1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(1.1 + M_PI * 0.5 + 0.2, bmap.at(8)[1]);
        EXPECT_AEQ(-1, bmap.at(8)[2]);
        EXPECT_AEQ(-M_PI / 4, bmap.at(8)[3]);

        r = {0.2, 0.5 - 1e-8};
        v = {1, -1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(1.1 + M_PI * 0.5 + 0.2, bmap.at(7)[1]);
        EXPECT_AEQ(1, bmap.at(7)[2]);
        EXPECT_AEQ(M_PI / 4, bmap.at(7)[3]);
    }

    void gate_point_is_inside() {
        Gate table(0.3, 1);
        RNG rng;
        double a, b;
        for (unsigned i = 0; i < 1000; i++) {
            a = rng.uniform_rand(0, 1);
            b = rng.uniform_rand(0, 2.1);
            if (b >= 1 and b <= 1.1) {
                if (a >= (1 - 0.3) / 2 and a <= (0.5 + 0.3 / 2))
                    ASSERT_TRUE(table.point_is_inside({a, b}));
                else
                    ASSERT_FALSE(table.point_is_inside({a, b}));
            } else
                ASSERT_TRUE(table.point_is_inside({a, b}));
        }
    }

    void openrectangle_point_is_inside() {
        OpenRectangle table(2, 5, 0.1);
        Vec2d r(0.5, 0.5);
        EXPECT_TRUE(table.point_is_inside(r));
        r = {2.1, 0.5};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {-0.1, 0.5};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {0.5, -0.1};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {0.5, 5.5};
        EXPECT_FALSE(table.point_is_inside(r));
        r = {0.99, 4.9};
        EXPECT_TRUE(table.point_is_inside(r));
    }

    void openrectangle_handle_escape() {
        pair<int> cell;
        unsigned hits = 0;
        bool hit_flag = false;
        OpenRectangle table(2, 5, 0.1);

        Vec2d r = {0.35, -1e-8};
        Vec2d v = {1, -1};

        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));

        EXPECT_TRUE(hit_flag);

        hit_flag = false;

        r = {1.65, -1e-8};
        v = {1, -1};

        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));

        EXPECT_TRUE(hit_flag);

        for (double y = 1e-8; y < 5; y += 0.1) {
            r[0] = 2 + 1e-8;
            r[1] = y;
            v = {1, -1};
            hit_flag = false;

            ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));

            ASSERT_FALSE(hit_flag);

            r[0] = -1e-8;
            r[1] = y;
            v = {-1, -1};
            hit_flag = false;

            ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));

            ASSERT_FALSE(hit_flag);
        }

        for (double x = 1e-8; x < 2; x += 0.1) {
            r[0] = x;
            r[1] = 5 + 1e-8;
            v = {-1, 1};
            hit_flag = false;

            ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));

            ASSERT_FALSE(hit_flag);
        }
    }

    void opencircle_point_is_inside() {
        OpenCircle* table;
        RNG rng;
        Vec2d rtmp, ctmp;
        double Rtmp, thetatmp;
        for (unsigned i = 0; i < 100; i++) {
            Rtmp = rng.uniform_rand(0, 100);
            thetatmp = rng.uniform_rand(0, 2 * M_PI);
            ctmp = rng.uniform_rand(0, 100) * Vec2d(cos(thetatmp), sin(thetatmp));

            table = new OpenCircle(ctmp, Rtmp);
            thetatmp = rng.uniform_rand(0, 2 * M_PI);
            rtmp = rng.uniform_rand(0, 100) * Vec2d(cos(thetatmp), sin(thetatmp));
            if ((rtmp - ctmp).norm() < Rtmp)
                EXPECT_TRUE(table->point_is_inside(rtmp));
            else
                EXPECT_FALSE(table->point_is_inside(rtmp));
            delete (table);
        }
    }

    void opencircle_handle_escape() {
        pair<int> cell;
        unsigned hits = 0;
        bool hit_flag = false;
        OpenCircle table({0, 0}, 1, 0.1);

        Vec2d r;
        Vec2d& v = r;

        for (double t = -M_PI; t <= M_PI; t += 0.1) {
            hit_flag = false;
            r = {cos(t) * (1 + 1e-8), sin(t) * (1 + 1e-8)};
            ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));
            if (t < 0.1 * 2 * M_PI and t > 0)
                EXPECT_TRUE(hit_flag);
            else {
                EXPECT_FALSE(hit_flag);
            }
        }
    }

    void openstadium_point_is_inside() {
        OpenStadium table(2, 5, 0.1);
        EXPECT_TRUE(table.point_is_inside({0.071, 2.09}));
        EXPECT_TRUE(table.point_is_inside({0.83, 3.51}));
        EXPECT_TRUE(table.point_is_inside({7.85, 3.64}));
        EXPECT_TRUE(table.point_is_inside({8.07, 0.44}));
        EXPECT_TRUE(table.point_is_inside({5, 2}));

        EXPECT_FALSE(table.point_is_inside({5, -1e-8}));
        EXPECT_FALSE(table.point_is_inside({5, 4 + 1e-8}));
        EXPECT_FALSE(table.point_is_inside({0.4, 0.35}));
        EXPECT_FALSE(table.point_is_inside({8.5, 3.6}));
    }

    void openstadium_handle_escape() {
        pair<int> cell;
        unsigned hits = 0;
        bool hit_flag = false;
        OpenStadium table(0.5, 1, 0.1);

        Vec2d r = {1, 1 + 1e-8};
        Vec2d v = {1, 1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        hit_flag = false;
        r = {1.24, 1 + 1e-8};
        v = {1, 1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        hit_flag = false;
        r = {0.75, 1 + 1e-8};
        v = {1, 1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        hit_flag = false;
        r = {0.73, 1 + 1e-8};
        v = {1, 1};
        EXPECT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));
        EXPECT_FALSE(hit_flag);
    }

    void opensinai_point_is_inside() {
        OpenSinai table(2, 0.4, 0.1);
        EXPECT_TRUE(table.point_is_inside({0.1, 0.1}));
        EXPECT_FALSE(table.point_is_inside({0.7, 0.9}));
        EXPECT_FALSE(table.point_is_inside({0.1, -1e-8}));
        EXPECT_FALSE(table.point_is_inside({1.9, -1e-8}));
        EXPECT_FALSE(table.point_is_inside({2 + 1e-8, 0.5}));
        EXPECT_FALSE(table.point_is_inside({0.1, 2 + 1e-8}));
        EXPECT_FALSE(table.point_is_inside({1.9, 2 + 1e-8}));
        EXPECT_FALSE(table.point_is_inside({-1e-8, 0.5}));

        EXPECT_TRUE(table.point_is_inside({1.4, 0.34}));
    }

    /* This is actually already tested for 'OpenRectangle'
     * void opensinai_handle_escape(){


    }*/

    void openmushroom_point_is_inside() {
        OpenMushroom table(2, 5, 5);
        EXPECT_FALSE(table.point_is_inside({3.99, 4}));
        EXPECT_FALSE(table.point_is_inside({6.01, 4}));
        EXPECT_FALSE(table.point_is_inside({5, -1e-8}));
        EXPECT_FALSE(table.point_is_inside({5, 10 + 1e-8}));
        EXPECT_FALSE(table.point_is_inside({8, 5 - 1e-8}));

        EXPECT_TRUE(table.point_is_inside({1, 6}));
        EXPECT_TRUE(table.point_is_inside({5, 4}));
        EXPECT_TRUE(table.point_is_inside({5, 8}));
        EXPECT_TRUE(table.point_is_inside({8, 8}));
    }

    void openmushroom_handle_escape() {
        pair<int> cell;
        unsigned hits = 0;
        bool hit_flag = false;
        OpenMushroom table(0.2, 0.5, 0.5, 0.1);

        Vec2d r = {0.5, 1 + 1e-8};
        Vec2d v = {0, 1};

        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        hit_flag = false;
        r[0] = 0.5 + 0.5 * sin(0.35708 * 0.9);
        r[1] = .5 + 0.5 * cos(0.35708 * 0.9) + 1e-8;
        v = {0, 1};

        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        hit_flag = false;
        r[0] = 0.5 + 0.5 * sin(-0.35708 * 0.9);
        r[1] = .5 + 0.5 * cos(-0.35708 * 0.9) + 1e-8;
        v = {0, 1};

        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));
        EXPECT_TRUE(hit_flag);

        hit_flag = false;
        r[0] = 0.5 + 0.5 * sin(0.35708 * 1.1);
        r[1] = .5 + 0.5 * cos(0.35708 * 1.1) + 1e-8;
        v = {0, 1};

        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag));
        EXPECT_FALSE(hit_flag);
    }

    void lorentzgas_point_is_inside() {
        LorentzGas table;

        EXPECT_TRUE(table.point_is_inside({0.5, 1e-8}));
        EXPECT_TRUE(table.point_is_inside({0.75, .4}));
        EXPECT_TRUE(table.point_is_inside({0.5, 0.5}));

        EXPECT_FALSE(table.point_is_inside({0.5, -1e-8}));
        EXPECT_FALSE(table.point_is_inside({0.75, 1e-8}));
        EXPECT_FALSE(table.point_is_inside({1.4, 0.75}));
        EXPECT_FALSE(table.point_is_inside({0.5, 0.75}));
        EXPECT_FALSE(table.point_is_inside({0.2, 0.1}));
    }

    void lorentzgas_handle_crossing_bmap() {
        std::vector<std::vector<double>> bmap;
        pair<int> cell;
        unsigned hits = 0;
        bool hit_flag = false;

        LorentzGas table;

        Vec2d r = {0.3 * cos(3 / 18. * M_PI), 0.3 * sin(3 / 18. * M_PI) - 1e-8};
        Vec2d v = {0, -1};

        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(3. / 36 * 2 * M_PI * 0.3, bmap.at(0)[1]);
        EXPECT_TRUE(bmap.at(0)[2] > 0);
        EXPECT_TRUE(bmap.at(0)[3] > 0);

        r = {0.3 * cos(3 / 18. * M_PI), 0.3 * sin(3 / 18. * M_PI) - 1e-8};
        v = {-1, 0};

        ASSERT_TRUE(table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag));

        EXPECT_AEQ(3. / 36 * 2 * M_PI * 0.3, bmap.at(1)[1]);
        EXPECT_TRUE(bmap.at(1)[2] < 0);
        EXPECT_TRUE(bmap.at(1)[3] < 0);

        r = {0.5, -1e-8};
        v = {1, -1};

        table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag);

        EXPECT_AEQ(6. / 36 * 2 * M_PI * 0.3 + 0.2, bmap.at(2)[1]);
        EXPECT_TRUE(bmap.at(2)[2] > 0);
        EXPECT_TRUE(bmap.at(2)[3] > 0);

        r = {0.5, -1e-8};
        v = {-1, -1};

        table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag);

        EXPECT_AEQ(6. / 36 * 2 * M_PI * 0.3 + 0.2, bmap.at(3)[1]);
        EXPECT_TRUE(bmap.at(3)[2] < 0);
        EXPECT_TRUE(bmap.at(3)[3] < 0);

        r = {1 - 0.3 * cos(3 / 18. * M_PI) + 1e-8, 0.3 * sin(3 / 18. * M_PI)};
        v = {1, 0};

        table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag);
        ASSERT_EQ(5, bmap.size());

        EXPECT_AEQ((6. / 36 + 3 / 36.) * 2 * M_PI * 0.3 + 0.4, bmap.at(4)[1]);
        EXPECT_TRUE(bmap.at(4)[2] > 0);
        EXPECT_TRUE(bmap.at(4)[3] > 0);

        r = {1 - 0.3 * cos(3 / 18. * M_PI) + 1e-8, 0.3 * sin(3 / 18. * M_PI)};
        v = {0, -1};

        table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag);
        ASSERT_EQ(6, bmap.size());

        EXPECT_AEQ((6. / 36 + 3 / 36.) * 2 * M_PI * 0.3 + 0.4, bmap.at(5)[1]);
        EXPECT_TRUE(bmap.at(5)[2] < 0);
        EXPECT_TRUE(bmap.at(5)[3] < 0);

        r = {1.3 + 1e-8, tan(6 / 18. * M_PI) * 0.3};
        v = {0, -1};

        table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag);
        ASSERT_EQ(7, bmap.size());

        EXPECT_AEQ((M_PI - 1) * 0.3 + 0.4 + 0.3 * sqrt(1 + pow(tan(6 / 18. * M_PI), 2)), bmap.at(6)[1]);
        EXPECT_TRUE(bmap.at(6)[2] < 0);
        EXPECT_TRUE(bmap.at(6)[3] < 0);

        r = {1.3 + 1e-8, tan(6 / 18. * M_PI) * 0.3};
        v = {1, 0};

        table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag);
        ASSERT_EQ(8, bmap.size());

        EXPECT_AEQ((M_PI - 1) * 0.3 + 0.4 + 0.3 * sqrt(1 + pow(tan(6 / 18. * M_PI), 2)), bmap.at(7)[1]);
        EXPECT_TRUE(bmap.at(7)[2] > 0);
        EXPECT_TRUE(bmap.at(7)[3] > 0);
    }
};

TEST(Rectangle, constructor) {
    Rectangle table(0.5, 2);
    EXPECT_EQ(table.getMaxCoords(), pair<double>(0.5, 2));
}

TEST(Rectangle, shape_name) {
    Rectangle table(2, 5);
    EXPECT_EQ(table.shape_name(), "rectangle(2,5)");
}

TEST(Rectangle, point_is_inside) {
    Rectangle table(2, 5);
    Vec2d r(0.5, 0.5);
    EXPECT_TRUE(table.point_is_inside(r));
    r = {2.1, 0.5};
    EXPECT_FALSE(table.point_is_inside(r));
    r = {-0.1, 0.5};
    EXPECT_FALSE(table.point_is_inside(r));
    r = {0.5, -0.1};
    EXPECT_FALSE(table.point_is_inside(r));
    r = {0.5, 5.5};
    EXPECT_FALSE(table.point_is_inside(r));
    r = {0.99, 4.9};
    EXPECT_TRUE(table.point_is_inside(r));
}

TEST(Rectangle, handle_boundary_crossings_bottom_ccw) {
    Rectangle table(1.2, 2);
    unsigned hits = 0;
    pair<int> cell;
    bool hit_flag = false;

    Vec2d r(0.3, -1e-4);
    Vec2d v(1.0, -1.0);

    ASSERT_FALSE(table.point_is_inside(r));

    table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag);

    ASSERT_TRUE(table.point_is_inside(r));
    EXPECT_FEQ(1, v[0]);
    EXPECT_FEQ(1, v[1]);
    EXPECT_AEQ(0.3, r[0]);
    EXPECT_AEQ(0, r[1]);
    EXPECT_EQ(1, hits);
}

TEST(Rectangle, handle_boundary_crossings_bottom_cw) {
    Rectangle table(1.2, 2);
    unsigned hits = 0;
    pair<int> cell;
    bool hit_flag = false;

    Vec2d r(0.5, -1e-4);
    Vec2d v(-1.0, -1.0);

    ASSERT_FALSE(table.point_is_inside(r));

    table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag);

    ASSERT_TRUE(table.point_is_inside(r));
    EXPECT_FEQ(-1, v[0]);
    EXPECT_FEQ(1, v[1]);
    EXPECT_AEQ(0, r[1]);
    EXPECT_AEQ(0.5, r[0]);
    EXPECT_EQ(1, hits);
}

TEST(Rectangle, handle_boundary_crossings_right_ccw) {
    Rectangle table(1.2, 2);
    pair<int> cell;
    unsigned hits = 0;
    bool hit_flag = false;

    Vec2d r(1.2 + 1e-4, 0.5);
    Vec2d v(1.0, 1.0);

    ASSERT_FALSE(table.point_is_inside(r));

    table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag);

    ASSERT_TRUE(table.point_is_inside(r));
    EXPECT_FEQ(-1, v[0]);
    EXPECT_FEQ(1, v[1]);
    EXPECT_AEQ(1.2, r[0]);
    EXPECT_AEQ(0.5, r[1]);
    EXPECT_EQ(1, hits);
}

TEST(Rectangle, handle_boundary_crossings_right_cw) {
    Rectangle table(1.2, 2);
    pair<int> cell;
    unsigned hits = 0;
    bool hit_flag = false;

    Vec2d r(1.2 + 1e-4, 0.5);
    Vec2d v(1.0, -1.0);

    ASSERT_FALSE(table.point_is_inside(r));

    table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag);

    ASSERT_TRUE(table.point_is_inside(r));
    EXPECT_FEQ(-1, v[0]);
    EXPECT_FEQ(-1, v[1]);
    EXPECT_AEQ(1.2, r[0]);
    EXPECT_AEQ(0.5, r[1]);
    EXPECT_EQ(1, hits);
}

TEST(Rectangle, handle_boundary_crossings_top_ccw) {
    Rectangle table(1.2, 2);
    pair<int> cell;
    unsigned hits = 0;
    bool hit_flag = false;

    Vec2d r(0.6, 2 + 1e-4);
    Vec2d v(-1.0, 1.0);

    ASSERT_FALSE(table.point_is_inside(r));

    table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag);

    ASSERT_TRUE(table.point_is_inside(r));
    EXPECT_FEQ(-1, v[0]);
    EXPECT_FEQ(-1, v[1]);
    EXPECT_AEQ(0.6, r[0]);
    EXPECT_AEQ(2, r[1]);
    EXPECT_EQ(1, hits);
}

TEST(Rectangle, handle_boundary_crossings_top_cw) {
    Rectangle table(1.2, 2);
    pair<int> cell;
    unsigned hits = 0;
    bool hit_flag = false;

    Vec2d r(0.6, 2 + 1e-4);
    Vec2d v(1.0, 1.0);

    EXPECT_FALSE(table.point_is_inside(r));

    table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag);

    ASSERT_TRUE(table.point_is_inside(r));
    EXPECT_FEQ(1, v[0]);
    EXPECT_FEQ(-1, v[1]);
    EXPECT_AEQ(0.6, r[0]);
    EXPECT_AEQ(2, r[1]);
    EXPECT_EQ(1, hits);
}

TEST(Rectangle, handle_boundary_crossings_left_ccw) {
    Rectangle table(1.2, 2);
    pair<int> cell;
    unsigned hits = 0;
    bool hit_flag = false;

    Vec2d r(-1e-4, 0.5);
    Vec2d v(-1.0, -1.0);

    EXPECT_FALSE(table.point_is_inside(r));

    table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag);

    ASSERT_TRUE(table.point_is_inside(r));
    EXPECT_FEQ(1, v[0]);
    EXPECT_FEQ(-1, v[1]);
    EXPECT_AEQ(0, r[0]);
    EXPECT_AEQ(0.5, r[1]);
    EXPECT_EQ(1, hits);
}

TEST(Rectangle, handle_boundary_crossings_left_cw) {
    Rectangle table(1.2, 2);
    pair<int> cell;
    unsigned hits = 0;
    bool hit_flag = false;

    Vec2d r(-1e-4, 0.5);
    Vec2d v(-1.0, 1.0);

    EXPECT_FALSE(table.point_is_inside(r));

    table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag);

    ASSERT_TRUE(table.point_is_inside(r));
    EXPECT_FEQ(1, v[0]);
    EXPECT_FEQ(1, v[1]);
    EXPECT_AEQ(0, r[0]);
    EXPECT_AEQ(0.5, r[1]);
    EXPECT_EQ(1, hits);
}

TEST(Rectangle, handle_boundary_crossings_bottom_ccw_bmap) {
    Rectangle table(1.2, 2);
    std::vector<std::vector<double>> bmap;
    pair<int> cell;
    unsigned hits = 0;
    bool hit_flag = false;

    Vec2d r(0.3, -1e-4);
    Vec2d v(1.0, -1.0);

    EXPECT_FALSE(table.point_is_inside(r));

    table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag);

    ASSERT_TRUE(table.point_is_inside(r));
    EXPECT_FEQ(1, v[0]);
    EXPECT_FEQ(1, v[1]);
    EXPECT_AEQ(0.3, r[0]);
    EXPECT_AEQ(0, r[1]);

    EXPECT_FEQ(0, bmap[0][0]);
    EXPECT_FEQ(0.3, bmap[0][1]);
    EXPECT_FEQ(1, bmap[0][2]);
    EXPECT_FEQ(M_PI / 4., bmap[0][3]);
    EXPECT_EQ(1, hits);
}

TEST(Rectangle, handle_boundary_crossings_bottom_cw_bmap) {
    Rectangle table(1.2, 2);
    std::vector<std::vector<double>> bmap;
    pair<int> cell;
    unsigned hits = 0;
    bool hit_flag = false;

    Vec2d r(0.5, -1e-4);
    Vec2d v(-1.0, -1.0);

    EXPECT_FALSE(table.point_is_inside(r));

    table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag);

    ASSERT_TRUE(table.point_is_inside(r));
    EXPECT_FEQ(-1, v[0]);
    EXPECT_FEQ(1, v[1]);
    EXPECT_AEQ(0, r[1]);
    EXPECT_AEQ(0.5, r[0]);

    EXPECT_FEQ(0, bmap[0][0]);
    EXPECT_FEQ(0.5, bmap[0][1]);
    EXPECT_FEQ(-1, bmap[0][2]);
    EXPECT_FEQ(-M_PI / 4., bmap[0][3]);
    EXPECT_EQ(1, hits);
}

TEST(NoTable, constructor) {
    NoTable table(0.5, 2);
    EXPECT_EQ(table.getMaxCoords(), pair<double>(0.5, 2));
}

TEST(NoTable, shape_name) {
    NoTable table(2, 5);
    EXPECT_EQ(table.shape_name(), "NoTable");
}

TEST(NoTable, point_is_inside) {
    NoTable table(2, 5);
    Vec2d r(0.5, 0.5);
    EXPECT_TRUE(table.point_is_inside(r));
    r = {2.1, 0.5};
    EXPECT_TRUE(table.point_is_inside(r));
    r = {-0.1, 0.5};
    EXPECT_TRUE(table.point_is_inside(r));
    r = {0.5, -0.1};
    EXPECT_TRUE(table.point_is_inside(r));
    r = {0.5, 5.5};
    EXPECT_TRUE(table.point_is_inside(r));
    r = {0.99, 4.9};
    EXPECT_TRUE(table.point_is_inside(r));
}

TEST(NoTable, handle_boundary_crossings_left_ccw) {
    NoTable table(1.2, 2);
    pair<int> cell;
    unsigned hits = 0;
    bool hit_flag = false;

    Vec2d r(-1e-4, 0.5);
    Vec2d v(-1.0, -1.0);
    std::vector<std::vector<double>> tmp;

    table.handle_boundary_crossings(r, v, cell, nullptr, 0, hits, hit_flag);

    EXPECT_EQ(-1, v[0]);
    EXPECT_EQ(-1, v[1]);
    EXPECT_EQ(-1e-4, r[0]);
    EXPECT_EQ(0.5, r[1]);
}

TEST(NoTable, handle_boundary_crossings_bottom_ccw) {
    NoTable table(1.2, 2);
    std::vector<std::vector<double>> bmap;
    pair<int> cell;
    unsigned hits = 0;
    bool hit_flag = false;

    Vec2d r(0.3, -1e-4);
    Vec2d v(1.0, -1.0);

    table.handle_boundary_crossings(r, v, cell, &bmap, 0, hits, hit_flag);

    EXPECT_EQ(0, bmap.size());
}

// Test Compoundshapes

TEST_F(compoundshape_tests, boundaryline_constructor) { boundaryline_constructor(); }

TEST_F(compoundshape_tests, boundaryline_point_is_inside) { boundaryline_point_is_inside(); }

TEST_F(compoundshape_tests, boundaryline_handle_crossing_forbidupper) { boundaryline_handle_crossing_forbidupper(); }

TEST_F(compoundshape_tests, boundaryline_handle_crossing_forbidlower) { boundaryline_handle_crossing_forbidlower(); }

TEST_F(compoundshape_tests, boundaryline_handle_crossing_bmap_forbidupper) {
    boundaryline_handle_crossing_bmap_forbidupper();
}

TEST_F(compoundshape_tests, boundaryline_handle_crossing_bmap_forbidlower) {
    boundaryline_handle_crossing_bmap_forbidlower();
}

TEST_F(compoundshape_tests, boundaryhorizontalline_point_is_inside) { boundaryhorizontalline_point_is_inside(); }

TEST_F(compoundshape_tests, boundaryhorizontalline_handle_crossing_forbidlower) {
    boundaryhorizontalline_handle_crossing_forbidlower();
}

TEST_F(compoundshape_tests, boundaryhorizontalline_handle_crossing_forbidupper) {
    boundaryhorizontalline_handle_crossing_forbidupper();
}

TEST_F(compoundshape_tests, boundaryhorizontalline_handle_crossing_bmap_forbidlower) {
    boundaryhorizontalline_handle_crossing_bmap_forbidlower();
}

TEST_F(compoundshape_tests, boundaryhorizontalline_handle_crossing_bmap_forbidupper) {
    boundaryhorizontalline_handle_crossing_bmap_forbidupper();
}

TEST_F(compoundshape_tests, boundaryhorizontalray_point_is_inside) { boundaryhorizontalray_point_is_inside(); }

TEST_F(compoundshape_tests, boundaryhorizontalray_handle_crossing_forbidlower) {
    boundaryhorizontalray_handle_crossing_forbidlower();
}

TEST_F(compoundshape_tests, boundaryhorizontalray_handle_crossing_forbidupper) {
    boundaryhorizontalray_handle_crossing_forbidupper();
}

TEST_F(compoundshape_tests, boundaryhorizontalray_handle_crossing_bmap_forbidlower) {
    boundaryhorizontalray_handle_crossing_bmap_forbidlower();
}

TEST_F(compoundshape_tests, boundaryhorizontalray_handle_crossing_bmap_forbidupper) {
    boundaryhorizontalray_handle_crossing_bmap_forbidupper();
}

TEST_F(compoundshape_tests, boundarycircle_point_is_inside) { boundarycircle_point_is_inside(); }

TEST_F(compoundshape_tests, boundarycircle_handle_crossing_forbidinside) {
    boundarycircle_handle_crossing_forbidinside();
}

TEST_F(compoundshape_tests, boundarycircle_handle_crossing_forbidoutside) {
    boundarycircle_handle_crossing_forbidoutside();
}

TEST_F(compoundshape_tests, boundarycircle_handle_crossing_forbidinside_CCW_S0Right_bmap) {
    boundarycircle_handle_crossing_forbidinside_CCW_S0Right_bmap();
}

TEST_F(compoundshape_tests, boundarycircle_handle_crossing_forbidoutside_CCW_S0Right_bmap) {
    boundarycircle_handle_crossing_forbidoutside_CCW_S0Right_bmap();
}

TEST_F(compoundshape_tests, boundarycircle_handle_crossing_forbidinside_CCW_S0Down_bmap) {
    boundarycircle_handle_crossing_forbidinside_CCW_S0Down_bmap();
}

TEST_F(compoundshape_tests, boundarycircle_handle_crossing_forbidoutside_CCW_S0Down_bmap) {
    boundarycircle_handle_crossing_forbidoutside_CCW_S0Down_bmap();
}

TEST_F(compoundshape_tests, boundarycircle_handle_crossing_forbidinside_CW_S0Right_bmap) {
    boundarycircle_handle_crossing_forbidinside_CW_S0Right_bmap();
}

TEST_F(compoundshape_tests, boundarycircle_handle_crossing_forbidoutside_CW_S0Right_bmap) {
    boundarycircle_handle_crossing_forbidoutside_CW_S0Right_bmap();
}

TEST_F(compoundshape_tests, boundarycircle_handle_crossing_forbidinside_CW_S0Down_bmap) {
    boundarycircle_handle_crossing_forbidinside_CW_S0Down_bmap();
}

TEST_F(compoundshape_tests, boundarycircle_handle_crossing_forbidoutside_CW_S0Down_bmap) {
    boundarycircle_handle_crossing_forbidoutside_CW_S0Down_bmap();
}

TEST_F(compoundshape_tests, boundaryellipse_point_is_inside) { boundaryellipse_point_is_inside(); }

TEST_F(compoundshape_tests, boundaryellipse_handle_crossing) { boundaryellipse_handle_crossing(); }

TEST_F(compoundshape_tests, boundaryellipse_handle_crossing_bmap) { boundaryellipse_handle_crossing_bmap(); }

TEST_F(compoundshape_tests, boundaryrectangle_point_is_inside) { boundaryrectangle_point_is_inside(); }

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_left_forbidoutside) {
    boundaryrectangle_handle_crossing_left_forbidoutside();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_bottom_forbidoutside) {
    boundaryrectangle_handle_crossing_bottom_forbidoutside();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_right_forbidoutside) {
    boundaryrectangle_handle_crossing_right_forbidoutside();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_top_forbidoutside) {
    boundaryrectangle_handle_crossing_top_forbidoutside();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_left_forbidinside) {
    boundaryrectangle_handle_crossing_left_forbidinside();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_bottom_forbidinside) {
    boundaryrectangle_handle_crossing_bottom_forbidinside();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_right_forbidinside) {
    boundaryrectangle_handle_crossing_right_forbidinside();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_top_forbidinside) {
    boundaryrectangle_handle_crossing_top_forbidinside();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_left_forbidoutside_bmap) {
    boundaryrectangle_handle_crossing_left_forbidoutside_bmap();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_bottom_forbidoutside_bmap) {
    boundaryrectangle_handle_crossing_bottom_forbidoutside_bmap();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_right_forbidoutside_bmap) {
    boundaryrectangle_handle_crossing_right_forbidoutside_bmap();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_top_forbidoutside_bmap) {
    boundaryrectangle_handle_crossing_top_forbidoutside_bmap();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_left_forbidinside_bmap) {
    boundaryrectangle_handle_crossing_left_forbidinside_bmap();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_bottom_forbidinside_bmap) {
    boundaryrectangle_handle_crossing_bottom_forbidinside_bmap();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_right_forbidinside_bmap) {
    boundaryrectangle_handle_crossing_right_forbidinside_bmap();
}

TEST_F(compoundshape_tests, boundaryrectangle_handle_crossing_top_forbidinside_bmap) {
    boundaryrectangle_handle_crossing_top_forbidinside_bmap();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_point_is_inside_top_right) {
    boundaryrectanglecorner_point_is_inside_top_right();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_point_is_inside_top_left) {
    boundaryrectanglecorner_point_is_inside_top_left();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_point_is_inside_bottom_left) {
    boundaryrectanglecorner_point_is_inside_bottom_left();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_point_is_inside_bottom_right) {
    boundaryrectanglecorner_point_is_inside_bottom_right();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_handle_crossing_top_right) {
    boundaryrectanglecorner_handle_crossing_top_right();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_handle_crossing_top_left) {
    boundaryrectanglecorner_handle_crossing_top_left();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_handle_crossing_bottom_left) {
    boundaryrectanglecorner_handle_crossing_bottom_left();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_handle_crossing_bottom_right) {
    boundaryrectanglecorner_handle_crossing_bottom_right();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_handle_crossing_top_right_bmap) {
    boundaryrectanglecorner_handle_crossing_top_right_bmap();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_handle_crossing_top_left_bmap) {
    boundaryrectanglecorner_handle_crossing_top_left_bmap();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_handle_crossing_bottom_left_bmap) {
    boundaryrectanglecorner_handle_crossing_bottom_left_bmap();
}

TEST_F(compoundshape_tests, boundaryrectanglecorner_handle_crossing_bottom_right_bmap) {
    boundaryrectanglecorner_handle_crossing_bottom_right_bmap();
}

TEST_F(compoundshape_tests, openboundaryrectangle_point_is_inside) { openboundaryrectangle_point_is_inside(); }

TEST_F(compoundshape_tests, openboundaryrectangle_handle_crossing_left_forbidoutside) {
    openboundaryrectangle_handle_crossing_left_forbidoutside();
}

TEST_F(compoundshape_tests, openboundaryrectangle_handle_crossing_bottom_forbidoutside) {
    openboundaryrectangle_handle_crossing_bottom_forbidoutside();
}

TEST_F(compoundshape_tests, openboundaryrectangle_handle_crossing_right_forbidoutside) {
    openboundaryrectangle_handle_crossing_right_forbidoutside();
}

TEST_F(compoundshape_tests, openboundaryrectangle_handle_crossing_top_forbidoutside) {
    openboundaryrectangle_handle_crossing_top_forbidoutside();
}

TEST_F(compoundshape_tests, openboundaryrectangle_handle_crossing_left_forbidinside) {
    openboundaryrectangle_handle_crossing_left_forbidinside();
}

TEST_F(compoundshape_tests, openboundaryrectangle_handle_crossing_bottom_forbidinside) {
    openboundaryrectangle_handle_crossing_bottom_forbidinside();
}

TEST_F(compoundshape_tests, openboundaryrectangle_handle_crossing_right_forbidinside) {
    openboundaryrectangle_handle_crossing_right_forbidinside();
}

TEST_F(compoundshape_tests, openboundaryrectangle_handle_crossing_top_forbidinside) {
    openboundaryrectangle_handle_crossing_top_forbidinside();
}

TEST_F(compoundshape_tests, openboundaryrectangle_handle_escape_forbidoutside_single_hole) {
    openboundaryrectangle_handle_escape_forbidoutside_single_hole();
}

TEST_F(compoundshape_tests, openboundaryrectangle_handle_escape_forbidoutside_two_holes) {
    openboundaryrectangle_handle_escape_forbidoutside_two_holes();
}

TEST_F(compoundshape_tests, openboundaryrectangle_handle_escape_forbidinside_single_hole) {
    openboundaryrectangle_handle_escape_forbidinside_single_hole();
}

TEST_F(compoundshape_tests, openboundaryrectangle_handle_escape_forbidinside_two_holes) {
    openboundaryrectangle_handle_escape_forbidinside_two_holes();
}

TEST_F(compoundshape_tests, openboundarycircle_point_is_inside) { openboundarycircle_point_is_inside(); }

TEST_F(compoundshape_tests, openboundarycircle_handle_crossing_forbidinside) {
    openboundarycircle_handle_crossing_forbidinside();
}

TEST_F(compoundshape_tests, openboundarycircle_handle_crossing_forbidoutside) {
    openboundarycircle_handle_crossing_forbidoutside();
}

TEST_F(compoundshape_tests, openboundarycircle_handle_escape_forbidoutside1) {
    openboundarycircle_handle_escape_forbidoutside1();
}

TEST_F(compoundshape_tests, openboundarycircle_handle_escape_forbidoutside2) {
    openboundarycircle_handle_escape_forbidoutside2();
}

TEST_F(compoundshape_tests, openboundarycircle_handle_escape_forbidoutside3) {
    openboundarycircle_handle_escape_forbidoutside3();
}

TEST_F(compoundshape_tests, openboundarycircle_handle_escape_forbidinside1) {
    openboundarycircle_handle_escape_forbidinside1();
}

TEST_F(compoundshape_tests, openboundarycircle_handle_escape_forbidinside2) {
    openboundarycircle_handle_escape_forbidinside2();
}

TEST_F(compoundshape_tests, openboundarycircle_handle_escape_forbidinside3) {
    openboundarycircle_handle_escape_forbidinside3();
}

TEST_F(compoundshape_tests, openboundaryhorizontalline_point_is_inside) {
    openboundaryhorizontalline_point_is_inside();
}

TEST_F(compoundshape_tests, openboundaryhorizontalline_handle_crossing_forbidlower) {
    openboundaryhorizontalline_handle_crossing_forbidlower();
}

TEST_F(compoundshape_tests, openboundaryhorizontalline_handle_crossing_forbidupper) {
    openboundaryhorizontalline_handle_crossing_forbidupper();
}

TEST_F(compoundshape_tests, openboundaryhorizontalline_handle_escape_forbidlower) {
    openboundaryhorizontalline_handle_escape_forbidlower();
}

TEST_F(compoundshape_tests, openboundaryhorizontalline_handle_escape_forbidupper) {
    openboundaryhorizontalline_handle_escape_forbidupper();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_point_is_inside_top_right) {
    openboundaryrectanglecorner_point_is_inside_top_right();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_point_is_inside_top_left) {
    openboundaryrectanglecorner_point_is_inside_top_left();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_point_is_inside_bottom_left) {
    openboundaryrectanglecorner_point_is_inside_bottom_left();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_point_is_inside_bottom_right) {
    openboundaryrectanglecorner_point_is_inside_bottom_right();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_handle_crossing_bottom_left) {
    openboundaryrectanglecorner_handle_crossing_bottom_left();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_handle_crossing_bottom_right) {
    openboundaryrectanglecorner_handle_crossing_bottom_right();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_handle_crossing_top_right) {
    openboundaryrectanglecorner_handle_crossing_top_right();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_handle_crossing_top_left) {
    openboundaryrectanglecorner_handle_crossing_top_left();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_handle_escape_top_right) {
    openboundaryrectanglecorner_handle_escape_top_right();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_handle_escape_top_left) {
    openboundaryrectanglecorner_handle_escape_top_left();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_handle_escape_bottom_right) {
    openboundaryrectanglecorner_handle_escape_bottom_right();
}

TEST_F(compoundshape_tests, openboundaryrectanglecorner_handle_escape_bottom_left) {
    openboundaryrectanglecorner_handle_escape_bottom_left();
}

TEST_F(compoundshape_tests, compoundshapeperiodic_point_is_inside) { compoundshapeperiodic_point_is_inside(); }

TEST_F(compoundshape_tests, compoundshapeperiodic_handle_cell) { compoundshapeperiodic_handle_cell(); }

TEST_F(compoundshape_tests, compoundshapeperiodic_handle_cell_bmap) { compoundshapeperiodic_handle_cell_bmap(); }

TEST_F(compoundshape_tests, triangle_point_is_inside) { triangle_point_is_inside(); }

TEST_F(compoundshape_tests, triangle_handle_crossing_bmap) { triangle_handle_crossing_bmap(); }

TEST_F(compoundshape_tests, circle_point_is_inside) { circle_point_is_inside(); }

TEST_F(compoundshape_tests, circle_handle_crossing_bmap) { circle_handle_crossing_bmap(); }

TEST_F(compoundshape_tests, ellipse_point_is_inside) { ellipse_point_is_inside(); }

TEST_F(compoundshape_tests, sinai_point_is_inside) { sinai_point_is_inside(); }

TEST_F(compoundshape_tests, ring_point_is_inside) { ring_point_is_inside(); }

TEST_F(compoundshape_tests, stadium_point_is_inside) { stadium_point_is_inside(); }

TEST_F(compoundshape_tests, stadium_handle_crossing_bmap) { stadium_handle_crossing_bmap(); }

TEST_F(compoundshape_tests, mushroom_point_is_inside) { mushroom_point_is_inside(); }

TEST_F(compoundshape_tests, mushroom_handle_crossing_bmap) { mushroom_handle_crossing_bmap(); }

TEST_F(compoundshape_tests, gate_point_is_inside) { gate_point_is_inside(); }

TEST_F(compoundshape_tests, openrectangle_point_is_inside) { openrectangle_point_is_inside(); }

TEST_F(compoundshape_tests, openrectangle_handle_escape) { openrectangle_handle_escape(); }

TEST_F(compoundshape_tests, opencircle_point_is_inside) { opencircle_point_is_inside(); }

TEST_F(compoundshape_tests, opencircle_handle_escape) { opencircle_handle_escape(); }

TEST_F(compoundshape_tests, openstadium_point_is_inside) { openstadium_point_is_inside(); }

TEST_F(compoundshape_tests, openstadium_handle_escape) { openstadium_handle_escape(); }

TEST_F(compoundshape_tests, opensinai_point_is_inside) { opensinai_point_is_inside(); }

TEST_F(compoundshape_tests, openmushroom_point_is_inside) { openmushroom_point_is_inside(); }

TEST_F(compoundshape_tests, openmushroom_handle_escape) { openmushroom_handle_escape(); }

TEST_F(compoundshape_tests, lorentzgas_point_is_inside) { lorentzgas_point_is_inside(); }

TEST_F(compoundshape_tests, lorentzgas_handle_crossing_bmap) { lorentzgas_handle_crossing_bmap(); }
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    testing::FLAGS_gtest_death_test_style = "threadsafe";

    return RUN_ALL_TESTS();
}
