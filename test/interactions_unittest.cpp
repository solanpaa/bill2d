/*
 * interactions_unittest.cpp
 *
 * This file is part of bill2d.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "interactions.hpp"
#include "vec2d.hpp"
#include "gtest/gtest-spi.h"
#include "gtest/gtest.h"
#include <cmath>
#include <vector>

#define EXPECT_FEQ5(A, B) EXPECT_NEAR(A, B, 1e-5)

#define EXPECT_FEQ(A, B) EXPECT_NEAR(A, B, 1e-10)

#define EXPECT_FNE(A, B) EXPECT_TRUE(fabsl((A) - (B)) > 1e-10)

namespace bill2d {

TEST(CoulombInteraction, force) {
    CoulombInteraction coulomb(1.5);
    Vec2d r1(1.0, 2.0);
    Vec2d r2(5.0, 3.0);
    Vec2d tmp = r1 - r2;
    tmp = tmp / pow(tmp.norm(), 3) * 1.5;
    EXPECT_FEQ(tmp[0], (coulomb.force(r1, r2))[0]);
    EXPECT_FEQ(tmp[1], (coulomb.force(r1, r2))[1]);
}

TEST(CoulombInteraction, energy) {
    CoulombInteraction coulomb(1.5);
    Vec2d r1(1.0, 2.0);
    Vec2d r2(5.0, 3.0);
    Vec2d tmp = r1 - r2;
    EXPECT_FEQ(1.5 / tmp.norm(), (coulomb.energy(r1, r2)));
}
}  // namespace bill2d

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
