# Bill2d README

[![pipeline status](https://gitlab.com/qcad.fi/bill2d_internal/badges/master/pipeline.svg)](https://gitlab.com/qcad.fi/bill2d_internal/commits/master)

## Introduction

Bill2d is a C++ software package for classical simulation two-dimensional
Hamiltonian systems with single or many particles. It can be used to simulate, e.g.,
hard-wall billiards, open billiards and periodic systems including, e.g., external potentials,
particle-particle interactions, and a magnetic field.

The software and additional scripts can be readily used to calculate and visualize several
aspects of the systems including, e.g., particle trajectories, histograms, Poincaré
sections, diffusion coefficients, and survival and escape probabilities.

If your use of this software leads to a publication, please cite our paper [Comp. Phys. Comm. 199, 133-138 (2016).](http://dx.doi.org/10.1016/j.cpc.2015.10.006)

## Program

The Bill2d package consists several binaries for specialized purposes. See USERGUIDE for more details and a tutorial.

In addition, the package contains several Python scripts for visualization and analysis of
the simulated data, e.g., trajectories, Poincaré sections, and energies, just to name
a few.

##  Installation

Bill2d uses CMake (v3.2 or later) for the deployment process. 

First you should obtain a C++14 compliant compiler, e.g., 
GCC 4.9 or later, Clang 3.7 or later, or Intel C++ Compiler 15 or later.
You can specify the compiler with **CXX** environment flag, i.e.,
```
$ CXX=my_super_compiler cmake .
```

Next, install the dependencies:

* HDF5 and its C++ API (www.hdfgroup.org/HDF5/)
* Boost C++ Libraries (www.boost.org/)
* GNU Scientific Library (www.gnu.org/software/gsl/)
* Doxygen (optional, for building API docs)
* Pdflatex (optional, for building developer manual)

For scripts, you should install Python 3,
and the following packages:

* Numpy (numpy.org)
* Matplotlib (matplotlib.org)
* H5py (h5py.org)

If wish to run the unit tests, please make sure you have a working internet
connection before installing as we download Google Test and Google Mock
during installation.


### LINUX/UNIX/OS X
After installing the dependencies and making sure that they can be found
in the search paths of the compiler, you can run

```
$ cmake -DCMAKE_INSTALL_PREFIX=/path/to/install/directory .
$ make
$ make install
```

to compile and install the Bill2d software package.

Note #1: If you get 'Illegal Instruction' when running the code, we've failed to correctly autodetect vectorization instructions supported by your processor. Use the CMake flag '-D SIMD_INSTRUCTIONS="SSE2 AVX"' to enable both SSE2 and AVX, and just drop one or both of them to disable those instructions. Use '-D CROSSCOMPILING=ON' to disable configuration checks for vectorization instructions.

Note #2: If you have headers, libraries, and programs installed in non-standard paths, please tell them to CMake using the options CMAKE_INCLUDE_PATH, CMAKE_LIBRARY_PATH, and CMAKE_PREFIX_PATH. For Boost, you also have BOOST_ROOT, BOOST_INCLUDEDIR, and BOOST_LIBRARYDIR. For HDF5, HDF5_ROOT. For GSL, GSL_ROOT_DIR. Example:
```
$ cmake -D BOOST_ROOT=/path/to/my/boost/prefix
```

### OTHER OPERATING SYSTEMS:
Consult CMake documentation. Installing python-scripts etc. is tailored
for UNIX systems, you might have to install them manually for other systems.


### Disabling some parts of installation

Scripts and their installation can be disabled with the CMake option '-DENABLE_SCRIPTS=OFF'.

Unit tests can be disabled with the CMake option '-DENABLE_TESTING=OFF'.

A binary for searching regular orbits in the phase space can be omitted with
'-D ENABLE_PHASE_SPACE_REGULARITY=OFF'.

Documentation can be disabled with '-D ENABLE_DOCUMENTATION=OFF'.

### Running unit tests

You can run the unit tests with 
```
$ make check
```
and/or with
```
$ make test
```

## Guides

For examples and a short user guide, see the file USERGUIDE.

The API documentation is at docs/bill2d_api.pdf.

The developer manual is at docs/developer_manual.pdf.



## Obtaining the latest release

The latest source code for bill2d is hosted at https://gitlab.com/qcad.fi/bill2d.


## Copying / license

Bill2d is licensed under GNU General Public License version. For the license, see the file COPYING.

While compiling unit tests, CMake downloads also Google Test and Google Mock, developed by Google Inc. These are licensed with the New BSD License.

In addition we incorporate a few CMake modules by Ryan Pavlik and David Corvoysier (licensed under the Boost and MIT licenses, respectively). We also use the ideas from Trond Norbye's blog post (http://trondn.blogspot.fi/2010/10/installing-python-script-from-automake.html), and Mark Moll's mailing list post (http://www.cmake.org/pipermail/cmake/2011-January/041666.html) to handle Python-stuff with CMake.


## Authors

Janne Solanpää

Perttu Luukko 

Esa Räsänen

Ahmed Salami

## Bugs

Bug reports can be sent to the email janne [at] REMOVETHIS bill2d [dot] work [dot] solanpaa [dot] fi

All bug reports should contain:

* Description of the bug
* Version (git commit hash)
* input-file that reproduces the bug, if possible
* Compiler, its version, and compilation flags
* Hardware and operating system



## Feature requests

Feature requests are welcomed but not quaranteed to be implemented.
