FROM registry.gitlab.com/qcad.fi/docker/minimal:latest

# Build details
ARG GIT_COMMIT="notset"
ARG TAG="none"

# Metadata
LABEL maintainer="janne@solanpaa.fi"
LABEL vcs-ref=${GIT_COMMIT}
LABEL vcs-tag=${TAG}

WORKDIR /root

############ INSTALL BILL2D  ##############
RUN mkdir bill2d
COPY ./ bill2d/
RUN mkdir bill2d/build
WORKDIR bill2d/build
RUN ls ../
RUN cmake .. -DCMAKE_BUILD_TYPE=Release \
    && make -j6 \
    && make install \
    && cd ../../ && rm -rf bill2d \
    && mandb


############ CLEANUP ###########
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN rm -rf /root && mkdir /root && cp /home/phys/.bashrc /root/.

WORKDIR /home/phys

CMD ["bill2d --savepath mnt/data/bill2d.h5"]
