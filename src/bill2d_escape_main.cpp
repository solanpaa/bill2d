/*
 * bill2d_escape_main.cpp
 *
 * This file is part of bill2d.
 *
 * A source for a executable that calculates escape probabilities as a function
 * of time and number of collisions.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file bill2d_escape_main.cpp
 * \brief Option parsing etc. for the 'bill2d_escape' binary
 */

/*
 * Example that can be used for writing escape simulation codes
 * Runs a single simulation for system described by 'system' with additional parameters 'params'
 */
#include "executables.hpp"
#include "parser.hpp"
#include "signal_handler.hpp"

#include <signal.h>

#include <iostream>

// Signal flag
using namespace bill2d;

int main(int argc, char** argv) {
    // Signal handler
    struct sigaction act;
    act.sa_handler = sig_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGINT, &act, nullptr);

    // Parse command line & config file
    Parser parser(argc, argv);
    bool help = parser.parse();
    // If help shown, exit
    if (help) {
        return 0;
    }

    // Get parameters
    ParameterList& plist = parser.getParameterList();

    if (plist.output_level >= 2) std::cout << "Escape simulation.." << std::endl;

    // Calculate the escape simulations
    escape_main(plist);

    return 0;
}
