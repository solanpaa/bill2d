/*
 * custom_psos.hpp
 *
 * This file is part of bill2d.
 *
 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "custom_psos.hpp"
#include "helperfunctions.hpp"

#include <cmath>
#include <iostream>

namespace bill2d {

CustomPSOS::CustomPSOS(
    std::function<double(const particleset&)> arg_PSOS,
    std::function<std::vector<double>(const std::vector<double>&)> arg_PSOS_gradient,
    std::function<std::vector<double>(const std::vector<double>&)> arg_PHS_to_PSOS
)
    : PSOS(std::move(arg_PSOS)),
      PSOS_gradient(std::move(arg_PSOS_gradient)),
      PHS_to_PSOS(std::move(arg_PHS_to_PSOS)) {}

void CustomPSOS::evaluate(const particleset& particles, const double time) {
    double PSOS_newval = PSOS(particles);

    if (PSOS_newval * PSOS_value < 0 or PSOS_newval == 0) handle_crossing(particles, time, PSOS_newval);

    PSOS_value = PSOS_newval;
}

void CustomPSOS::particleset_to_vectors(
    const particleset& particles, std::vector<double>& state, std::vector<double>& forces
) {
    state.reserve(particles.size() * 4);
    forces.reserve(particles.size() * 2);
    for (const auto& particle : particles) {
        state.push_back(particle.d_r[0]);
        state.push_back(particle.d_r[1]);
        state.push_back(particle.d_v[0]);
        state.push_back(particle.d_v[1]);
        forces.push_back(particle.d_F[0]);
        forces.push_back(particle.d_F[1]);
    }
}

void CustomPSOS::handle_crossing(const particleset& particles, const double time, const double PSOS_newval) {
    // Initial state Z & forces
    std::vector<double> Z;
    std::vector<double> forces;

    particleset_to_vectors(particles, Z, forces);
    if (PSOS_newval != 0) {
        // Gradient of the PSOS S(x1,y1,vx1,vy1,...)
        std::vector<double> S_grad = PSOS_gradient(Z);

        Z.push_back(time);
        assert(S_grad.size() == particles.size() * 4);

        auto ODE = [&forces, &S_grad](const std::vector<double>& x0, std::vector<double>& dxdxs, const double /*dxs*/) {
            // Calculate dS/dt
            double dSdt = 0;
            for (unsigned n = 0; n < x0.size() / 4; n++) {
                dSdt += x0[4 * n + 2] * S_grad[4 * n];          // rx0 -term
                dSdt += x0[4 * n + 3] * S_grad[4 * n + 1];      // ry0 -term
                dSdt += forces[2 * n] * S_grad[4 * n + 2];      // vx0 -term
                dSdt += forces[2 * n + 1] * S_grad[4 * n + 3];  // vy0 -term
            }

            for (unsigned n = 0; n < x0.size() / 4; n++) {
                dxdxs[4 * n] = x0[4 * n + 2] / dSdt;
                dxdxs[4 * n + 1] = x0[4 * n + 3] / dSdt;
                dxdxs[4 * n + 2] = forces[2 * n] / dSdt;      // vx0 -term
                dxdxs[4 * n + 3] = forces[2 * n + 1] / dSdt;  // vy0 -term
            }
            // Time variable
            dxdxs[x0.size() - 1] = 1.0 / dSdt;
        };
        std::vector<double> Z_backup(Z);

        boost::numeric::odeint::runge_kutta_fehlberg78<std::vector<double>> stepper;
        stepper.do_step(ODE, Z, PSOS_newval /*xs_0*/, -PSOS_newval /*dx*/);

        // Occasionally the high-order stepper might fail, and then we use the Euler method
        bool step_failed = false;
        std::for_each(Z.begin(), Z.end(), [&step_failed](double& x) {
            if (std::isinf(x) or std::isnan(x)) step_failed = true;
        });
        if (step_failed) {
            boost::numeric::odeint::euler<std::vector<double>> stepper_basic;
            stepper_basic.do_step(ODE, Z_backup, PSOS_newval /*xs_0*/, -PSOS_newval /*dx*/);
            PSOS_data.push_back(PHS_to_PSOS(Z_backup));
        } else
            PSOS_data.push_back(PHS_to_PSOS(Z));
    } else {
        Z.push_back(time);
        PSOS_data.push_back(PHS_to_PSOS(Z));
    }
}

double CustomPSOS::linePSOS(const particleset& particles, const triplet<double> line) {
    double S = 1;
    for (const auto& p : particles) {
        S *= (p.d_r[0] * line[0] + p.d_r[1] * line[1] + line[2]);
    }
    return S;
}

std::vector<double> CustomPSOS::linePSOS_gradfun(const std::vector<double>& z, const triplet<double> line) {
    std::vector<double> derivs(z.size(), 0.0);
    for (unsigned i = 0; i < z.size() / 4; i++) {  // Loop over all particles
        double Srest = 1.0;
        for (unsigned j = 0; j < z.size() / 4; j++) {  // Loop over all except the one we are calculating the derivs for
            if (j != i) {
                Srest *= z[4 * i] * line[0] + z[4 * i + 1] * line[1] + line[2];
            }
        }
        derivs[4 * i] = line[0] * Srest;
        derivs[4 * i + 1] = line[1] * Srest;
    }
    return derivs;
}

std::vector<double> CustomPSOS::phase_space_point_to_linePSOS_point(
    const std::vector<double>& z, triplet<double> line
) {
    assert(line[1] != 0);

    std::vector<double> Svals(z.size() / 4, 0.0);
    for (unsigned i = 0; i < z.size() / 4; i++)  // Loop over all particles
        Svals[i] = line[0] * z[4 * i] + line[1] * z[4 * i + 1] + line[2];

    // Find which particle is at the boundary
    auto it = z.cbegin();
    for (; it < z.cend(); std::advance(it, 4)) {
        if (fabs(*it * line[0] + *(it + 1) * line[1] + line[2]) < 1e-5) {
            break;
        }
    }
    if (it == z.cend()) {
        std::cerr << "No particle found at the PSOS! Error! Exiting." << std::endl;
        exit(1);
    }

    // Now 'it' holds the info on that particle. Let's save its
    // from y=0 along the line, velocity's tangential component, and angle.
    // Tangent vector (b, -a) (since normalized already!)
    std::vector<double> result(4, 0.0);
    result[0] = z.back();  // time
    if (line[0] != 0) {    // if line is not horizontal
        result[1] = std::copysign(
            sqrt(pow(*(it + 1), 2.0) + pow(*it + line[2] / line[0], 2.0)), *it + line[2] / line[0]
        );                                                          // distance from y=0 via the line
        result[2] = line[1] * (*(it + 2)) - line[0] * (*(it + 3));  // dot product between tangent and the velocity
        double vnorm[2] = {0.0, 0.0};
        vnorm[1] = 1 / sqrt(1 + pow(line[0] / line[1], 2));
        vnorm[0] = line[0] / line[1] * vnorm[1];
        result[3] = atan2(vnorm[0] * (*(it + 2)) + vnorm[1] * (*(it + 3)), result[2]);
    } else {  // line is horizontal
        result[1] = *it;
        result[2] = *(it + 2);  // vx
        result[3] = atan2(*(it + 3), *(it + 2));
    }

    // Let's append the whole phase space point to the end of the result
    result.insert(result.end(), z.begin(), z.end() - 1);

    return result;
}

Vec2d CustomPSOS::linePSOS_to_position(const double s, const triplet<double> line) {
    assert(!(line[1] == 0.0 and line[2] == 0.0));

    Vec2d r;

    if (line[0] == 0.0) {  // b y + c = 0
        r[0] = s;
        r[1] = -line[2] / line[1];
    } else if (line[1] == 0.0) {  // a x + c == 0
        r[0] = -line[2] / line[0];
        r[1] = s;
    } else {
        const double slope = -line[0] / line[1];
        r[0] = -line[2] / line[0] + fabs(line[1]) * s / sqrt(pow(line[0], 2) + pow(line[1], 2));
        r[1] = sign(slope) * fabs(line[0]) * s / sqrt(pow(line[0], 2) + pow(line[1], 2));
    }

    return r;
}

Vec2d CustomPSOS::linePSOS_to_velocity(const double theta, const triplet<double> line, const double vnorm) {
    const double angle = (line[1] == 0.0) ? M_PI / 2.0 : atan(-line[0] / line[1]);  // atan(slope)
    return vnorm * Vec2d{cos(angle + theta), sin(angle + theta)};
}

//! These give estimate in terms of hard boundaries, if we have potentials, those are handled by the position
//! initializer
double CustomPSOS::linepsos_minimum_s(const Table& table, const RNG& rng, const triplet<double> line) {
    std::function<bool(double)> point_is_inside = [&table, &line](const double sval) {
        return table.point_is_inside(CustomPSOS::linePSOS_to_position(sval, line));
    };

    if (table.shape_name() == "NoTable") return -std::hypot(table.getMaxCoords()[0], table.getMaxCoords()[1]);

    double s = 0;
    while (!point_is_inside(s)) s = rng.uniform_rand(-10, 10);
    while (point_is_inside(s)) s -= 2.0;
    std::tie(std::ignore, s) = boost::math::tools::bisect(
        [&point_is_inside](const double sval) { return point_is_inside(sval) ? 1.0 : -1.0; },
        s,
        s + 2.0,
        [](double a, double b) { return (b - a) < 1e-7; }
    );
    return s;
}

//! These give estimate in terms of hard boundaries, if we have potentials, those are handled by the position
//! initializer
double CustomPSOS::linepsos_maximum_s(const Table& table, const RNG& rng, const triplet<double> line) {
    std::function<bool(double)> point_is_inside = [&table, &line](const double sval) {
        return table.point_is_inside(CustomPSOS::linePSOS_to_position(sval, line));
    };

    if (table.shape_name() == "NoTable") return std::hypot(table.getMaxCoords()[0], table.getMaxCoords()[1]);

    double s = 0;
    while (!point_is_inside(s)) s = rng.uniform_rand(-10, 10);

    while (point_is_inside(s)) s += 2.0;

    // Bisect for maximum s
    std::tie(s, std::ignore) = boost::math::tools::bisect(
        [&point_is_inside](const double sval) { return point_is_inside(sval) ? -1.0 : 1.0; },
        s - 2.0,
        s,
        [](double a, double b) { return (b - a) < 1e-5; }
    );
    return s;
}

bool CustomPSOS::randomize_initial_conditions_on_linePSOS(
    particleset& particles, const RNG& rng, const Table& table, std::function<double()> epot,
    std::function<double()> ekin, const triplet<double> line, const double energy, const twopairs<double> spawnarea
) {
    const double smax = CustomPSOS::linepsos_maximum_s(table, rng, line);
    const double smin = CustomPSOS::linepsos_minimum_s(table, rng, line);

    double s;

    // Randomize positions and velocities of the particles along the PSOS; only first particle on the PSOS, others
    // everywhere
    do {
        auto& p = particles.at(0);
        s = rng.uniform_rand(smin, smax);

        do {
            p.d_r0 = CustomPSOS::linePSOS_to_position(s, line);
            p.d_r = p.d_r0;
        } while (!table.point_is_inside(p.d_r));

        for (unsigned i = 1; i < particles.size(); i++) {
            auto& p2 = particles.at(i);
            do {
                p2.d_r =
                    Vec2d(rng.uniform_rand(spawnarea[0], spawnarea[2]), rng.uniform_rand(spawnarea[1], spawnarea[3]));
                p2.d_r0 = p.d_r;
            } while (!table.point_is_inside(p.d_r));
        }

    } while (energy - epot() < 0);

    for (auto& p : particles)
        p.d_v = CustomPSOS::linePSOS_to_velocity(rng.uniform_rand(-M_PI, M_PI), line, rng.uniform_rand(0, 1000));

    double KR = energy - epot();
    double K = ekin();

    assert(K >= 0);
    assert(KR >= 0);

    for (auto& p : particles) {
        p.d_v *= sqrt(KR / K);
        p.d_v0 = p.d_v;
    }

    assert(fabs(energy - epot() - ekin()) < 1e-9);
    return true;
}

}  // namespace bill2d
