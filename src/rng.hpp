/*
 * rng.hpp
 *
 * This file is part of bill2d.
 *
 * A simple wrapper class for random number generation based on GSL
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file rng.hpp
 * \brief The random number generator is defined here.
 */

#ifndef _RNG_HPP_
#define _RNG_HPP_

#include "bill2d_config.h"

#include <random>
#ifdef UNITTESTING
#include "gtest/gtest_prod.h"
#endif
#define RNG_TYPE gsl_rng_mt19937

namespace bill2d {

//! A simple wrapper class for random number generation based on C++11 standard library
class RNG {
public:
    //! Initialize the system with random seed
    RNG();

    //! Delete copy constructor
    RNG(const RNG&) = delete;

    virtual ~RNG();
    //! Initialize with a given seed
    RNG(unsigned long int seed);

    /*! \brief Get a random real number from uniform distribution
     *
     *  \f$ P(x)=\frac{1}{\text{max}-\text{min}} \f$
     */
    double uniform_rand(double min, double max) const;

    /*! \brief Get a random real number from a gaussian distribution with zero mean
     *
     * \f$ P(x)=\frac{1}{\sigma \sqrt {2\pi}} \exp\left[\frac{1}{2}\left(\frac{x}{\sigma}\right)^2\right] \f$
     */
    double gaussian_rand(double sigma) const;

private:
#ifdef UNITTESTING
    FRIEND_TEST(RNG, default_constructor);
    FRIEND_TEST(RNG, custom_constructor);
#endif
    std::mt19937* d_rng;
};
}  // namespace bill2d
#endif  // _RNG_HPP_
