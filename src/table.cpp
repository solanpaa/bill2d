/*
 * table.hpp
 *
 * This file is part of bill2d.
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file table.hpp
 * \brief The hard-wall billiard tables, open billiard tables, table components,
 * and periodic unit-cells are defined in this file.
 */

#include "table.hpp"
namespace bill2d {

Rectangle::Rectangle(double w, double h) {
    assert(w >= 0);
    assert(h >= 0);
    d_w = w;
    d_h = h;
    d_shape_name << "rectangle(" << d_w << "," << d_h << ")";
    maxCoords.x = d_w;
    maxCoords.y = d_h;
}

bool Rectangle::handle_collisions(
    Vec2d& r, Vec2d& v, __attribute__((unused)) pair<int>& cell, std::vector<std::vector<double>>* bmap,
    const double time, unsigned& hits, __attribute__((unused)) bool& hit_flag
) const {
    if (point_is_inside(r)) return false;
    // Handle collisions
    if (r[0] < 0) {
        r[0] *= -1;
        v[0] *= -1;
        hits++;
    } else if (r[0] > d_w) {
        r[0] = 2.0 * d_w - r[0];
        v[0] *= -1;
        hits++;
    }
    if (r[1] < 0) {
        if (bmap) {
            std::vector<double> hit(4, 0);
            hit.at(0) = time;
            hit.at(1) = r[0];
            hit.at(2) = v[0];
            hit.at(3) = atan(-v[0] / v[1]);
            bmap->emplace_back(hit);
        }

        r[1] *= -1;
        v[1] *= -1;
        hits++;

    } else if (r[1] > d_h) {
        r[1] = 2.0 * d_h - r[1];
        v[1] *= -1;
        hits++;
    }
    if (!point_is_inside(r))
        handle_collisions(r, v, cell, bmap, time, hits, hit_flag);  // Handle again if still not inside the table
    return true;
}

/*
 * Methods of class CompoundShape
 */

bool CompoundShape::handle_collisions(
    Vec2d& r, Vec2d& v, pair<int>&, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
    bool& hit_flag
) const {
    bool check_again = false;
    bool checked_again = false;

    do {
        check_again = false;

        // Check shapes, which do not contribute to bounce maps
        for (const auto& shape : shapes) {
            if (shape->handle_crossing(r, v, nullptr, time, hits, hit_flag)) {
                check_again = true;
            }
        }
        // Check shapes, which can contribute to bounce maps
        for (const auto& bshape : bshapes) {
            if (bshape->handle_crossing(r, v, bmap, time, hits, hit_flag)) {
                check_again = true;
            }
        }
        if (check_again) checked_again = true;
    } while (check_again);
    return checked_again;
}

CompoundShape::~CompoundShape() {}

/*
 * Methods of derived subclass BoundaryLine
 */

CompoundShape::BoundaryLine::BoundaryLine(
    enum Direction arg_dir, double arg_A, double arg_B, double arg_C, double arg_s0, Vec2d arg_r0
)
    : BoundaryShape(arg_s0) {
    dir = arg_dir;
    A = arg_A;
    B = arg_B;
    C = arg_C;
    unormal = Vec2d(A, B);
    unormal.scale_to_unit();
    utangent = Vec2d(-B, A);
    utangent.scale_to_unit();
    invsqrAB = 1.0 / sqrt(A * A + B * B);

    // Position of s = arg_s0;
    d_r0 = arg_r0;

    // Check that it actually lies on the line
    if (fabs(A * d_r0[0] + B * d_r0[1] + C) > 1e-10) {
        std::cerr << "Error in definition of BoundaryLine, point of s = s0 not on the line" << std::endl;
        exit(1);
    }
}

bool CompoundShape::BoundaryLine::
    handle_crossing(Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits, bool&)
        const {
    std::vector<double> hit(4, 0);
    if (dir == ForbidUpper) {
        if (A * r[0] + B * r[1] + C > 0) {
            double vtan = v.dot(utangent);
            if (bmap) {
                hit.at(0) = time;
                if (r[1] <= d_r0[1])
                    hit.at(1) = d_s0 - (r - d_r0).norm();
                else
                    hit.at(1) = d_s0 + (r - d_r0).norm();
                hit.at(2) = vtan;
                hit.at(3) = asin(hit.at(2) / v.norm());
                bmap->emplace_back(hit);
            }

            r -= 2 * fabs(A * r[0] + B * r[1] + C) * invsqrAB * unormal;
            v = 2 * vtan * utangent - v;
            hits++;

            return true;
        }
    } else {
        if (A * r[0] + B * r[1] + C < 0) {
            double vtan = -v.dot(utangent);

            if (bmap) {
                hit.at(0) = time;
                if (r[1] <= d_r0[1])
                    hit.at(1) = d_s0 + (r - d_r0).norm();
                else
                    hit.at(1) = d_s0 - (r - d_r0).norm();
                hit.at(2) = vtan;
                hit.at(3) = asin(hit.at(2) / v.norm());
                bmap->emplace_back(hit);
            }

            r += 2 * fabs(A * r[0] + B * r[1] + C) * invsqrAB * unormal;
            v = -2 * vtan * utangent - v;
            hits++;
            return true;
        }
    }
    return false;
}

/*
 * Methods of derived subclass BoundaryHorizontalLine
 */

CompoundShape::BoundaryHorizontalLine::BoundaryHorizontalLine(
    enum Direction arg_dir, double y, double arg_s0, double arg_x0
)
    : BoundaryShape(arg_s0) {
    dir = arg_dir;
    pos_y = y;
    beg_x = arg_x0;
}

bool CompoundShape::BoundaryHorizontalLine::
    handle_crossing(Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits, bool&)
        const {
    std::vector<double> hit(4, 0);
    if (dir == ForbidUpper) {
        if (r[1] > pos_y) {
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 + beg_x - r[0];
                hit.at(2) = -v[0];
                hit.at(3) = -atan(v[0] / v[1]);
                bmap->emplace_back(hit);
            }

            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            hits++;

            return true;
        }
    } else {
        if (r[1] < pos_y) {
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 - beg_x + r[0];
                hit.at(2) = v[0];
                hit.at(3) = atan(-v[0] / v[1]);
                bmap->emplace_back(hit);
            }

            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            hits++;

            return true;
        }
    }
    return false;
}

/*
 * Methods of derived subclass BoundaryHorizontalRay
 */

CompoundShape::BoundaryHorizontalRay::BoundaryHorizontalRay(
    enum Direction arg_dir, enum InfiniteDirection arg_dir2, double x, double y, double arg_s0
)
    : BoundaryShape(arg_s0) {
    dir = arg_dir;
    dir2 = arg_dir2;
    pos_x = x;
    pos_y = y;
}

bool CompoundShape::BoundaryHorizontalRay::
    handle_crossing(Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits, bool&)
        const {
    if (dir2 == Left) {
        if (r[0] > pos_x) return false;
    } else if (r[0] < pos_x)
        return false;
    std::vector<double> hit(4, 0);
    if (dir == ForbidUpper) {
        if (r[1] > pos_y) {
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 + pos_x - r[0];
                hit.at(2) = -v[0];
                hit.at(3) = -atan(v[0] / v[1]);
                bmap->emplace_back(hit);
            }
            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            hits++;
            return true;
        }
    } else {
        if (r[1] < pos_y) {
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 - pos_x + r[0];
                hit.at(2) = v[0];
                hit.at(3) = atan(-v[0] / v[1]);
                bmap->emplace_back(hit);
            }
            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            hits++;
            return true;
        }
    }
    return false;
}

/*
 * Methods of derived subclass BoundaryEllipse
 */

CompoundShape::BoundaryEllipse::BoundaryEllipse(double arg_a, double arg_b, double arg_s0) : BoundaryShape(arg_s0) {
    a = arg_a;
    b = arg_b;
    // Essentricity

    if (a > b) {
        e = sqrt(1 - pow(b / a, 2));
        s_integral_fix = ellint_2(e);
    } else {
        e = sqrt(1 - pow(a / b, 2));
        s_integral_fix = 0;
    }
    major_axis = fmax(a, b);
}

bool CompoundShape::BoundaryEllipse::
    handle_crossing(Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits, bool&)
        const {
    if (point_is_inside(r))
        return false;
    else {
        // Calculate linearly back to intersection point
        double A = b * b * v[0] * v[0] + a * a * v[1] * v[1];
        double B = b * b * v[0] * r[0] + a * a * v[1] * r[1];
        double C = a * a * b * b * (A - pow(v[1] * r[0] - v[0] * r[1], 2));
        double t1 = (-B - sqrt(C)) / A;
        double t = (-B + sqrt(C)) / A;

        // Choose correct time
        if (t > 0) {
            t = t1;
        }

        // The intersection point
        Vec2d I = r + t * v;

        // Normal vector at I
        Vec2d N(-b * I[0] / a, -a * I[1] / b);

        if (bmap) {
            // Tangent vector at I
            Vec2d T(-a * I[1] / b, b * I[0] / a);

            // Tangential velocity
            double v_tan = v.dot(T) / T.norm();

            // Arc length s
            double phi = atan2(a * r[1], b * r[0]);
            if (phi < 0) phi += 2 * M_PI;

            double s = major_axis * (ellint_2(e, phi + M_PI / 2.0) - s_integral_fix);

            std::vector<double> hit(4, 0);
            hit.at(0) = time;
            hit.at(1) = s;
            hit.at(2) = v_tan;
            hit.at(3) = asin(v_tan / v.norm());
            bmap->emplace_back(hit);
        }
        hits++;

        // Invert velocity
        v = v - 2 * v.dot(N) * N / N.dot(N);

        // Propagate forward
        r = I - v * t;
        return true;
    }
}

/*
 * Methods of derived subclass BoundaryCircle
 */

CompoundShape::BoundaryCircle::BoundaryCircle(
    Direction arg_dir, Vec2d arg_center, double arg_radius, IsHalved arg_halved, double arg_s0,
    ArcLengthZero arg_origin, IncreasingSDirection arg_sdir
)
    : BoundaryShape(arg_s0) {
    halved = arg_halved;
    dir = arg_dir;
    sdir = arg_sdir;
    center = arg_center;
    radius = arg_radius;
    invradsqr = 1.0 / (radius * radius);
    origin = arg_origin;
}

bool CompoundShape::BoundaryCircle::
    handle_crossing(Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits, bool&)
        const {
    if (point_is_inside(r))
        return false;
    else {
        std::vector<double> hit(4, 0);
        const Vec2d d = r - center;
        // Solve crossing point u = d-tv
        // TODO: This is currently probably quite slow
        const double vsqr = v.normsqr();
        const double dsqr = d.normsqr();
        const double ddotv = d.dot(v);
        const double t = (ddotv / vsqr) * (1 - sqrt(1 - vsqr * (dsqr - radius * radius) / (ddotv * ddotv)));
        Vec2d u = d - t * v;
        assert(fabs(u.norm() - radius) < 1e-9);
        const double C = -radius * radius - u.dot(center);

        Vec2d tangent = Vec2d(-u[1], u[0]);
        if (bmap) {
            double v_tan = tangent.dot(v) / tangent.norm();
            double theta = atan(d[1] / d[0]);
            double phi = asin(v_tan / v.norm());
            switch (origin) {
                // Theta \in [-Pi, Pi]
                case ADown:
                    if ((r[0] < center[0]))
                        theta -= M_PI_2;
                    else
                        theta += M_PI / 2;

                    break;

                // Theta \in [0, 2\Pi]
                case ARight:

                    if (theta > 0) {
                        if (r[0] < center[0]) theta += M_PI;
                    } else {
                        if (r[0] >= center[0])
                            theta += 2 * M_PI;
                        else
                            theta += M_PI;
                    }
                    break;

                default:
                    std::cerr << "This shouldn't happen" << std::endl;
                    break;
            }

            if (sdir == CLOCKWISE) {
                if (origin == ADown)
                    theta *= -1;
                else
                    theta = 2 * M_PI - theta;

                v_tan *= -1;
                phi *= -1;
            }

            double s = d_s0 + theta * radius;
            hit.at(0) = time;
            hit.at(1) = s;
            hit.at(2) = v_tan;
            hit.at(3) = phi;
            bmap->emplace_back(hit);
        }
        if (dir == ForbidOutside)
            r -= 2 * fabs(u.dot(r) + C) * u * invradsqr;
        else
            r += 2 * fabs(u.dot(r) + C) * u * invradsqr;
        v = 2 * v.dot(tangent) * tangent * invradsqr - v;
        hits++;
        return true;
    }
}

/*
 * Methods of derived subclass BoundaryRectangle
 */

CompoundShape::BoundaryRectangle::BoundaryRectangle(
    Direction arg_dir, double arg_minx, double arg_maxx, double arg_miny, double arg_maxy, double arg_s0
)
    : BoundaryShape(arg_s0) {
    assert(arg_minx < arg_maxx);
    assert(arg_miny < arg_maxy);
    assert(arg_dir == ForbidInside or arg_dir == ForbidOutside);  // Allows only 2 arguments
    dir = arg_dir;
    minx = arg_minx;
    maxx = arg_maxx;
    miny = arg_miny;
    maxy = arg_maxy;
    d_w = maxx - minx;
    d_h = maxy - miny;
}

bool CompoundShape::BoundaryRectangle::
    handle_crossing(Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits, bool&)
        const {
    std::vector<double> hit(4, 0);
    if (point_is_inside(r)) return false;

    if (dir == ForbidOutside) {  // If ForbidOutside
        if (r[0] < minx) {       // Left
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 + 2 * d_w + 2 * d_h - r[1] + miny;
                hit.at(2) = -v[1];
                hit.at(3) = atan(v[1] / v[0]);
                bmap->emplace_back(hit);
            }

            r[0] = 2.0 * minx - r[0];
            v[0] *= -1;
            hits++;
            return true;
        } else if (r[0] > maxx) {  // Right
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 + d_w + r[1] - miny;
                hit.at(2) = v[1];
                hit.at(3) = atan(v[1] / v[0]);
                bmap->emplace_back(hit);
            }

            r[0] = 2.0 * maxx - r[0];
            v[0] *= -1;
            hits++;
            return true;
        }
        if (r[1] < miny) {  // Bottom
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 + r[0] - minx;
                hit.at(2) = v[0];
                hit.at(3) = atan(-v[0] / v[1]);
                bmap->emplace_back(hit);
            }

            r[1] = 2.0 * miny - r[1];
            v[1] *= -1;
            hits++;

            return true;
        } else if (r[1] > maxy) {  // Top
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 + d_w + d_h + maxx - r[0];
                hit.at(2) = -v[0];
                hit.at(3) = atan(-v[0] / v[1]);
                bmap->emplace_back(hit);
            }

            r[1] = 2.0 * maxy - r[1];
            v[1] *= -1;
            hits++;
            return true;
        }
        return false;
    }

    else {  // If ForbidInside, ~ Janne
        // Test which line of the rectangle the particle passed.
        enum LastPassed { LEFT, RIGHT, UP, DOWN } last, upordown, leftorright;
        double tx, ty;
        double pos_x;
        double pos_y;

        if (v[1] > 0)
            upordown = DOWN;
        else
            upordown = UP;

        if (v[0] > 0)
            leftorright = LEFT;
        else
            leftorright = RIGHT;

        // Has it passed the horizontal or vertical boundary?

        // Sets the two lines that are compared
        if (upordown == DOWN)
            pos_y = miny;
        else
            pos_y = maxy;

        if (leftorright == LEFT)
            pos_x = minx;
        else
            pos_x = maxx;

        // What's the time it takes the particle to go backwards to the x- and y- intersection points?
        if (leftorright == LEFT) {
            tx = (r[0] - pos_x) / v[0];
        } else {
            tx = (r[0] - pos_x) / v[0];
        }
        if (upordown == DOWN) {
            ty = (r[1] - pos_y) / v[1];
        } else {
            ty = (r[1] - pos_y) / v[1];
        }

        if (tx < 0)  // If tx is <0, then it has passed through 2 lines, the last of which is x-line. Thus, last
                     // allowed collision was with y-line.
            last = upordown;
        else if (ty < 0)  // If ty is <0, then it has passed through 2 lines, the last of which is y-line. Thus, last
                          // allowed collision was with x-line.
            last = leftorright;
        else if (tx < ty)  // Time to travel to latest x and y intersection points. If tx<ty, then x-line was the
                           // last one passed through.
            last = leftorright;
        else
            last = upordown;

        // Mirrors the position in respect to the last line passed through and reverses the corresponding velocity.
        if (last == LEFT) {
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 + 2 * d_w + 2 * d_h - r[1] + miny;
                hit.at(2) = -v[1];
                hit.at(3) = atan(-v[1] / v[0]);
                bmap->emplace_back(hit);
            }

            r[0] = 2 * pos_x - r[0];
            v[0] *= -1;
            hits++;

        }

        else if (last == RIGHT) {
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 + d_w + r[1] - miny;
                hit.at(2) = v[1];
                hit.at(3) = atan(-v[1] / v[0]);
                bmap->emplace_back(hit);
            }

            r[0] = 2 * pos_x - r[0];
            v[0] *= -1;
            hits++;
        } else if (last == UP) {
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 + d_w + d_h + maxx - r[0];
                hit.at(2) = -v[0];
                hit.at(3) = atan(v[0] / v[1]);
                bmap->emplace_back(hit);
            }
            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            hits++;
        } else {
            if (bmap) {
                hit.at(0) = time;
                hit.at(1) = d_s0 + r[0] - minx;
                hit.at(2) = v[0];
                hit.at(3) = atan(v[0] / v[1]);
                bmap->emplace_back(hit);
            }

            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            hits++;
        }
        return true;
    }
}

/*
 * Methods of derived subclass BoundaryRectangleCorner
 */

CompoundShape::BoundaryRectangleCorner::BoundaryRectangleCorner(Direction arg_dir, double x, double y, double arg_s0)
    : BoundaryShape(arg_s0) {
    dir = arg_dir;
    pos_x = x;
    pos_y = y;
}

bool CompoundShape::BoundaryRectangleCorner::
    handle_crossing(Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits, bool&)
        const {
    if (point_is_inside(r)) return false;
    // This gets slightly complicated as we need to test which line of the corner the particle passed.
    enum LastPassed { X, Y } last;
    const double tx = (r[0] - pos_x) / v[0];
    const double ty = (r[1] - pos_y) / v[1];
    if (tx < 0)
        last = Y;
    else if (ty < 0)
        last = X;
    else if (tx < ty)
        last = X;
    else
        last = Y;

    // Save information on collision to bouncing map
    if (bmap) {
        std::vector<double> hit(4, 0);
        hit.at(0) = time;
        hit.at(1) = d_s0;

        switch (dir) {
            case TopLeft:
                if (last == Y) {
                    hit.at(1) += r[0] - pos_x;
                    hit.at(2) = v[0];
                    hit.at(3) = atan(-v[0] / v[1]);
                } else {
                    hit.at(1) -= pos_y - r[1];
                    hit.at(2) = v[1];
                    hit.at(3) = atan(v[1] / v[0]);
                }
                break;

            case TopRight:
                if (last == Y) {
                    hit.at(1) -= pos_x - r[0];
                    hit.at(2) = v[0];
                    hit.at(3) = atan(-v[0] / v[1]);
                } else {
                    hit.at(1) += pos_y - r[1];
                    hit.at(2) = v[1];
                    hit.at(3) = atan(v[1] / v[0]);
                }
                break;

            case BottomLeft:
                if (last == Y) {
                    hit.at(1) -= r[0] - pos_x;
                    hit.at(2) = -v[0];
                    hit.at(3) = atan(-v[0] / v[1]);
                } else {
                    hit.at(1) += r[1] - pos_y;
                    hit.at(2) = v[1];
                    hit.at(3) = atan(v[1] / v[0]);
                }
                break;

            case BottomRight:
                if (last == Y) {
                    hit.at(1) += pos_x - r[0];
                    hit.at(2) = -v[0];
                    hit.at(3) = atan(-v[0] / v[1]);
                } else {
                    hit.at(1) -= r[1] - pos_y;
                    hit.at(2) = -v[1];
                    hit.at(3) = atan(v[1] / v[0]);
                }
                break;

            default:
                std::cerr << "BoundaryRectangleCorner: bmap handling failed, this shouldn't happen." << std::endl;
        }

        bmap->emplace_back(hit);
    }

    hits++;

    // Handle boundary crossing
    if (last == X) {
        r[0] = 2 * pos_x - r[0];
        v[0] *= -1;

    } else {
        r[1] = 2 * pos_y - r[1];
        v[1] *= -1;
    }
    return true;
}

CompoundShape::OpenBoundaryRectangle::OpenBoundaryRectangle(
    Direction adir, double leftX, double rightX, double bottomY, double topY, double holeWidth, double holePeriodicity,
    double lowerLeftBoundaryLengthValue
)
    : CompoundShape::BoundaryRectangle::BoundaryRectangle(
          adir, leftX, rightX, bottomY, topY, lowerLeftBoundaryLengthValue
      ) {
    d_holeWidth = holeWidth;
    d_holePeriodicity = holePeriodicity;
}

bool CompoundShape::OpenBoundaryRectangle::handle_crossing(
    Vec2d& r, Vec2d& v, std::vector<std::vector<double>>*, const double, unsigned& hits, bool& hit_flag
) const {
    double s = d_s0;
    if (point_is_inside(r)) return false;

    if (dir == ForbidOutside) {  // If ForbidOutside
        // Test which line of the rectangle the particle passed.
        enum LastPassed { LEFT, RIGHT, UP, DOWN } last, upordown, leftorright;
        double tx, ty;
        double pos_x;
        double pos_y;

        if (v[1] > 0)
            upordown = UP;
        else
            upordown = DOWN;

        if (v[0] > 0)
            leftorright = RIGHT;
        else
            leftorright = LEFT;

        // Has it passed the horizontal or vertical boundary?

        // Sets the two lines that are compared
        if (upordown == DOWN)
            pos_y = miny;
        else
            pos_y = maxy;

        if (leftorright == LEFT)
            pos_x = minx;
        else
            pos_x = maxx;

        // What's the time it takes the particle to go backwards to the x- and y- intersection points?
        tx = (r[0] - pos_x) / v[0];
        ty = (r[1] - pos_y) / v[1];

        if (tx < 0)  // If tx is <0,
            last = upordown;
        else if (ty < 0)
            last = leftorright;
        else if (tx < ty)  // Time to travel to latest x and y intersection points. If tx<ty, then x-line was the
                           // last one passed through.
            last = leftorright;
        else
            last = upordown;

        // Mirrors the position in respect to the last line passed through and reverses the corresponding velocity.
        if (last == LEFT) {
            r[0] = 2 * pos_x - r[0];
            v[0] *= -1;

            // Check if hit a hole
            s += 2 * d_w + 2 * d_h - r[1];
            if ((s > 0) and fmod(s, d_holePeriodicity) < d_holeWidth) {
                hit_flag = true;
                hits--;
            }
        }

        else if (last == RIGHT) {
            r[0] = 2 * pos_x - r[0];
            v[0] *= -1;

            // Check if hit a hole
            s += d_w + r[1];
            if ((s > 0) and fmod(s, d_holePeriodicity) < d_holeWidth) {
                hit_flag = true;
                hits--;
            }
        } else if (last == UP) {
            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            s += 2 * d_w + d_h - r[0];
            if ((s > 0) and fmod(s, d_holePeriodicity) < d_holeWidth) {
                hit_flag = true;
                hits--;
            }
        } else {
            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            s += r[0] - minx;
            if ((s > 0) and fmod(s, d_holePeriodicity) < d_holeWidth) {
                hit_flag = true;
                hits--;
            }
        }

        hits++;
        return true;
    } else {
        // Test which line of the rectangle the particle passed.
        enum LastPassed { LEFT, RIGHT, UP, DOWN } last, upordown, leftorright;
        double tx, ty;
        double pos_x;
        double pos_y;

        if (v[1] > 0)
            upordown = DOWN;
        else
            upordown = UP;

        if (v[0] > 0)
            leftorright = LEFT;
        else
            leftorright = RIGHT;

        // Has it passed the horizontal or vertical boundary?

        // Sets the two lines that are compared
        if (upordown == DOWN)
            pos_y = miny;
        else
            pos_y = maxy;

        if (leftorright == LEFT)
            pos_x = minx;
        else
            pos_x = maxx;

        // What's the time it takes the particle to go backwards to the x- and y- intersection points?
        if (leftorright == LEFT) {
            tx = (r[0] - pos_x) / v[0];
        } else {
            tx = (r[0] - pos_x) / v[0];
        }
        if (upordown == DOWN) {
            ty = (r[1] - pos_y) / v[1];
        } else {
            ty = (r[1] - pos_y) / v[1];
        }

        if (tx < 0)  // If tx is <0,
            last = upordown;
        else if (ty < 0)  // If ty is <0, then it has passed through 2 lines, the last of which is y-line. Thus, last
                          // allowed collision was with x-line.
            last = leftorright;
        else if (tx < ty)  // Time to travel to latest x and y intersection points. If tx<ty, then x-line was the
                           // last one passed through.
            last = leftorright;
        else
            last = upordown;

        // Mirrors the position in respect to the last line passed through and reverses the corresponding velocity.
        if (last == LEFT) {
            r[0] = 2 * pos_x - r[0];
            v[0] *= -1;

            // Check if hit a hole
            s += 2 * d_w + 2 * d_h - r[1];
            if ((s > 0) and fmod(s, d_holePeriodicity) < d_holeWidth) {
                hit_flag = true;
                hits--;
            }
        }

        else if (last == RIGHT) {
            r[0] = 2 * pos_x - r[0];
            v[0] *= -1;

            // Check if hit a hole
            s += d_w + r[1];
            if ((s > 0) and fmod(s, d_holePeriodicity) < d_holeWidth) {
                hit_flag = true;
                hits--;
            }
        } else if (last == UP) {
            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            s += 2 * d_w + d_h - r[0];
            if ((s > 0) and fmod(s, d_holePeriodicity) < d_holeWidth) {
                hit_flag = true;
                hits--;
            }
        } else {
            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            s += r[0] - minx;
            if ((s > 0) and fmod(s, d_holePeriodicity) < d_holeWidth) {
                hit_flag = true;
                hits--;
            }
        }

        hits++;
        return true;
    }
    return false;
}

CompoundShape::OpenBoundaryCircle::OpenBoundaryCircle(
    CompoundShape::BoundaryCircle::Direction arg_dir, Vec2d arg_center, double arg_radius, IsHalved arg_halved,
    double arg_l, double arg_L, double arg_s0, ArcLengthZero arg_origin
)
    : CompoundShape::BoundaryCircle::BoundaryCircle(arg_dir, arg_center, arg_radius, arg_halved, arg_s0, arg_origin) {
    d_l = arg_l;
    d_L = arg_L;
}

bool CompoundShape::OpenBoundaryCircle::handle_crossing(
    Vec2d& r, Vec2d& v, std::vector<std::vector<double>>*, const double, unsigned& hits, bool& hit_flag
) const {
    if (point_is_inside(r))
        return false;
    else {
        const Vec2d d = r - center;
        // Solve crossing point u = d-tv
        // TODO: This is currently probably quite slow
        const double vsqr = v.normsqr();
        const double dsqr = d.normsqr();
        const double ddotv = d.dot(v);
        const double t = (ddotv / vsqr) * (1 - sqrt(1 - vsqr * (dsqr - radius * radius) / (ddotv * ddotv)));
        Vec2d u = d - t * v;
        assert(fabs(u.norm() - radius) < 1e-9);
        const double C = -radius * radius - u.dot(center);

        Vec2d tangent = Vec2d(-u[1], u[0]);

        double theta = atan(d[1] / d[0]);
        switch (origin) {
            // Theta \in [0, 2\Pi]
            case ADown:
                if ((r[0] < center[0]))
                    theta += M_PI / 2;
                else
                    theta += 3 * M_PI / 2;
                break;

            // Theta \in [0, 2\Pi]
            case ARight:

                if (theta > 0) {
                    if (r[0] < center[0]) theta += M_PI;
                } else {
                    if (r[0] > center[0])
                        theta += 2 * M_PI;
                    else
                        theta += M_PI;
                }
                break;

            default:
                std::cerr << "This shouldn't happen" << std::endl;
                break;
        }
        double s = d_s0 + theta * radius;

        // Check if hit a hole
        if ((s > 0) and fmod(s, d_L) < d_l) {
            hit_flag = true;

        } else {
            hits++;
        }
        // DEBUG
        /*
        if (hit_flag)
            std::cout << r[0] << " " << r[1] << std::endl;
        */
        if (dir == ForbidOutside)
            r -= 2 * fabs(u.dot(r) + C) * u * invradsqr;
        else
            r += 2 * fabs(u.dot(r) + C) * u * invradsqr;
        v = 2 * v.dot(tangent) * tangent * invradsqr - v;

        return true;
    }
}

// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /
// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /// /

CompoundShape::OpenBoundaryHorizontalLine::OpenBoundaryHorizontalLine(
    enum Direction arg_dir, double y, double arg_s0, double arg_x0, double arg_l, double arg_L
)
    : CompoundShape::BoundaryHorizontalLine::BoundaryHorizontalLine(arg_dir, y, arg_s0, arg_x0) {
    d_l = arg_l;
    d_L = arg_L;
}

bool CompoundShape::OpenBoundaryHorizontalLine::handle_crossing(
    Vec2d& r, Vec2d& v, std::vector<std::vector<double>>*, const double, unsigned& hits, bool& hit_flag
) const {
    double s = d_s0;
    if (dir == ForbidUpper) {
        if (r[1] > pos_y) {
            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            hits++;

            s += beg_x - r[0];
            if (fmod(s, d_L) < d_l) hit_flag = true;

            return true;
        }
    } else {
        if (r[1] < pos_y) {
            r[1] = 2 * pos_y - r[1];
            v[1] *= -1;
            hits++;

            s += r[0] - beg_x;
            if (fmod(s, d_L) < d_l) hit_flag = true;

            return true;
        }
    }
    return false;
}

CompoundShape::OpenBoundaryRectangleCorner::OpenBoundaryRectangleCorner(
    Direction arg_dir, double x, double y, double arg_s0, double arg_l, double arg_L
)
    : CompoundShape::BoundaryRectangleCorner(arg_dir, x, y, arg_s0) {
    d_l = arg_l;
    d_L = arg_L;
}

bool CompoundShape::OpenBoundaryRectangleCorner::handle_crossing(
    Vec2d& r, Vec2d& v, std::vector<std::vector<double>>*, const double, unsigned& hits, bool& hit_flag
) const {
    if (point_is_inside(r)) return false;
    // This gets slightly complicated as we need to test which line of the corner the particle passed.
    enum LastPassed { X, Y } last;
    const double tx = (r[0] - pos_x) / v[0];
    const double ty = (r[1] - pos_y) / v[1];
    if (tx < 0)
        last = Y;
    else if (ty < 0)
        last = X;
    else if (tx < ty)
        last = X;
    else
        last = Y;

    if (last == X) {
        r[0] = 2 * pos_x - r[0];
        v[0] *= -1;

    } else {
        r[1] = 2 * pos_y - r[1];
        v[1] *= -1;
    }

    double s = d_s0;

    switch (dir) {
        case TopLeft:
            if (last == Y)
                s += r[0] - pos_x;
            else
                s -= pos_y - r[1];

            break;

        case TopRight:
            if (last == Y)
                s -= pos_x - r[0];
            else
                s += pos_y - r[1];
            break;

        case BottomLeft:
            if (last == Y)
                s -= r[0] - pos_x;
            else
                s += r[1] - pos_y;
            break;

        case BottomRight:
            if (last == Y)
                s += pos_x - r[0];
            else
                s -= r[1] - pos_y;
            break;

        default:
            std::cerr << "OpenBoundaryRectangleCorner: s handling failed, this shouldn't happen." << std::endl;
    }

    // Check if hit a hole
    if (s > 0 and fmod(s, d_L) < d_l) {
        hit_flag = true;
    }

    hits++;
    return true;
}

/*
 * Methods of class Triangle
 */

Triangle::Triangle(double scale_x, double scale_y) {
    bshapes.emplace_back(new BoundaryHorizontalLine(BoundaryHorizontalLine::ForbidLower, 0, 0, 0));  // Bottom
    bshapes.emplace_back(new BoundaryLine(
        BoundaryLine::ForbidUpper,
        -sqrt(3) * scale_y / scale_x,
        1,
        0,
        scale_x + 2 * sqrt(scale_x * scale_x / 4 + 3 * scale_y * scale_y / 4),
        Vec2d(scale_x / 2.0, scale_y * sqrt(0.75))
    ));  // Left side
    bshapes.emplace_back(new BoundaryLine(
        BoundaryLine::ForbidUpper, 1, scale_x / (scale_y * sqrt(3)), -scale_x, scale_x, Vec2d(scale_x, 0.0)
    ));  // Right side
    d_shape_name << "triangle(" << scale_x << "," << scale_y << ")";
    maxCoords.x = scale_x;
    maxCoords.y = scale_y;
}

/*
 * Methods of class Circle
 */

Circle::Circle(Vec2d arg_center, double arg_radius) {
    bshapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidOutside, arg_center, arg_radius, BoundaryCircle::No, 0, BoundaryCircle::ADown
    ));
    d_shape_name << "circle(" << arg_center[0] << "," << arg_center[1] << "," << arg_radius << ")";
    maxCoords.x = 2 * arg_radius;
    maxCoords.y = 2 * arg_radius;
}

/*
 * Methods of class Ellipse
 */

Ellipse::Ellipse(double arg_a, double arg_b) {
    bshapes.emplace_back(new BoundaryEllipse(arg_a, arg_b, 0));
    double e = sqrt(1 - pow(std::min(arg_a, arg_b) / std::max(arg_a, arg_b), 2));

    double max_s = 4 * std::max(arg_a, arg_b) * ellint_2(e);
    d_shape_name << "ellipse(" << arg_a << "," << arg_b << "," << max_s << ")";
    maxCoords.x = arg_a;
    maxCoords.y = arg_b;
}

/*
 * Methods of class AlternativeRectangle
 */

/*AlternativeRectangle::AlternativeRectangle(double w, double h) {
    bshapes.emplace_back(new BoundaryRectangle(BoundaryRectangle::ForbidOutside, 0, w, 0, h, 0));
    d_shape_name << "rectangle(" << w << "," << h << ")";
    maxCoords.x = w;
    maxCoords.y = h;
}*/

/*
 * Methods of class Sinai
 */

Sinai::Sinai(double side_l, double radius) {
    assert(radius < 0.5 * side_l);
    bshapes.emplace_back(new BoundaryRectangle(BoundaryRectangle::ForbidOutside, 0, side_l, 0, side_l, 0));
    shapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidInside,
        Vec2d(side_l / 2, side_l / 2),
        radius,
        BoundaryCircle::No,
        0,
        BoundaryCircle::ADown
    ));
    d_shape_name << "sinai(" << side_l << "," << radius << ")";
    maxCoords.x = side_l;
    maxCoords.y = side_l;
}

/*
 * Methods of class Ring
 */

Ring::Ring(double inner_r, double outer_r) {
    assert(inner_r < outer_r);
    bshapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidOutside, Vec2d(outer_r, outer_r), outer_r, BoundaryCircle::No, 0, BoundaryCircle::ADown
    ));
    shapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidInside, Vec2d(outer_r, outer_r), inner_r, BoundaryCircle::No, 0, BoundaryCircle::ADown
    ));
    d_shape_name << "ring(" << inner_r << "," << outer_r << ")";
    maxCoords.x = 2 * outer_r;
    maxCoords.y = 2 * outer_r;
}

/*
 * Methods of class Stadium
 */

Stadium::Stadium(double R, double L) {
    bshapes.emplace_back(new BoundaryHorizontalLine(BoundaryHorizontalLine::ForbidUpper, 2 * R, L + M_PI * R, R + L));
    bshapes.emplace_back(new BoundaryHorizontalLine(BoundaryHorizontalLine::ForbidLower, 0, 0, R));
    bshapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidOutside, Vec2d(R, R), R, BoundaryCircle::Left, 2 * L + 2 * M_PI * R, BoundaryCircle::ADown
    ));
    bshapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidOutside, Vec2d(R + L, R), R, BoundaryCircle::Right, L, BoundaryCircle::ADown
    ));
    d_shape_name << "stadium(" << R << "," << L << ")";
    maxCoords.x = 2 * R + L;
    maxCoords.y = 2 * R;
}

/*
 * Methods of class Mushroom
 */

Mushroom::Mushroom(double foot_width, double foot_height, double radius) {
    assert(foot_width <= 2 * radius);
    double circumference = (2 + M_PI) * radius + 2 * foot_height;
    bshapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidOutside,
        Vec2d(radius, foot_height),
        radius,
        BoundaryCircle::Top,
        radius + foot_height + foot_width / 2,
        BoundaryCircle::ARight
    ));
    bshapes.emplace_back(new BoundaryRectangleCorner(
        BoundaryRectangleCorner::TopRight, radius - 0.5 * foot_width, foot_height, circumference - foot_height
    ));
    bshapes.emplace_back(new BoundaryRectangleCorner(
        BoundaryRectangleCorner::TopLeft, radius + 0.5 * foot_width, foot_height, foot_height + foot_width
    ));
    bshapes.emplace_back(
        new BoundaryHorizontalLine(BoundaryHorizontalLine::ForbidLower, 0, 0, radius - 0.5 * foot_width)
    );
    d_shape_name << "mushroom(" << radius << "," << foot_height << "," << foot_width << ")";
    maxCoords.x = 2 * radius;
    maxCoords.y = foot_height + radius;
}

/*
 * Methods of class Gate
 */

Gate::Gate(double gate_width, double box_side) {
    // Square boxes and 0.1 wide wall with gate_width length width of the contact
    assert(gate_width <= 1.0);
    shapes.emplace_back(new BoundaryRectangle(BoundaryRectangle::ForbidOutside, 0, box_side, 0, 0.1 + 2 * box_side, 0));
    shapes.emplace_back(new BoundaryRectangle(
        BoundaryRectangle::ForbidInside, -0.5, box_side / 2 - gate_width / 2, box_side, box_side + 0.1, 0
    ));
    shapes.emplace_back(new BoundaryRectangle(
        BoundaryRectangle::ForbidInside, box_side / 2 + gate_width / 2, box_side + 0.5, box_side, box_side + 0.1, 0
    ));
    d_shape_name << "gate(" << gate_width << "," << box_side << ")";
    maxCoords.x = box_side;
    maxCoords.y = 2 * box_side + 0.1;
}

/*
 * Methods for open tables
 */

OpenRectangle::OpenRectangle(double width, double height, double holeRatio) {
    double circumference = 2 * width + 2 * height;
    double holeWidth = circumference * holeRatio;
    assert(holeWidth <= width);

    shapes.emplace_back(new OpenBoundaryRectangle(
        BoundaryRectangle::ForbidOutside,
        0,
        width,
        0,
        height,
        holeWidth,
        circumference * 1.1,
        -0.5 * (width - holeWidth)
    ));
    d_shape_name << "openrectangle(" << width << "," << height << "," << holeWidth << ")";
    maxCoords.x = width;
    maxCoords.y = height;
}

OpenCircle::OpenCircle(Vec2d centerPoint, double radius, double holeRatio) {
    double circumference = 2 * M_PI * radius;
    double holeWidth = circumference * holeRatio;

    shapes.emplace_back(new OpenBoundaryCircle(
        BoundaryCircle::ForbidOutside,
        centerPoint,
        radius,
        BoundaryCircle::No,
        holeWidth,
        circumference * 1.1,
        0,
        BoundaryCircle::ARight
    ));
    d_shape_name << "opencircle(" << centerPoint[0] << "," << centerPoint[1] << "," << radius << "," << holeWidth
                 << ")";
    maxCoords.x = 2 * radius;
    maxCoords.y = 2 * radius;
}

OpenStadium::OpenStadium(double halfCircleRadius, double sideLength, double holeRatio) {
    const double& L = sideLength;
    const double& R = halfCircleRadius;

    double circumference = 2 * L + 2 * M_PI * R;
    double holeWidth = circumference * holeRatio;
    assert(holeWidth <= L);

    double holePeriodicity = 3. / 2. * L + M_PI * R - holeWidth / 2;  // This must be large enough

    shapes.emplace_back(new BoundaryHorizontalLine(BoundaryHorizontalLine::ForbidLower, 0, 0, R));
    shapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidOutside, Vec2d(R + L, R), R, BoundaryCircle::Right, L, BoundaryCircle::ADown
    ));

    shapes.emplace_back(new OpenBoundaryHorizontalLine(
        BoundaryHorizontalLine::ForbidUpper, 2 * R, L + M_PI * R, R + L, holeWidth, holePeriodicity
    ));

    shapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidOutside, Vec2d(R, R), R, BoundaryCircle::Left, circumference, BoundaryCircle::ADown
    ));

    d_shape_name << "openstadium(" << R << "," << L << "," << holeWidth << ")";

    maxCoords.x = 2 * R + L;
    maxCoords.y = 2 * R;
}

OpenSinai::OpenSinai(double sideLength, double radius, double holeRatio) {
    assert(2 * radius <= sideLength);

    double w = sideLength;
    double h = sideLength;

    double circumference = 4 * sideLength + 2 * M_PI * radius;

    double holeWidth = circumference * holeRatio;
    assert(holeWidth <= sideLength);

    shapes.emplace_back(new OpenBoundaryRectangle(
        BoundaryRectangle::ForbidOutside, 0, w, 0, h, holeWidth, circumference, -0.5 * (w - holeWidth)
    ));
    shapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidInside,
        Vec2d(sideLength / 2, sideLength / 2),
        radius,
        BoundaryCircle::No,
        0,
        BoundaryCircle::ADown
    ));

    d_shape_name << "opensinai(" << radius << "," << sideLength << "," << holeWidth << ")";

    maxCoords.x = sideLength;
    maxCoords.y = sideLength;
}

OpenMushroom::OpenMushroom(double footWidth, double footHeight, double radius, double holeRatio) {
    assert(footWidth <= 2 * radius);
    double circumference = (2 + M_PI) * radius + 2 * footHeight;

    double holewidth = circumference * holeRatio;
    shapes.emplace_back(new OpenBoundaryCircle(
        BoundaryCircle::ForbidOutside,
        Vec2d(radius, footHeight),
        radius,
        BoundaryCircle::Top,
        holewidth,
        M_PI * radius * 2,
        -(M_PI * radius - holewidth) / 2,
        BoundaryCircle::ARight
    ));
    shapes.emplace_back(new BoundaryRectangleCorner(
        BoundaryRectangleCorner::TopRight, radius - 0.5 * footWidth, footHeight, circumference - footHeight
    ));
    shapes.emplace_back(new BoundaryRectangleCorner(
        BoundaryRectangleCorner::TopLeft, radius + 0.5 * footWidth, footHeight, footHeight + footWidth
    ));
    shapes.emplace_back(new BoundaryHorizontalLine(BoundaryHorizontalLine::ForbidLower, 0, 0, radius - 0.5 * footWidth)
    );
    d_shape_name << "openmushroom(" << radius << "," << footHeight << "," << footWidth << "," << holewidth << ")";

    maxCoords.x = 2 * radius;
    maxCoords.y = footHeight + radius;
}

LorentzGas::LorentzGas(triplet<double> uc_params, double radius)
    : CompoundShapePeriodic(uc_params, radius * (uc_params[0] - 1), radius * (M_PI - 3) + uc_params[1]) {
    assert(radius <= uc_params[1] / 2);
    assert(radius <= uc_params[2] / 2);
    double phi = uc_params[0];
    double L1 = uc_params[1];
    double L2 = uc_params[2];
    bshapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidInside,
        Vec2d(0.0, 0.0),
        radius,
        BoundaryCircle::No,
        radius * (M_PI_2 + phi),
        BoundaryCircle::ADown,
        BoundaryCircle::CLOCKWISE
    ));
    shapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidInside,
        Vec2d(cos(phi) * L2, sin(phi) * L2),
        radius,
        BoundaryCircle::No,
        0,
        BoundaryCircle::ADown
    ));
    shapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidInside,
        Vec2d(L1 + cos(phi) * L2, sin(phi) * L2),
        radius,
        BoundaryCircle::No,
        0,
        BoundaryCircle::ADown
    ));
    bshapes.emplace_back(new BoundaryCircle(
        BoundaryCircle::ForbidInside,
        Vec2d(L1, 0.0),
        radius,
        BoundaryCircle::No,
        (phi - 2 - M_PI) * radius + L1,
        BoundaryCircle::ARight,
        BoundaryCircle::CLOCKWISE
    ));

    d_shape_name << "lorentzgas(" << radius << ")";
    maxCoords.x = L1 + cos(phi) * L2;
    maxCoords.y = L2 * sin(phi);
}

/*
 * Methods of class CompoundShapePeriodic
 */
CompoundShapePeriodic::CompoundShapePeriodic(triplet<double> uc_params, double s_1, double s_2) {
    // Initialize parameters relating to the parallellogram unit cell
    d_L1 = uc_params[1];
    d_L2 = uc_params[2];

    d_Al = tan(uc_params[0]);
    d_Br = 1 / d_Al;

    if (std::fabs(uc_params[0] - M_PI / 2) < 1e-6) {
        const double L1 = d_L1;
        over_right_boundary = [L1](const Vec2d& r) { return r[0] > L1; };
        over_left_boundary = [](const Vec2d& r) { return r[0] < 0; };
    } else {
        const double Br = d_Br;
        const double L1 = d_L1;
        const double Al = d_Al;

        over_right_boundary = [Br, L1](const Vec2d& r) { return -r[0] + Br * r[1] + L1 < 0; };

        over_left_boundary = [Al](const Vec2d& r) { return -Al * r[0] + r[1] > 0; };
    }

    d_alpha = uc_params[0];
    d_uh = sin(uc_params[0]) * d_L2;
    d_hd = d_L2 * cos(uc_params[0]);
    d_s1 = s_1;
    d_s2 = s_2;
    d_right_boundary_tangent = Vec2d(d_hd, d_uh);
    d_right_boundary_tangent /= d_right_boundary_tangent.norm();
    d_shelp1 = d_s1 + d_s2 + d_L2 + d_L1 + d_hd;
    d_shelp2 = 2 * d_s2 + d_s1 + 2 * d_L2;
}

bool CompoundShapePeriodic::
    handle_cell(Vec2d& r, const Vec2d& v, pair<int>& cell, std::vector<std::vector<double>>* bmap, const double time, bool&)
        const {
    bool new_cell = false;
    std::vector<double> hit(4, 0);

    // If passed through to upper cell
    if (r[1] > d_uh) {
        if (bmap) {
            hit.at(0) = time;
            hit.at(1) = d_shelp1 - r[0];
            hit.at(2) = -v[0];
            hit.at(3) = atan(-v[0] / v[1]);
            bmap->emplace_back(hit);
        }
        r[1] -= d_uh;
        r[0] -= d_hd;
        cell[1]++;
        new_cell = true;
    }
    if (r[1] < 0) {  // To lower cell
        if (bmap) {
            hit.at(0) = time;
            hit.at(1) = d_s1 + r[0];
            hit.at(2) = v[0];
            hit.at(3) = atan(-v[0] / v[1]);
            bmap->emplace_back(hit);
        }
        r[1] += d_uh;
        r[0] += d_hd;
        cell[1]--;
        new_cell = true;
    }
    if (over_right_boundary(r)) {  // To right cell
        if (bmap) {
            hit.at(0) = time;
            hit.at(1) = d_s2 + sqrt(r[1] * r[1] + pow(r[0] - d_L1, 2));
            hit.at(2) = v.dot(d_right_boundary_tangent);
            double gamma = atan(v[1] / v[0]);
            if ((v[1] >= 0) or ((v[0] >= 0) and (gamma < M_PI / 2 - d_alpha))) {
                hit.at(3) = M_PI / 2 - d_alpha + gamma;
            } else {
                hit.at(3) = -(d_alpha + atan(v[0] / v[1]));
            }

            bmap->emplace_back(hit);
        }
        r[0] -= d_L1;
        cell[0]++;
        new_cell = true;
    }
    if (over_left_boundary(r)) {  // To left cell
        if (bmap) {
            hit.at(0) = time;
            hit.at(1) = d_shelp2 - r.norm();
            hit.at(2) = -v.dot(d_right_boundary_tangent);
            double gamma = atan(v[1] / v[0]);
            if ((v[0] < 0) and (gamma > -M_PI / 2 + d_alpha)) {
                hit.at(3) = M_PI / 2 - d_alpha + gamma;
            } else {
                hit.at(3) = -(d_alpha + atan(v[0] / v[1]));
            }

            bmap->emplace_back(hit);
        }

        r[0] += d_L1;
        cell[0]--;
        new_cell = true;
    }
    return new_cell;
}

bool CompoundShapePeriodic::handle_collisions(
    Vec2d& r, Vec2d& v, pair<int>& cell, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
    bool& hit_flag
) const {
    bool check_again = false;
    bool checked_again = false;
    do {
        check_again = false;

        // Check shapes, which do not contribute to bounce maps
        for (const auto& shape : shapes) {
            if (shape->handle_crossing(r, v, nullptr, time, hits, hit_flag)) {
                check_again = true;
            }
        }
        // Check shapes, which can contribute to bounce maps
        for (const auto& bshape : bshapes) {
            if (bshape->handle_crossing(r, v, bmap, time, hits, hit_flag)) {
                check_again = true;
            }
        }

        if (handle_cell(r, v, cell, bmap, time, hit_flag)) check_again = true;

        if (check_again) checked_again = true;

    } while (check_again);
    return checked_again;
}

}  // End of namespace bill2d
