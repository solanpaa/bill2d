/*
 * particle.cpp
 *
 * This file is part of bill2d.
 *
 * External potentials are defined in this file.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "particle.hpp"

namespace bill2d {

Particle::Particle(
    unsigned arg_ID, double time, pair<double> rhist_lims, bool save_trajectory, bool save_velocity,
    bool save_histograms, bool save_bouncemap, std::size_t bins, double velocity_histogram_cutoff,
    double speed_histogram_cutoff, unsigned buffersize
) {
    spawntime = time;
    saveidx = 0;
    ID = arg_ID;
    d_r = {0.0, 0.0};

    // Allocate memory for buffers we wish to use
    if (save_trajectory) {
        traj = new double[buffersize * 2];
        assert(traj != nullptr);
    } else
        traj = nullptr;

    if (save_velocity) {
        vel = new double[buffersize * 2];
        assert(vel != nullptr);
    } else
        vel = nullptr;

    if (save_histograms) {
        d_rhist = new rhist(bins, rhist_lims);
        d_vhist = new vhist(bins, velocity_histogram_cutoff);
        d_shist = new shist(bins, speed_histogram_cutoff);

    } else {
        d_rhist = nullptr;
        d_vhist = nullptr;
        d_shist = nullptr;
    }

    if (save_bouncemap)
        bmap_ptr = &bmap;
    else
        bmap_ptr = nullptr;
}

Particle::Particle(Particle&& op) {
    spawntime = op.spawntime;
    saveidx = op.saveidx;
    ID = op.ID;

    d_r = op.d_r;
    d_r0 = op.d_r0;
    d_v = op.d_v;
    d_v0 = op.d_v0;
    d_F = op.d_F;

    d_cell = op.d_cell;

    bmap = std::move(op.bmap);
    if (op.bmap_ptr)
        bmap_ptr = &bmap;
    else
        bmap_ptr = nullptr;

    traj = op.traj;
    op.traj = nullptr;

    vel = op.vel;
    op.vel = nullptr;

    d_rhist = op.d_rhist;
    d_vhist = op.d_vhist;
    d_shist = op.d_shist;

    op.d_rhist = nullptr;
    op.d_vhist = nullptr;
    op.d_shist = nullptr;
}

void Particle::operator=(Particle&& op) {
    // Free buffers
    if (traj != nullptr) delete[] traj;

    if (vel != nullptr) delete[] vel;

    if (d_rhist != nullptr) {
        delete d_rhist;
        delete d_vhist;
        delete d_shist;
    }

    spawntime = op.spawntime;
    saveidx = op.saveidx;
    ID = op.ID;

    d_r = op.d_r;
    d_r0 = op.d_r0;
    d_v = op.d_v;
    d_v0 = op.d_v0;
    d_F = op.d_F;

    d_cell = op.d_cell;

    bmap = std::move(op.bmap);
    if (op.bmap_ptr)
        bmap_ptr = &bmap;
    else
        bmap_ptr = nullptr;

    traj = op.traj;
    op.traj = nullptr;

    vel = op.vel;
    op.vel = nullptr;

    d_rhist = op.d_rhist;
    d_vhist = op.d_vhist;
    d_shist = op.d_shist;

    op.d_rhist = nullptr;
    op.d_vhist = nullptr;
    op.d_shist = nullptr;
}

Particle::~Particle() {
    // Free buffers
    if (traj != nullptr) delete[] traj;

    if (vel != nullptr) delete[] vel;

    if (d_rhist != nullptr) {
        delete d_rhist;
        delete d_vhist;
        delete d_shist;
    }
}

}  // namespace bill2d
