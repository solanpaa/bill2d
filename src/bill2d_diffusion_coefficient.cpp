/*
 * bill2d_diffusion_coefficient.cpp
 *
 * This file is part of bill2d.
 *
 * A source for executable that calculates diffusion coefficients
 * for periodic systems
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file bill2d_diffusion_coefficient.cpp
 * \brief Simulation of diffusion in periodic systems with the binary 'bill2d_diffusion' implemented here.
 */

#include "billiard_factory.hpp"
#include "executables.hpp"

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/error_of.hpp>
#include <boost/accumulators/statistics/error_of_mean.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/stats.hpp>

#include <signal.h>

#include <fstream>

using namespace boost::accumulators;

void bill2d::diffusion_main(ParameterList& plist) {
    bool individual_data = plist.individual_data;
    const unsigned ensemble_N = plist.simulations;
    const double DT = plist.min_t;

    // Create all the systems, this does not consume too much memory
    std::vector<std::unique_ptr<Billiard>> ensemble;
    ensemble.reserve(ensemble_N);
    double D_ensemble[ensemble_N];
    BilliardFactory bfactory(plist);  // BilliardFactory not thread-safe but here we use this in a single thread, and
                                      // no trajectories are saved. Hence, ok.

    std::cout << "#Time <||r(t)-r(0)||^2>  error_of_mean" << std::endl;
    for (unsigned i = 0; i < ensemble_N; i++) {
        ensemble.push_back(bfactory.get_bill());
        ensemble[i]->prepare(plist.energy);
    }

    std::size_t iters = 0;
    double t = 0;
    std::ofstream inpos_file;
    std::ofstream indiv_file;
    // Save individual D-coeffs to 'savepath' as ASCII file
    if (individual_data) {
        inpos_file.open(std::string(plist.savefilepath).append("_initial_positions"), std::ios::out);
        indiv_file.open(plist.savefilepath, std::ios::out);
        indiv_file << t << " ";
        for (unsigned i = 0; i < ensemble_N; i++) {
            // Save the initial positions as accurately as possible (max_digits10)
            inpos_file << std::setprecision(std::numeric_limits<double>().max_digits10)
                       << ensemble[i]->get_initial_position()[0] << " " << ensemble[i]->get_initial_position()[1] << " "
                       << ensemble[i]->get_initial_velocity()[0] << " " << ensemble[i]->get_initial_velocity()[1]
                       << std::endl;
            indiv_file << 0 << " ";
        }
        indiv_file << std::endl;
        inpos_file.close();
    }

    bool kill_exit = false;
#ifdef _OPENMP
#pragma omp parallel shared(                                                                                           \
        t, kill_exit, plist, ensemble, D_ensemble, SIG_FLAG, std::cout, individual_data, indiv_file, iters             \
)
    {
#endif
        do {
#ifdef _OPENMP

#pragma omp for schedule(dynamic, 50)
#endif
            for (unsigned i = 0; i < ensemble_N; i++) D_ensemble[i] = single_run_diff_coeff(ensemble[i].get());

#ifdef _OPENMP
#pragma omp single
            {
#endif
                accumulator_set<double, stats<tag::mean, tag::error_of<tag::mean>>> acc;

                std::for_each(
                    D_ensemble, D_ensemble + ensemble_N, std::bind<void>(std::ref(acc), std::placeholders::_1)
                );

                iters++;
                t = iters * DT;

                if (individual_data) {
                    indiv_file << t << " ";
                    for (unsigned i = 0; i < ensemble_N; i++) {
                        indiv_file << D_ensemble[i] << " ";
                    }
                    indiv_file << "\n";
                    indiv_file.flush();
                }

                std::cout << t << " " << mean(acc) << " " << error_of<tag::mean>(acc) << std::endl;

                // Handle sigint and sigterm
                if ((SIG_FLAG == SIGINT) or (SIG_FLAG == SIGTERM)) {
                    kill_exit = true;
                }
#ifdef _OPENMP
            }
#endif

        } while ((t < plist.max_t) and (not kill_exit));

#ifdef _OPENMP
    }
#endif
    indiv_file.close();
}

double bill2d::single_run_diff_coeff(Billiard* bill) {
    size_t iter = bill->get_plist().minimum_number_of_iterations;
    // Propagate
    for (size_t i = 0; i < iter; i++) {
        bill->propagate();
    }

    return bill->get_difference_squared_to_initial_position();
}
