/*
 * interactions.hpp
 *
 * This file is part of bill2d.
 *
 * Particle-particle interactions are defined in this file.
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file interactions.hpp
 * \brief The particle-particle interactions are defined here
 */

#ifndef _INTERACTIONS_HPP_
#define _INTERACTIONS_HPP_
#include "vec2d.hpp"

namespace bill2d {

//! Base class for particle-particle interactions
class Interaction {
public:
    //! The pp-interaction force acting on particle 1
    /*!
     \param x position of particle 1
     \param y position of particle 2
     */
    virtual const Vec2d force(const Vec2d& x, const Vec2d& y) const = 0;
    //! Total energy of the particle
    virtual double energy(const Vec2d& x, const Vec2d& y) const = 0;
    virtual ~Interaction() {}
};

//! Coulombic interaction
class CoulombInteraction : public Interaction {
public:
    // explicit: Don't allow converting double to CoulombInteraction..
    explicit CoulombInteraction(double strength = 1.0);

    const Vec2d force(const Vec2d& x, const Vec2d& y) const override;
    double energy(const Vec2d& x, const Vec2d& y) const override;

private:
    //! \f$\alpha\f$
    double d_strength;
};
}  // namespace bill2d
#endif
