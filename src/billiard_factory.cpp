/*
 * billiard_factory.cp
 *
 * This file is part of bill2d.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */
#include "billiard_factory.hpp"
#include "billiard.hpp"
#include "potentials.hpp"

/*! \file billiard_factory.cpp
 *  \brief A source file containing the implementation of the class bill2d::BilliardFactory
 */

namespace bill2d {
unsigned BaseBilliardFactory::d_file_nbr = 0;

void BaseBilliardFactory::create_RNG() {
    if (d_rng) return;
    d_rng = std::make_unique<const RNG>();
}

void BaseBilliardFactory::create_Potential() {
    if (d_potential) return;
    unsigned nbr_pots = 0;

    if (plist.harmonic_potential) nbr_pots++;
    if (plist.soft_stadium_potential) nbr_pots++;
    if (plist.gaussian_bath) nbr_pots++;
    if (plist.soft_lorentz_gas) nbr_pots++;
    if (plist.soft_lieb_gas) nbr_pots++;
    if (plist.bias_voltage) nbr_pots++;
    if (plist.gaussian_bumps) nbr_pots += plist.gaussian_bumps_manual_amplitudes.size();
    if (plist.gaussian_bumps and !plist.gaussian_bumps_itp2d_file.empty())
        nbr_pots += 2;  // Actually there will be many many bumps
    if (plist.gaussian_bumps_fast_itp2d) nbr_pots++;
    if (plist.soft_circle) nbr_pots++;
    if (nbr_pots > 1) {
        std::unique_ptr<Potential_cont> pot = std::make_unique<Potential_cont>();

        if (plist.harmonic_potential) {
            if (plist.output_level >= 2) std::cout << "Using Harmonic potential.." << std::endl;
            pot->add(new HarmonicPotential(plist.potential_strength, plist.centerpoint));
        }

        if (plist.soft_stadium_potential) {
            if (plist.output_level >= 2) std::cout << "Using Soft Stadium potential.." << std::endl;
            pot->add(new SoftStadium(plist.soft_stadium_power, plist.geometry[0], plist.geometry[1]));
        }

        if (plist.gaussian_bath) {
            if (plist.output_level >= 2)
                std::cout << "Using Gaussian heat bath with temperature " << plist.temperature << ".." << std::endl;
            pot->add(new GaussianBath(plist.temperature));
        }

        if (plist.soft_lorentz_gas) {
            if (plist.output_level >= 2)
                std::cout << "Using " << (plist.soft_lorentz_inverted ? "inverted " : "")
                          << "soft Lorentz gas with the lattice sum truncation "
                          << plist.soft_lorentz_gas_lattice_sum_truncation << " .." << std::endl;
            pot->add(new SoftLorentzGas(
                plist.uc_params,
                plist.soft_lorentz_radius,
                plist.soft_lorentz_sharpness,
                plist.soft_lorentz_gas_lattice_sum_truncation,
                plist.soft_lorentz_inverted
            ));
        }

        if (plist.soft_lieb_gas) {
            if (plist.output_level >= 2)
                std::cout << "Using soft Lieb gas with the lattice sum truncation "
                          << plist.soft_lieb_gas_lattice_sum_truncation << " .." << std::endl;
            pot->add(new SoftLiebGas(
                plist.uc_params,
                plist.soft_lieb_radius,
                plist.soft_lieb_sharpness,
                plist.soft_lieb_gas_lattice_sum_truncation
            ));
        }

        if (plist.bias_voltage) {
            if (plist.output_level >= 2)
                std::cout << "Using bias voltage, strength: " << plist.bias_str << ",  direction: ("
                          << plist.bias_direction[0] << ", " << plist.bias_direction[1] << ")" << std::endl;
            // pot -> add( new Bias(plist.bias_str, Vec2d{plist.bias_direction[0], plist.bias_direction[1]}));
        }

        if (plist.gaussian_bumps_fast_itp2d) {
            std::cout << "Using gaussian bumps from an itp2d simulation" << std::endl;

            assert(not plist.gaussian_bumps_itp2d_file.empty());
            pot->add(new GaussianPotentialCluster(plist.gaussian_bumps_itp2d_file));
        }

        if (plist.gaussian_bumps) {
            assert(plist.gaussian_bumps_manual_amplitudes.size() == plist.gaussian_bumps_manual_widths.size());
            assert(plist.gaussian_bumps_manual_locations.size() == plist.gaussian_bumps_manual_amplitudes.size());

            std::cout << "Using gaussian bumps:" << std::endl;

            for (unsigned i = 0; i < plist.gaussian_bumps_manual_amplitudes.size(); i++) {
                std::cout << "   at " << plist.gaussian_bumps_manual_locations.at(i) << " with width "
                          << plist.gaussian_bumps_manual_widths.at(i) << std::endl
                          << "   and amplitude " << plist.gaussian_bumps_manual_amplitudes.at(i) << std::endl;
                pot->add(new GaussianPotential(
                    plist.gaussian_bumps_manual_amplitudes.at(i),
                    plist.gaussian_bumps_manual_widths.at(i),
                    plist.gaussian_bumps_manual_locations.at(i)
                ));
            }

            // Read itp2d bumps
            if (not plist.gaussian_bumps_itp2d_file.empty()) {
                std::vector<double> itp2d_noise = hdf5_1darray_to_vector(plist.gaussian_bumps_itp2d_file, "noise_data");
                std::cout << "  itp2d bumps: " << itp2d_noise.size() / 4 << std::endl;

                bool itp2d_output = false;
                if (itp2d_noise.size() / 4 < 10) itp2d_output = true;

                for (unsigned i = 0; i < itp2d_noise.size() / 4; i++) {
                    Vec2d location{itp2d_noise.at(4 * i), itp2d_noise.at(4 * i + 1)};
                    double amplitude = itp2d_noise.at(4 * i + 2);
                    double width = itp2d_noise.at(4 * i + 3);
                    if (itp2d_output) {
                        std::cout << "   at " << location << " with width " << width << std::endl
                                  << "   and amplitude " << amplitude << std::endl;
                    }
                    pot->add(new GaussianPotential(amplitude, width, location));
                }
            }
        }

        if (plist.soft_circle) {
            if (plist.output_level >= 2) {
                std::cout << "Using soft circular billiard, with "
                          << (plist.soft_circle_type == ParameterList::SoftCircleType::ERROR_FUNCTION ?
                                  "error function" :
                                  "Fermi pole")
                          << " type potential.." << std::endl;
            }
            pot->add(new SoftCircle(plist.soft_circle_type, plist.soft_circle_softness, plist.soft_circle_eccentricity)
            );
        }

        d_potential = std::move(pot);

    } else if (plist.harmonic_potential) {
        if (plist.output_level >= 2) std::cout << "Using Harmonic potential.." << std::endl;
        d_potential = std::make_shared<const HarmonicPotential>(plist.potential_strength, plist.centerpoint);
    } else if (plist.soft_stadium_potential) {
        if (plist.output_level >= 2) std::cout << "Using Soft Stadium potential.." << std::endl;
        d_potential =
            std::make_shared<const SoftStadium>(plist.soft_stadium_power, plist.geometry[0], plist.geometry[1]);
    } else if (plist.gaussian_bath) {
        if (plist.output_level >= 2)
            std::cout << "Using Gaussian heat bath with temperature " << plist.temperature << ".." << std::endl;
        d_potential = std::make_shared<const GaussianBath>(plist.temperature);
    } else if (plist.soft_lorentz_gas) {
        if (plist.output_level >= 2)
            std::cout << "Using " << (plist.soft_lorentz_inverted ? "inverted " : "")
                      << "soft Lorenz gas with the lattice sum truncation "
                      << plist.soft_lorentz_gas_lattice_sum_truncation << " .." << std::endl;
        d_potential = std::make_shared<const SoftLorentzGas>(
            plist.uc_params,
            plist.soft_lorentz_radius,
            plist.soft_lorentz_sharpness,
            plist.soft_lorentz_gas_lattice_sum_truncation,
            plist.soft_lorentz_inverted
        );
    } else if (plist.soft_lieb_gas) {
        if (plist.output_level >= 2)
            std::cout << "Using soft Lieb gas with the lattice sum truncation "
                      << plist.soft_lieb_gas_lattice_sum_truncation << " .." << std::endl;

        d_potential = std::make_shared<const SoftLiebGas>(
            plist.uc_params,
            plist.soft_lieb_radius,
            plist.soft_lieb_sharpness,
            plist.soft_lieb_gas_lattice_sum_truncation
        );
    } else if (plist.bias_voltage) {
        if (plist.output_level >= 2)
            std::cout << "Using bias voltage, strength: " << plist.bias_str << ",  direction: ("
                      << plist.bias_direction[0] << ", " << plist.bias_direction[1] << ")" << std::endl;
        // d_potential = std::make_shared<const Bias>(plist.bias_str, Vec2d{plist.bias_direction[0],
        // plist.bias_direction[1]});
        exit(1);
    } else if (plist.gaussian_bumps) {
        d_potential = std::make_shared<const GaussianPotential>(
            plist.gaussian_bumps_manual_amplitudes.at(0),
            plist.gaussian_bumps_manual_widths.at(0),
            plist.gaussian_bumps_manual_locations.at(0)
        );
        std::cout << "Using a gaussian bump:" << std::endl
                  << "   at " << plist.gaussian_bumps_manual_locations.at(0) << " with width "
                  << plist.gaussian_bumps_manual_widths.at(0) << std::endl
                  << "   and amplitude " << plist.gaussian_bumps_manual_amplitudes.at(0) << std::endl;
    } else if (plist.gaussian_bumps_fast_itp2d) {
        std::cout << "Using gaussian bumps from an itp2d simulation" << std::endl;

        assert(not plist.gaussian_bumps_itp2d_file.empty());
        d_potential = std::make_shared<const GaussianPotentialCluster>(plist.gaussian_bumps_itp2d_file);
    } else if (plist.soft_circle) {
        if (plist.output_level >= 2) {
            std::cout << "Using soft circular billiard, with "
                      << (plist.soft_circle_type == ParameterList::SoftCircleType::ERROR_FUNCTION ? "error function" :
                                                                                                    "Fermi pole")
                      << " type potential.." << std::endl;
        }
        d_potential = std::make_shared<SoftCircle>(
            plist.soft_circle_type, plist.soft_circle_softness, plist.soft_circle_eccentricity
        );
    }

    else {
        if (plist.output_level >= 2) std::cout << "No external potential" << std::endl;
    }
}

void BaseBilliardFactory::create_Interaction() {
    if (d_interaction) return;
    switch (plist.interactiontype) {
        case ParameterList::InteractionType::COULOMB:
            if (plist.output_level >= 2) std::cout << "Using coulomb interactions" << std::endl;
            d_interaction = std::make_shared<const CoulombInteraction>(plist.interaction_strength);
            break;

        case ParameterList::InteractionType::NONE:
            if (plist.output_level >= 2) std::cout << "No particle-particle interactions" << std::endl;
            break;

        default:
            std::cerr << "Error: This shouldn't happen, error in initiating interactions." << std::endl;
            exit(1);
            break;
    }
}

void BaseBilliardFactory::create_Datafile() {
    if (d_file) return;  // Return if datafile already created
    if (plist.save_nothing) return;

    ParameterList tmp_params(plist);
    if (d_file_nbr != 0) {
        if (tmp_params.savefilepath.compare(tmp_params.savefilepath.length() - 3, 3, ".h5") == 0) {
            tmp_params.savefilepath = tmp_params.savefilepath.substr(0, tmp_params.savefilepath.length() - 3)
                                          .append("_")
                                          .append(std::to_string(d_file_nbr))
                                          .append(".h5");
        } else
            tmp_params.savefilepath.append(std::to_string(d_file_nbr)).append(".h5");

    } else {
        if (tmp_params.savefilepath.compare(tmp_params.savefilepath.length() - 3, 3, ".h5") != 0) {
            tmp_params.savefilepath.append(std::to_string(d_file_nbr)).append(".h5");
        }
    }
    d_file = std::make_unique<Datafile>(tmp_params);
    d_file->initialize(plist);
    d_file->write_attribute("table_shape", d_table->shape_name());
    d_file_nbr++;
}

void BaseBilliardFactory::create_CustomPSOS() {
    if (!plist.calculate_custom_psos) return;
    //! Create the S(x1 y1 vx1 vy1 ...) = (a*x1+b*y1+c)*(a*x2+b*y2+c) = 0 function

    // Normalize the S-line:
    double tmp = sqrt(
        plist.custom_psos_line[0] * plist.custom_psos_line[0] + plist.custom_psos_line[1] * plist.custom_psos_line[1]
    );
    plist.custom_psos_line[0] /= tmp;
    plist.custom_psos_line[1] /= tmp;
    plist.custom_psos_line[2] /= tmp;

    std::function<double(const particleset&)> PSOS =
        std::bind(CustomPSOS::linePSOS, std::placeholders::_1, plist.custom_psos_line);

    // The gradient of S

    std::function<std::vector<double>(const std::vector<double>&)> PSOS_grad =
        std::bind(CustomPSOS::linePSOS_gradfun, std::placeholders::_1, plist.custom_psos_line);

    std::function<std::vector<double>(const std::vector<double>&)> PH_to_PSOS =
        std::bind(CustomPSOS::phase_space_point_to_linePSOS_point, std::placeholders::_1, plist.custom_psos_line);

    d_custom_psos = std::make_unique<CustomPSOS>(std::move(PSOS), std::move(PSOS_grad), std::move(PH_to_PSOS));
}

void BaseBilliardFactory::create_Billiard() {
    if (d_bill) return;
    if (!d_table) create_Table();
    if (!d_file) create_Datafile();
    if (!d_rng) create_RNG();
    if (plist.calculate_custom_psos and !d_custom_psos) create_CustomPSOS();
    create_Potential();
    create_Interaction();

    switch (plist.billtype) {
        case ParameterList::BilliardType::BILLIARD:
            d_bill = std::make_unique<Billiard>(
                d_table, std::move(d_rng), plist, std::move(d_file), std::move(d_custom_psos)
            );

            if (plist.output_level >= 2)
                std::cout << "Using non-interacting billiard without magneticfield" << std::endl;
            break;

        case ParameterList::BilliardType::MAGNETICBILLIARD:
            d_bill = std::make_unique<MagneticBilliard>(
                d_table, std::move(d_rng), plist, std::move(d_file), std::move(d_custom_psos)
            );

            if (plist.output_level >= 2) std::cout << "Using non-interacting billiard with magneticfield" << std::endl;
            break;

        case ParameterList::BilliardType::INTERACTINGBILLIARD:
            d_bill = std::make_unique<InteractingBilliard>(
                d_interaction,
                d_potential,
                d_table,
                std::move(d_rng),
                plist,
                std::move(d_file),
                std::move(d_custom_psos)
            );

            if (plist.output_level >= 2) std::cout << "Using interacting billiard without magneticfield" << std::endl;
            break;

        case ParameterList::BilliardType::INTERACTINGMAGNETICBILLIARD:
            d_bill = std::make_unique<InteractingMagneticBilliard>(
                d_interaction,
                d_potential,
                d_table,
                std::move(d_rng),
                plist,
                std::move(d_file),
                std::move(d_custom_psos)
            );

            if (plist.output_level >= 2) std::cout << "Using interacting billiard with magneticfield" << std::endl;
            break;
    }

    assert(!d_custom_psos);
    assert(!d_file);
    assert(!d_rng);
}

//! Constructor
BilliardFactory::BilliardFactory(ParameterList& arg_plist) : BaseBilliardFactory(arg_plist) {}

//! Copy constructor
BilliardFactory::BilliardFactory(BilliardFactory& copythis) : BaseBilliardFactory(copythis.plist) {}

void BilliardFactory::create_Table() {
    if (d_table) return;

    if (!plist.default_geometry) {
        switch (plist.tabletype) {
            case ParameterList::TableType::NOTABLE:
                if (plist.periodic)
                    d_table = std::make_shared<const NoTablePeriodic>(plist.uc_params);
                else
                    d_table = std::make_shared<const NoTable>(plist.spawnarea[2], plist.spawnarea[3]);
                break;

            case ParameterList::TableType::RECTANGLE:
                d_table = std::make_shared<const Rectangle>(plist.geometry[0], plist.geometry[1]);
                break;

            case ParameterList::TableType::TRIANGLE:
                d_table = std::make_shared<const Triangle>(plist.geometry[0], plist.geometry[1]);
                break;

            case ParameterList::TableType::CIRCLE:
                d_table =
                    std::make_shared<const Circle>(Vec2d(plist.geometry[0], plist.geometry[0]), plist.geometry[0]);
                break;

            case ParameterList::TableType::ELLIPSE:
                d_table = std::make_shared<const Ellipse>(plist.geometry[0], plist.geometry[1]);
                break;

            case ParameterList::TableType::SINAI:
                d_table = std::make_shared<const Sinai>(plist.geometry[0], plist.geometry[1]);
                break;

            case ParameterList::TableType::RING:
                d_table = std::make_shared<const Ring>(plist.geometry[0], plist.geometry[1]);
                break;

            case ParameterList::TableType::STADIUM:
                d_table = std::make_shared<const Stadium>(plist.geometry[0], plist.geometry[1]);
                break;

            case ParameterList::TableType::MUSHROOM:
                d_table = std::make_shared<const Mushroom>(plist.geometry[0], plist.geometry[1], plist.geometry[1]);
                break;

            case ParameterList::TableType::GATE:
                d_table = std::make_shared<const Gate>(plist.geometry[1], plist.geometry[0]);
                break;

            case ParameterList::TableType::LORENTZGAS:
                d_table = std::make_shared<const LorentzGas>(plist.uc_params, plist.geometry[0]);
                break;

            default:
                std::cerr << "Error: This shouldn't happen, error in initiating the table." << std::endl;
                exit(1);
                break;
        }
    } else {
        switch (plist.tabletype) {
            case ParameterList::TableType::NOTABLE:
                if (plist.periodic)
                    d_table = std::make_shared<const NoTablePeriodic>(plist.uc_params);
                else
                    d_table = std::make_shared<const NoTable>(
                        std::max(plist.spawnarea[2], 1.0), std::max(plist.spawnarea[3], 1.0)
                    );
                break;

            case ParameterList::TableType::RECTANGLE:
                d_table = std::make_shared<const Rectangle>();
                break;

            case ParameterList::TableType::TRIANGLE:
                d_table = std::make_shared<const Triangle>();
                break;

            case ParameterList::TableType::CIRCLE:
                d_table = std::make_shared<const Circle>();
                break;

            case ParameterList::TableType::ELLIPSE:
                d_table = std::make_shared<const Ellipse>();
                break;

            case ParameterList::TableType::SINAI:
                d_table = std::make_shared<const Sinai>();
                break;

            case ParameterList::TableType::RING:
                d_table = std::make_shared<const Ring>();
                break;

            case ParameterList::TableType::STADIUM:
                d_table = std::make_shared<const Stadium>();
                break;

            case ParameterList::TableType::MUSHROOM:
                d_table = std::make_shared<const Mushroom>();
                break;

            case ParameterList::TableType::GATE:
                d_table = std::make_shared<const Gate>();
                break;

            case ParameterList::TableType::LORENTZGAS:
                d_table = std::make_shared<const LorentzGas>();
                plist.geometry = {
                    0.3, 0.3
                };  // Otherwise saves wrong 'geometry' to datafile, and trajectory plotting will look odd
                break;

            default:
                std::cerr << "Error: This shouldn't happen, error in initiating the table." << std::endl;
                exit(1);
                break;
        }
    }

    if (plist.default_spawnarea) {
        pair<double> ar = d_table->getMaxCoords();
        plist.spawnarea[0] = 0;
        plist.spawnarea[1] = 0;
        plist.spawnarea[2] = ar.x;
        plist.spawnarea[3] = ar.y;
    }
}

//! Constructor
OpenBilliardFactory::OpenBilliardFactory(ParameterList& arg_plist) : BaseBilliardFactory(arg_plist) {}

//! Copy constructor
OpenBilliardFactory::OpenBilliardFactory(OpenBilliardFactory& copythis) : BaseBilliardFactory(copythis.plist) {}

void OpenBilliardFactory::create_Table() {
    if (d_table) return;

    if (!plist.default_geometry) {
        switch (plist.tabletype) {
            case ParameterList::TableType::CIRCLE:
                d_table = std::make_shared<const OpenCircle>(
                    Vec2d(plist.geometry[0], plist.geometry[0]), plist.geometry[0], plist.holeratio
                );
                break;

            case ParameterList::TableType::RECTANGLE:
                d_table = std::make_shared<const OpenRectangle>(plist.geometry[0], plist.geometry[1], plist.holeratio);
                break;

            case ParameterList::TableType::SINAI:
                d_table = std::make_shared<const OpenSinai>(plist.geometry[0], plist.geometry[1], plist.holeratio);
                break;

            case ParameterList::TableType::STADIUM:
                d_table = std::make_shared<const OpenStadium>(plist.geometry[0], plist.geometry[1], plist.holeratio);
                break;

            case ParameterList::TableType::MUSHROOM:  // 0.5 is the foot height
                d_table = std::make_shared<const OpenMushroom>(
                    plist.geometry[0], plist.geometry[1], plist.geometry[1], plist.holeratio
                );
                break;

            default:
                std::cerr << "Error: This shouldn't happen, error in initiating the table." << std::endl;
                std::cerr << " Maybe you requested an unsupported table for the selected simulation type." << std::endl;
                exit(1);
                break;
        }
    } else if (!plist.default_holeratio()) {
        switch (plist.tabletype) {
            case ParameterList::TableType::CIRCLE:
                d_table = std::make_shared<const OpenCircle>(HoleRatio(plist.holeratio));
                break;

            case ParameterList::TableType::RECTANGLE:
                d_table = std::make_shared<const OpenRectangle>(HoleRatio(plist.holeratio));
                break;

            case ParameterList::TableType::SINAI:
                d_table = std::make_shared<const OpenSinai>(HoleRatio(plist.holeratio));
                break;

            case ParameterList::TableType::STADIUM:
                d_table = std::make_shared<const OpenStadium>(HoleRatio(plist.holeratio));
                break;

            case ParameterList::TableType::MUSHROOM:  // 0.5 is the foot height
                d_table = std::make_shared<const OpenMushroom>(HoleRatio(plist.holeratio));
                break;

            default:
                std::cerr << "Error: This shouldn't happen, error in initiating the table.";
                std::cerr << " Maybe you requested an unsupported table for the selected simulation type." << std::endl;
                exit(1);
                break;
        }

    } else {
        switch (plist.tabletype) {
            case ParameterList::TableType::CIRCLE:
                d_table = std::make_shared<const OpenCircle>();
                break;

            case ParameterList::TableType::RECTANGLE:
                d_table = std::make_shared<const OpenRectangle>();
                break;

            case ParameterList::TableType::SINAI:
                d_table = std::make_shared<const OpenSinai>();
                break;

            case ParameterList::TableType::STADIUM:
                d_table = std::make_shared<const OpenStadium>();
                break;

            case ParameterList::TableType::MUSHROOM:  // 0.5 is the foot height
                d_table = std::make_shared<const OpenMushroom>();
                break;

            default:
                std::cerr << "Error: This shouldn't happen, error in initiating the table.";
                std::cerr << " Maybe you requested an unsupported table for the selected simulation type." << std::endl;
                exit(1);
                break;
        }
    }
    if (plist.default_spawnarea) {
        pair<double> ar = d_table->getMaxCoords();
        plist.spawnarea[0] = 0;
        plist.spawnarea[1] = 0;
        plist.spawnarea[2] = ar.x;
        plist.spawnarea[3] = ar.y;
    }
}

ParallelBilliardFactory::ParallelBilliardFactory(ParameterList& arg_plist) : BilliardFactory(arg_plist) {}

void ParallelBilliardFactory::create_Billiard() {
    if (d_bill) return;
    if (!d_table) create_Table();
    if (!d_rng) create_RNG();
    if (plist.calculate_custom_psos and !d_custom_psos) create_CustomPSOS();
    create_Potential();
    create_Interaction();

    switch (plist.billtype) {
        case ParameterList::BilliardType::BILLIARD:
            d_bill = std::make_unique<Billiard>(d_table, std::move(d_rng), plist, nullptr, std::move(d_custom_psos));

            if (plist.output_level >= 2)
                std::cout << "Using non-interacting billiard without magneticfield" << std::endl;
            break;

        case ParameterList::BilliardType::MAGNETICBILLIARD:
            d_bill =
                std::make_unique<MagneticBilliard>(d_table, std::move(d_rng), plist, nullptr, std::move(d_custom_psos));

            if (plist.output_level >= 2) std::cout << "Using non-interacting billiard with magneticfield" << std::endl;
            break;

        case ParameterList::BilliardType::INTERACTINGBILLIARD:
            d_bill = std::make_unique<InteractingBilliard>(
                d_interaction, d_potential, d_table, std::move(d_rng), plist, nullptr, std::move(d_custom_psos)
            );

            if (plist.output_level >= 2) std::cout << "Using interacting billiard without magneticfield" << std::endl;
            break;

        case ParameterList::BilliardType::INTERACTINGMAGNETICBILLIARD:
            d_bill = std::make_unique<InteractingMagneticBilliard>(
                d_interaction, d_potential, d_table, std::move(d_rng), plist, nullptr, std::move(d_custom_psos)
            );

            if (plist.output_level >= 2) std::cout << "Using interacting billiard with magneticfield" << std::endl;
            break;
    }

    assert(!d_custom_psos);
    assert(!d_rng);
}

}  // namespace bill2d
