/*
 * datasetinfo.hpp
 *
 * This file is part of bill2d.
 *
 * A class (and supporting classes) which is used to handle all output to
 * HDF5-files
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DATASETINFO_HPP_
#define _DATASETINFO_HPP_

#include <H5Cpp.h>
#include <memory>

using namespace H5;

namespace bill2d {

/*!
 * Container for dataset writing with all the info needed to write data to file:
 * DataSet, DataSpaces, PropList, dimensions, type, ranks
 */
struct DatasetInfo {
    // Constructors and destructors
    /*!
       Constructor
       \param file Reference to the HDF5-file instance
       \param name Name of the dataset in file
       \param rank Dimensions of the dataset
       \param rank_mem Dimensions of the data in simulation buffer
       \param arg_dims Dimensions of the dataset in file
       \param arg_maxdims Maximum dimensions of the dataset in file (set to H5S_UNLIMITED if unlimited in some
       dimension, i.e. we want possibility to expand the dataset in future)
       \param arg_memdims Dimensions of the data in simulation buffer
       \param arg_type Type of the data
       \param arg_chuncked If saved in chuncks rather than everything at once. No harm in setting to true.
       \param arg_compression Compresses the data slightly, no significant effect on the simulation time, set to
       true.
     */
    DatasetInfo(
        H5File& file, std::string name, unsigned rank, unsigned rank_mem, const hsize_t arg_dims[],
        const hsize_t arg_maxdims[], const hsize_t arg_memdims[], DataType arg_type, bool arg_chuncked = true,
        bool arg_compression = true
    );

    //! Constructor in case we do not wish to set the member variables
    // DatasetInfo() {};

    //! Copy constructor
    DatasetInfo(const DatasetInfo& copythis);

    // HDF5-objects
    //! Dataset
    DataSet dset;

    //! Dataspace on file
    DataSpace fspace;

    //! Dataspace in memory
    DataSpace memspace;

    //! Property list
    DSetCreatPropList plist;

    //! Datatype for both buffer and file
    DataType type;

    //! Offset for the next save
    std::unique_ptr<hsize_t[]> offset;

    //! Size of chunck
    std::unique_ptr<hsize_t[]> chuncksize;

    //! Dimensions on file
    std::unique_ptr<hsize_t[]> dims;

    //! Dimensions on memory buffer
    std::unique_ptr<hsize_t[]> memdims;

    //! Maximum dimensions on file
    std::unique_ptr<hsize_t[]> maxdims;

    //! Number of dimensions on file
    unsigned rank;

    //! Number of dimensions on memory
    unsigned rank_mem;

    //! Chuncked (true/false)
    bool chuncked;

    //! Compression (true/false)
    bool compression;

    //! Pointer to the HDF5-file
    H5File& d_file;

    //! Name of the dataset
    std::string d_name;

    bool initialized;
};

}  // namespace bill2d

#endif  // _DATASETINFO_HPP_
