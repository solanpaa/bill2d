
/*
 * datatypes.hpp
 *
 * This file is part of bill2d.
 *
 * External potentials are defined in this file.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file multithread_tools.hpp
 * \brief A few thread-safe containers are defined here
 */

#ifndef _MULTITHREAD_TOOLS_HPP_
#define _MULTITHREAD_TOOLS_HPP_

#include <memory>
#include <mutex>
#include <queue>
#include <vector>

namespace bill2d {

//! Container for thead-safe pushing and accessing
template <class T>
class data_container {
public:
    data_container(std::size_t length) : _mu() { _values.reserve(length); }
    data_container() : _mu() {}

    T* data() { return _values.data(); }  // Call this only if you do not call other methods afterwards

    void push_back(const T value) {
        std::lock_guard<std::mutex> lock(_mu);
        _values.push_back(value);
    }

    T at(std::size_t pos) {
        std::lock_guard<std::mutex> lock(_mu);
        return _values.at(pos);
    }

    void clear() {
        std::lock_guard<std::mutex> lock(_mu);
        _values.clear();
    }

    std::size_t size() {
        std::lock_guard<std::mutex> lock(_mu);
        return _values.size();
    }

    template <class InputIt>
    void insert(InputIt begin, InputIt end) {
        std::lock_guard<std::mutex> lock(_mu);
        _values.insert(_values.end(), begin, end);
    }

    std::vector<T>& get_data() { return _values; }

private:
    std::vector<T> _values;
    std::mutex _mu;
};

//! Queue for ensemble of std::unique_ptr<T>.
//! Thread-safe access
template <class T>
class concurrent_uptr_queue {
public:
    std::unique_ptr<T> pop() {
        std::lock_guard<std::mutex> lock(_mu);
        std::unique_ptr<T> elem;
        if (!q.empty()) {
            elem = std::move(q.front());
            q.pop();
        }
        return std::move(elem);
    }

    void push(std::unique_ptr<T> new_elem) {
        std::lock_guard<std::mutex> lock(_mu);
        q.push(std::move(new_elem));
    }

    void swap(concurrent_uptr_queue<T>& q2) { q.swap(q2.q); }

private:
    std::queue<std::unique_ptr<T>> q;
    std::mutex _mu;
};

}  // namespace bill2d
#endif
