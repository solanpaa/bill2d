/*
 * bill2d_parallel_main.cpp
 *
 * This file is part of bill2d.
 *
 * A source for executable that runs simulations in parallel
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file bill2d_parallel_main.cpp
 * \brief Option parsing etc. for the binary 'bill2d_parallel'.
 */

#include "executables.hpp"
#include "parser.hpp"
#include "signal_handler.hpp"

#include <signal.h>

using namespace bill2d;

int main(int argc, char** argv) {
    // Setup signal handlers for (potentially) graceful shutdown
    struct sigaction act;
    act.sa_handler = sig_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGINT, &act, nullptr);
    sigaction(SIGTERM, &act, nullptr);

    Parser parser(argc, argv);

    // Exit if help is shown
    if (parser.parse()) {
        return EXIT_SUCCESS;
    }

    ParameterList& plist = parser.getParameterList();

    // Some of the same checks as in bill2d_diffusion_coefficient
    // TODO: Move these to their own function

    // If option 'periodic' not supplied
    if (plist.soft_lorentz_gas && !plist.periodic) {
        std::cerr << "Specify periodicity (--periodic) and unit cell (--unit-cell) with Soft Lorentz gas" << std::endl;
        return 1;
    }

    // If calculating soft Lorentz gas, there should be no hard tables
    if ((plist.soft_lorentz_gas) and plist.tabletype != ParameterList::TableType::NOTABLE) {
        std::cerr << "Use '--table 0' with soft lorentz gas" << std::endl;
        return 1;
    }

    // If calculating soft lorentz-gas, one should use interacting billiard (or interacting magnetic billiard)
    if ((plist.soft_lorentz_gas) and (plist.billtype == ParameterList::BilliardType::BILLIARD or
                                      plist.billtype == ParameterList::BilliardType::MAGNETICBILLIARD)) {
        std::cerr << "Use billtype 3 or 4 with soft lorentz gas" << std::endl;
        return 1;
    }

    // The table needs to be either NOTABLE or LORENTZGAS
    if (plist.tabletype != ParameterList::TableType::NOTABLE and
        plist.tabletype != ParameterList::TableType::LORENTZGAS) {
        std::cerr << "Wrong table type." << std::endl;
        return 1;
    }

    // Only single-particle simulations allowed
    if (plist.num_particles > 1) {
        std::cerr << "Only single-particle simulations allowed." << std::endl;
        return 1;
    }

    plist.save_particlenum = false;

    std::cout << "Saving variables to " << plist.savefilepath << std::endl;

    parallel_main(plist);

    return EXIT_SUCCESS;
}
