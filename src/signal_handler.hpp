/*
 * signal_handler.hpp
 *
 * This file is part of bill2d.
 *
 * A source for the main executable that calculates a single billiard simulation
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

#ifndef _SIGNAL_HANDLER_HPP_
#define _SIGNAL_HANDLER_HPP_

#include "bill2d_config.h"

namespace bill2d {
//! Handles the keyboard signals (SIGINT, SIGTERM, etc.) from user
void sig_handler_func(int sig);

void sig_handler(int sig);

}  // namespace bill2d

#endif
