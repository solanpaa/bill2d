/*
 * helperfunctions.cpp
 *
 * This file is part of bill2d.
 *
 * The functions and classes implemented in this file
 * are little things that make life easier.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 *
 */

/*! \file helperfunctions.cpp
 * \brief Implementation of the convenience functions.
 */

#include "helperfunctions.hpp"
#include "exceptions.hpp"

#include <boost/filesystem.hpp>

#include <fstream>
#include <iostream>
#include <sstream>

using H5::ArrayType;
using H5::DataSet;
using H5::DataSpace;
using H5::FileIException;
using H5::Group;
using H5::H5File;
using std::cout;
using std::endl;
using std::ifstream;
using std::ios;
using std::istringstream;
using std::ofstream;
using std::string;
using std::stringstream;
using std::vector;
namespace bill2d {

// Read initial positions from hdf5-file
void hdf52vector2d_inpos(
    std::string filename, size_t num_particles, std::vector<std::vector<double>>& myvector, enum Position pos
) {
    try {
        if (not FileExists(filename)) throw FileDoesNotExist(filename, "reading initial positions.");
        const hsize_t pair_dims[] = {2};
        ArrayType pair(H5::PredType::NATIVE_DOUBLE, 1, pair_dims);
        // Open file&groups
        H5File file(filename, H5F_ACC_RDONLY);
        Group traj = file.openGroup("/trajectories");
        Group vel = file.openGroup("/velocities");

        hsize_t traj_dims[1];  // 0 - pisteiden määr
        hsize_t vel_dims[1];
        DataSpace traj_space, vel_space;
        DataSet traj_set;
        DataSet vel_set;
        stringstream name;
        auto traj_buf = new double[2];
        auto vel_buf = new double[2];
        std::vector<double> particle;

        // For all particles, read the positions and velocities
        for (unsigned i = 0; i < num_particles; i++) {
            particle.clear();
            name.str("");
            name << "particle" << i + 1;
            traj_set = traj.openDataSet(name.str().c_str());
            vel_set = vel.openDataSet(name.str().c_str());
            traj_space = traj_set.getSpace();
            vel_space = vel_set.getSpace();
            traj_space.getSimpleExtentDims(traj_dims, nullptr);
            vel_space.getSimpleExtentDims(vel_dims, nullptr);
            hsize_t mdims[] = {1};
            DataSpace memspace(1, mdims);

            hsize_t count[1] = {1};
            hsize_t offset[1];
            switch (pos) {
                case FIRST:
                    offset[0] = 0;
                    break;

                case LAST:
                    offset[0] = traj_dims[0] - 1;
                    break;
            }
            traj_space.selectHyperslab(H5S_SELECT_SET, count, offset);
            vel_space.selectHyperslab(H5S_SELECT_SET, count, offset);
            traj_set.read(traj_buf, pair, memspace, traj_space);
            vel_set.read(vel_buf, pair, memspace, vel_space);
            // Push particle's initial state to myvector
            particle.push_back(traj_buf[0]);
            particle.push_back(traj_buf[1]);
            particle.push_back(vel_buf[0]);
            particle.push_back(vel_buf[1]);
            myvector.push_back(particle);
        }
        delete[] traj_buf;
        delete[] vel_buf;
    } catch (FileIException&) {
        std::cout << "Error: Cannot read initial positions from '" << filename
                  << "'. Did you remember to save the velocities?" << std::endl;
        exit(1);
    } catch (FileDoesNotExist& e) {
        std::cout << e.what() << std::endl;
        exit(1);
    }
}

std::vector<double> hdf5_1darray_to_vector(std::string filename, std::string dataset_name) {
    try {
        if (not FileExists(filename)) throw FileDoesNotExist(filename, "reading a custom dataset.");
        // Open file&groups
        H5File file(filename, H5F_ACC_RDONLY);
        Group root = file.openGroup("/");

        hsize_t dims[1];  // 0 - pisteiden määr
        DataSpace dspace;
        DataSet dset;
        stringstream name;

        dset = root.openDataSet(dataset_name.c_str());
        dspace = dset.getSpace();
        dspace.getSimpleExtentDims(dims, nullptr);
        dspace.selectAll();

        std::vector<double> output(dims[0]);

        hsize_t mdims[] = {dims[0]};
        DataSpace memspace(1, mdims);

        dset.read(output.data(), H5::PredType::NATIVE_DOUBLE, memspace, dspace);
        return output;
    } catch (FileIException&) {
        std::cout << "Error: Cannot read dataset '" << dataset_name << "' from '" << filename << "'" << std::endl;
        exit(1);
    } catch (FileDoesNotExist& e) {
        std::cout << e.what() << std::endl;
        exit(1);
    }
}

std::vector<std::string> split(std::string str, std::string delim) {
    std::vector<std::string> fields;
    std::size_t i0 = 0, i1;

    do {
        i1 = str.find_first_of(delim, i0);
        if (i1 != std::string::npos) {
            fields.push_back(str.substr(i0, i1 - i0));
            i0 = i1 + 1;
        }
    } while (i1 != std::string::npos);

    fields.push_back(str.substr(i0, i1 - i0));
    return fields;
}

std::string strip_trailing_whitespace(std::string str) {
    for (auto it = str.rbegin(); it < str.rend(); it++) {
        if (*it != ' ')
            break;
        else
            str.erase(std::next(it).base());  // according to
        // http://stackoverflow.com/a/1830240/738107
    }
    return str;
}

// Read an ASCII array to vector
void file2vector2d(std::string filename, std::vector<std::vector<double>>& myvector) {
    if (not FileExists(filename)) throw FileDoesNotExist(filename, "reading 2d vector from file.");
    ifstream inpf(filename);
    std::string srow;
    std::vector<double> rowvec;

    while (std::getline(inpf, srow)) {
        rowvec.clear();
        srow = strip_trailing_whitespace(srow);

        try {
            auto elems = split(srow, " ");
            for (auto& elem : elems) rowvec.push_back(std::stod(elem));
        } catch (std::istringstream::failure& e) {  // Catch interpretation errors
            std::cerr << "Error reading ASCII array." << std::endl;
            exit(1);
        }

        myvector.push_back(rowvec);  // If one particle data succesfully extracted, save it to 'myvector'
    }
}

bool FileExists(std::string strFilename) { return boost::filesystem::exists(strFilename); }

// Check if lock-file exists
bool lockExists(std::string filename) {
    string lockfile(filename);
    lockfile.append(".lock");
    return FileExists(lockfile);
}

// Create lock-file
void createLock(std::string filename) {
    string lockfile(".");
    lockfile.append(filename).append(".lock");
    boost::filesystem::path path(lockfile);
    while (not boost::filesystem::create_directory(lockfile)) sleep(5);
}

// Remove lock-file
void rmLock(std::string filename) {
    string lockfile(".");
    lockfile.append(filename).append(".lock");
    boost::filesystem::path path(lockfile);
    boost::filesystem::remove(path);
}

void rm_file(std::string filename) {
    boost::filesystem::path path(filename);
    boost::filesystem::remove(path);
}

// Save 2d vector to file (append)
void vector2d2file(std::string filename, const std::vector<std::vector<double>>& myArray) {
    // Make sure that the path exists
    boost::filesystem::path path(filename);
    if (path.parent_path() != "") boost::filesystem::create_directories(path.parent_path());
    string lockfile(filename);
    lockfile.append(".lock");
    while (FileExists(lockfile)) {
        sleep(5);
    }
    createLock(filename);
    ofstream out(filename.c_str(), ios::app);
    for (const auto& l : myArray) {
        for (const auto& p : l) {
            out << p << " ";
        }
        out << endl;
    }
    out.close();
    rmLock(filename);
}

// Append 1d vector to file
void vector2file(std::string filename, const std::vector<double>& myArray) {
    // cout << "Writing array to a file.."<<endl;
    // Make sure that the path exists
    boost::filesystem::path path(filename);
    if (path.parent_path() != "") boost::filesystem::create_directories(path.parent_path());

    string lockfile(filename);
    lockfile.append(".lock");
    while (FileExists(lockfile)) {
        sleep(5);
    }
    createLock(filename);
    ofstream out(filename.c_str(), ios::app);
    for (auto l = myArray.begin(); l < myArray.end(); l++) {
        out << *l << " ";
    }

    out << endl;

    out.close();
    rmLock(filename);
}

unsigned round_up(unsigned value, unsigned multiple) {
    if (multiple == 0) return value;
    unsigned modulus = value % multiple;
    if (modulus == 0) return value;

    value += multiple - modulus;
    // assert(value%multiple==0);
    return value;
}
#if defined(HAVE_ALIGNED_ALLOC) || defined(HAVE_POSIX_MEMALIGN)
void* bill2d_aligned_alloc(size_t alignment, size_t size) {
#if defined(HAVE_ALIGNED_ALLOC)
    void* ret = aligned_alloc(alignment, size);
    return ret;
#elif defined(HAVE_POSIX_MEMALIGN)
    void* ret;
    posix_memalign(&ret, alignment, size);
    return ret;
#else
    static_assert(
        false, "Needs either aligned_alloc or posix_memalign. Disable vectorization if you do not have these."
    );
    return nullptr;
#endif
}
#endif
}  // namespace bill2d
