/*
 * bill2d_diffusion_coefficient.cpp
 *
 * This file is part of bill2d.
 *
 * A source for executable that calculates diffusion coefficients
 * for periodic systems
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file bill2d_diffusion_coefficient_main.cpp
 * \brief Option parsing etc. for the binary 'bill2d_diffusion'.
 */
#include "executables.hpp"
#include "parser.hpp"
#include "signal_handler.hpp"

#include <signal.h>

#include <iostream>

using namespace bill2d;

int main(int argc, char** argv) {
    // Signal handler
    struct sigaction act;
    act.sa_handler = sig_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGINT, &act, nullptr);

    // Parse command line & config file
    Parser parser(argc, argv);
    bool help = parser.parse();

    // If help shown, exit
    if (help) {
        return 0;
    }

    // Now let's get the supplied arguments/parameters and check that they are ok
    ParameterList& plist = parser.getParameterList();

    // If option 'periodic' not supplied
    if (not plist.periodic) {
        std::cerr << "Specify periodicity (--periodic) and unit cell (--unit-cell)" << std::endl;
        return 1;
    }

    // If not using any of the Lorentz-gas systems (hard, soft or soft old)
    if (((not plist.soft_lorentz_gas) and (not plist.soft_lieb_gas)) and
        plist.tabletype != ParameterList::TableType::LORENTZGAS) {
        std::cerr << "Specify hard or soft Lorentz gas ('--soft-lorentz-gas' with '--table 0' or '--table 10' with "
                     "'--geometry a 0')"
                  << std::endl;
        return 1;
    }

    // If calculating soft Lorentz gas, there should be no hard tables
    if ((plist.soft_lorentz_gas) and plist.tabletype != ParameterList::TableType::NOTABLE) {
        std::cerr << "Use '--table 0' with soft lorentz gas" << std::endl;
        return 1;
    }

    // If calculating soft lorentz-gas, one should use interacting billiard (or interacting magnetic billiard)
    if ((plist.soft_lorentz_gas) and (plist.billtype == ParameterList::BilliardType::BILLIARD or
                                      plist.billtype == ParameterList::BilliardType::MAGNETICBILLIARD)) {
        std::cerr << "Use billtype 3 or 4 with soft lorentz gas" << std::endl;
        return 1;
    }

    // The table needs to be either NOTABLE or LORENTZGAS
    if (plist.tabletype != ParameterList::TableType::NOTABLE and
        plist.tabletype != ParameterList::TableType::LORENTZGAS) {
        std::cerr << "Wrong table type." << std::endl;
        return 1;
    }

    // Only single-particle simulations allowed
    if (plist.num_particles > 1) {
        std::cerr << "Only single-particle simulations allowed." << std::endl;
        return 1;
    }

    // Set particle buffers off; these are not saved in diffusion calculations anyway
    plist.save_bouncemap = false;
    plist.save_energies = false;
    plist.save_full_trajectory = false;
    plist.save_histograms = false;
    plist.save_particlenum = false;
    plist.save_velocity = false;
    plist.save_nothing = true;

    std::cout << "#Calculating the diffusion coefficient" << std::endl;

    // Run the diffusion calculation
    diffusion_main(plist);

    return 0;
}
