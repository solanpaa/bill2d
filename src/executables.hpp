/*
 * executables.hpp
 *
 * This file is part of bill2d.
 *
 * Definitions for the main simulation routines and signal handlers
 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

#ifndef SIMULATION_frames_HPP
#define SIMULATION_frames_HPP

#include "bill2d_config.h"
#include "billiard.hpp"
#include "particle.hpp"
#include "rng.hpp"
#include "table.hpp"

#include <boost/filesystem.hpp>
#include <gsl/gsl_fit.h>
#ifdef _OPENMP
#include <omp.h>
#endif

#include <memory>

namespace bill2d {

/*
 * Functions which calculate different kinds of simulations: normal, escape, and diffusion
 */

//! Single simulation
void single_simulation(ParameterList& plist);

//! Escape simulations
void escape_main(ParameterList& plist);
std::pair<double, double> single_escape(std::unique_ptr<Billiard> bill);

//! Diffusion coefficient
void diffusion_main(ParameterList& plist);
double single_run_diff_coeff(Billiard* bill);

//! Phase space regularity (search)
void phase_space_regularity_main(ParameterList& plist);
double target(const std::vector<double>& x, std::vector<double>&, void* params);  // This will be optimized
// Returns dist, s0, v_parallel_0, theta_0, x0, y0, vx0, vy0, ...
std::vector<double> phase_space_point_minimum_distance_to_initial_state(
    std::unique_ptr<Billiard> bill,
    std::function<bool(
        particleset&, const RNG& rng, const Table& table, std::function<double()> epot, std::function<double()> ekin
    )>
        randomizer,
    const double energy, const double max_t
);

//! Phase space regularity (search, just for finding possible periodic orbits)
void phase_space_regularity_checker_main(ParameterList& plist);
double target_checker(const std::vector<double>& x, std::vector<double>&, void* params);  // This will be optimized
// Returns dist, s0, v_parallel_0, theta_0, x0, y0, vx0, vy0, ...
std::vector<std::vector<double>> phase_space_trajectory_minimum_distances_within_PSOS(
    std::unique_ptr<Billiard> bill,
    std::function<bool(
        particleset&, const RNG& rng, const Table& table, std::function<double()> epot, std::function<double()> ekin
    )>
        randomizer,
    const double energy, const double max_t
);

//! Parallel version
void parallel_main(ParameterList& plist);

}  // namespace bill2d

#endif
