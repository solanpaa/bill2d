/*
 * parameterlist.hpp
 *
 * This file is part of bill2d.
 *
 * A class that contains all parameters that can be given for the program.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file exceptions.hpp
 * \brief A few custom exceptions are defined here.
 */

#ifndef _EXCEPTIONS_HPP_
#define _EXCEPTIONS_HPP_

#include <stdexcept>
#include <string>

namespace bill2d {

//! An error class (runtime error) for case when a requested member does not exist
class NoSuchMember : public std::runtime_error {
public:
    NoSuchMember() : std::runtime_error("No such member"){};
};

//! Exception class in case a file does not exist
class FileDoesNotExist : public std::exception {
public:
    FileDoesNotExist(const std::string file, const std::string whatdoing) {
        message =
            std::string("Error: While ").append(whatdoing).append(": File '").append(file).append("' does not exist.");
    }
    const char* what() const noexcept override { return message.c_str(); }

protected:
    std::string message;
};

class SignalFinish : public std::exception {
public:
    SignalFinish() {}

    const char* what() const noexcept override { return "Exiting as ordered.."; }

protected:
    std::string signame;
};

}  // namespace bill2d
#endif
