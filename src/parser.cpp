/*
 * parser.cpp
 *
 * This file is part of bill2d.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file parser.cpp
 * \brief The parser class is implemented here.
 */

#include "parser.hpp"
#include "bill2d_config.h"

#include <fstream>
#include <iostream>

namespace bill2d {

// Constructor
Parser::Parser(int argc, char** argv)
    : generic_options("Generic options"),
      cmdline_options("Command line options"),
      config_file_options("Config file options"),
      simulation_options("Simulation options"),
      system_options("BilliardFactory options"),
      d_plist() {
    d_argv = argv;
    d_argc = argc;

    // Start adding options
    // generic=new po::options_description("Generic options");
    generic_options.add_options()("help", "Show the help message")(
        "config", po::value<std::string>(&d_plist.config_path), "Path to the input file"
    );

    // simulation options
    simulation_options.add_options()

            ("propagator", po::value<std::string>(&d_plist.propagator_string),
             "For available propagators, see USERGUIDE")

            ("max-t", po::value<double>(&d_plist.max_t),
             "Maximum simulation time")

            ("min-t", po::value<double>(&d_plist.min_t),
             "Size of memory buffers in units of time")

            ("delta-t", po::value<double>(&d_plist.delta_t),
             "Time step")

            ("max-collisions", po::value<unsigned>(&d_plist.max_collisions),
            "Maximum number of collisions with the boundary")

            ("sparsesave", po::value<unsigned>(&d_plist.spsave),
                                                              "Saves every 'arg'th iteration to file")(
            "histogram-bins", po::value<size_t>(&d_plist.bins), "Number of bins in histograms")(
            "initial-positions", po::value<std::string>(&d_plist.initial_positions_path),
            "Path to initial positions (ASCII or HDF5)")

            ("continue-run", po::value<bool>(&d_plist.continue_run)->default_value(false)->implicit_value(true),
            "Continue the simulation from the hdf5-file given by '--initial-positions'")

            ("ignore-E", po::value<bool>(&d_plist.ignore_energy)->default_value(false)->implicit_value(true),
            "Ignores energy when selecting initial positions. Only affects cases when the initial positions are given "
            "manually.")

            ("perturbate", po::value<bool>(&d_plist.perturbate_initial_positions)->default_value(false)->implicit_value(true),
                         "Perturbates slightly the given initial state")

            ("perturbation-std", po::value<double>(&d_plist.perturbation_std),
            "Standard deviation of the gaussian distribution for the perturbation")

            ("spawnarea", po::value<twopairs<double>>(&d_plist.spawnarea)->multitoken(),
            "Coordiante area for the random initial positions: xmin ymin xmax ymax")

            ("save-trajectory", po::value<bool>(&d_plist.save_full_trajectory)->default_value(true)->implicit_value(true), "Save the trajectory to file")

            ("save-velocity", po::value<bool>(&d_plist.save_velocity)->default_value(false)->implicit_value(true),
            "Save the velocities to file")

            ("save-energies", po::value<bool>(&d_plist.save_energies)->default_value(true)->implicit_value(true),
            "Save the energies to file")

            ("save-histograms", po::value<bool>(&d_plist.save_histograms)->default_value(false)->implicit_value(true),
            "Save the position, velocity and speed histograms to file")

            ("save-particlenum", po::value<bool>(&d_plist.save_particlenum)->default_value(false)->implicit_value(true),
            "Save the number of particles as a function of time to file")

            ("poincare-map", po::value<bool>(&d_plist.save_bouncemap)->default_value(false)->implicit_value(true),
            "Save Poincare map / bouncing map / collision map")

            ("custom-psos", po::value<bool>(&d_plist.calculate_custom_psos)->default_value(false)->implicit_value(true),
            "Calculate crossings with the custom Poincare section a*x+b*y+c = 0")

            ("custom-psos-line", po::value<triplet<double>>(&d_plist.custom_psos_line)->multitoken(),
            "a,b,c for the custom Poincare section a*x+b*y+c = 0")

            ("savepath", po::value<std::string>(&d_plist.savefilepath),
            "Path of the output datafile")

            ("save-nothing", po::value<bool>(&d_plist.save_nothing)->default_value(false)->implicit_value(true),
            "Disables all saving to files (automatically on). Do not use for configuration files.")

            ("output", po::value<unsigned>(&d_plist.output_level),
            "0 - none, 1 - some, 2 - full")

            ("compression", po::value<bool>(&d_plist.compression)->default_value(true)->implicit_value(true),
            "If set, the output datafile will be compressed (default=yes)")

            ("ensemble-size", po::value<unsigned>(&d_plist.simulations),
            "Size of ensemble, e.g. in the calculation of diffusion coefficients")

            ("regularity-analysis-local-optimization-threshold", po::value<double>(&d_plist.regularity_analysis_local_optimization_threshold),
            "Threshold for local optimization in bill2d_regularity_analysis.")

            ("save-individual-dist-sqr", po::value<bool>(&d_plist.individual_data)->default_value(false)->implicit_value(true),
            "bill2d_diffusion_coefficient: Save distance squared of each trajectory to 'savepath' as txt file");

    // system options
    system_options.add_options()("billtype", po::value<unsigned>(),
                                     "Interaction types:\n 1 - Non-interacting particles without magnetic field \n 2 - "
                                     "Non-interacting particles in a magnetic field\n 3 - Interacting particles "
                                     "without magnetic field\n 4 - Interacting particles in a magnetic field \n *   "
                                     "Use 2 or 4 if using a magnetic field\n *   Use 3 or 4 if using external "
                                     "potentials or interparticle interactions")
            ("table", po::value<unsigned>(),
            "Table types:\n 0 - No table\n 1 - Rectangular table\n 2 - Triangular table\n 3 - Circular table\n 4 - "
            "Sinai\n 5 - Ring shaped table\n 6 - Stadium\n 7 - Mushroom\n 8 - Isolated point contact\n 9 - Ellipse\n "
            "10- Lorentz gas, use also '--periodic'")

            ("geometry", po::value<pair<double>>(&d_plist.geometry)->multitoken(),
            "Geometry of the boundary: arg #1 arg#2")

            ("holeratio", po::value<double>(&d_plist.holeratio),
            "The ratio of the hole to the total circumference for open tables.")

            ("periodic", po::value<bool>(&d_plist.periodic)->default_value(false)->implicit_value(true),
            "Activates periodicity with parallellogram unit cell. Use '--unit_cell' to specify the cell.")

            ("unit-cell", po::value<std::vector<double>>()->multitoken(),
            "Parameters for the unit cell: #arg 1 (length of the sides), #arg 2 (length of the vertical side, if "
            "given), #arg3 (angle, if given)")

            ("soft-lorentz-gas", po::value<bool>(&d_plist.soft_lorentz_gas)->default_value(false)->implicit_value(true),
            "Activates soft Lorentz gas.")

	        ("soft-lieb-gas", po::value<bool>(&d_plist.soft_lieb_gas)->default_value(false)->implicit_value(true),
	            "Activates soft Lieb gas.")

            ("soft-lorentz-radius", po::value<double>(&d_plist.soft_lorentz_radius),
            "Radius of the Fermi poles in the Lorentz gas.")

            ("soft-lorentz-sharpness", po::value<double>(&d_plist.soft_lorentz_sharpness),
            "Sharpness of the Fermi poles in the Lorentz gas.")

            ("soft-lorentz-lattice-sum-truncation",
            po::value<unsigned>(&d_plist.soft_lorentz_gas_lattice_sum_truncation),
            "Truncation of the potential lattice sum.")

            ("soft-lorentz-inverted",
             po::value<bool>(&d_plist.soft_lorentz_inverted)->default_value(false)->implicit_value(true),
             "Inverts the Fermi potentials, making them potential wells instead of bumps.")

	        ("soft-lieb-radius", po::value<double>(&d_plist.soft_lieb_radius),
	        "Radius of the Fermi poles in the Lieb gas.")

	     	("soft-lieb-sharpness", po::value<double>(&d_plist.soft_lieb_sharpness),
	        "Sharpness of the Fermi poles in the Lieb gas.")

	        ("soft-lieb-lattice-sum-truncation",
	        po::value<unsigned>(&d_plist.soft_lieb_gas_lattice_sum_truncation),
	        "Truncation of the potential lattice sum.")
			
            ("num-particles", po::value<size_t>(&d_plist.num_particles),
            "Number of particles")

            ("energy", po::value<double>(&d_plist.energy),
            "Total energy")

            ("interaction-strength", po::value<double>(&d_plist.interaction_strength)->default_value(1.0),
            "Strength of the interparticle interaction")

            ("magnetic-field", po::value<double>(&d_plist.B),
            "Magnetic field")

            ("harmonic-potential", po::value<bool>(&d_plist.harmonic_potential)->default_value(false)->implicit_value(true),
            "Activates harmonic potential")

            ("potential-strength", po::value<double>(&d_plist.potential_strength)->default_value(1.0),
            "Strength of the potential")

            ("bias-voltage", po::value<bool>(&d_plist.bias_voltage)->default_value(false)->implicit_value(true),
            "Activates bias voltage (not implemented)")

            ("bias-strength", po::value<double>(&d_plist.bias_str),
            "Strength of the bias voltage. NOT IMPLEMENTED.")

            ("bias-direction", po::value<pair<double>>(&d_plist.bias_direction)->multitoken(),
            "Direction of the bias voltage: x y. NOT IMPLEMENTED")

            ("centerpoint", po::value<pair<double>>(&d_plist.centerpoint)->multitoken(),
            "Centerpoint of the system for certain potentials: x y")

            ("soft-stadium-potential", po::value<bool>(&d_plist.soft_stadium_potential)->default_value(false)->implicit_value(true),
            "Activates soft stadium potential")

            ("soft-stadium-power", po::value<double>(&d_plist.soft_stadium_power),
            "Sharpness of the stadium potential. Larger = sharper")

            ("gaussian-bumps-manual", po::value<bool>(&d_plist.gaussian_bumps)->default_value(false)->implicit_value(true),
            "Activates gaussian bumps, i.e., noise to the potential")

            ("gaussian-bumps-fast-from-itp2d", po::value<bool>(&d_plist.gaussian_bumps_fast_itp2d)->default_value(false)->implicit_value(true),
            "Activates gaussian bumps, i.e., noise to the potential. Uses only the itp2d bumps. Vectorized calculation.")

            ("gaussian-bumps-manual-locations", po::value<std::vector<double>>()->multitoken(),
            "Manually set locations (x,y) of the gaussian bumps.")

            ("gaussian-bumps-manual-widths", po::value<std::vector<double>>()->multitoken(),
            "Manually set widths of the gaussian bumps.")

            ("gaussian-bumps-manual-amplitudes", po::value<std::vector<double>>()->multitoken(),
            "Manually set amplitudes of the gaussian bumps.")

            ("gaussian-bumps-itp2d-file",  po::value<std::string>(&d_plist.gaussian_bumps_itp2d_file),
            "itp2d-file to read the bump data from")

            ("gaussian-bath", po::value<bool>(&d_plist.gaussian_bath)->default_value(false)->implicit_value(true),
            "Activates gaussian heat bath (random forces). Experimental. Not rigorously implemented.")

            ("temperature", po::value<double>(&d_plist.temperature),
            "Temperature for the gaussian heat bath")

            ("soft-circle", po::value<bool>(&d_plist.soft_circle)->default_value(false)->implicit_value(true),
             "Activates the soft circular billiard, defined by either an inverted Fermi pole or through the error function (erf). "
             "Use --soft-circle-type to choose the potential function.")

            ("soft-circle-type", po::value<unsigned int>(), "Selects the type of the soft circular potential.\n"
                                                            " 0 - Inverted Fermi pole\n"
                                                            " 1 - Error function")

            ("soft-circle-softness", po::value<double>(&d_plist.soft_circle_softness),
             "Defines the softness (Fermi pole) or hardness (error function) parameter for the soft circular billiard.")

            ("soft-circle-eccentricity", po::value<double>(&d_plist.soft_circle_eccentricity),
             "Defines the eccentricity of the soft circular potential. Has an effect only with the error function type of potential.");

    cmdline_options.add(generic_options);
    cmdline_options.add(system_options);
    cmdline_options.add(simulation_options);

    config_file_options.add(generic_options);
    config_file_options.add(system_options);
    config_file_options.add(simulation_options);
}

// Parsing function. Returns true if help was called, false if not.
bool Parser::parse() {
    try {
        // Parse first cmd-line and if config-file given, parse that too
        using namespace po::command_line_style;
        po::store(
            po::parse_command_line(
                d_argc,
                d_argv,
                cmdline_options,
                po::command_line_style::unix_style ^ po::command_line_style::allow_short
            ),
            vm
        );

        std::ifstream infile;

        // auto v = vm["test"].as<std::pair<double,double>>();
        // std::cout << std::get<0>(v) << std::get<1>(v) << std::endl;

        if (vm.count("config")) {
            if (!FileExists(vm["config"].as<std::string>()))  // Throw error if config file doesn't exist
                throw FileDoesNotExist(vm["config"].as<std::string>(), "reading config file");
            infile.open(vm["config"].as<std::string>().c_str(), std::ifstream::in);
            po::store(po::parse_config_file(infile, config_file_options), vm);
            infile.close();
        }

        po::notify(vm);

        // Show help
        if (vm.count("help")) {
            std::cout << "Bill2d";
            if (!std::string(GITHASH).empty())
                std::cout << " (from the git commit " << GITHASH << ")" << std::endl;
            else
                std::cout << std::endl;
            std::cout << "Compilation flags: " << COMPILER_FLAGS << std::endl << std::endl;

            std::cout << cmdline_options << "\n";
            return true;
        }

        // Table
        if (vm.count("table")) {
            switch (vm["table"].as<unsigned>()) {
                case 0:
                    d_plist.tabletype = ParameterList::TableType::NOTABLE;
                    break;

                case 1:
                    d_plist.tabletype = ParameterList::TableType::RECTANGLE;
                    break;

                case 2:
                    d_plist.tabletype = ParameterList::TableType::TRIANGLE;
                    break;

                case 3:
                    d_plist.tabletype = ParameterList::TableType::CIRCLE;
                    break;

                case 4:
                    d_plist.tabletype = ParameterList::TableType::SINAI;
                    break;

                case 5:
                    d_plist.tabletype = ParameterList::TableType::RING;
                    break;

                case 6:
                    d_plist.tabletype = ParameterList::TableType::STADIUM;
                    break;

                case 7:
                    d_plist.tabletype = ParameterList::TableType::MUSHROOM;
                    break;

                case 8:
                    d_plist.tabletype = ParameterList::TableType::GATE;
                    break;

                case 9:
                    d_plist.tabletype = ParameterList::TableType::ELLIPSE;
                    break;

                case 10:
                    d_plist.tabletype = ParameterList::TableType::LORENTZGAS;
                    break;

                default:
                    po::invalid_option_value e(std::string(std::to_string(vm["table"].as<unsigned>())));
                    e.set_option_name("table");
                    throw e;
            }
        }

        if (vm.count("spawnarea")) d_plist.default_spawnarea = false;

        if (vm.count("geometry") or vm.count("unit-cell") or vm.count("holeratio")) d_plist.default_geometry = false;

        // Check system type etc.
        if (vm.count("billtype")) {
            switch (vm["billtype"].as<unsigned>()) {
                case 1:
                    d_plist.billtype = ParameterList::BilliardType::BILLIARD;
                    break;

                case 2:
                    d_plist.billtype = ParameterList::BilliardType::MAGNETICBILLIARD;
                    break;

                case 3:
                    d_plist.billtype = ParameterList::BilliardType::INTERACTINGBILLIARD;
                    break;

                case 4:
                    d_plist.billtype = ParameterList::BilliardType::INTERACTINGMAGNETICBILLIARD;
                    break;

                default:
                    po::invalid_option_value e(std::string(std::to_string(vm["billtype"].as<unsigned>())));
                    e.set_option_name("billtype");
                    throw e;
            }
        }

        if (vm["perturbate"].as<bool>() && not(vm.count("initial-positions"))) {
            po::required_option e("initial-positions");
            throw e;
        }

        // In case of certaing billiardtypes, put certain parameters to zero
        if (d_plist.billtype == ParameterList::BilliardType::BILLIARD or
            d_plist.billtype == ParameterList::BilliardType::INTERACTINGBILLIARD)
            d_plist.B = 0;
        if (d_plist.billtype == ParameterList::BilliardType::BILLIARD or
            d_plist.billtype == ParameterList::BilliardType::MAGNETICBILLIARD) {
            d_plist.potential_strength = 0;
            d_plist.interaction_strength = 0;
        }
        if (not(d_plist.billtype == ParameterList::BilliardType::INTERACTINGBILLIARD or
                d_plist.billtype == ParameterList::BilliardType::INTERACTINGMAGNETICBILLIARD)) {
            d_plist.interactiontype = ParameterList::InteractionType::NONE;

        } else {
            if ((d_plist.interaction_strength == 0) or (d_plist.num_particles < 2))
                d_plist.interactiontype = ParameterList::InteractionType::NONE;
            else {
                d_plist.interactiontype = ParameterList::InteractionType::COULOMB;
            }
        }

        if ((vm["soft-lorentz-gas"].as<bool>() or vm["soft-lieb-gas"].as<bool>() or
             (vm.count("table") and vm["table"].as<unsigned>() == 10)) and
            not vm["periodic"].as<bool>())
            throw ConflictingArguments("Using periodic potential/table but not periodicity specified.");

        if (vm["soft-circle"].as<bool>()) {
            if (vm.count("soft-circle-type")) {
                switch (vm["soft-circle-type"].as<unsigned int>()) {
                    case 0:
                        d_plist.soft_circle_type = ParameterList::SoftCircleType::FERMI_POLE;
                        break;
                    case 1:
                        d_plist.soft_circle_type = ParameterList::SoftCircleType::ERROR_FUNCTION;
                        break;
                    default:
                        po::invalid_option_value e(std::string(std::to_string(vm["soft-circle-type"].as<unsigned>())));
                        e.set_option_name("soft-circle-type");
                        throw e;
                }
            }
        }

        if (vm["periodic"].as<bool>() and (d_plist.interactiontype != ParameterList::InteractionType::NONE)) {
            throw ConflictingArguments("Use simulations WITHOUT particle-particle interactions with 'periodic'");
        }

        std::vector<double> tmp;
        if (vm.count("unit-cell")) {
            tmp = vm["unit-cell"].as<std::vector<double>>();
            switch (tmp.size()) {
                case 1:
                    d_plist.uc_params[0] = M_PI / 3.;
                    d_plist.uc_params[1] = tmp[0];
                    d_plist.uc_params[2] = tmp[0];
                    break;

                case 2:
                    d_plist.uc_params[0] = M_PI / 3.;
                    d_plist.uc_params[1] = tmp[0];
                    d_plist.uc_params[2] = tmp[1];
                    break;

                case 3:
                    d_plist.uc_params[0] = tmp[2];
                    d_plist.uc_params[1] = tmp[0];
                    d_plist.uc_params[2] = tmp[1];
                    break;

                default:
                    po::invalid_option_value e("Wrong number of arguments");
                    e.set_option_name("unit-cell");
                    throw e;
                    break;
            }
        }

        // Handle propagator

        if (vm.count("propagator")) {
            if (vm["propagator"].as<std::string>() == "2") {
                d_plist.propagator = ParameterList::PropagatorType::SECOND;
                d_plist.propagator_string = "velocity Verlet, 2nd order";
            } else if (vm["propagator"].as<std::string>() == "2opt") {
                d_plist.propagator = ParameterList::PropagatorType::SECOND_OPTIMAL;
                d_plist.propagator_string = "2nd order optimal";
            } else if (vm["propagator"].as<std::string>() == "3") {
                d_plist.propagator = ParameterList::PropagatorType::THIRD;
                d_plist.propagator_string = "3rd order";
            } else if (vm["propagator"].as<std::string>() == "4") {
                d_plist.propagator = ParameterList::PropagatorType::FOURTH;
                d_plist.propagator_string = "4th order";
            } else if (vm["propagator"].as<std::string>() == "4opt") {
                d_plist.propagator = ParameterList::PropagatorType::FOURTH_OPTIMAL;
                d_plist.propagator_string = "4th order optimal";
            } else if (vm["propagator"].as<std::string>() == "6opt") {
                d_plist.propagator = ParameterList::PropagatorType::SIXTH;
                d_plist.propagator_string = "6th order";
            } else if (vm["propagator"].as<std::string>() == "boris") {
                d_plist.propagator = ParameterList::PropagatorType::BORIS;
                d_plist.propagator_string = "Boris";
            } else if (vm["propagator"].as<std::string>() == "yhe2") {
                d_plist.propagator = ParameterList::PropagatorType::YHE2;
                d_plist.propagator_string = "YHe2";
            } else if (vm["propagator"].as<std::string>() == "prk4") {
                d_plist.propagator = ParameterList::PropagatorType::PRK4;
                d_plist.propagator_string = "PRK4";
            } else if (vm["propagator"].as<std::string>() == "prk6") {
                d_plist.propagator = ParameterList::PropagatorType::PRK6;
                d_plist.propagator_string = "PRK6";
            } else if (vm["propagator"].as<std::string>() == "chin_2b") {
                d_plist.propagator = ParameterList::PropagatorType::CHIN_2B;
                d_plist.propagator_string = "Chin_2b";
            } else if (vm["propagator"].as<std::string>() == "chin_2a") {
                d_plist.propagator = ParameterList::PropagatorType::CHIN_2A;
                d_plist.propagator_string = "Chin_2a";
            } else if (vm["propagator"].as<std::string>() == "scovel") {
                d_plist.propagator = ParameterList::PropagatorType::SCOVEL;
                d_plist.propagator_string = "Scovel";
            } else if (vm["propagator"].as<std::string>() == "rk4") {
                d_plist.propagator = ParameterList::PropagatorType::RK4;
                d_plist.propagator_string = "RK4";
            } else if (vm["propagator"].as<std::string>() == "yoshida4") {
                d_plist.propagator = ParameterList::PropagatorType::YOSHIDA4;
                d_plist.propagator_string = "Yoshida4";
            } else if (vm["propagator"].as<std::string>() == "suzuki4") {
                d_plist.propagator = ParameterList::PropagatorType::SUZUKI4;
                d_plist.propagator_string = "Suzuki4";
            } else if (vm["propagator"].as<std::string>() == "ss6_7") {
                d_plist.propagator = ParameterList::PropagatorType::SS6_7;
                d_plist.propagator_string = "McLachlan SS6_7";
            } else if (vm["propagator"].as<std::string>() == "ss6_9") {
                d_plist.propagator = ParameterList::PropagatorType::SS6_9;
                d_plist.propagator_string = "McLachlan SS6_9";
            } else if (vm["propagator"].as<std::string>() == "ss8_15") {
                d_plist.propagator = ParameterList::PropagatorType::SS8_15;
                d_plist.propagator_string = "McLachlan SS8_15";
            } else if (vm["propagator"].as<std::string>() == "ss8_17") {
                d_plist.propagator = ParameterList::PropagatorType::SS8_17;
                d_plist.propagator_string = "McLachlan SS8_17";
            } else if (vm["propagator"].as<std::string>() == "ss4_5") {
                d_plist.propagator = ParameterList::PropagatorType::SS4_5;
                d_plist.propagator_string = "McLachlan SS4_5";
            } else {
                po::invalid_option_value e(vm["propagator"].as<std::string>());
                e.set_option_name("propagator");
                throw e;
            }
        }

#define IS_MANUALLY_SET_TO_TRUE(X) (!vm[X].defaulted() and vm[X].as<bool>())

        if (IS_MANUALLY_SET_TO_TRUE("save-nothing")) {
            if (IS_MANUALLY_SET_TO_TRUE("save-trajectory") or IS_MANUALLY_SET_TO_TRUE("save-velocity") or
                IS_MANUALLY_SET_TO_TRUE("save-energies") or IS_MANUALLY_SET_TO_TRUE("save-histograms") or
                IS_MANUALLY_SET_TO_TRUE("save-particlenum") or IS_MANUALLY_SET_TO_TRUE("bounce-map"))
                throw ConflictingArguments("Save or not to save. You can not choose both.");

            d_plist.save_full_trajectory = false;
            d_plist.save_velocity = false;
            d_plist.save_energies = false;
            d_plist.save_histograms = false;
            d_plist.save_particlenum = false;
            d_plist.save_bouncemap = false;
        }

        // Handle manually set gaussian bumps
        if (IS_MANUALLY_SET_TO_TRUE("gaussian-bumps-manual")) {
            bool manual_bumps = false;

            bool gbml = vm.count("gaussian-bumps-manual-locations");
            bool gbmw = vm.count("gaussian-bumps-manual-widths");
            bool gbma = vm.count("gaussian-bumps-manual-amplitudes");
            if (gbml and gbmw and gbma) {
                auto sgbml = vm["gaussian-bumps-manual-locations"].as<std::vector<double>>().size();
                auto sgbmw = vm["gaussian-bumps-manual-widths"].as<std::vector<double>>().size();
                auto sgbma = vm["gaussian-bumps-manual-amplitudes"].as<std::vector<double>>().size();
                if (sgbml % 2 != 0) {
                    po::invalid_option_value e("Wrong number of arguments (needs even number of arguments)");
                    e.set_option_name("gaussian-bumps-manual-locations");
                    throw e;
                } else if ((sgbml / 2 != sgbmw) or (sgbml / 2 != sgbma) or (sgbml * sgbmw * sgbma == 0)) {
                    const std::string es{"Wrong number of arguments for manually set gaussian bumps"};
                    throw po::validation_error(
                        po::validation_error::invalid_option_value, "gaussian-bumps-manual-locations", "abc"
                    );
                } else {
                    // Checks passed, let's write the bump info to our parameterlist
                    d_plist.gaussian_bumps_manual_widths = vm["gaussian-bumps-manual-widths"].as<std::vector<double>>();
                    d_plist.gaussian_bumps_manual_amplitudes =
                        vm["gaussian-bumps-manual-amplitudes"].as<std::vector<double>>();
                    auto gbml_v = vm["gaussian-bumps-manual-locations"].as<std::vector<double>>();
                    for (size_t i = 0; i < sgbml / 2; i++)
                        d_plist.gaussian_bumps_manual_locations.push_back(Vec2d{gbml_v.at(2 * i), gbml_v.at(2 * i + 1)}
                        );
                }
                manual_bumps = true;

            } else if (!gbml and !gbmw and !gbma) {
                ;  // Everything's ok
            } else {
                throw ConflictingArguments(
                    "If gaussian bumps are manually set, you have to set locations, widths and amplitudes"
                );
            }

            if (not manual_bumps and d_plist.gaussian_bumps_itp2d_file.empty())
                throw ConflictingArguments(
                    "You have to give the itp2d file or manual bump information if you wan't to use gaussian bumps."
                );

            if (IS_MANUALLY_SET_TO_TRUE("gaussian-bumps-fast-from-itp2d")) {
                if (IS_MANUALLY_SET_TO_TRUE("gaussian-bumps"))
                    throw ConflictingArguments(
                        "Fast gaussian bumps cannot be used with the old gaussian bumps potential."
                    );

                if (gbml or gbmw or gbma)
                    throw ConflictingArguments("If using fast Gaussian Bumps, no manual bumps can be used");

                if (not vm.count("gaussian-bumps-itp2d-file"))
                    throw ConflictingArguments("Fast gaussian bumps need an itp2d file to read the bump location from");
            }
        }
        // Finally calculate buffersizes
        d_plist.calculate_buffersizes();
    }  // Handle errors
    catch (po::unknown_option& e) {
        std::cerr << "Error: Unknown option '" << e.get_option_name() << "'." << std::endl;
        exit(1);
    } catch (po::multiple_occurrences& e) {
        std::cerr << "Error: Option '" << e.get_option_name() << "' given multiple times." << std::endl;
        exit(1);
    } catch (po::invalid_command_line_syntax& e) {
        std::cerr << "Error: Syntax error: " << e.what() << std::endl;
        exit(1);
    } catch (po::required_option& e) {
        std::cerr << "Error: Required option '" << e.get_option_name() << "' missing." << std::endl;
        exit(1);
    } catch (po::reading_file& e) {
        std::cerr << "Error: config-file cannot be read: " << e.what() << std::endl;
        exit(1);
    } catch (po::invalid_command_line_style& e) {
        std::cerr << "Error: Invalid command line style. Programming error." << std::endl;
        exit(1);
    } catch (po::invalid_option_value& e) {
        std::cerr << "Error: ";
        std::cerr << e.what() << std::endl;
        exit(1);
    } catch (po::validation_error& e) {
        std::cerr << "Error: Invalid value given to option:" << std::endl;
        std::cerr << e.what() << std::endl;
        exit(1);
    } catch (po::invalid_syntax& e) {
        std::cerr << "Error: Invalid syntax: " << e.what() << std::endl;
        exit(1);
    } catch (po::multiple_values& e) {
        std::cerr << "Error: Multiple values given to option '" << e.get_option_name() << "'." << std::endl;
        exit(1);
    } catch (po::too_many_positional_options_error&) {
        std::cerr << "Error: Too many positional options. Programming error. " << std::endl;
        exit(1);
    } catch (FileDoesNotExist& e) {
        std::cerr << e.what() << std::endl;
        exit(1);
    } catch (ConflictingArguments& e) {
        std::cerr << e.what() << std::endl;
        exit(1);
    }
    return false;
}
}  // namespace bill2d
