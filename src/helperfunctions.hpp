/*
 * helperfunctions.hpp
 *
 * This file is part of bill2d.
 *
 * The functions and classes introduced in this file
 * are little things that make life easier.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file helperfunctions.hpp
 * \brief A few convenience functions, which do not fit into
 * role of any class of bill2d.
 */

#ifndef HELPERFUNCTIONS_HPP
#define HELPERFUNCTIONS_HPP

#include "bill2d_config.h"

#include <H5Cpp.h>
#include <boost/filesystem.hpp>

#include <sys/stat.h>
#if defined(HAVE_AVX) || defined(HAVE_SSE2)
#include <x86intrin.h>
#endif

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>

namespace bill2d {

inline double sign(double x) { return (x >= 0) ? 1.0 : -1.0; };

//! Return true if file exists
bool FileExists(std::string path);
//! Return true if lockfile (filename.lock) exists
bool lockExists(std::string filename);
//! Create lockfile
void createLock(std::string filename);
//! Remove lockfile
void rmLock(std::string filename);

//! Remove a file
void rm_file(std::string filename);

//! Read a 2d ascii array
void file2vector2d(std::string filename, std::vector<std::vector<double>>& myvector);

//! Enum for hdf52vector2d_inpos
enum Position { FIRST, LAST };

//! Read initial positions from a hdf5-file
void hdf52vector2d_inpos(
    std::string filename, size_t num_particles, std::vector<std::vector<double>>& output, enum Position pos
);

std::vector<double> hdf5_1darray_to_vector(std::string filename, std::string dataset_name);

std::vector<std::string> split(std::string str, std::string delim);

std::string strip_trailing_whitespace(std::string str);

//! Append a 2d array to ascii file
void vector2d2file(std::string filename, const std::vector<std::vector<double>>& myArray);
//! Append a 1d array to ascii file (to new line)
void vector2file(std::string filename, const std::vector<double>& myArray);

//! Rounds up \param value to nearest bigger multiple of \param multiple
unsigned round_up(unsigned value, unsigned multiple);
#if defined(HAVE_ALIGNED_ALLOC) || defined(HAVE_POSIX_MEMALIGN)
//! Aligned alloc using either posix_memalign or aligned_alloc
void* bill2d_aligned_alloc(size_t alignment, size_t size);
#endif

#ifdef HAVE_AVX
//! Union class for accessing __m256d
union alignas(32) AVXvecd {
    __m256d vec;
    alignas(4 * sizeof(double)) double arr[4];
};
#endif

#ifdef HAVE_SSE2
//! Union class for accessing __m128d
union alignas(16) SSE2vecd {
    __m128d vec;
    alignas(2 * sizeof(double)) double arr[2];
};

#endif

#if defined(HAVE_AVX) && !defined(HAVE_AVX_COSH)
//! Non-vectorized cosh
inline __m256d _mm256_cosh_pd(__m256d x) {
    AVXvecd xd;
    xd.vec = x;
    for (auto& v : xd.arr) v = cosh(v);
    return xd.vec;
}
#endif

#if defined(HAVE_AVX) && !defined(HAVE_AVX_EXP)
//! Non-vectorized exp
inline __m256d _mm256_exp_pd(__m256d x) {
    AVXvecd xd;
    xd.vec = x;
    for (auto& v : xd.arr) v = exp(v);
    return xd.vec;
}
#endif

#if defined(HAVE_SSE2) && !defined(HAVE_SSE2_COSH)
//! Non-vectorized cosh
inline __m128d _mm_cosh_pd(__m128d x) {
    SSE2vecd xd;
    xd.vec = x;
    for (auto& v : xd.arr) v = cosh(v);
    return xd.vec;
}
#endif
//! Structure for lazy design of Datafile- and EscapeDatafile classes (should have done polymorphism...)
struct dont_prepare_base_tag {};

}  // namespace bill2d
#endif
