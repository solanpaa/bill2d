/*
 * billiard.hpp
 *
 * This file is part of bill2d.
 *
 * The particles, algorithms ("billiard"), potentials, and interactions
 * are defined in this file.
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file billiard.hpp
 * \brief Propagation, algorithms etc. are defined here.
 */

#ifndef _BILLIARD_HPP_
#define _BILLIARD_HPP_

#include "bill2d_config.h"
#include "custom_psos.hpp"
#include "datafile.hpp"
#include "datatypes.hpp"
#include "interactions.hpp"
#include "potentials.hpp"
#include "table.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <memory>
#include <string>
#include <vector>

#ifdef UNITTESTING
#include "gtest/gtest_prod.h"
#endif
extern int SIG_FLAG;

namespace bill2d {

//! Base class for billiards
class Billiard {
public:
    // public methods
    //! Constructor
    /*!
      \param table std::shared_ptr to the \Bold{table} instance used in the simulation
      \param rng std::shared_ptr to an instance of the random number generator
      \param plist Reference to the parameterlist
      \param file std::unique_ptr to the Datafile
      */
    enum class SelectPropagator { NO, YES };

    Billiard(
        std::shared_ptr<const Table> table, std::unique_ptr<const RNG> rng, ParameterList& plist,
        std::unique_ptr<Datafile> file, std::unique_ptr<CustomPSOS> custom_psos = std::unique_ptr<CustomPSOS>(),
        SelectPropagator = SelectPropagator::YES
    );

    //! Delete copy constructor
    Billiard(const Billiard&) = delete;
    virtual ~Billiard();

    void propagate();

    //! Return number of particles currently in simulation
    inline unsigned N() const { return static_cast<unsigned>(d_particles.size()); }

    //! Prepares the system: Choose initial state randomly withing the given parameters
    virtual void prepare(double parameter);

    //! Prepares the system from given positions and velocities
    /*!
     \param rv Vector containing the initial positions and velocities
     \param total_energy Parameter which sets the total energy of the system
     \param ignore_energy If set, the total energy reguirement will be ignored and the velocities will be read
     directly from \Bold{rv} instead of rescaling them to fit the energy reguirement
     \param perturbate If true, the given initial positions will be perturbated a bit using a gaussian distribution
     with standard deviation given by \Bold{std}
     \param std Standard deviation for the normal distribution used in perturbating the initial positions
     */
    void prepare_manual(
        const std::vector<std::vector<double>>& rv, double total_energy, bool ignore_energy, bool perturbate, double std
    );

    // Returns false if the custom_inpos cannot select a position
    bool prepare_custom(
        std::function<bool(
            particleset&, const RNG& rng, const Table& table, std::function<double()> epot, std::function<double()> ekin
        )>
            custom_inpos,
        const double total_energy
    );

    const particleset& get_particles() const { return d_particles; }

    Particle& particle() { return d_particles.at(0); }

    //! Kinetic energy
    double kinetic_energy() const;

    //! Potential energy
    virtual double potential_energy() const { return 0; }

    //! To be used after propagation, save the rest of the data
    /*! Saves bouncing maps, histograms and a few attributes !*/
    void save_remaining_data();

    //! Get reference of the variable counting the number of collisions with the table boundary
    const unsigned& get_hits() const { return d_hits; }

    //! Get reference of the variable counting the time
    const double& get_time() const { return d_time; }

    //! Method for creation of a new particle
    virtual void create_particle(std::pair<Vec2d, Vec2d> (*f)(
        double /*Ekin*/, double /*Epot*/, unsigned /*num_particles*/, const Potential* /*potential*/,
        const Table* /*table*/, const RNG* /*Random number generator*/, ParameterList* /*parameterlist*/
    ));

    //! Compares two Billiard instances and calculates the instantatinous phase space separation
    double get_phase_space_difference(Billiard& compBill);

    //! Returns the positions and velocities of the particles as a vector of double-vectors
    std::vector<std::vector<double>> get_state();

    //! Return the initial position of the ith particle
    Vec2d get_initial_position(unsigned i = 0) {
        auto p = d_particles.begin();
        std::advance(p, i);
        assert(p < d_particles.end());
        return p->d_r0;
    }

    //! Return the initial velocity of the ith particle
    Vec2d get_initial_velocity(unsigned i = 0) {
        auto p = d_particles.begin();
        std::advance(p, i);
        assert(p < d_particles.end());
        return p->d_v0;
    }

    //! Return the position of the ith particle
    Vec2d get_position(unsigned i = 0) {
        auto p = d_particles.begin();
        std::advance(p, i);
        assert(p < d_particles.end());
        return p->d_r;
    }

    //! Return the velocity of the ith particle
    Vec2d get_velocity(unsigned i = 0) {
        auto p = d_particles.begin();
        std::advance(p, i);
        assert(p < d_particles.end());
        return p->d_v;
    }

    //! Recalculates the forces on the particles
    virtual inline void update_forces(){};

    //! Returns squared difference to initial position in (x,v)-space
    inline double get_difference_squared_to_initial_position() const {
        double diff = 0;
        Vec2d r_true;
        for (const Particle& p : d_particles) {
            r_true[0] = p.d_r[0] + p.d_cell[0] * d_L1 + p.d_cell[1] * d_hd;
            r_true[1] = p.d_r[1] + p.d_cell[1] * d_uh;
            diff += pow((r_true[0] - p.d_r0[0]), 2) + pow((r_true[1] - p.d_r0[1]), 2);
        }

        return diff;
    }

    //! Returns difference to initial position in (x_reduced,v)-space
    inline double get_reduced_difference_to_initial_position() const {
        double diff = 0;
        for (const Particle& p : d_particles) {
            diff += sqrt(
                pow((p.d_r[0] - p.d_r0[0]), 2) + pow((p.d_r[1] - p.d_r0[1]), 2) + pow((p.d_v[0] - p.d_v0[0]), 2) +
                pow((p.d_v[1] - p.d_v0[1]), 2)
            );
        }

        return diff;
    }

    //! Removes all particles that satisfy the condition given by the argument function
    bool remove_particle(bool (*f)(Vec2d, Vec2d)) {
        auto it_particles_end = d_particles.cend();
        auto it_to_be_removed_begin =
            std::remove_if(d_particles.begin(), d_particles.end(), [&f](Particle& p) { return f(p.d_r, p.d_v); });
        d_particles.erase(it_to_be_removed_begin, it_particles_end);

        return it_to_be_removed_begin != it_particles_end;
    }

    bool& get_hit_flag() { return d_hit_flag; }

    ParameterList& get_plist() { return d_plist; }

    const CustomPSOS& get_custom_psos() const { return *d_custom_psos; };

    void write_sigflag(int sigflag) {
        if (d_file) {
            d_file->flush();
            d_file->write_attribute("terminating signal", sigflag);
        }
    }

    double get_dt() const { return d_dt; }

    void set_parallel(
        const bool parallel, const std::function<void(double*, double*, Particle&, unsigned long)> save_callback
    ) {
        parallel_ = parallel;
        save_callback_ = save_callback;
    }

protected:
#ifdef UNITTESTING
    friend class billiard_tests;
#endif

    typedef void (Billiard::*propagator_function)();
    //! Pointer to the correct propagator method
    propagator_function d_propagator;

    typedef bool (Table::*
                      collision_handler_function)(Vec2d&, Vec2d&, pair<int>&, std::vector<std::vector<double>>&, double, unsigned&)
        const;
    //! Pointer to the correct collision handler (normal or bouncing map)
    collision_handler_function d_collision_handler;

    //! Pointer to the billiard table
    std::shared_ptr<const Table> d_table;

    //! Container for particles
    particleset d_particles;

    //! unit of propagation time
    double d_dt;

    //! Parameterlist
    ParameterList& d_plist;

    //! Current time
    double d_time;

    //! Number of collisions with the boundary (so far)
    unsigned d_hits;

    //! Hit flag
    bool d_hit_flag;

    //! Datafile
    std::unique_ptr<Datafile> d_file;

    //! What to save
    bool d_save_traj, d_save_vel, d_save_energies, d_saveparticlenum, d_save_bmap, d_save_histograms;

    //! Buffer: particlenumber
    unsigned* d_particlenum;

    //! Buffer: kinetic energy
    double* d_ekin;

    //! Buffer: potential energy
    double* d_epot;

    //! The index of the next datapoint in databuffers
    size_t d_saveidx;

    //! Index of the iteration for the buffers
    unsigned long long int d_iteration_index;

    //! Save sparcing
    unsigned long long int d_sparsesave;

    //! Random number generator
    std::unique_ptr<const RNG> d_rng;

    //! Allowed are to spawn (xmin, ymin, xmax, ymax)
    twopairs<double> d_spawnarea;

    //! Last index of the buffers
    unsigned long long int d_minimum_number_of_iterations_minus_one;

    //! Geometry
    pair<double> d_geometry;
    /*!
    These parameters define the parallellogram unit cell. The boundary on the left hand side is: -a_l*x+y > 0
    On right hand side -x+d_Br*y+d_L1 < 0
    \param d_L1 length of the horizontal side
    \param d_L2 length of the vertical side
    \param d_uh height of the parallellogram
    \param d_hd displacement of the parallellogram in horizontal direction
    */

    double d_Al, d_Br, d_Cr, d_L1, d_L2, d_uh, d_hd;

    //! Do have periodicity on rectangular unit cell
    bool d_periodic;

    //! The newest particleID
    unsigned d_currID;

    //! Number of particles in the beginning of the simulation
    size_t d_Ninitial;

    unsigned long long int d_num_buffersaves;

    //! (Re)calculates the coefficients of the propagator
    virtual void calculate_propagator_coefficients() {}

    std::unique_ptr<CustomPSOS> d_custom_psos;

    //! Flag telling if the Billiard is used as a part of parallel simulations
    bool parallel_;
    std::function<void(double*, double*, Particle&, unsigned long)> save_callback_;

    //! Selects the propagator method

    //! Saves trajectories, velocities, kinetic and potential energy, number of particles to buffer
    /*! This must be called after each time step !*/
    void save_iteration();

    //! To be used after buffers are full, save data to file
    void save_data();

    void d_create_particle(
        std::pair<Vec2d, Vec2d> (*f)(
            double /*Ekin*/, double /*Epot*/, unsigned /*num_particles*/, const Potential* /*potential*/,
            const Table* /*table*/, const RNG* /*Random number generator*/, ParameterList* /*parameterlist*/
        ),
        const Potential* pot = nullptr
    );

    //! Method for removing a particle
    virtual void remove_particle(const particle_iterator& p);

    //! Propagate the particles with Euler method (velocity Verlet without interactions)
    void d_propagate2();

private:
    void select_propagator();
};

//! A derived class representing billiards with a magnetic field
class MagneticBilliard : public Billiard {
public:
    //! Constructor
    /*!
     \param table pointer to table
     \param rng random number generator
     \param plist parameterlist
     */
    MagneticBilliard(
        std::shared_ptr<const Table> table, std::unique_ptr<const RNG> rng, ParameterList& plist,
        std::unique_ptr<Datafile> file, std::unique_ptr<CustomPSOS> custom_psos = std::unique_ptr<CustomPSOS>(nullptr)
    );

    // public methods

protected:
    void d_propagate2();

private:
    void calculate_propagator_coefficients() override;

    //! the magnetic field (in the direction of the surface normal)
    double d_B;
    double invB;      // 1/B
    double invdt;     // 1/dt
    double sinBdt;    // sin(B*dt)
    double cosBdt;    // cos(B*dt)
    double cosBdtm1;  // cos(B*dt)-1

    //! Propagate the particles with method from Q. Spreiter and M. Walter, J. Comp. Phys \textbf{152}, 102--119
    //(1999).
    void select_propagator();
};

//! Billiards with particle-particle interactions
/*! Billiards with particle-particle interactions is implemented with a class
   InteractingBilliard derived from Billiard, and a separate class for
   interactions.!*/
class InteractingBilliard : public Billiard {
public:
    enum class SelectPropagator { NO, YES };
    /*!
      \param inter pointer to instance of a p-p interaction
      \param pot pointer to potential (may be nullptr)
      \param table pointer to the billiard table
      \param rng random number generator
      \param plist parameterlist
     */
    InteractingBilliard(
        std::shared_ptr<const Interaction> inter, std::shared_ptr<const Potential> pot,
        std::shared_ptr<const Table> table, std::unique_ptr<const RNG> rng, ParameterList& plist,
        std::unique_ptr<Datafile> file, std::unique_ptr<CustomPSOS> custom_psos = std::unique_ptr<CustomPSOS>(nullptr),
        Billiard::SelectPropagator = Billiard::SelectPropagator::YES
    );

    ~InteractingBilliard() {
        if (d_Ccoeffs) delete d_Ccoeffs;
        if (d_Dcoeffs) delete d_Dcoeffs;
    }

    //! Delete copy constructor
    InteractingBilliard(const InteractingBilliard&) = delete;

    //! Potential energy
    double potential_energy() const override;

    //! Prepare the system
    void prepare(double total_energy) override;

    //! Method for creation of a particle
    void create_particle(std::pair<Vec2d, Vec2d> (*f)(
        double /*Ekin*/, double /*Epot*/, unsigned /*num_particles*/, const Potential* /*potential*/,
        const Table* /*table*/, const RNG* /*Random number generator*/, ParameterList* /*parameterlist*/
    )) override;

    // There's a protected method InteractingBilliard::remove_particle that overrides a base class method
    // and hence hides Billiard::remove_particle(bool (*)(Vec2d,Vec2d)). Let's make it visible again.
    using Billiard::remove_particle;

private:
    void calculate_propagator_coefficients() override;
    void select_propagator();

protected:
    //! Propagation with velocity Verlet scheme (M. P. Allen and D. J. Tildesley, Computer Simulation of Liquids,
    // Oxford University Press, USA, 1989)
    void d_propagate2();

    //! Propagation with symmetric 2nd order scheme [R. I. McLachlan, SIAM J. Sci. Comput. 16(1), 151–168. (On page
    // 14, type S, m=2)]
    void d_propagate2_optimal();

    //! Propagation with 3rd order scheme [M. Suzuki, J. Phys. Soc. Jpn 64, 3015--3019 (page 3018, case r=5)]
    void d_propagate3();

    //! Propagation with 4th order scheme [see, e.g., H. Yoshida, Phys. Lett. A 150, 262--268 (1990); or E. Forest
    // and R. D. Ruth, Physica D 43, 105 (1990)].
    void d_propagate4();

    //! Propagation with 4th order scheme with optimal coeffs [R. I. McLachlan, SIAM J. Sci. Comput. 16(1), 151–168.
    //(On page 14, type $SB^3A$, m=4)]
    void d_propagate4_optimal();

    //! Propagation with 6th order scheme with optimal coeffs [R. I. McLachlan, SIAM J. Sci. Comput. 16(1), 151–168.
    //(On page 15, type SB^3A, m=7)]
    void d_propagate6_optimal();

#ifdef UNITTESTING
    friend class billiard_tests;
    friend class billiardfactory_tests;
#endif

    typedef Vec2d (InteractingBilliard::*force_pointer)(const Particle&) const;
    //! This chooses where d_force points to
    inline force_pointer get_force_pointer();
    //! This is used to get the external (other than e-e i/a) forces
    force_pointer d_force;
    //! Only external potential
    Vec2d d_fpot(const Particle& p) const;
    //! No external potentials
    Vec2d d_fno_ext(const Particle& p) const;

    //! Interaction
    std::shared_ptr<const Interaction> d_inter;
    //! Potential
    std::shared_ptr<const Potential> d_pot;
    //! Update the forces (used after creation of a particle)
    inline void update_forces() override;
    //! A method for removing a particle
    void remove_particle(const particle_iterator&) override;

    //! Coefficients for higher order symplectic propagation schemes
    double* d_Ccoeffs;
    double* d_Dcoeffs;
    unsigned d_num_coeff_iters;
};

// This can be used to update the forces of the particles in case a new particle is created
inline void InteractingBilliard::update_forces() {
    for (Particle& p : d_particles) {
        p.d_F = (this->*d_force)(p);
        if (d_inter) {
            for (Particle& op : d_particles) {
                if (&p != &op) {
                    p.d_F += d_inter->force(p.d_r, op.d_r);
                }
            }
        }
    }
}

inline InteractingBilliard::force_pointer InteractingBilliard::get_force_pointer() {
    if (d_pot == nullptr) {
        return (&InteractingBilliard::d_fno_ext);
    } else {
        return (&InteractingBilliard::d_fpot);
    }
}

//! A derived class combining interactions and magnetic field
// Multiple inheritance would be nice here but makes things messy
class InteractingMagneticBilliard : public InteractingBilliard {
public:
    /*!
     \param inter Pointer to the interaction
     \param pot Pointer to the external potential (may be nullptr)
     \param table Pointer to the \Bold{table} instance used in the simulation
     \param rng Pointer to a random number generator
     \param plist Pointer to the parameterlist
     */
    InteractingMagneticBilliard(
        std::shared_ptr<const Interaction> inter, std::shared_ptr<const Potential> pot,
        std::shared_ptr<const Table> table, std::unique_ptr<const RNG> rng, ParameterList& plist,
        std::unique_ptr<Datafile> file, std::unique_ptr<CustomPSOS> custom_psos = std::unique_ptr<CustomPSOS>(nullptr),
        Billiard::SelectPropagator = Billiard::SelectPropagator::YES
    );

private:
    void calculate_propagator_coefficients() override;

    //! the magnetic field (in the direction of the surface normal)
    double d_B;
    double invB;
    double invdt;
    // precalculated data to be used in calculations
    double Bdt;

    // Spreiter & Walter
    double C0;
    double C1;
    double C2;
    double S0;
    double S1;
    double S2;

    // Chin 2a
    double tC0;
    double tC1;
    double tC2;
    double tS0;
    double tS1;
    double tS2;

    // Boris
    double m_B2dt2_plus_4;
    double B2dt2_plus_4;
    double Bdt_times_4;

    // YHE2
    double Y1;
    double Y2;

    double yoshida_a1, yoshida_a2;

    double suzuki_b1, suzuki_b2, suzuki_b3;

    double prk4_a1, prk4_a2, prk4_a3, prk4_a4, prk4_b1, prk4_b2, prk4_b3;

    std::array<double, 6> prk6_a;
    std::array<double, 5> prk6_b;

    std::array<double, 4> ss6_7;
    std::array<double, 5> ss6_9;

    std::array<double, 8> ss8_15;
    std::array<double, 9> ss8_17;

    std::array<double, 3> ss4_5;

    void select_propagator();

private:
    void d_propagate_initial_steps();

    //! Propagate with 2nd order scheme from Q. Spreiter and M. Walter, J. Comp. Phys \textbf{152}, 102--119 (1999).
    void d_propagate2();

    void d_propagate_boris();

    void d_propagate_chin2b();

    void d_propagate_chin2a(
    );  // This should apparently be the same as d_propagate2, but implemented slightly differently

    void d_propagate_yhe2();

    void d_propagate_scovel();

    void d_propagate_rk4();

    void d_propagate_prk4(
    );  // Blanes & Moan, Practical symplectic partitioned Runge–Kutta and Runge–Kutta–Nyström methods

    void d_propagate_prk6(
    );  // Blanes & Moan, Practical symplectic partitioned Runge–Kutta and Runge–Kutta–Nyström methods

    // From R. I. MCLACHLAN, "On symmetric composition methods". These implementations can be optimized by amalgamating
    // some consecutive terms
    void d_propagate_SS4_5();
    void d_propagate_SS6_7();

    void d_propagate_SS6_9();

    void d_propagate_SS8_15();

    void d_propagate_SS8_17();
    //

    void d_propagate_yoshida4();

    void d_propagate_suzuki4();

    inline const Vec2d phi1_x(const Vec2d&, const Vec2d& v, const Vec2d&, const double h) { return h * v; }

    inline const Vec2d phi2_v(const Vec2d&, const Vec2d& v, const Vec2d& F, const double h) {
        const double A1 = sin(d_B * h);
        const double A2 = 1 - cos(d_B * h);

        return h * F + A1 * v.rotcw90() + A2 / d_B * F.rotcw90() - A2 * v - (h - A1 / d_B) * F;
    }

    inline void G2(const double dt) {
        for (Particle& p : d_particles) {
            p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, 0.5 * dt);
        }

        update_forces();

        for (Particle& p : d_particles) {
            p.d_v += phi2_v(p.d_r, p.d_v, p.d_F, dt);
        }

        for (Particle& p : d_particles) {
            p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, 0.5 * dt);
        }
    }

    unsigned initial_steps_taken;
};
}  // namespace bill2d
#endif  // _BILLIARD_HPP_
