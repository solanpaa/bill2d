/*
 * timer.hpp
 *
 * This file is part of bill2d.
 *
 * A simple timer class that is used measuring the time it takes, e.g.,
 * for the normal billiard simulation to run.
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file timer.hpp
 *  \brief Definitions of the class bill2d::Timer, which is used for
 *  measuring wall-clock time.
 */

#ifndef _TIMER_HPP_
#define _TIMER_HPP_

#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif
// Modern GCC warns without this
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include "bill2d_config.h"

#include <chrono>
#ifdef UNITTESTING
#include "gtest/gtest_prod.h"
#endif
namespace bill2d {

//! A class for measuring wall-clock time
class Timer {
public:
    typedef struct timeval timeval;
    //! Starts the timer
    void start();

    //! Stops the timer and returns elapsed time in seconds
    double stop();

private:
#ifdef UNITTESTING
    FRIEND_TEST(Timer, constructor);
    FRIEND_TEST(Timer, start);
#endif
    std::chrono::time_point<std::chrono::system_clock> start_time;
};

}  // namespace bill2d
#endif  // _TIMER_HPP_
