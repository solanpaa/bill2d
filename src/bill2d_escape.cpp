/*
 * bill2d_escape.cpp
 *
 * This file is part of bill2d.
 *
 * A source for a executable that calculates escape probabilities as a function
 * of time and number of collisions.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file bill2d_escape.cpp
 * \brief Simulation of open billiards with the binary 'bill2d_escape' implemented here.
 */

/*
 * Example that can be used for writing escape simulation codes
 * Runs a single simulation for system described by 'system' with additional parameters 'params'
 */
#include "billiard_factory.hpp"
#include "executables.hpp"
#include "multithread_tools.hpp"
#include "timer.hpp"

#include <signal.h>

#include <atomic>
#include <mutex>
#include <thread>

/*
 * This function parallelizes the calculation of the escape simulations
 * and writes the resulting data to a hdf5-file
 */

void bill2d::escape_main(ParameterList& plist) {
    Timer timer;
    timer.start();

    auto get_trajectory_datafile_path = [](const std::string& original_path) {
        std::stringstream unique_path;
        if (original_path.compare(original_path.length() - 3, 3, ".h5") == 0)
            unique_path << original_path.substr(0, original_path.length() - 3);
        else
            unique_path << original_path;

        unique_path << "_trajectories/bill2d_";
        return unique_path.str();
    };

    // The data on number of collisions and times till the escape
    data_container<double> times(plist.simulations);
    data_container<double> collisions(plist.simulations);

    ParameterList simulation_plist(plist);
    // Save trajectories to correct location

    simulation_plist.savefilepath = get_trajectory_datafile_path(simulation_plist.savefilepath);

    OpenBilliardFactory bfactory(simulation_plist);

    auto worker_func = [](data_container<double>& a_times,
                          data_container<double>& a_collisions,
                          OpenBilliardFactory& factory,
                          const unsigned max_simulations,
                          std::mutex& mu_worker,
                          unsigned output_level,
                          std::atomic_size_t& simulations_done) {
        std::unique_lock<std::mutex> lock(mu_worker, std::defer_lock);
        while ((SIG_FLAG != SIGINT) and (SIG_FLAG != SIGTERM) and (simulations_done < max_simulations)) {
            try {
                simulations_done++;
                std::pair<double, double> res = single_escape(factory.get_bill());
                a_times.push_back(std::get<0>(res));
                a_collisions.push_back(std::get<1>(res));
                lock.lock();
                if (output_level >= 1) std::cout << a_times.size() << " / " << max_simulations << std::endl;
                lock.unlock();
            } catch (SignalFinish& e) {
                break;
            }
        }
    };

    unsigned num_threads = std::thread::hardware_concurrency();

    if (num_threads > 32) num_threads = 32;

    //  Actually only one thread supported if saving trajectories
    if (!plist.save_nothing) num_threads = 1;

    // Generate worker threads
    std::vector<std::thread> workers;
    std::mutex mu;
    std::atomic_size_t simulations_done{0};
    for (unsigned i = 0; i < num_threads; i++) {
        std::thread worker(
            worker_func,
            std::ref(times),
            std::ref(collisions),
            std::ref(bfactory),
            plist.simulations,
            std::ref(mu),
            plist.output_level,
            std::ref(simulations_done)
        );
        workers.emplace_back(std::move(worker));
    }

    // Join worker threads to main here
    for (auto& thread : workers) thread.join();

    if (plist.output_level >= 1) cout << "Writing data.." << endl;

    // Prepare datafile
    EscapeDatafile file(plist);
    file.initialize(plist);

    // Write events
    file.write_escape_events("escape_collisions", collisions.data(), collisions.size());
    file.write_escape_events("escape_times", times.data(), times.size());

    cout.precision(4);
    double elapsed_time = timer.stop();
    if (plist.output_level >= 1) {
        std::cout << "Finished in " << elapsed_time << " seconds." << endl;
        std::cout << static_cast<double>(collisions.size()) / elapsed_time << " events / s." << std::endl;
    }
}

/*
 * This function handles a single escape simulation and returns
 * the collisions with the boundary and time at the moment of escape
 */

std::pair<double, double> bill2d::single_escape(std::unique_ptr<Billiard> bill) {
    ParameterList& plist = bill->get_plist();

    if (plist.output_level >= 2) cout << "Preparing..." << endl;
    Timer timer;

    // Simulation variables here

    double delta_t = plist.delta_t;

    // Prepare the billiard system
    try {
        bill->prepare(plist.energy);
    } catch (SignalFinish& e) {
        std::cout << e.what() << std::endl;
        throw;
    }

    // Initialize some variables for the simulation

    if (plist.output_level >= 2) cout << "Simulation starts." << endl;
    timer.start();

    const unsigned& hits = bill->get_hits();
    const double& time = bill->get_time();

    /*
     * This vector (currently only first element is used)
     * has boolean variables, which will turn from false
     * to true when a particle hits a hole in the boundary
     */

    bool& hit_flag = bill->get_hit_flag();

    bool stop = false;

    // Run the simulation
    do {
        for (unsigned i = 0; i < (plist.minimum_number_of_iterations); i++) {
            if (hit_flag) {  // If a particle hit a hole, end simulation
                stop = true;
                break;
            } else  // Otherwise simulate further
                bill->propagate();
        }

        if (plist.output_level >= 3) cout << "T = " << time << ", collisions=" << hits << endl;

        // Check end criteria

        if (fabs(plist.max_t) > 1e-3) {
            if (fabs(time - plist.max_t) < delta_t or time >= plist.max_t) stop = true;
        }
        if (plist.max_collisions != 0) {
            if (hits > plist.max_collisions) stop = true;
        }
    } while (!stop);

    // Stop timer
    double elapsed_time = timer.stop();

    // Some output
    cout.precision(4);
    if (plist.output_level >= 3) cout << "Finished in " << elapsed_time << " seconds." << endl;

    // Return time and collisions at the end of the simulation

    return std::make_pair(time, static_cast<double>(hits));
}
