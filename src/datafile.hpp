/*
 * datafile.hpp
 *
 * This file is part of bill2d.
 *
 * A class (and supporting classes) which is used to handle all output to
 * HDF5-files
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file datafile.hpp
 * \brief Output to HDF5-files is via the Datafile-class, which is defined here.
 */

#ifndef _DATAFILE_HPP_
#define _DATAFILE_HPP_

#include "bill2d_config.h"

#include "custom_psos.hpp"
#include "datasetinfo.hpp"
#include "helperfunctions.hpp"
#include "hist.hpp"
#include "hist2d.hpp"
#include "parameterlist.hpp"

#include <H5Cpp.h>
#include <boost/filesystem.hpp>

#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <string.h>
#include <string>
#include <vector>

#ifdef UNITTESTING
#include "gtest/gtest_prod.h"
#endif

using std::cerr;
using std::cout;
using std::endl;
using std::stringstream;
using std::vector;
using namespace H5;
namespace bill2d {

//! Class which handles the file-operations
class Datafile {
public:
    typedef std::map<std::string, DatasetInfo> Datasets;
    // Constructors & Destructors

    Datafile() = delete;

    //! Delete copy constructor
    Datafile(const Datafile&) = delete;

    explicit Datafile(ParameterList&);

    virtual ~Datafile() {}

    /*!
     * Constructor. Only for single simulation of billiard
     * \param plist Reference to the parameterlist.
     */

    // Methods for creating datasets
    /*!
       Creates a dataset
       \param dataset_name Name of the dataset in file
       \param rank Dimensions of the dataset
       \param memrank Dimensions of the data in simulation buffer
       \param dims Dimensions of the dataset in file
       \param maxdims Maximum dimensions of the dataset in file (set to H5S_UNLIMITED if unlimited in some
       dimension, i.e. we want possibility to expand the dataset in future)
       \param memdims Dimensions of the data in simulation buffer
       \param type Type of the data
       \param chuncked If saved in chuncks rather than everything at once. No harm in setting to true.
       \param compression Compresses the data slightly, no significant effect on the simulation time, set to true.
     */
    void create_dataset(
        const std::string dataset_name, const unsigned rank, const unsigned memrank, const hsize_t dims[],
        const hsize_t maxdims[], const hsize_t memdims[], const DataType& type, const bool chuncked = true,
        const bool compression = true
    );

    //! Overloading for simple use. Here memory and file buffers are set to be similar (rank, dimensions etc.)
    void create_dataset(
        const std::string dataset_name, const unsigned rank, const hsize_t dims[], const DataType& type,
        const bool compression = true
    ) {
        create_dataset(dataset_name, rank, rank, dims, dims, dims, type, false, compression);
    }  // For simple usage

    // Methods for writing attributes
    //! Writes string-attribute
    void write_attribute(const std::string attribute_name, const std::string value);
    //! Writse double attribute
    void write_attribute(const std::string attribute_name, const double);
    //! Writes unsigned attribute
    void write_attribute(const std::string attribute_name, const unsigned);
    //! Writes int attribute
    void write_attribute(const std::string attribute_name, const int);

// Define only if size_t is not unsigned, this is checked by
// the configure-script. If size_t=unsigned, overloading not allowed
#ifndef SIZE_T_EQUALS_UNSIGNED
    //! Writes size_t attribute
    void write_attribute(const std::string attribute_name, const size_t val) {
        write_attribute(attribute_name, static_cast<unsigned>(val));
    }
#endif

    //! Writes bool attribute
    void write_attribute(const std::string attribute_name, const bool);
    //! Writes pair<double> attribute
    void write_attribute(const std::string attribute_name, const pair<double>&);
    //! Writes twopairs<double> attribute
    void write_attribute(const std::string attribute_name, const twopairs<double>&);

    // General methods for writing buffers of different types
    //! Writes double*-buffer \param buffer up to \param length to dataset \param dataset_name
    void write_buffer(const std::string dataset_name, const double* buffer, size_t length = 0);
    //! Writes unsigned*-buffer up to size_t
    void write_buffer(const std::string dataset_name, const unsigned* buffer, size_t length = 0);

    // Methods for writing particle trajectories and velocities from bill2d
    //! Writes the trajectory of a certain particle
    void write_trajectory(unsigned particleID, const double* buffer, size_t length, double time);
    //! Writes the velocity of a certain particle
    void write_velocity(unsigned particleID, const double* buffer, size_t length, double time);

    // General methods for writing histograms
    //! Writes 1D histogram
    void write_buffer(const std::string dataset_name, const Hist*);

    //! Writes 2D histogram
    void write_buffer(const std::string dataset_name, const Hist2d*);

    // Methods for writing bill2d-histograms
    //! Writes position histogram of a certain particle
    void write_rhist(const unsigned particleID, const Hist2d*);
    //! Writes velocity histogram of a certain particle
    void write_vhist(const unsigned particleID, const Hist2d*);
    //! Writes speed histogram of a certain particle
    void write_shist(const unsigned particleID, const Hist*);

    //! Writes vector of vectors (which should all be the same size)
    void write_buffer(
        const std::string dataset_name, const std::vector<std::vector<double>>& vec, const bool compression = true
    );

    // Miscellaneous methods
    //! Writes bouncing map \param bmap of the particle identified by \param particleID
    void write_bmap(const unsigned particleID, const std::vector<std::vector<double>>& bmap);

    //! Writes parameters of the simulation
    void write_parameters(const ParameterList& params);

    //! Flushes all data written so far to disk (traj, vel, energies, num_particles, parameters)
    void flush() { file.flush(H5F_SCOPE_LOCAL); }

    //! Initializes the datafile for normal simulation
    virtual void initialize(ParameterList& params);

protected:
    Datafile(dont_prepare_base_tag) : scalar_space(H5S_SCALAR), pairarr(nullptr), twopairarr(nullptr) {}

#ifdef UNITTESTING
    FRIEND_TEST(Datafile, initialization);
    FRIEND_TEST(Datafile, writing_buffers);
    FRIEND_TEST(Datafile, creating_new_dataset);
#endif

    //! Datasets in the file
    Datasets fileobjects;
    //! The file
    H5File file;
    //! Root group
    Group root_group;
    //! Group for the bouncing maps
    Group bmap_group;
    //! Group for the parameters
    Group params_group;
    //! Group for the trajectories
    Group traj_group;
    //! Group for the velocities
    Group vel_group;
    //! Group for the position histograms
    Group rhist_group;
    //! Group for the velocity histograms
    Group vhist_group;
    //! Group for the speed histograms
    Group shist_group;
    //! Group for the escape time distributions
    Group escape_group;

    //! Scalar dataspace
    DataSpace scalar_space;
    DataSpace range_space;
    //! Datatype for double
    DataType double_type;
    //! Datatype for unsigned integer
    DataType biguint_type;

    // ArrayTypes, let's make them unique_ptr to not to forger to free the memory
    //! Datatype for a double pair
    std::unique_ptr<ArrayType> pairarr;
    //! Datatype for a quartet
    std::unique_ptr<ArrayType> twopairarr;

    //! Compression of the hdf5-file
    bool d_compression;

    // Buffersizes
    //! Normal buffersize
    hsize_t d_buffersize;
    //! Dimensions of 2D histogram
    hsize_t hist2d_dims[2];
    //! Dimensions of 1D histogram
    hsize_t hist_dims;

    //! Level of output
    int output_level;

    //! Number of simulations in escape rate calculations
    unsigned num_simulations;
};

//! Class which handles the file-operations for statistical data on ensemble simulations
class EscapeDatafile : public Datafile {
public:
    typedef std::map<std::string, DatasetInfo> Datasets;
    // Constructors & Destructors

    //! Delete copy constructor
    EscapeDatafile(const EscapeDatafile&) = delete;

    /*!
     * Constructor. Only for single simulation of billiard
     * \param plist Reference to the parameterlist.
     */
    explicit EscapeDatafile(ParameterList& plist);
    ~EscapeDatafile() {
#ifndef UNITTESTING
        sleep(5);
#endif
        try {
            rmLock(lockedfile);
        } catch (...) {}
    }

    //! Writes escape probability distribution
    void write_escape_events(const std::string bname, const double* buffer, const size_t length = 0);

    //! Initializes the datafile for saving escape events (and only escape events!)
    void initialize(ParameterList& params) override;

protected:
#ifdef UNITTESTING
    FRIEND_TEST(EscapeDatafile, initialization_for_escape_simulations);
#endif

    //! Set to true if the escape events are appended to an existing dataset
    bool escape_file_append;

    //! Number of simulations in escape rate calculations
    unsigned num_simulations;

    std::string lockedfile;
};

class ParallelDatafile : public Datafile {
public:
    ParallelDatafile(const ParallelDatafile&) = delete;
    ParallelDatafile(const ParallelDatafile&&) = delete;

    explicit ParallelDatafile(ParameterList& plist);
    ~ParallelDatafile() {}

    void write_initial_positions(const std::vector<std::vector<double>>& initial_positions);
    void write_squared_differences(const std::vector<std::vector<double>>& squared_differences);
    void write_psos(const unsigned int index, const CustomPSOS& custom_psos);
    void write_energy(const unsigned int index, const double* e_kin, const double* e_pot, const size_t length);

    //! Initializes the datafile for saving stuff
    virtual void initialize(ParameterList& params) override;

private:
    Group psos_group;
    Group kinetic_energy_group;
    Group potential_energy_group;
};

}  // namespace bill2d
#endif
