/*
 * custom_psos.hpp
 *
 * This file is part of bill2d.
 *
 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CUSTOM_PSOS_HPP_
#define _CUSTOM_PSOS_HPP_

#include "bill2d_config.h"

#include "datatypes.hpp"
#include "particle.hpp"
#include "rng.hpp"
#include "table.hpp"

#include <boost/numeric/odeint.hpp>

#include <functional>
#include <vector>

#ifdef UNITTESTING
#include "gtest/gtest_prod.h"
#endif

namespace bill2d {

class CustomPSOS {
public:
    CustomPSOS(
        std::function<double(const particleset&)> arg_PSOS,
        std::function<std::vector<double>(const std::vector<double>&)> arg_PSOS_gradient,
        std::function<std::vector<double>(const std::vector<double>&)> arg_PHS_to_PSOS
    );
    void evaluate(const particleset& ps, const double time);
    inline void initialize(const particleset& ps) { PSOS_value = PSOS(ps); }

    inline void reset(const particleset& ps) { initialize(ps); }

    const std::vector<std::vector<double>>& get_data() const { return PSOS_data; }

private:
    void handle_crossing(const particleset& ps, const double, const double);
    static void particleset_to_vectors(
        const particleset& particles, std::vector<double>& state, std::vector<double>& forces
    );
    std::function<double(const particleset&)> PSOS;
    std::function<std::vector<double>(const std::vector<double>&)> PSOS_gradient;
    std::function<std::vector<double>(const std::vector<double>&)> PHS_to_PSOS;
    double PSOS_value;
    std::vector<std::vector<double>> PSOS_data;
    std::function<void(const std::vector<double>& x, std::vector<double>& dxdt, const double /* t */)> transformed_eom;

#ifdef UNITTESTING
    friend class custom_psos_tests;
    friend class billiardfactory_tests;
#endif

public:
    static double linePSOS(const particleset& particles, const triplet<double> line);

    static std::vector<double> linePSOS_gradfun(const std::vector<double>& z, const triplet<double> line);

    static std::vector<double> phase_space_point_to_linePSOS_point(const std::vector<double>& z, triplet<double> line);

    static Vec2d linePSOS_to_position(const double s, const triplet<double> line);

    static Vec2d linePSOS_to_velocity(const double theta, const triplet<double> line, const double vnorm);

    static bool randomize_initial_conditions_on_linePSOS(
        particleset& particles, const RNG& rng, const Table& table, std::function<double()> epot,
        std::function<double()> ekin, const triplet<double> line, const double energy, const twopairs<double> spawnarea
    );

    static double linepsos_minimum_s(const Table& table, const RNG& rng, const triplet<double> line);

    static double linepsos_maximum_s(const Table& table, const RNG& rng, const triplet<double> line);
};
}  // namespace bill2d

#endif
