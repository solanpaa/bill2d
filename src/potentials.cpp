/*
 * potentials.cpp
 *
 * This file is part of bill2d.
 *
 * External potentials are defined in this file.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file potentials.cpp
 * \brief External potentials are implemented here.
 */

#include "potentials.hpp"
#include "helperfunctions.hpp"

namespace bill2d {

HarmonicPotential::HarmonicPotential(double str, pair<double> centerpoint) {
    d_str = str;
    d_centerpoint = {centerpoint[0], centerpoint[1]};
}

const Vec2d HarmonicPotential::force(const Vec2d& r) const { return d_str * (d_centerpoint - r); }

double HarmonicPotential::energy(const Vec2d& r) const {
    Vec2d p;
    p[0] = d_centerpoint[0];
    p[1] = d_centerpoint[1];
    return d_str * (p - r).norm() * (p - r).norm() / 2;
}

/*
 * Methods of class SoftStadium
 */

SoftStadium::SoftStadium(double n, double R, double L) {
    d_n = n;
    cpl = Vec2d(R, R);
    cpr = Vec2d(R + L, R);
    d_R_p1 = R + 1;
}

double SoftStadium::energy(const Vec2d& r) const {
    if (r[0] < cpl[0]) {
        return d_e((r - cpl).norm());
    } else if (r[0] > cpr[0]) {
        return d_e((r - cpr).norm());
    } else {
        return d_e(fabs(r[1] - cpl[1]));
    }
}

const Vec2d SoftStadium::force(const Vec2d& r) const {
    double norm = 0;
    if (r[0] < cpl[0]) {
        norm = (r - cpl).norm();
        Vec2d dir = (cpl - r);
        dir.scale_to_unit();
        return dir * d_n * pow(d_R_p1 - norm, -d_n - 1);
    } else if (r[0] > cpr[0]) {
        norm = (r - cpr).norm();
        Vec2d dir = (cpr - r);
        dir.scale_to_unit();
        return dir * d_n * pow(d_R_p1 - norm, -d_n - 1);
    } else {
        norm = fabs(r[1] - cpl[1]);
        if (r[1] >= cpl[1])
            return Vec2d(0.0, -1.0 * d_n * pow(d_R_p1 - norm, -1 - d_n));
        else
            return Vec2d(0.0, d_n * pow(d_R_p1 - norm, -1 - d_n));
    }
}

/*
 * Methods of class GaussianBath
 */

const Vec2d GaussianBath::force(__attribute__((unused)) const Vec2d& r) const {
    return Vec2d(rng.gaussian_rand(d_temp_sqr), rng.gaussian_rand(d_temp_sqr));
}

/*
 * Methods of class SoftLorentzGas
 */

SoftLorentzGas::SoftLorentzGas(triplet<double> uc_params, double radius, double sharpness, int N, bool inverted)
    : d_N(4 * N * N),
      d_radius(radius),
      d_sharpness(sharpness),
      d_inverted(inverted) {
#if (defined(HAVE_ALIGNED_ALLOC) || defined(HAVE_POSIX_MEMALIGN)) && (defined(HAVE_AVX) || defined(HAVE_SSE2))
#if defined(HAVE_AVX)
    // We have to do aligned allocation of memory due to using SIMD intrinsics
    Rx = static_cast<double*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * d_N));
    Ry = static_cast<double*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * d_N));
    ONE = static_cast<__m256d*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * 4));
    TWO = static_cast<__m256d*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * 4));
    rad = static_cast<__m256d*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * 4));
    sharp = static_cast<__m256d*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * 4));
    Rxs = (__m256d*)Rx;
    Rys = (__m256d*)Ry;
    *ONE = _mm256_set1_pd(1.0);
    *TWO = _mm256_set1_pd(2.0);
    *rad = _mm256_set1_pd(d_radius);
    *sharp = _mm256_set1_pd(d_sharpness);

#elif defined(HAVE_SSE2)  // HAVE_SSE2
    // Generate Rx, Ry
    Rx = static_cast<double*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * d_N));
    Ry = static_cast<double*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * d_N));
    Rxs = (__m128d*)Rx;
    Rys = (__m128d*)Ry;
    ONE = static_cast<__m128d*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * 2));
    TWO = static_cast<__m128d*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * 2));
    rad = static_cast<__m128d*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * 2));
    sharp = static_cast<__m128d*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * 2));
    *ONE = _mm_set1_pd(1.0);
    *TWO = _mm_set1_pd(2.0);
    *rad = _mm_set1_pd(d_radius);
    *sharp = _mm_set1_pd(d_sharpness);
#endif
#else  // No vectorization possible, either no vectorization in CPU or no aligned alloc
       // Generate Rx, Ry
    Rx = new double[d_N];
    Ry = new double[d_N];
#endif
    double dx = uc_params[1];
    double dy = uc_params[2] * sin(uc_params[0]);
    double dhx = uc_params[2] * cos(uc_params[0]);

    for (int j = -N + 1; j <= N; j++) {
        for (int i = -N + 1; i <= N; i++) {
            Rx[i + (N - 1) + (j + N - 1) * 2 * N] = i * dx + j * dhx;
            Ry[i + (N - 1) + (j + N - 1) * 2 * N] = j * dy;
        }
    }
}

const Vec2d SoftLorentzGas::force(const Vec2d& r) const {
#if (defined(HAVE_ALIGNED_ALLOC) || defined(HAVE_POSIX_MEMALIGN)) && (defined(HAVE_AVX) || defined(HAVE_SSE2))

#ifdef HAVE_AVX
    double Fx = 0;
    double Fy = 0;
    alignas(32) __m256d x, y, dx, dy, dist, div;
    x = _mm256_set1_pd(r[0]);
    y = _mm256_set1_pd(r[1]);
    AVXvecd vFx;
    vFx.vec = _mm256_set1_pd(0.0);
    AVXvecd vFy;
    vFy.vec = _mm256_set1_pd(0.0);

    for (unsigned i = 0; i < d_N / 4; i++) {
        dx = x - *(Rxs + i);
        dy = y - *(Rys + i);
        dist = _mm256_sqrt_pd(dx * dx + dy * dy);
        div = (*TWO) * (dist) * (*sharp) * (*ONE + _mm256_cosh_pd((*rad - dist) / (*sharp)));
        div = *ONE / div;
        vFx.vec += dx * div;
        vFy.vec += dy * div;
    }

    vFx.vec = _mm256_hadd_pd(vFx.vec, vFx.vec);
    vFy.vec = _mm256_hadd_pd(vFy.vec, vFy.vec);
    __m256d tmp = _mm256_permute2f128_pd(vFx.vec, vFx.vec, 1);
    vFx.vec += tmp;
    tmp = _mm256_permute2f128_pd(vFy.vec, vFy.vec, 1);
    vFy.vec += tmp;

    Fx = vFx.arr[0];
    Fy = vFy.arr[0];

#elif defined(HAVE_SSE2)
    double Fx = 0;
    double Fy = 0;
    alignas(16) __m128d x, y, dx, dy, dist, div;
    x = _mm_set1_pd(r[0]);
    y = _mm_set1_pd(r[1]);
    SSE2vecd vFx;
    vFx.vec = _mm_set1_pd(0.0);
    SSE2vecd vFy;
    vFy.vec = _mm_set1_pd(0.0);
    for (unsigned i = 0; i < d_N / 2; i++) {
        dx = x - *(Rxs + i);
        dy = y - *(Rys + i);
        dist = _mm_sqrt_pd(dx * dx + dy * dy);
        div = (*TWO) * (dist) * (*sharp) * (*ONE + _mm_cosh_pd((*rad - dist) / (*sharp)));
        div = *ONE / div;
        vFx.vec += dx * div;
        vFy.vec += dy * div;
    }

    Fx = vFx.arr[0] + vFx.arr[1];
    Fy = vFy.arr[0] + vFy.arr[1];
#endif
#else  // If we don't have vectorization
    double dist, div;

    double Fx = 0;
    double Fy = 0;
    // icpc 13.1.0 succesfully vectorizes this

    for (unsigned i = 0; i < d_N; i++) {
        dist = sqrt(pow(r[0] - Rx[i], 2) + pow(r[1] - Ry[i], 2));
        div = 2 * dist * d_sharpness * (1 + cosh((d_radius - dist) / d_sharpness));
        Fx += (r[0] - Rx[i]) / div;
        Fy += (r[1] - Ry[i]) / div;
    }
#endif
    if (d_inverted) {
        return Vec2d(-Fx, -Fy);
    }
    return Vec2d(Fx, Fy);
}

double SoftLorentzGas::energy(const Vec2d& r) const {
    double E = 0;
    double dist = 0;
    // icpc 13.1.0 succesfully vectorizes thi
    if (d_inverted) {
        for (unsigned i = 0; i < d_N; i++) {
            dist = sqrt(pow(r[0] - Rx[i], 2) + pow(r[1] - Ry[i], 2));
            // Set the base level to zero
            E += -1.0 / (1.0 + exp((dist - d_radius) / d_sharpness));
        }
        // Set the base level to zero
        E += 1.0;
    } else {
        for (unsigned i = 0; i < d_N; i++) {
            dist = sqrt(pow(r[0] - Rx[i], 2) + pow(r[1] - Ry[i], 2));
            E += 1.0 / (1.0 + exp((dist - d_radius) / d_sharpness));
        }
    }

    return E;
}

/*
 * Methods of class SoftLiebGas
 */

SoftLiebGas::SoftLiebGas(triplet<double> uc_params, double radius, double sharpness, int N)
    : d_N(5 * (2 * N + 1) * (2 * N + 1)),  // Total number of scatterers that we take into account
      d_radius(radius),
      d_sharpness(sharpness) {
#if (defined(HAVE_ALIGNED_ALLOC) || defined(HAVE_POSIX_MEMALIGN)) && (defined(HAVE_AVX) || defined(HAVE_SSE2))
#if defined(HAVE_AVX)
    // We have to do aligned allocation of memory due to using SIMD intrinsics
    Rx = static_cast<double*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * d_N));
    Ry = static_cast<double*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * d_N));
    ONE = static_cast<__m256d*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * 4));
    TWO = static_cast<__m256d*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * 4));
    rad = static_cast<__m256d*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * 4));
    sharp = static_cast<__m256d*>(bill2d_aligned_alloc(4 * sizeof(double), sizeof(double) * 4));
    Rxs = (__m256d*)Rx;
    Rys = (__m256d*)Ry;
    *ONE = _mm256_set1_pd(1.0);
    *TWO = _mm256_set1_pd(2.0);
    *rad = _mm256_set1_pd(d_radius);
    *sharp = _mm256_set1_pd(d_sharpness);

#elif defined(HAVE_SSE2)  // HAVE_SSE2
    // Generate Rx, Ry
    Rx = static_cast<double*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * d_N));
    Ry = static_cast<double*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * d_N));
    Rxs = (__m128d*)Rx;
    Rys = (__m128d*)Ry;
    ONE = static_cast<__m128d*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * 2));
    TWO = static_cast<__m128d*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * 2));
    rad = static_cast<__m128d*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * 2));
    sharp = static_cast<__m128d*>(bill2d_aligned_alloc(2 * sizeof(double), sizeof(double) * 2));
    *ONE = _mm_set1_pd(1.0);
    *TWO = _mm_set1_pd(2.0);
    *rad = _mm_set1_pd(d_radius);
    *sharp = _mm_set1_pd(d_sharpness);
#endif
#else  // No vectorization possible, either no vectorization in CPU or no aligned alloc

    // Allocate storage for x and y coordinates of the scatterers
    Rx = new double[d_N];
    Ry = new double[d_N];
#endif
    // Relative fractional position of scatterers to unit cell origin
    double R_rel[5][2] = {{0.25, 0.25}, {0.75, 0.25}, {0.5, 0.5}, {0.25, 0.75}, {0.75, 0.75}};

    // Calculate the x and y coordinates of the scatterers
    double dx = uc_params[1];
    double dy = uc_params[2] * sin(uc_params[0]);

    int Nref = (2 * N + 1) * (2 * N + 1);
    double R_ref[Nref][2];

    for (int j = -N; j <= N; j++) {
        for (int i = -N; i <= N; i++) {
            R_ref[i + N + (2 * N + 1) * (j + N)][0] = i * dx;
            R_ref[i + N + (2 * N + 1) * (j + N)][1] = j * dy;
        }
    }

    for (int i = 0; i < Nref; i++) {
        for (int j = 0; j < 5; j++) {
            Rx[5 * i + j] = R_ref[i][0] + dx * R_rel[j][0];
            Ry[5 * i + j] = R_ref[i][1] + dy * R_rel[j][1];
        }
    }

    // Shear the rectangular cell horizontally
    for (unsigned int i = 0; i < d_N; i++) {
        Rx[i] = Rx[i] + Ry[i] * tan(M_PI / 2 - uc_params[0]);
    }
}

const Vec2d SoftLiebGas::force(const Vec2d& r) const {
#if (defined(HAVE_ALIGNED_ALLOC) || defined(HAVE_POSIX_MEMALIGN)) && (defined(HAVE_AVX) || defined(HAVE_SSE2))

#ifdef HAVE_AVX
    double Fx = 0;
    double Fy = 0;
    alignas(32) __m256d x, y, dx, dy, dist, div;
    x = _mm256_set1_pd(r[0]);
    y = _mm256_set1_pd(r[1]);
    AVXvecd vFx;
    vFx.vec = _mm256_set1_pd(0.0);
    AVXvecd vFy;
    vFy.vec = _mm256_set1_pd(0.0);

    for (unsigned i = 0; i < d_N / 4; i++) {
        dx = x - *(Rxs + i);
        dy = y - *(Rys + i);
        dist = _mm256_sqrt_pd(dx * dx + dy * dy);
        div = (*TWO) * (dist) * (*sharp) * (*ONE + _mm256_cosh_pd((*rad - dist) / (*sharp)));
        vFx.vec += dx / div;
        vFy.vec += dy / div;
    }

    Fx = vFx.arr[0] + vFx.arr[1] + vFx.arr[2] + vFx.arr[3];
    Fy = vFy.arr[0] + vFy.arr[1] + vFy.arr[2] + vFy.arr[3];

    // Force for last memory location d_N%4 = 1, Rx[d_N-1] ==== *(Rx+d_N-1)
    double last_dist = sqrt(pow(r[0] - Rx[d_N - 1], 2) + pow(r[1] - Ry[d_N - 1], 2));
    double last_div = 2 * last_dist * d_sharpness * (1 + cosh((d_radius - last_dist) / d_sharpness));
    Fx += (r[0] - Rx[d_N - 1]) / last_div;
    Fy += (r[1] - Ry[d_N - 1]) / last_div;

#elif defined(HAVE_SSE2)
    double Fx = 0;
    double Fy = 0;
    alignas(16) __m128d x, y, dx, dy, dist, div;
    x = _mm_set1_pd(r[0]);
    y = _mm_set1_pd(r[1]);
    SSE2vecd vFx;
    vFx.vec = _mm_set1_pd(0.0);
    SSE2vecd vFy;
    vFy.vec = _mm_set1_pd(0.0);

    for (unsigned i = 0; i < d_N / 2; i++) {
        dx = x - *(Rxs + i);
        dy = y - *(Rys + i);
        dist = _mm_sqrt_pd(dx * dx + dy * dy);
        div = (*TWO) * (dist) * (*sharp) * (*ONE + _mm_cosh_pd((*rad - dist) / (*sharp)));
        vFx.vec += dx / div;
        vFy.vec += dy / div;
    }

    Fx = vFx.arr[0] + vFx.arr[1];
    Fy = vFy.arr[0] + vFy.arr[1];

    // Force for last memory location d_N%2 = 1
    double last_dist = sqrt(pow(r[0] - Rx[d_N - 1], 2) + pow(r[1] - Ry[d_N - 1], 2));
    double last_div = 2 * last_dist * d_sharpness * (1 + cosh((d_radius - last_dist) / d_sharpness));
    Fx += (r[0] - Rx[d_N - 1]) / last_div;
    Fy += (r[1] - Ry[d_N - 1]) / last_div;

#endif
#else  // If we don't have vectorization
    double dist, div;

    double Fx = 0;
    double Fy = 0;

    for (unsigned i = 0; i < d_N; i++) {
        dist = sqrt(pow(r[0] - Rx[i], 2) + pow(r[1] - Ry[i], 2));
        div = 2 * dist * d_sharpness * (1 + cosh((d_radius - dist) / d_sharpness));
        Fx += (r[0] - Rx[i]) / div;
        Fy += (r[1] - Ry[i]) / div;
    }
#endif
    return Vec2d(Fx, Fy);
}

double SoftLiebGas::energy(const Vec2d& r) const {
    double E = 0;
    double dist = 0;
    // icpc 13.1.0 succesfully vectorizes this
    for (unsigned i = 0; i < d_N; i++) {
        dist = sqrt(pow(r[0] - Rx[i], 2) + pow(r[1] - Ry[i], 2));
        E += 1 / (1 + exp((dist - d_radius) / d_sharpness));
    }

    return E;
}

SoftCircle::SoftCircle(ParameterList::SoftCircleType type, double softness, double eccentricity)
    : d_type(type),
      d_softness(softness),
      d_eccentricity(eccentricity),
      d_b_squared(1.0 - d_eccentricity * d_eccentricity),
      d_force_multiplier(-2.0 * softness / std::sqrt(M_PI)),
      d_pole(Vec2d(0.0, 0.0), 1.0, softness, true) {}

const Vec2d SoftCircle::force(const Vec2d& r) const {
    switch (d_type) {
        case ParameterList::SoftCircleType::FERMI_POLE: {
            return d_pole.force(r);
        }
        case ParameterList::SoftCircleType::ERROR_FUNCTION: {
            // 10.1103/PhysRevE.94.022218
            double inner_term = r[0] * r[0] + r[1] * r[1] / d_b_squared - 1.0;
            double exp_term = std::exp(-d_softness * d_softness * inner_term * inner_term);
            return Vec2d(d_force_multiplier * r[0] * exp_term, d_force_multiplier * r[1] / d_b_squared * exp_term);
        }
        default: return Vec2d(0.0, 0.0);
    }
}

double SoftCircle::energy(const Vec2d& r) const {
    switch (d_type) {
        case ParameterList::SoftCircleType::FERMI_POLE: {
            return d_pole.energy(r) + 1.0;
        }
        case ParameterList::SoftCircleType::ERROR_FUNCTION: {
            // Normalize the energy between 0 and 1
            return (std::erf(d_softness * (r[0] * r[0] + r[1] * r[1] / d_b_squared - 1.0)) + 1.0) / 2.0;
        }
        default: return 0.0;
    }
}

GaussianPotentialCluster::GaussianPotentialCluster(std::string itp2d_filename) : filename{itp2d_filename} {
    std::vector<double> itp2d_noise = hdf5_1darray_to_vector(filename, "noise_data");

    auto N_true = itp2d_noise.size() / 4;
    N = (N_true % 4 == 0) ? N_true : (N_true / 4 + 1) * 4;

    X.reserve(N);
    Y.reserve(N);
    amplitudes.reserve(N);
    widths.reserve(N);

    // Set the values
    for (unsigned i = 0; i < N_true; i++) {
        X.push_back(itp2d_noise[4 * i]);
        Y.push_back(itp2d_noise[4 * i + 1]);
        amplitudes.push_back(itp2d_noise[4 * i + 2]);
        widths.push_back(itp2d_noise[4 * i + 3]);
    }

    for (unsigned i = N_true; i < N; i++) {
        X.push_back(0);
        Y.push_back(0);
        amplitudes.push_back(0);
        widths.push_back(1);
    }
#ifdef HAVE_AVX
    s_x = reinterpret_cast<const __m256d*>(X.data());
    s_y = reinterpret_cast<const __m256d*>(Y.data());
    s_amplitudes = reinterpret_cast<const __m256d*>(amplitudes.data());
    s_widths = reinterpret_cast<const __m256d*>(widths.data());
#endif
}

const Vec2d GaussianPotentialCluster::force(const Vec2d& r) const {
#ifdef HAVE_AVX

    AVXvecd vFx, vFy, vdx, vdy, vexponent, vwidth2, vmultipl;
    vFx.vec = _mm256_set1_pd(0.0);
    vFy.vec = _mm256_set1_pd(0.0);
    AVXvecd x, y;
    x.vec = _mm256_set1_pd(r[0]);
    y.vec = _mm256_set1_pd(r[1]);

    for (unsigned i = 0; i < N / 4; i++) {
        vdx.vec = x.vec - s_x[i];
        vdy.vec = y.vec - s_y[i];
        vwidth2.vec = s_widths[i] * s_widths[i];
        vexponent.vec = -(vdx.vec * vdx.vec + vdy.vec * vdy.vec) / (_mm256_set1_pd(2.0) * vwidth2.vec);
        vmultipl.vec = _mm256_exp_pd(vexponent.vec) * s_amplitudes[i] / vwidth2.vec;
        vFx.vec += vdx.vec * vmultipl.vec;
        vFy.vec += vdy.vec * vmultipl.vec;
    }

    vFx.vec = _mm256_hadd_pd(vFx.vec, vFx.vec);
    vFy.vec = _mm256_hadd_pd(vFy.vec, vFy.vec);
    __m256d tmp = _mm256_permute2f128_pd(vFx.vec, vFx.vec, 1);
    vFx.vec += tmp;
    tmp = _mm256_permute2f128_pd(vFy.vec, vFy.vec, 1);
    vFy.vec += tmp;
    return Vec2d{vFx.arr[0], vFy.arr[0]};
#else
    Vec2d F{0.0, 0.0};

    for (unsigned i = 0; i < N; i++) {
        const double dx = r[0] - X[i];
        const double dy = r[1] - Y[i];

        const double exponent = -(dx * dx + dy * dy) / (2 * widths[i] * widths[i]);
        F[0] += dx * (exp(exponent) * amplitudes[i] / (widths[i] * widths[i]));
        F[1] += dy * (exp(exponent) * amplitudes[i] / (widths[i] * widths[i]));
    }
    return F;
#endif
}

double GaussianPotentialCluster::energy(const Vec2d& r) const {
#ifdef HAVE_AVX
    AVXvecd vE, vdx, vdy, vexponent;
    vE.vec = _mm256_set1_pd(0.0);

    for (unsigned i = 0; i < N / 4; i++) {
        vdx.vec = _mm256_set1_pd(r[0]) - s_x[i];
        vdy.vec = _mm256_set1_pd(r[1]) - s_y[i];
        vexponent.vec = -(vdx.vec * vdx.vec + vdy.vec * vdy.vec) / (_mm256_set1_pd(2.0) * s_widths[i] * s_widths[i]);
        vE.vec += _mm256_exp_pd(vexponent.vec) * s_amplitudes[i];
    }

    vE.vec = _mm256_hadd_pd(vE.vec, vE.vec);
    __m256d tmp = _mm256_permute2f128_pd(vE.vec, vE.vec, 1);
    vE.vec += tmp;

    return vE.arr[0];

#else
    double E = 0;
    for (unsigned i = 0; i < N; i++) {
        const double dx = r[0] - X.at(i);
        const double dy = r[1] - Y.at(i);

        const double exponent = -(dx * dx + dy * dy) / (2 * widths.at(i) * widths.at(i));
        E += exp(exponent) * amplitudes.at(i);
    }

    return E;
#endif
}

}  // namespace bill2d
