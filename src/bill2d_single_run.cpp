/*
 * bill2d_single_run.cpp
 *
 * This file is part of bill2d.
 *
 * A source for the main executable that calculates a single billiard simulation
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file bill2d_single_run.cpp
 * \brief What bill2d-binary does is implemented here.
 */

#include "billiard_factory.hpp"
#include "executables.hpp"
#include "timer.hpp"

#include <signal.h>

/*
 * This function runs a single billiard simulation with parameters from
 * the argument 'plist'
 */

void bill2d::single_simulation(ParameterList& plist) {
    // Generate the system, tables etc.
    BilliardFactory system(plist);

    if (system.plist.output_level >= 1) cout << "Preparing..." << endl;

    // This is a timer for the walltime
    Timer timer{};

    // Get pointers and system parameters for saving and calculations
    std::unique_ptr<Billiard> bill = system.get_bill();

    // Number of particles
    size_t num_particles = system.plist.num_particles;

    // Prepare the billiard system
    if (system.plist.initial_positions_path != "") {  // If initial positions file is given

        // Vector to hold the initial positions (including velocities)
        std::vector<std::vector<double>> inpos{};

        // This is set to true if the given file is a hdf5-file
        bool h5{false};

        // Length of the string 'plist.initial_positions_path'
        size_t len = system.plist.initial_positions_path.length();

        // Check if the filename ends with ".h5"
        if (system.plist.initial_positions_path.compare(len - 3, 3, ".h5") == 0) {
            h5 = true;
        }

        if (h5) {
            if (system.plist.continue_run)  // If we are continuing the run from the last position
                hdf52vector2d_inpos(system.plist.initial_positions_path, num_particles, inpos, LAST);
            else  // If we are taking the initial position from the trajectories in the file
                hdf52vector2d_inpos(system.plist.initial_positions_path, num_particles, inpos, FIRST);
        } else {
            // If the initial positions are given in an ASCII-file
            file2vector2d(system.plist.initial_positions_path, inpos);
        }

        if (system.plist.output_level >= 2) cout << "Setting up the system" << endl;

        // Prepare the system with the given initial position and parameters
        try {
            bill->prepare_manual(
                inpos,
                system.plist.energy,
                system.plist.ignore_energy,
                system.plist.perturbate_initial_positions,
                system.plist.perturbation_std
            );
        } catch (SignalFinish& e) {
            std::cout << e.what() << std::endl;
            return;
        }
    } else {
        // Prepare the system in a random state with the given total energy
        try {
            bill->prepare(system.plist.energy);
        } catch (SignalFinish& e) {
            std::cout << e.what() << std::endl;
            return;
        }
    }

    // Number of collisions to hard walls
    const auto& hits = bill->get_hits();

    // Time
    const auto& time = bill->get_time();

    // Maximum time (modify to better account for rounding errors; not bulletproof)
    double max_t_mod = system.plist.max_t - system.plist.delta_t / 2.;

    // This is set to true when the simulation should stop
    bool stop = false;

    // Maximum number of collisions (after this simulation stops)
    unsigned max_hits = system.plist.max_collisions;

    if (system.plist.output_level >= 1) std::cout << "Propagator: " << system.plist.propagator_string << std::endl;

    if (system.plist.output_level >= 1) std::cout << "Simulation starts." << std::endl;

    timer.start();

    // Get the pointer to the propagator

    // Whether to check for maximum time or maximum number of hits or just run indefinitely
    bool check_time_criterium = (system.plist.max_t != 0);
    bool check_hits_criterium = (max_hits != 0);

    // Run the simulation
    do {
        // First loop simulates up to minimum number of iterations
        for (size_t i = 0; i < (system.plist.minimum_number_of_iterations); i++) {
            // Propagate
            bill->propagate();

            // Check ending conditions
            if (((time >= max_t_mod) and (check_time_criterium)) or ((hits >= max_hits) and (check_hits_criterium))) {
                stop = true;
                break;
            }
        }

        // Some output every 'min_t'
        if (system.plist.output_level >= 2)
            std::cout << "T = " << time << ", collisions=" << hits << ", dE/E="
                      << fabs(1 - (bill->kinetic_energy() + bill->potential_energy()) / system.plist.energy)
                      << std::endl;

        if ((SIG_FLAG == SIGINT) or (SIG_FLAG == SIGTERM)) {
            bill->write_sigflag(SIG_FLAG);
            stop = true;
        }

    } while (!stop);

    // Stop timer
    double elapsed_time = timer.stop();

    // Some output
    cout.precision(4);
    if (system.plist.output_level >= 2) cout << "Finished in " << elapsed_time << " seconds." << endl;
    if (SIG_FLAG == 0) bill->write_sigflag(0);
}
