/*
 * timer.cpp
 *
 * This file is part of bill2d.
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file timer.cpp
 *  \brief Implementation of the class bill2d::Timer, which is used for
 *  measuring wall-clock time.
 */

#include "timer.hpp"
namespace bill2d {

void Timer::start() { start_time = std::chrono::system_clock::now(); }

double Timer::stop() {
    std::chrono::duration<double> elapsed_seconds = std::chrono::system_clock::now() - start_time;
    return elapsed_seconds.count();
}

}  // namespace bill2d
