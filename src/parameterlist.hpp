/*
 * parameterlist.hpp
 *
 * This file is part of bill2d.
 *
 * A class that contains all parameters that can be given for the program.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file parameterlist.hpp
 * \brief The parameterlist class is defined here
 */

#ifndef _PARAMETERLIST_HPP_
#define _PARAMETERLIST_HPP_

#include "datatypes.hpp"
#include "vec2d.hpp"

#include <H5Cpp.h>

#include <cmath>
#include <string>
#include <vector>

namespace bill2d {

//! class which contains all parameters of the simulation
struct ParameterList {
    //! Billiard type
    enum class BilliardType { BILLIARD, MAGNETICBILLIARD, INTERACTINGBILLIARD, INTERACTINGMAGNETICBILLIARD };
    //! Billiard table
    enum class TableType {
        NOTABLE,
        RECTANGLE,
        TRIANGLE,
        CIRCLE,
        ELLIPSE,
        SINAI,
        RING,
        STADIUM,
        MUSHROOM,
        GATE,
        LORENTZGAS
    };
    //! e-e interaction type
    enum class InteractionType { NONE, COULOMB };
    //! Propagator type
    enum class PropagatorType {
        SECOND = 2,
        THIRD = 3,
        FOURTH = 4,
        SECOND_OPTIMAL,
        FOURTH_OPTIMAL,
        SIXTH,
        BORIS,
        YHE2,
        CHIN_2A,
        CHIN_2B,
        SCOVEL,
        RK4,
        YOSHIDA4,
        SUZUKI4,
        SS6_7,
        SS6_9,
        SS8_15,
        SS8_17,
        SS4_5,
        PRK4,
        PRK6
    };
    //! Soft circle potential type
    enum class SoftCircleType { FERMI_POLE, ERROR_FUNCTION };

    ParameterList();

    ParameterList(const ParameterList&);

    //! Number of particles
    std::size_t num_particles;
    //! Magnetic field
    double B;
    //! Energy
    double energy;
    //! Strength of the interaction
    double interaction_strength;
    //! Strength of the potential
    double potential_strength;
    //! Temperature (for gaussian heat baths)
    double temperature;

    //! Activates itp2d-style gaussian bumps
    bool gaussian_bumps, gaussian_bumps_fast_itp2d;
    std::vector<Vec2d> gaussian_bumps_manual_locations;
    std::vector<double> gaussian_bumps_manual_widths;
    std::vector<double> gaussian_bumps_manual_amplitudes;
    std::string gaussian_bumps_itp2d_file;

    //! If true, ignores energy when spawning the particles
    bool ignore_energy;

    //! Use gaussian heatbath (external "potential", GaussianBath)
    bool gaussian_bath;
    //! Activates harmonic potential (HarmonicPotential)
    bool harmonic_potential;
    //! Activates bias voltage( Bias-class )
    bool bias_voltage;
    pair<double> bias_direction;
    double bias_str;

    //! Type of billiard simulation (chooses between Billiard, MagneticBilliard, InteractingBilliard and
    // InteractingMagneticBilliard)
    enum BilliardType billtype;
    //! Type of billiard table
    enum TableType tabletype;
    //! Type of particle-particle interaction
    enum InteractionType interactiontype;
    //! Propagator
    enum PropagatorType propagator;
    std::string propagator_string;

    //! Use default geometry parameters
    bool default_geometry;
    //! Parameters for the table geometry
    pair<double> geometry;
    //! Centerpoint of the potential
    pair<double> centerpoint;

    //! If true, use default spawnarea given by Table
    bool default_spawnarea;
    //! Custom spawn area
    /*! First pair = pair<double>s of lower left corner of the spawnbox, 2nd pair = coords of upper right corner of
     * the spawnbox */
    twopairs<double> spawnarea;

    //! If true, bill2d calculates the custom Poincare section
    bool calculate_custom_psos;

    //! Variables (a,b,c) defining the custom PSOS: a*x+b*y+c = 0
    triplet<double> custom_psos_line;

    // File-operation, buffer and simulation parameters
    //! Number of bins in histograms
    std::size_t bins;
    //! Timestep
    double delta_t;
    //! Minimum number of iteration time, determines buffer size
    double min_t;
    //! Maximum simulation time (in simulation units)
    double max_t;
    //! Choose how many iterations to skip while saving trajectories, velocities and energies. Does not affect
    // histograms.
    unsigned spsave;

    //! Are we using table's default holeratio
    bool default_holeratio() { return fabs(holeratio - 1 / 50.) < 1e-10; }
    //! Ratio of holes compared to the circumference of the table
    double holeratio;

    //! Velocity histogram cutoff
    double velocity_histogram_cutoff;
    //! Speed histogram cutoff
    double speed_histogram_cutoff;

    //! If true, save and calculate trajectories
    bool save_full_trajectory;
    //! If true, save and calculate velocities
    bool save_velocity;
    //! If true, save and calculate kinetic and potential energies
    bool save_energies;
    //! If true, save and calculate histograms
    bool save_histograms;
    //! If true, save and calculate particle number
    bool save_particlenum;
    //! If true, save and calculate bouncing map
    bool save_bouncemap;

    //! If true, compresses the hdf5-datasets (default = true)
    bool compression;

    //! Path to config file for the parser
    std::string config_path;

    //! Path to initial positions file (ascii or hdf5-file)
    std::string initial_positions_path;

    //! If true, read initial positions from file
    bool read_initial_positions_from_a_file;

    //! Savefilepath
    std::string savefilepath;

    //! Set to true for BilliardFactory class if nothing is to be saved
    bool save_nothing;

    //! If true, individual D-coeffs are saved
    bool individual_data;

    //! Level of output: 0,1,2
    unsigned output_level;

    //! Buffer: Minimum number of iterations to be calculated between saving points
    unsigned long long int minimum_number_of_iterations;
    //! Size of buffer
    unsigned long long int buffersize;
    //! Size of buffer
    hsize_t buffersize_ht;
    //! Size of buffer
    std::size_t buffersize_t;

    //! Calculate buffersizes
    void calculate_buffersizes();

    //! Have the buffersizes been calculated
    bool buffersizes_calculated;

    //! Whether to perturbate initial positions
    bool perturbate_initial_positions;
    //! standard deviation of the gaussian perturbation distribution (perturbation in phase space)
    double perturbation_std;

    //! Number of simulations in escape and diffusion simulations
    unsigned simulations;

    //! If true, reads initial positions from the end of the hdf5-file
    bool continue_run;

    //! Max. number of collisions
    unsigned max_collisions;

    //! Activates soft stadium potential
    bool soft_stadium_potential;
    //! Power n of the soft stadium: 1/x^n
    double soft_stadium_power;

    //! Periodicity with parallelogram unit cell
    bool periodic;

    //! Unit cell parameters, phi, L1, L2
    triplet<double> uc_params;

    //! Activates potentials giving soft Lorentz gas
    bool soft_lorentz_gas;

    //! Level of lattice sum truncation
    unsigned soft_lorentz_gas_lattice_sum_truncation;

    //! Radius of the fermi poles in Lorentz gas
    double soft_lorentz_radius;

    //! Sharpness of the fermi poles in Lorentz gas
    double soft_lorentz_sharpness;

    //! Inverts the Fermi potentials and makes them potential wells instead of bumps
    bool soft_lorentz_inverted;

    //! Enables the soft circular billiards
    bool soft_circle;

    //! The type of the soft circular billiard
    SoftCircleType soft_circle_type;

    //! The softness of the soft circular billiard
    double soft_circle_softness;

    //! The eccentricity of the soft circular billiard (only for the error function potential)
    double soft_circle_eccentricity;

    //! Phase space distance threshold for starting local optimization
    double regularity_analysis_local_optimization_threshold;

    //! Activates the soft lieb gas
    bool soft_lieb_gas;

    //! Level of lattice sum truncation
    unsigned soft_lieb_gas_lattice_sum_truncation;

    //! Radius of the fermi poles in Lieb gas
    double soft_lieb_radius;

    //! Sharpness of the fermi poles in Lieb gas
    double soft_lieb_sharpness;
};
}  // namespace bill2d
#endif  // _PARAMETERLIST_HPP_
