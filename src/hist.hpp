/*
 * hist.hpp
 *
 * This file is part of bill2d.
 *
 * A wrapper for 1D GSL histograms
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file hist.hpp
 * \brief Definition of 1D histograms
 */

#ifndef _HIST_HPP_
#define _HIST_HPP_
#include <cstdlib>
#include <gsl/gsl_histogram.h>
namespace bill2d {

//! Wrapper for 1D GSL histograms
class Hist {
public:
    Hist(std::size_t n, double min, double max);

    //! Copy constructor
    Hist(const Hist&);

    //! Move constructor
    Hist(Hist&&);
    //! Add increment bin of \Bold{x}
    inline int add(double x);
    //! Get value from bin number \Bold{i}
    inline double get(std::size_t i) const;
    //! Set a certain bin to value
    inline int set(double x, double value);
    //! Get histogram range
    inline const double* range() const;
    //! Get pointer to histogram (bin) data
    inline const double* data() const;
    //! Calculate sum of all bin values
    inline double sum() const;
    ~Hist();

private:
    gsl_histogram* h;
};

inline int Hist::set(double x, double value) { return gsl_histogram_accumulate(h, x, value); }
inline int Hist::add(double x) { return gsl_histogram_increment(h, x); }

inline double Hist::get(std::size_t i) const { return gsl_histogram_get(h, i); }

inline const double* Hist::range() const { return h->range; }

inline const double* Hist::data() const { return h->bin; }

inline double Hist::sum() const { return gsl_histogram_sum(h); }
}  // namespace bill2d
#endif
