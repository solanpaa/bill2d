/*
 * parameterlist.cpp
 *
 * This file is part of bill2d.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file parameterlist.hpp
 * \brief The parameterlist class is implemented here (mostly defines
 * the default values & how to calculate buffersizes)
 */

#include "parameterlist.hpp"

#include "helperfunctions.hpp"

#include <iostream>

namespace bill2d {

// Default values
ParameterList::ParameterList() {
    num_particles = 1;
    B = 5;
    energy = 3;
    propagator = PropagatorType::SECOND;
    propagator_string = "velocity Verlet, 2nd order";

    ignore_energy = false;
    // gausswidth=1;

    gaussian_bumps = false;
    gaussian_bumps_fast_itp2d = false;

    gaussian_bath = false;
    temperature = 0;
    harmonic_potential = false;
    bias_voltage = false;
    bias_str = 1.0;
    bias_direction = {1.0, 0.0};
    soft_stadium_potential = false;
    soft_stadium_power = 2;

    billtype = BilliardType::BILLIARD;
    tabletype = TableType::RECTANGLE;
    interactiontype = InteractionType::NONE;

    individual_data = false;

    interaction_strength = 0;
    potential_strength = 0;

    default_geometry = true;
    geometry.x = 1;
    geometry.y = 1;
    centerpoint.x = 0;
    centerpoint.y = 0;

    default_spawnarea = true;
    spawnarea.lleft.x = 0;
    spawnarea.lleft.y = 0;
    spawnarea.uright.x = 1;
    spawnarea.uright.y = 1;

    // File-operation, buffer and simulation parameters
    bins = 128;
    delta_t = 1e-5;
    min_t = 1;
    max_t = 5;
    spsave = 50;

    holeratio = 1 / 50.;

    save_nothing = false;
    velocity_histogram_cutoff = 4.0;
    speed_histogram_cutoff = 8.0;

    initial_positions_path = "";
    read_initial_positions_from_a_file = false;

    savefilepath = "data/bill2d.h5";

    calculate_custom_psos = false;
    custom_psos_line[0] = -1;
    custom_psos_line[1] = 1;
    custom_psos_line[2] = 0;

    output_level = 2;

    compression = true;
    config_path = "";

    perturbate_initial_positions = false;
    perturbation_std = 1e-4;
    simulations = 5;
    save_bouncemap = false;

    continue_run = false;
    max_collisions = 0;
    regularity_analysis_local_optimization_threshold = 5e-3;

    save_full_trajectory = true;

    save_velocity = false;
    save_energies = true;
    save_histograms = false;
    save_particlenum = false;

    periodic = false;
    uc_params[0] = M_PI / 2;
    uc_params[1] = 1;
    uc_params[2] = 1;

    soft_lorentz_gas = false;

    soft_lorentz_radius = 0.5;

    soft_lorentz_sharpness = 0.1;
    soft_lorentz_gas_lattice_sum_truncation = 1;

    soft_lorentz_inverted = false;

    soft_lieb_gas = false;

    soft_lieb_radius = 0.5;

    soft_lieb_sharpness = 0.1;
    soft_lieb_gas_lattice_sum_truncation = 1;

    soft_circle = false;
    soft_circle_type = ParameterList::SoftCircleType::FERMI_POLE;
    soft_circle_softness = 0.1;
    soft_circle_eccentricity = 0;

    buffersizes_calculated = false;

    // Set buffersizes to zero to avoid strange errors
    buffersize = 0;
    buffersize_t = 0;
    buffersize_ht = 0;
    minimum_number_of_iterations = 0;
    calculate_buffersizes();
}

// Default values
ParameterList::ParameterList(const ParameterList& copythis) {
    num_particles = copythis.num_particles;
    B = copythis.B;
    energy = copythis.energy;
    propagator = copythis.propagator;
    propagator_string = copythis.propagator_string;

    ignore_energy = copythis.ignore_energy;

    gaussian_bath = copythis.gaussian_bath;
    temperature = copythis.temperature;
    harmonic_potential = copythis.harmonic_potential;
    soft_stadium_potential = copythis.soft_stadium_potential;
    soft_stadium_power = copythis.soft_stadium_power;

    billtype = copythis.billtype;
    tabletype = copythis.tabletype;
    interactiontype = copythis.interactiontype;

    individual_data = copythis.individual_data;

    interaction_strength = copythis.interaction_strength;
    potential_strength = copythis.potential_strength;

    default_geometry = copythis.default_geometry;
    geometry.x = copythis.geometry.x;
    geometry.y = copythis.geometry.y;
    centerpoint.x = copythis.centerpoint.x;
    centerpoint.y = copythis.centerpoint.y;

    default_spawnarea = copythis.default_spawnarea;
    spawnarea.lleft.x = copythis.spawnarea.lleft.x;
    spawnarea.lleft.y = copythis.spawnarea.lleft.y;
    spawnarea.uright.x = copythis.spawnarea.uright.x;
    spawnarea.uright.y = copythis.spawnarea.uright.y;

    // File-operation, buffer and simulation parameters
    bins = copythis.bins;
    delta_t = copythis.delta_t;
    min_t = copythis.min_t;
    max_t = copythis.max_t;
    spsave = copythis.spsave;

    holeratio = copythis.holeratio;

    save_nothing = copythis.save_nothing;
    velocity_histogram_cutoff = copythis.velocity_histogram_cutoff;
    speed_histogram_cutoff = copythis.speed_histogram_cutoff;

    initial_positions_path = copythis.initial_positions_path;
    read_initial_positions_from_a_file = copythis.read_initial_positions_from_a_file;

    savefilepath = copythis.savefilepath;

    output_level = copythis.output_level;

    compression = copythis.compression;
    config_path = copythis.config_path;

    perturbate_initial_positions = copythis.perturbate_initial_positions;
    perturbation_std = copythis.perturbation_std;
    simulations = copythis.simulations;
    save_bouncemap = copythis.save_bouncemap;

    continue_run = copythis.continue_run;
    max_collisions = copythis.max_collisions;

    save_full_trajectory = copythis.save_full_trajectory;

    save_velocity = copythis.save_velocity;
    save_energies = copythis.save_energies;
    save_histograms = copythis.save_histograms;
    save_particlenum = copythis.save_particlenum;

    periodic = copythis.periodic;
    uc_params[0] = copythis.uc_params[0];
    uc_params[1] = copythis.uc_params[1];
    uc_params[2] = copythis.uc_params[2];

    soft_lorentz_gas = copythis.soft_lorentz_gas;

    soft_lorentz_radius = copythis.soft_lorentz_radius;

    soft_lorentz_sharpness = copythis.soft_lorentz_sharpness;
    soft_lorentz_gas_lattice_sum_truncation = copythis.soft_lorentz_gas_lattice_sum_truncation;
    soft_lorentz_inverted = copythis.soft_lorentz_inverted;

    soft_lieb_gas = copythis.soft_lieb_gas;

    soft_lieb_radius = copythis.soft_lieb_radius;

    soft_lieb_sharpness = copythis.soft_lieb_sharpness;
    soft_lieb_gas_lattice_sum_truncation = copythis.soft_lieb_gas_lattice_sum_truncation;

    soft_circle = copythis.soft_circle;
    soft_circle_type = copythis.soft_circle_type;
    soft_circle_softness = copythis.soft_circle_softness;
    soft_circle_eccentricity = copythis.soft_circle_eccentricity;

    buffersizes_calculated = copythis.buffersizes_calculated;

    // Set buffersizes to zero to avoid strange errors
    buffersize = copythis.buffersize;
    buffersize_t = copythis.buffersize_t;
    buffersize_ht = copythis.buffersize_ht;
    minimum_number_of_iterations = copythis.minimum_number_of_iterations;
}

// Calculate buffersizes (this needs to be called before you can start simulating!)
void ParameterList::calculate_buffersizes() {
    minimum_number_of_iterations = static_cast<unsigned long long int>(round(min_t / delta_t));
    if ((minimum_number_of_iterations % spsave != 0) and (output_level >= 2)) {
        std::cerr << "Warning: rounding min-t for better buffers.." << std::endl;
    }
    minimum_number_of_iterations = round_up(minimum_number_of_iterations, static_cast<unsigned long long int>(spsave));
    if (minimum_number_of_iterations * delta_t > 10 * min_t) {
        std::cerr << "Error: Have to change min-t over tenfold. Please consider other parameters." << std::endl;
        exit(1);
    }
    min_t = minimum_number_of_iterations * delta_t;
    buffersize =
        static_cast<unsigned long long int>((minimum_number_of_iterations / static_cast<unsigned long long int>(spsave))
        );

    buffersize++;
    buffersize_ht = static_cast<hsize_t>(buffersize);
    buffersize_t = static_cast<std::size_t>(buffersize);

    if (static_cast<unsigned long long int>(buffersize_t) != buffersize) {
        std::cerr << "Integer overflow in buffersizes. Please use smaller min-t." << std::endl;
        exit(1);
    }

    double tmp_maxiters = max_t / delta_t;
    if (fabs(
            tmp_maxiters - static_cast<double>(buffersize * static_cast<unsigned long long int>(max_t / min_t * spsave))
        ) / tmp_maxiters >
        10) {
        std::cerr
            << "Integer overflow in buffersizes. Please contact developer if you want such extremely long simulations."
            << std::endl;
        exit(1);
    }

    if (initial_positions_path != "") read_initial_positions_from_a_file = true;

    buffersizes_calculated = true;
}

}  // namespace bill2d
