/*
 * hist2d.hpp
 *
 * This file is part of bill2d.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file hist2d.cpp
 * \brief Implementation of 2D histograms
 */

#include "hist2d.hpp"
namespace bill2d {

Hist2d::Hist2d(std::size_t nx, std::size_t ny, double minx, double maxx, double miny, double maxy) {
    h = gsl_histogram2d_alloc(nx, ny);
    gsl_histogram2d_set_ranges_uniform(h, minx, maxx, miny, maxy);
}

Hist2d::Hist2d(const Hist2d& copythis) { h = gsl_histogram2d_clone(copythis.h); }

Hist2d::~Hist2d() { gsl_histogram2d_free(h); }
}  // namespace bill2d
