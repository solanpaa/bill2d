/*
 * parser.hpp
 *
 * This file is part of bill2d.
 *
 * A class that handles all command line and configuration file parsing.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file parser.hpp
 * \brief The parser class is defined here.
 */

#ifndef _PARSER_HPP_
#define _PARSER_HPP_

#include "datatypes.hpp"
#include "helperfunctions.hpp"
#include "parameterlist.hpp"

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/errors.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>

#include <exception>
#include <string>
#include <vector>

namespace po = boost::program_options;

namespace bill2d {

//! Parser for pair<T>
template <class T>
void validate(boost::any& v, const std::vector<std::string>& values, pair<T>*, int) {
    pair<T> out;

    if (values.size() != 2) throw po::invalid_option_value("Invalid pair<T> specification");

    out.x = std::stod(values.at(0));
    out.y = std::stod(values.at(1));

    v = out;
}

//! Parser for triplet<T>
template <class T>
void validate(boost::any& v, const std::vector<std::string>& values, triplet<T>*, int) {
    triplet<T> out;

    if (values.size() != 3) throw po::invalid_option_value("Invalid pair<T> specification");

    out.x = std::stod(values.at(0));
    out.y = std::stod(values.at(1));
    out.z = std::stod(values.at(2));

    v = out;
}

//! Parser for twopairs<T>
template <class T>
void validate(boost::any& v, const std::vector<std::string>& values, twopairs<T>*, int) {
    twopairs<T> out;

    if (values.size() != 4) throw po::invalid_option_value("Invalid pair<T> specification");

    out.lleft.x = std::stod(values.at(0));
    out.lleft.y = std::stod(values.at(1));
    out.uright.x = std::stod(values.at(2));
    out.uright.y = std::stod(values.at(3));

    v = out;
}

//! Class for parsing the command line and config file options
class Parser {
public:
    // Public methods
    //! Constructor
    Parser(int argc, char** argv);
    //! Parses the options to a parameterlist
    bool parse();
    //! Returns the parameterlist
    ParameterList& getParameterList() { return d_plist; };

private:
    // Different option descriptions for grouping
    //! Generic options
    po::options_description generic_options;
    //! Only command line options
    po::options_description cmdline_options;
    //! Only config file options
    po::options_description config_file_options;
    //! Options for the diffusion simulations
    po::options_description diffusion_options;
    //! Options describing the simulation
    po::options_description simulation_options;
    //! Options describing the system
    po::options_description system_options;

    // variables map and command line arguments from main
    //! Variables
    po::variables_map vm;
    //! argc from command line
    int d_argc;
    //! argv from command line
    char** d_argv;
    //! The parameterlist
    ParameterList d_plist;
};

//! A simple class for conflicting arguments.
class ConflictingArguments : public std::exception {
public:
    ConflictingArguments(std::string m) { message = std::string("Error: Conflicting arguments: ").append(m); }
    virtual const char* what() const noexcept override { return message.c_str(); }

private:
    std::string message;
};
}  // namespace bill2d
#endif  // _PARSER_HPP_
