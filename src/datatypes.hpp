/*
 * datatypes.hpp
 *
 * This file is part of bill2d.
 *
 * External potentials are defined in this file.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file datatypes.hpp
 * \brief A few custom datatypes are defined here.
 */

#ifndef _DATATYPES_HPP_
#define _DATATYPES_HPP_

#include "exceptions.hpp"
#include "hist.hpp"
#include "hist2d.hpp"

namespace bill2d {

//! Struct representing a pair of numbers
template <class Y>
struct pair {
    Y x;
    Y y;
    template <class T>
    inline Y& operator[](T i);

    template <class T>
    inline Y const& operator[](T i) const;
    pair() {
        x = 0;
        y = 0;
    }
    pair(Y a_x, Y a_y) {
        x = a_x;
        y = a_y;
    }

    bool operator==(const pair<Y>& rhs) const {
        if ((x == rhs.x) and (y == rhs.y)) return true;
        return false;
    }
    bool operator!=(const pair<Y>& rhs) const { return !((*this) == rhs); }
};

//! Struct representing a triplet
template <class Y>
struct triplet {
    Y x;
    Y y;
    Y z;
    template <class T>
    inline Y& operator[](T i);

    template <class T>
    inline Y const& operator[](T i) const;
    triplet() {
        x = 0;
        y = 0;
        z = 0;
    }
    triplet(Y a_x, Y a_y, Y a_z) {
        x = a_x;
        y = a_y;
        z = a_z;
    }
    bool operator==(const triplet<Y>& rhs) const {
        if ((x == rhs.x) and (y == rhs.y) and (z == rhs.z)) return true;
        return false;
    }
    bool operator!=(const triplet<Y>& rhs) const { return !((*this) == rhs); }
};

//! Struct representing two pairs of numbers
template <class Y>
struct twopairs {
    pair<Y> lleft;
    pair<Y> uright;

    twopairs() {
        lleft[0] = 0;
        lleft[1] = 0;
        uright[0] = 0;
        uright[1] = 0;
    }
    twopairs(Y a, Y b, Y c, Y d) {
        lleft = {a, b};
        uright = {c, d};
    }

    template <class T>
    inline Y& operator[](T i);
    template <class T>
    inline Y const& operator[](T i) const;

    bool operator==(const twopairs<Y>& rhs) const {
        if ((lleft == rhs.lleft) and (uright == rhs.uright)) return true;
        return false;
    }
    bool operator!=(const twopairs<Y>& rhs) const { return !((*this) == rhs); }
};

template <class Y>
template <class T>
inline Y& pair<Y>::operator[](T i) {
    switch (i) {
        case 0:
            return x;
            break;

        case 1:
            return y;
            break;

        default:
            throw NoSuchMember();
            break;
    }
}
template <class Y>
template <class T>
inline Y const& pair<Y>::operator[](T i) const {
    switch (i) {
        case 0:
            return x;
            break;

        case 1:
            return y;
            break;

        default:
            throw NoSuchMember();
            break;
    }
}

template <class Y>
template <class T>
inline Y& triplet<Y>::operator[](T i) {
    switch (i) {
        case 0:
            return x;
            break;

        case 1:
            return y;
            break;

        case 2:
            return z;
            break;

        default:
            throw NoSuchMember();
            break;
    }
}
template <class Y>
template <class T>
inline Y const& triplet<Y>::operator[](T i) const {
    switch (i) {
        case 0:
            return x;
            break;

        case 1:
            return y;
            break;

        case 2:
            return z;
            break;

        default:
            throw NoSuchMember();
            break;
    }
}

template <class Y>
template <class T>
inline Y& twopairs<Y>::operator[](T i) {
    switch (i) {
        case 0:
            return lleft.x;
            break;

        case 1:
            return lleft.y;
            break;

        case 2:
            return uright.x;
            break;

        case 3:
            return uright.y;
            break;

        default:
            throw NoSuchMember();
            break;
    }
}
template <class Y>
template <class T>
inline Y const& twopairs<Y>::operator[](T i) const {
    switch (i) {
        case 0:
            return lleft.x;
            break;

        case 1:
            return lleft.y;
            break;

        case 2:
            return uright.x;
            break;

        case 3:
            return uright.y;
            break;

        default:
            throw NoSuchMember();
            break;
    }
}

struct HoleRatio {
    HoleRatio(double val) : value(val) {}
    double value;
};

// Define histograms
//! Position histogram
struct rhist : public Hist2d {
    rhist(size_t bins, pair<double> geometry) : Hist2d(bins, bins, 0.0, geometry.x, 0.0, geometry.y) {}
};

//! Velocity histogram
struct vhist : public Hist2d {
    vhist(size_t bins, double velocity_histogram_cutoff)
        : Hist2d(
              bins, bins, -velocity_histogram_cutoff, velocity_histogram_cutoff, -velocity_histogram_cutoff,
              velocity_histogram_cutoff
          ) {}
};

//! Speed histogram
struct shist : public Hist {
    shist(size_t bins, double speed_histogram_cutoff) : Hist(bins, 0, speed_histogram_cutoff) {}
};

}  // namespace bill2d
#endif
