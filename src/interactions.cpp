/*
 * interactions.hpp
 *
 * This file is part of bill2d.
 *
 * Particle-particle interactions are defined in this file.
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file interactions.cpp
 * \brief The particle-particle interactions are implemented here
 */

#include "interactions.hpp"

namespace bill2d {

/*
 * Methods of class CoulombInteraction
 */

CoulombInteraction::CoulombInteraction(double strength) : d_strength(strength) {}

const Vec2d CoulombInteraction::force(const Vec2d& x, const Vec2d& y) const {
    Vec2d delta_r = x - y;
    double dist = delta_r.norm();
    return delta_r * (d_strength / (dist * dist * dist));
}

double CoulombInteraction::energy(const Vec2d& x, const Vec2d& y) const {
    Vec2d delta_r = x - y;
    return d_strength / delta_r.norm();
}

}  // namespace bill2d
