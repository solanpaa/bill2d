/*
 * bill2d_phase_space_regularity.cpp
 *
 * This file is part of bill2d.
 *
 * A source for the main executable that calculates a single billiard simulation
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file bill2d_phase_space_regularity.cpp
 * \brief What bill2d_phase_space_regularity-binary does is implemented here.
 */
#include "billiard_factory.hpp"
#include "executables.hpp"
#include "multithread_tools.hpp"
#include "rng.hpp"
#include "vec2d.hpp"

#include <boost/math/tools/roots.hpp>
#include <nlopt.hpp>

#include <signal.h>

#include <algorithm>
#include <cmath>
#include <functional>

namespace {

std::vector<std::vector<double>> find_all_distances(std::vector<std::vector<double>>& psos_data, unsigned N = 1) {
    std::vector<std::vector<double>> output;

    for (auto it = psos_data.begin(); it != psos_data.end(); it++) {
        std::vector<double> diff_sqrs;

        for (auto it2 = psos_data.begin(); it2 != psos_data.end(); it2++) {
            if (it2 != it) {
                double diff_sqr = 0.0;
                for (unsigned i = 0; i < 4 * N; i++) diff_sqr += std::pow(it->at(4 + i) - it2->at(4 + i), 2);
                if (not std::isnan(diff_sqr) and not std::isinf(diff_sqr)) diff_sqrs.push_back(diff_sqr);
            }
        }

        double mindist = std::sqrt(*std::min_element(diff_sqrs.begin() + 1, diff_sqrs.end())
        );  // The first element will correspond to the initial position

        std::vector<double> retvec = {mindist};
        retvec.insert(retvec.end(), it->begin() + 1, it->end());
        output.emplace_back(retvec);
    }

    std::sort(output.begin(), output.end(), [](auto& e1, auto& e2) { return e1.at(0) < e2.at(0); });

    return output;
}

}  // namespace

namespace bill2d {

struct target_params {
    BilliardFactory& factory;
    ParameterList& plist;
    data_container<std::vector<double>>& phase_space_data;
};

// This is the target function for local optimization
double target_checker(const std::vector<double>& x, std::vector<double>&, void* params);

void phase_space_regularity_checker_main(ParameterList& plist) {
    assert(plist.num_particles == 1);
    plist.save_nothing = true;
    plist.calculate_custom_psos = true;
    BilliardFactory factory(plist);
    assert(plist.custom_psos_line[1] != 0);  // ^^

    data_container<std::vector<double>> phase_space_data;  // s, theta, min_dist, x0, y0, vx0, vy0

    target_params t_params{factory, plist, phase_space_data};

    nlopt::opt optimizer(nlopt::LN_SBPLX, 2 /*DIM of phase space*/);
    optimizer.set_min_objective(&target_checker, &t_params);

    RNG rng;
    std::vector<double> lower_bounds{
        CustomPSOS::linepsos_minimum_s(*(factory.get_table()), rng, plist.custom_psos_line), -M_PI
    };
    std::vector<double> upper_bounds{
        CustomPSOS::linepsos_maximum_s(*(factory.get_table()), rng, plist.custom_psos_line), M_PI
    };

    optimizer.set_lower_bounds(lower_bounds);
    optimizer.set_upper_bounds(upper_bounds);

    optimizer.set_xtol_abs(1e-3);
    optimizer.set_ftol_abs(1e-5);
    optimizer.set_maxeval(200);
    optimizer.set_initial_step({0.01, 0.01});
    auto initial_position_randomizer = std::bind(
        CustomPSOS::randomize_initial_conditions_on_linePSOS,
        std::placeholders::_1,
        std::placeholders::_2,
        std::placeholders::_3,
        std::placeholders::_4,
        std::placeholders::_5,
        plist.custom_psos_line,
        plist.energy,
        plist.spawnarea
    );

    try {
        if (plist.output_level >= 1) std::cout << "Simulating random initial conditions..." << std::endl;
        for (unsigned i = 0; i < plist.simulations; i++) {
            auto ret = phase_space_trajectory_minimum_distances_within_PSOS(
                factory.get_bill(), initial_position_randomizer, plist.energy, plist.max_t
            );

            if (ret.size() != 0) {
                auto first = ret.at(0);

                if (plist.output_level >= 1)
                    std::cout << " s = " << first.at(1) << " th = " << first.at(3) << " diff = " << first.at(0)
                              << " # = " << ret.size() << std::endl;

                phase_space_data.insert(ret.begin(), ret.end());

                if (first.at(0) < plist.regularity_analysis_local_optimization_threshold) {
                    if (plist.output_level >= 1) std::cout << "Local optimization: " << std::endl;

                    std::vector<double> initial = {first.at(1), first.at(3)};
                    double target_val;
                    optimizer.optimize(initial, target_val);
                }
            } else {
                if (plist.output_level >= 1)
                    std::cout << " Initial condition not allowed, trying again..." << std::endl;
            }

            if ((SIG_FLAG == SIGINT) or (SIG_FLAG == SIGTERM)) throw(SignalFinish());
        }

    } catch (...) {}
    vector2d2file("phase_space_regularity.txt", phase_space_data.get_data());
}

std::vector<std::vector<double>> phase_space_trajectory_minimum_distances_within_PSOS(
    std::unique_ptr<Billiard> bill,
    std::function<bool(
        particleset&, const RNG& rng, const Table& table, std::function<double()> epot, std::function<double()> ekin
    )>
        inpos_fun,
    const double total_energy, const double max_t
) {
    if (!bill->prepare_custom(inpos_fun, total_energy))
        return std::vector<std::vector<double>>{};  // If the initial position is not allowed
    const unsigned N = bill->N();
    std::vector<double> inpos;
    for (unsigned i = 0; i < N; i++) {
        inpos.push_back(bill->get_initial_position(i)[0]);
        inpos.push_back(bill->get_initial_position(i)[1]);
        inpos.push_back(bill->get_initial_velocity(i)[0]);
        inpos.push_back(bill->get_initial_velocity(i)[1]);
    }
    inpos.push_back(0.0);  // time

    const auto initial_psos =
        CustomPSOS::phase_space_point_to_linePSOS_point(inpos, bill->get_plist().custom_psos_line);

    const double& t = bill->get_time();
    while (t <= max_t) bill->propagate();

    // Now, let's get the custom PSOS
    std::vector<std::vector<double>> psos_data = bill->get_custom_psos().get_data();
    if (psos_data.size() < 1) return std::vector<std::vector<double>>{};

    // Erase the very first few crossings, which may be corrupted due to approximations we make in calculating the PSOS
    const double dt = bill->get_dt();
    if (psos_data.begin()->at(0) < 5 * dt) {
        auto it = psos_data.begin();

        while (it != psos_data.end() and it->back() < 10 * dt) it++;

        psos_data.erase(psos_data.begin(), it);
    }

    if (psos_data.size() < 2) return std::vector<std::vector<double>>{};

    return find_all_distances(psos_data, N);
}

// This is the target function for local optimization
double target_checker(const std::vector<double>& x, std::vector<double>&, void* params) {
    if ((SIG_FLAG == SIGINT) or (SIG_FLAG == SIGTERM)) {
        throw(SignalFinish());
    }
    const auto set_initial_position =
        [&x](particleset& particles, const RNG&, const Table& table, std::function<double()> epot, std::function<double()>, const triplet<double> line, const double energy, const twopairs<double>) {
            assert(particles.size() == 1);
            particles[0].d_r = CustomPSOS::linePSOS_to_position(x[0], line);
            particles[0].d_r0 = particles[0].d_r;
            if (!table.point_is_inside(particles[0].d_r)) return false;
            if (epot() > energy) return false;

            double kinE = energy - epot();
            double speed = sqrt(2 * kinE);

            particles[0].d_v = CustomPSOS::linePSOS_to_velocity(/*x[1]*/ M_PI, line, speed);
            particles[0].d_v0 = particles[0].d_v;
            return true;
        };

    target_params& prms = *static_cast<target_params*>(params);

    const auto res = phase_space_trajectory_minimum_distances_within_PSOS(
        prms.factory.get_bill(),
        std::bind(
            set_initial_position,
            std::placeholders::_1,
            std::placeholders::_2,
            std::placeholders::_3,
            std::placeholders::_4,
            std::placeholders::_5,
            prms.plist.custom_psos_line,
            prms.plist.energy,
            prms.plist.spawnarea
        ),
        prms.plist.energy,
        prms.plist.max_t
    );

    if (res.size() == 0) return 100000000.0;

    prms.phase_space_data.insert(res.begin(), res.end());
    std::cout << "   s = " << x[0] << " th = " << x[1] << ": diff = " << res.at(0).at(0) << " # = " << res.size()
              << std::endl;

    return res.at(0).at(0);
}

}  // namespace bill2d
