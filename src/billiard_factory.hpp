/*
 * billiard_factory.hpp
 *
 * This file is part of bill2d.
 *
 * A nice little class that handles initialization of relevant classes
 * such as the billiard system, tables, potentials, interactions etc.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file billiard_factory.hpp
 *  \brief A header file containing the definition of the class bill2d::BilliardFactory
 *
 */

#ifndef _BILLIARD_FACTORY_HPP_
#define _BILLIARD_FACTORY_HPP_

#include "bill2d_config.h"

#include "datafile.hpp"
#include "interactions.hpp"

#include <memory>
#include <mutex>

// When compiling the class in a unit testing framework,
// include the following header for friend classes:
#ifdef UNITTESTING
#include "gtest/gtest_prod.h"
#endif

namespace bill2d {

// Forward declarations
class Billiard;
class Potential;
class Interaction;

//! \brief A class that can be used to initialize and select all the potentials, interaction,
//! propagation algorithm, tables etc. from bill2d::ParameterList.
//! IMPORTANT: NOT THREAD-SAFE, create own factory for each thread

class BaseBilliardFactory {
public:
    //! Constructor
    explicit BaseBilliardFactory(ParameterList& arg_plist) : plist(arg_plist) {}

    BaseBilliardFactory(const BaseBilliardFactory&) = delete;

    //! Destructor
    virtual ~BaseBilliardFactory() {}

    //! Get a pointer to bill2d::Billiard
    std::unique_ptr<Billiard> get_bill() {
        // For thread safe use
        std::lock_guard<std::mutex> lock(d_mutex);
        if (!d_bill) create_Billiard();
        return std::move(d_bill);
    }

    std::shared_ptr<const Table> get_table() {
        // For thread safe use
        std::lock_guard<std::mutex> lock(d_mutex);
        if (!d_bill) create_Table();
        return d_table;
    }

    //! Parameterlist
    ParameterList& plist;

protected:
// When doing unit tests, declare the following classes as friends
#ifdef UNITTESTING
    friend class billiardfactory_tests;
    friend class openbilliardfactory_tests;
#endif

    virtual void create_Billiard();
    void create_RNG();
    virtual void create_Datafile();
    virtual void create_Table() = 0;
    void create_Potential();
    void create_Interaction();
    void create_CustomPSOS();

    static unsigned d_file_nbr;
    std::mutex d_mutex;

    std::unique_ptr<const RNG> d_rng;
    std::unique_ptr<Billiard> d_bill;
    std::unique_ptr<Datafile> d_file;
    std::shared_ptr<const Table> d_table;
    std::shared_ptr<const Interaction> d_interaction;
    std::shared_ptr<const Potential> d_potential;
    std::unique_ptr<CustomPSOS> d_custom_psos;  // unique_ptr since
                                                // CustomPSOS takes care of data of a single simulation
};

//! \brief A class that can be used to initialize and select all the potentials, interaction,
//! propagation algorithm, tables etc. from bill2d::ParameterList. This uses closed tables and/or periodic systems.
//! IMPORTANT: NOT THREAD-SAFE, create own factory for each thread

class BilliardFactory : public BaseBilliardFactory {
public:
    //! Constructor
    explicit BilliardFactory(ParameterList&);

    //! Copy constructor
    BilliardFactory(BilliardFactory&);

    //! Destructor
    virtual ~BilliardFactory() {}

protected:
    void create_Table() override;
};

//! \brief A class that can be used to initialize and select all the potentials, interaction,
//! propagation algorithm, tables etc. from bill2d::ParameterList; This class uses open tables.
//! IMPORTANT: NOT THREAD-SAFE, create own factory for each thread

class OpenBilliardFactory : public BaseBilliardFactory {
public:
    //! Constructor
    explicit OpenBilliardFactory(ParameterList&);

    //! Copy constructor
    OpenBilliardFactory(OpenBilliardFactory&);

    //! Destructor
    ~OpenBilliardFactory() {}

protected:
    void create_Table() override;
};

class ParallelBilliardFactory : public BilliardFactory {
public:
    //! Constructor
    explicit ParallelBilliardFactory(ParameterList&);

    //! Destructor
    ~ParallelBilliardFactory() {}

protected:
    virtual void create_Billiard() override;
};
}  // namespace bill2d

#endif
