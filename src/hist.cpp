/*
 * hist.hpp
 *
 * This file is part of bill2d.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file hist.cpp
 * \brief Implementation of 1D histograms
 */

#include "hist.hpp"
namespace bill2d {

Hist::Hist(std::size_t n, double min, double max) {
    h = gsl_histogram_alloc(n);
    gsl_histogram_set_ranges_uniform(h, min, max);
}

Hist::Hist(const Hist& copythis) { h = gsl_histogram_clone(copythis.h); }

Hist::Hist(Hist&& movethis) {
    h = movethis.h;
    movethis.h = nullptr;
}

Hist::~Hist() {
    if (h) gsl_histogram_free(h);
}
}  // namespace bill2d
