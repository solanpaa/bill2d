/*
 * bill2d_phase_space_regularity_main.cpp
 *
 * This file is part of bill2d.
 *
 * A source for the main executable that calculates a single billiard simulation
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file bill2d_phase_space_regularity_main.cpp
 * \brief Option parsing etc. for the 'bill2d_phase_space_regularity' binary
 */

#include "executables.hpp"
#include "parser.hpp"
#include "signal_handler.hpp"

#include <signal.h>

#include <iostream>

using namespace bill2d;

int main(int argc, char** argv) {
    // Signal handler
    struct sigaction act;
    act.sa_handler = sig_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGINT, &act, nullptr);

    // Parse command line & config file
    Parser parser(argc, argv);
    bool help = parser.parse();

    // If help shown, exit
    if (help) {
        return 0;
    }

    // Get simulation parameters
    ParameterList& plist = parser.getParameterList();

    // Run the simulation
    if (plist.output_level >= 2) std::cout << "Building image of the phase space.." << std::endl;
    phase_space_regularity_checker_main(plist);

    return 0;
}
