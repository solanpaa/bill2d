/*
 * signal_handler.cpp
 *
 * This file is part of bill2d.
 *
 * A source for the main executable that calculates a single billiard simulation
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

#include "signal_handler.hpp"

#include <signal.h>

#include <iostream>
#include <mutex>

volatile int SIG_FLAG = 0;

std::once_flag sighandler_flag;

void bill2d::sig_handler(int sig) { std::call_once(sighandler_flag, sig_handler_func, std::move(sig)); }

//! Handles the keyboard signals from user
void bill2d::sig_handler_func(int sig) {
    std::cout << std::endl;
    std::string signame;
    switch (sig) {
        case (SIGINT):
            signame = "SIGINT";
            break;

        case (SIGTERM):
            signame = "SIGTERM";
            break;

        default:
            signame = "Signal ";
            signame.append(std::to_string(sig));
            break;
    }

    std::cout << signame << " caught, finishing.." << std::endl;
    SIG_FLAG = sig;
}
