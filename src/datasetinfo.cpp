/*
 * datasetinfo.cpp
 *
 * This file is part of bill2d.
 *


 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "datasetinfo.hpp"

namespace bill2d {

DatasetInfo::DatasetInfo(
    H5File& file, std::string name, unsigned arank, unsigned arank_mem, const hsize_t arg_dims[],
    const hsize_t arg_maxdims[], const hsize_t arg_memdims[], DataType arg_type, bool arg_chuncked, bool arg_compression
)
    : d_file(file) {
    d_name = name;
    compression = arg_compression;
    rank = arank;
    rank_mem = arank_mem;
    offset = std::make_unique<hsize_t[]>(rank);
    chuncksize = std::make_unique<hsize_t[]>(rank);
    maxdims = std::make_unique<hsize_t[]>(rank);
    chuncked = arg_chuncked;
    bool isinf = false;
    initialized = true;
    if (chuncked) {
        for (unsigned i = 0; i < rank; i++) {
            if (arg_maxdims[i] == H5S_UNLIMITED) {
                isinf = true;
            }
        }
        if (not isinf) throw Exception("DatasetInfo", "Set chuncked saving even if no dimension is unlimited.");
    }

    dims = std::make_unique<hsize_t[]>(rank);
    memdims = std::make_unique<hsize_t[]>(rank_mem);
    type = arg_type;
    for (unsigned i = 0; i < rank; i++) {
        offset[i] = 0;
        chuncksize[i] = arg_dims[i];
        dims[i] = arg_dims[i];
        maxdims[i] = arg_maxdims[i];
    }
    for (unsigned i = 0; i < rank_mem; i++) {
        memdims[i] = arg_memdims[i];
    }
    try {
        // Create dataspace, proplist and dataset
        fspace = DataSpace(rank, dims.get(), maxdims.get());
        memspace = DataSpace(rank_mem, memdims.get());
        if (chuncked) {
            plist.setChunk(rank, chuncksize.get());
            if (compression)  // compression only possible with chuncked datasets?
                plist.setDeflate(9);
        }

        // Create the dataset
        dset = d_file.createDataSet(name, type, fspace, plist);

    }

    catch (Exception& e) {
        e.printErrorStack();
        std::exit(1);
    }
}

// Copy-constructor
DatasetInfo::DatasetInfo(const DatasetInfo& copythis) : d_file(copythis.d_file) {
    rank = copythis.rank;
    rank_mem = copythis.rank_mem;
    type = copythis.type;
    chuncked = copythis.chuncked;
    compression = copythis.compression;
    plist = copythis.plist;

    offset = std::make_unique<hsize_t[]>(rank);
    chuncksize = std::make_unique<hsize_t[]>(rank);
    dims = std::make_unique<hsize_t[]>(rank);
    maxdims = std::make_unique<hsize_t[]>(rank);
    for (unsigned i = 0; i < rank; i++) {
        dims[i] = copythis.dims[i];
        maxdims[i] = copythis.maxdims[i];
        chuncksize[i] = copythis.chuncksize[i];
        offset[i] = copythis.offset[i];
    }

    memdims = std::make_unique<hsize_t[]>(rank_mem);
    for (unsigned i = 0; i < rank_mem; i++) {
        memdims[i] = copythis.memdims[i];
    }

    try {
        fspace.copy(copythis.fspace);
        memspace.copy(copythis.memspace);
        plist.copy(copythis.plist);

        d_name = copythis.d_name;
        // Since this is a copy-constructor, the dataset has already been created, so only open it
        dset = d_file.openDataSet(d_name);

        initialized = true;
    }

    catch (Exception& e) {
        e.printErrorStack();
        std::exit(1);
    }
}

}  // namespace bill2d
