/*
 * bill2d_parallel.cpp
 *
 * This file is part of bill2d.
 *
 * A source for executable that runs bill2d in parallel
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file bill2d_parallel.cpp
 * \brief Simulation similar to bill2d_diffusion_coefficient with support for saving anything.
 */

#include "billiard_factory.hpp"
#include "executables.hpp"

#include <omp.h>

#include <signal.h>
#include <unistd.h>

#include <chrono>

using namespace bill2d;

void bill2d::parallel_main(ParameterList& plist) {
    if (!omp_get_cancellation()) {
        std::cerr << "Warning: OpenMP thread cancellation is not enabled. "
                     "Please set OMP_CANCELLATION=true to speed up shutdown after an interrupt."
                  << std::endl;
    }

    const unsigned int ensemble_size = plist.simulations;
    const double save_delta = plist.min_t;

    ParallelBilliardFactory factory(plist);
    ParallelDatafile datafile(plist);
    datafile.initialize(plist);
    datafile.write_attribute("table_shape", factory.get_table()->shape_name());

    // Use -1 as a placeholder value to distinguish from (obviously positive) squared differences
    std::vector<std::vector<double>> squared_distance = {};
    if (plist.individual_data) {
        squared_distance = std::vector<std::vector<double>>(
            std::ceil(plist.max_t / save_delta), std::vector<double>(plist.simulations, -1)
        );
    }

    // Write the initial positions to the datafile
    std::vector<std::vector<double>> initial_positions = {};
    if (plist.initial_positions_path != "") {
        file2vector2d(plist.initial_positions_path, initial_positions);
    } else {
        initial_positions = std::vector<std::vector<double>>(ensemble_size, {0, 0, 0, 0});
    }

    auto save_callback =
        [&plist, &datafile](unsigned int id, double* e_kin, double* e_pot, Particle& part, unsigned long d_saveidx) {
#pragma omp critical
            {
                if (part.saveidx > 0) {
                    if (plist.save_full_trajectory) {
                        datafile.write_trajectory(id, part.traj, part.saveidx, part.spawntime);
                    }
                    if (plist.save_velocity) {
                        datafile.write_velocity(id, part.vel, part.saveidx, part.spawntime);
                    }
                    if (plist.save_energies) {
                        datafile.write_energy(id, e_kin, e_pot, d_saveidx);
                    }
                    part.saveidx = 0;
                }
            }
        };

    bool killed = false;
    unsigned int completed = 0;

    auto last_print = std::chrono::steady_clock::now();
    auto now = std::chrono::steady_clock::now();
    std::chrono::duration<double> diff;
    const std::chrono::seconds one_second(1);
    const std::chrono::seconds non_tty_cout_interval(10);

    // The single-line updating count is only displayed if STDOUT is a TTY
    bool is_stdout_tty = isatty(STDOUT_FILENO);

// Both are needed because #pragma omp parallel for is actually equivalent to #pragma omp for nowait
#pragma omp parallel
#pragma omp for schedule(dynamic, 1)
    for (unsigned int i = 0; i < ensemble_size; ++i) {
        if (!killed) {
            double time = 0.0;
            unsigned int time_index = 0;
            std::unique_ptr<Billiard> bill = nullptr;

// BilliardFactory is not thread-safe
#pragma omp critical
            { bill = factory.get_bill(); }

            if (plist.initial_positions_path != "") {
                std::vector<std::vector<double>> single_initpos = {initial_positions[i]};
                bill->prepare_manual(single_initpos, plist.energy, plist.ignore_energy, false, 0);
            } else {
                bill->prepare(plist.energy);
                auto init_pos = bill->get_initial_position();
                auto init_vel = bill->get_initial_velocity();
                initial_positions[i][0] = init_pos[0];
                initial_positions[i][1] = init_pos[1];
                initial_positions[i][2] = init_vel[0];
                initial_positions[i][3] = init_vel[1];
            }
            bill->set_parallel(
                true,
                std::bind(
                    save_callback,
                    i,
                    std::placeholders::_1,
                    std::placeholders::_2,
                    std::placeholders::_3,
                    std::placeholders::_4
                )
            );

            size_t iter_count = bill->get_plist().minimum_number_of_iterations;

            do {
                for (size_t j = 0; j < iter_count; ++j) {
                    bill->propagate();
                }
                if (plist.individual_data) {
                    squared_distance[time_index][i] = bill->get_difference_squared_to_initial_position();
                }
                ++time_index;
                time = time_index * save_delta;
                if (SIG_FLAG == SIGINT || SIG_FLAG == SIGTERM) {
                    killed = true;
#pragma omp cancel for
                }
            } while (time < plist.max_t && !killed);

            // Safe, because the save lambda has a critical section
            bill->save_remaining_data();

#pragma omp critical
            {
                if (plist.calculate_custom_psos) {
                    datafile.write_psos(i, bill->get_custom_psos());
                }
                Particle& particle = bill->particle();
                if (plist.save_bouncemap && particle.bmap.size() > 0) {
                    datafile.write_bmap(i, particle.bmap);
                }
                if (plist.save_histograms) {
                    datafile.write_rhist(i, particle.d_rhist);
                    datafile.write_shist(i, particle.d_shist);
                    datafile.write_vhist(i, particle.d_vhist);
                }

                ++completed;
                if (plist.output_level >= 1) {
                    now = std::chrono::steady_clock::now();
                    diff = now - last_print;
                    if (is_stdout_tty) {
                        if (diff > one_second) {
                            std::cout << "\rCompleted: " << completed << "/" << ensemble_size << std::flush;
                            last_print = now;
                        }
                    } else {
                        std::cout << "Completed: " << completed << "/" << ensemble_size << "\n";
                        if (diff >= non_tty_cout_interval) {
                            std::cout.flush();
                            last_print = now;
                        }
                    }
                }
            }
        }
    }

    if (killed) {
        datafile.write_attribute("terminating signal", SIG_FLAG);
        datafile.flush();
    }

    if (is_stdout_tty) {
        std::cout << "\rCompleted: " << completed << "/" << ensemble_size << std::endl;
    }

    if (!killed) {
        datafile.write_initial_positions(initial_positions);
        if (plist.individual_data) {
            datafile.write_squared_differences(squared_distance);
        }
    }
}
