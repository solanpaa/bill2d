/*
 * particle.hpp
 *
 * This file is part of bill2d.
 *
 * External potentials are defined in this file.
 *

 *
 * bill2d is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * bill2d is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with bill2d.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _PARTICLE_HPP_
#define _PARTICLE_HPP_

#include "datatypes.hpp"
#include "vec2d.hpp"

#include <vector>

namespace bill2d {

//! a subclass representing a single particle
class Particle {
public:
    //! Constructor
    /*!
     \param plist Pointer to parameterlist
     \param aID Id given for the particle
     \param time Time at the creation of the particle
     \param rhist_lims Limits for the position histogram
     */
    Particle(
        unsigned arg_ID = 0, double time = 0.0, pair<double> rhist_lims = {1.0, 1.0}, bool save_trajectory = false,
        bool save_velocity = false, bool save_histograms = false, bool save_bouncemap = false, std::size_t bins = 1,
        double velocity_histogram_cutoff = 1, double speed_histogram_cutoff = 1, unsigned buffersize = 1
    );
    Particle(Particle&&);  // Move constructor
    inline void operator=(Particle&) = delete;
    void operator=(Particle&& op);
    Particle(Particle&) = delete;
    ~Particle();
    //! get position
    inline const Vec2d& r() const { return d_r; }

    //! get velocity
    inline const Vec2d& v() const { return d_v; }

    //! get force acting on the particle
    inline const Vec2d& F() const { return d_F; }

    //! set position
    inline void set_r(double x, double y) {
        d_r[0] = x;
        d_r[1] = y;
    }

    //! set position
    inline void set_r(Vec2d new_r) { d_r = new_r; }

    //! set velocity
    inline void set_v(double vx, double vy) {
        d_v[0] = vx;
        d_v[1] = vy;
    }

    //! set velocity
    inline void set_v(Vec2d new_v) { d_v = new_v; }

    //! set force
    inline void set_F(double Fx, double Fy) {
        d_F[0] = Fx;
        d_F[1] = Fy;
    }

    //! set force
    inline void set_F(Vec2d new_F) { d_F = new_F; }

    //! get speed of the particle
    double velocity() const { return d_v.norm(); }

    //! position
    Vec2d d_r;

    //! initial_position
    Vec2d d_r0;

    //! velocity
    Vec2d d_v;

    //! initial_velocity
    Vec2d d_v0;

    //! the external force on the particle
    Vec2d d_F;

    //! Self-explanatory
    double spawntime;

    //! Trajectory buffer
    double* traj;

    //! Velocity buffer
    double* vel;

    //! Pointer to position histogram
    rhist* d_rhist;
    //! Pointer to velocity histogram
    vhist* d_vhist;
    //! Pointer to speed histogram
    shist* d_shist;

    //! Pointer to bmap (or nullptr)
    std::vector<std::vector<double>>* bmap_ptr;

    //! In periodic systems: which cell
    pair<int> d_cell;

    //! The index of the next datapoint in buffers
    unsigned saveidx;

    //! Unique particle identifier
    unsigned ID;

    //! Bouncing map
    std::vector<std::vector<double>> bmap;

};  // end of subclass Particle

// typedefs
//! Contains pointers to particles
typedef std::vector<Particle> particleset;
//! iterator for particleset
typedef particleset::iterator particle_iterator;
//! const_iterator for particleset
typedef particleset::const_iterator const_particle_iterator;

}  // namespace bill2d

#endif  // _PARTICLE_HPP_
