/*
 * datafile.cpp
 *
 * This file is part of bill2d.
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file datafile.cpp
 * \brief Output to HDF5-files is via the Datafile-class, which is implemented here.
 */

#include "datafile.hpp"

namespace bill2d {

// Constructor for Datafile
Datafile::Datafile(ParameterList& plist) : scalar_space(H5S_SCALAR), pairarr(nullptr), twopairarr(nullptr) {
    try {
        H5::Exception::dontPrint();
        const hsize_t pair_dims[1] = {2};
        const hsize_t twopair_dims[1] = {4};
        double_type = PredType::NATIVE_DOUBLE;
        biguint_type = PredType::NATIVE_UINT;

        pairarr = std::make_unique<ArrayType>(double_type, 1, pair_dims);
        twopairarr = std::make_unique<ArrayType>(double_type, 1, twopair_dims);
        try {
            file = H5File(plist.savefilepath, H5F_ACC_TRUNC);
        } catch (H5::FileIException& e) {
            std::cerr << "Warning: Creation of savefile failed, trying to create the path..." << std::endl;
            boost::filesystem::path path(plist.savefilepath);
            boost::filesystem::create_directories(path.parent_path());
            file = H5File(plist.savefilepath, H5F_ACC_TRUNC);
        }
        root_group = file.openGroup("/");
    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

// Constructor for Datafile
EscapeDatafile::EscapeDatafile(ParameterList& plist) : Datafile(dont_prepare_base_tag{}), escape_file_append{false} {
    try {
        H5::Exception::dontPrint();

        const hsize_t pair_dims[1] = {2};
        const hsize_t twopair_dims[1] = {4};
        double_type = PredType::NATIVE_DOUBLE;
        biguint_type = PredType::NATIVE_UINT;

        pairarr = std::make_unique<ArrayType>(double_type, 1, pair_dims);
        twopairarr = std::make_unique<ArrayType>(double_type, 1, twopair_dims);

        // Check if file contains escape simulations, and if so (and calculating escape rates)
        // then append to that file

        if (not boost::filesystem::exists(plist.savefilepath)) {  // The savefile does not exist
            try {
                createLock(plist.savefilepath);
                file = H5File(plist.savefilepath, H5F_ACC_TRUNC);
            } catch (H5::FileIException& e) {
                std::cerr << "Warning: Creation of savefile failed, trying to create the path..." << std::endl;
                boost::filesystem::path path(plist.savefilepath);
                boost::filesystem::create_directories(path.parent_path());
                createLock(plist.savefilepath);
                file = H5File(plist.savefilepath, H5F_ACC_TRUNC);
            }
            lockedfile = plist.savefilepath;
        } else {  // The savefile exists

            // Check for lock
            while (lockExists(plist.savefilepath)) sleep(5);
            createLock(plist.savefilepath);

            // Check if the savefile is for escape simulations
            bool has_escape_data = true;
            try {
                auto tmpfile = new H5File(plist.savefilepath, H5F_ACC_RDONLY);
                Group tmproot_group = tmpfile->openGroup("/");
                hid_t loc_id = tmproot_group.getLocId();

                if (H5Lexists(loc_id, "escape_events", 0) == false) {
                    has_escape_data = false;
                }

                tmproot_group.close();
                delete tmpfile;
            } catch (...) {
                has_escape_data = false;
            }

            if (has_escape_data) {
                lockedfile = plist.savefilepath;

                file = H5File(plist.savefilepath, H5F_ACC_RDWR);
                escape_file_append = true;
            } else {
                rmLock(plist.savefilepath);

                auto last_basename_index = plist.savefilepath.find_last_of(".");
                std::string filename(plist.savefilepath);

                if (last_basename_index == std::string::npos)
                    filename.append("_new");
                else
                    filename = filename.substr(0, last_basename_index).append("_new.h5");

                while (lockExists(filename)) sleep(5);
                createLock(filename);

                lockedfile = filename;
                std::cerr << "Warning: Given savefile exists and does not contain escape simulation data." << std::endl
                          << "Saving to " << filename << " instead" << std::endl;
                createLock(filename);
                file = H5File(filename, H5F_ACC_TRUNC);
            }
        }
        root_group = file.openGroup("/");

    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

// Creating a dataobject
void Datafile::create_dataset(
    const std::string dataset_name, const unsigned rank, const unsigned memrank, const hsize_t dims[],
    const hsize_t maxdims[], const hsize_t memdims[], const DataType& type, const bool chuncked, const bool compression
) {
    std::pair<Datasets::iterator, bool> res = fileobjects.insert(std::make_pair(
        dataset_name,
        DatasetInfo(file, dataset_name, rank, memrank, dims, maxdims, memdims, type, chuncked, compression)
    ));
    if (!res.second) {
        throw Exception("bill2d: create_dataset", "A dataset with that name already exists");
    }
}

// Methods for writing attributes
void Datafile::write_attribute(const std::string attribute_name, const std::string value) {
    try {
        std::string tmp;
        if (value.size() == 0)
            tmp = "none";
        else
            tmp = value;
        DataType string_type = StrType(0, tmp.size() + 1);
        Attribute attr = params_group.createAttribute(attribute_name, string_type, scalar_space);
        attr.write(string_type, tmp.c_str());

    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_attribute(const std::string attribute_name, const bool value) {
    try {
        if (value)
            write_attribute(attribute_name, static_cast<int>(1));
        else
            write_attribute(attribute_name, static_cast<int>(0));
    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_attribute(const std::string attribute_name, const double value) {
    try {
        Attribute attr = params_group.createAttribute(attribute_name, double_type, scalar_space);
        attr.write(double_type, &value);
    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_attribute(const std::string attribute_name, const pair<double>& value) {
    try {
        Attribute attr = params_group.createAttribute(attribute_name, *pairarr, scalar_space);
        auto tmp = new double[2];
        *tmp = value.x;
        *(tmp + 1) = value.y;
        attr.write(*pairarr, tmp);
        delete[] tmp;
    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_attribute(const std::string attribute_name, const twopairs<double>& value) {
    try {
        Attribute attr = params_group.createAttribute(attribute_name, *twopairarr, scalar_space);
        auto tmp = new double[4];
        *tmp = value.lleft.x;
        *(tmp + 1) = value.lleft.y;
        *(tmp + 2) = value.uright.x;
        *(tmp + 3) = value.uright.y;

        attr.write(*twopairarr, tmp);
        delete[] tmp;
    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_attribute(const std::string attribute_name, const int value) {
    try {
        Attribute attr = params_group.createAttribute(attribute_name, PredType::NATIVE_INT, scalar_space);
        attr.write(PredType::NATIVE_INT, &value);
    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_attribute(const std::string attribute_name, const unsigned value) {
    try {
        Attribute attr = params_group.createAttribute(attribute_name, PredType::NATIVE_UINT, scalar_space);
        attr.write(PredType::NATIVE_UINT, &value);
    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_rhist(const unsigned particleID, const Hist2d* histogram) {
    stringstream particleName;
    particleName << "particle" << particleID;
    create_dataset(
        std::string("/position_histograms/").append(particleName.str()),
        2,
        2,
        hist2d_dims,
        hist2d_dims,
        hist2d_dims,
        double_type,
        false,
        false
    );
    write_buffer(std::string("/position_histograms/").append(particleName.str()), histogram);
}

void Datafile::write_vhist(const unsigned particleID, const Hist2d* histogram) {
    stringstream particleName;
    particleName << "particle" << particleID;
    create_dataset(
        std::string("/velocity_histograms/").append(particleName.str()),
        2,
        2,
        hist2d_dims,
        hist2d_dims,
        hist2d_dims,
        double_type,
        false,
        false
    );
    write_buffer(std::string("/velocity_histograms/").append(particleName.str()), histogram);
}

void Datafile::write_shist(const unsigned particleID, const Hist* histogram) {
    stringstream particleName;
    particleName << "particle" << particleID;
    create_dataset(
        std::string("/speed_histograms/").append(particleName.str()),
        1,
        1,
        &hist_dims,
        &hist_dims,
        &hist_dims,
        double_type,
        false,
        false
    );
    write_buffer(std::string("/speed_histograms/").append(particleName.str()), histogram);
}

void Datafile::write_buffer(const std::string dataset_name, const Hist* data) {
    try {
        DatasetInfo& tmp = fileobjects.at(dataset_name);
        tmp.fspace.selectAll();
        tmp.dset.write(data->data(), tmp.type, tmp.memspace, tmp.fspace);
        Attribute rattr = tmp.dset.createAttribute("range", double_type, range_space);
        rattr.write(double_type, data->range());

    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_buffer(const std::string dataset_name, const Hist2d* data) {
    try {
        DatasetInfo& tmp = fileobjects.at(dataset_name);
        tmp.fspace.selectAll();
        tmp.dset.write(data->data(), tmp.type, tmp.memspace, tmp.fspace);
        Attribute xattr = tmp.dset.createAttribute("xrange", double_type, range_space);
        xattr.write(double_type, data->xrange());
        Attribute yattr = tmp.dset.createAttribute("yrange", double_type, range_space);
        yattr.write(double_type, data->yrange());
    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_buffer(const std::string dataset_name, const double* buffer, const size_t nbr_points) {
    try {
        DatasetInfo& tmp = fileobjects.at(dataset_name);
        if ((tmp.maxdims[0] <= (tmp.offset[0] + static_cast<hsize_t>(nbr_points))))
            throw Exception("write_buffer: double", "Trying to write larger buffer than maximum length of the dataset");

        if (tmp.chuncked) {
            for (unsigned i = 0; i < tmp.rank; i++) {
                if ((nbr_points != 0) and (i == 0))
                    tmp.dims[0] = tmp.offset[0] + static_cast<hsize_t>(nbr_points);
                else
                    tmp.dims[i] = tmp.offset[i] + tmp.chuncksize[i];
            }

            tmp.dset.extend(tmp.dims.get());
        }

        hsize_t tmp2 = tmp.chuncksize[0];
        if (nbr_points != 0) tmp.chuncksize[0] = nbr_points;
        DataSpace tempspace = tmp.dset.getSpace();
        hsize_t tmp3 = tmp.memdims[0];
        if (nbr_points != 0) {
            tmp.memdims[0] = nbr_points;
            tmp.memspace = DataSpace(tmp.rank_mem, tmp.memdims.get());
        }

        tempspace.selectHyperslab(H5S_SELECT_SET, tmp.chuncksize.get(), tmp.offset.get());
        tmp.dset.write(buffer, tmp.type, tmp.memspace, tempspace);
        if (tmp.chuncked) {
            for (unsigned i = 0; i < tmp.rank; i++) {
                if (tmp.maxdims[i] == H5S_UNLIMITED) {
                    tmp.offset[i] += tmp.chuncksize[i];
                }
            }
        }
        if (nbr_points != 0) {
            tmp.chuncksize[0] = tmp2;
            tmp.memdims[0] = tmp3;
        }

    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_buffer(const std::string dataset_name, const unsigned* buffer, const size_t nbr_points) {
    try {
        DatasetInfo& tmp = fileobjects.at(dataset_name);

        if ((tmp.maxdims[0] <= (tmp.offset[0] + static_cast<hsize_t>(nbr_points))))
            throw Exception(
                "write_buffer: unsigned", "Trying to write larger buffer than maximum length of the dataset"
            );
        if (tmp.chuncked) {
            for (unsigned i = 0; i < tmp.rank; i++) {
                if ((nbr_points != 0) and (i == 0))
                    tmp.dims[0] = tmp.offset[0] + static_cast<hsize_t>(nbr_points);
                else
                    tmp.dims[i] = tmp.offset[i] + tmp.chuncksize[i];
            }

            tmp.dset.extend(tmp.dims.get());
        }

        hsize_t tmp2 = tmp.chuncksize[0];
        if (nbr_points != 0) tmp.chuncksize[0] = nbr_points;
        DataSpace tempspace = tmp.dset.getSpace();
        hsize_t tmp3 = tmp.memdims[0];
        if (nbr_points != 0) {
            tmp.memdims[0] = nbr_points;
            tmp.memspace = DataSpace(tmp.rank_mem, tmp.memdims.get());
        }

        tempspace.selectHyperslab(H5S_SELECT_SET, tmp.chuncksize.get(), tmp.offset.get());
        tmp.dset.write(buffer, tmp.type, tmp.memspace, tempspace);
        if (tmp.chuncked) {
            for (unsigned i = 0; i < tmp.rank; i++) {
                if (tmp.maxdims[i] == H5S_UNLIMITED) {
                    tmp.offset[i] += tmp.chuncksize[i];
                }
            }
        } else {
            for (unsigned i = 0; i < tmp.rank; i++) tmp.offset[i] += tmp.dims[i];
        }
        if (nbr_points != 0) {
            tmp.chuncksize[0] = tmp2;
            tmp.memdims[0] = tmp3;
        }

    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_buffer(
    const std::string dataset_name, const std::vector<std::vector<double>>& buffer, const bool compression
) {
    try {
        hsize_t fdims[2];
        if (buffer.size() != 0) {
            fdims[0] = buffer.size();
            fdims[1] = buffer.at(0).size();
        } else {
            fdims[0] = 0;
            fdims[1] = 0;
        }

        hsize_t memdims = fdims[0];
        hsize_t chunckdims[] = {fdims[0], 1};
        // Create dataspaces and dataset
        DataSpace fspace(2, fdims);
        DataSpace memspace(1, &memdims);

        DSetCreatPropList plist;
        plist.setChunk(2, chunckdims);
        if (compression) {
            plist.setDeflate(9);
        }
        DataSet dset = root_group.createDataSet(dataset_name.c_str(), double_type, fspace, plist);

        std::vector<double> tmp_buffer(fdims[0], 0.0);

        hsize_t offset[] = {0, 0};
        // Write each column at a time
        if (buffer.size() != 0) {
            for (size_t i = 0; i < fdims[1]; ++i) {
                std::transform(buffer.begin(), buffer.end(), tmp_buffer.begin(), [&i](const auto& vec) {
                    return vec[i];
                });
                fspace.selectHyperslab(H5S_SELECT_SET, chunckdims, offset);
                dset.write(tmp_buffer.data(), double_type, memspace, fspace);
                ++offset[1];
            }
        }

    } catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

void Datafile::write_bmap(const unsigned particleID, const std::vector<std::vector<double>>& bmap) {
    stringstream particle_name;
    particle_name << "/bounce maps/particle" << particleID;
    write_buffer(particle_name.str(), bmap);
}

// Save parameters
void Datafile::write_parameters(const ParameterList& params) {
    if (params.periodic) {
        write_attribute("periodic", true);
        write_attribute("uc_phi", params.uc_params[0]);
        write_attribute("uc_L1", params.uc_params[1]);
        write_attribute("uc_L2", params.uc_params[2]);
    } else
        write_attribute("periodic", false);

    if (params.soft_lorentz_gas) {
        write_attribute("soft-lorentz-gas", true);
        write_attribute("soft-lorentz-radius", params.soft_lorentz_radius);
        write_attribute("soft-lorentz-sharpness", params.soft_lorentz_sharpness);
        write_attribute("soft-lorentz-inverted", params.soft_lorentz_inverted);
    } else
        write_attribute("soft-lorentz-gas", false);

    if (params.soft_lieb_gas) {
        write_attribute("soft-lieb-gas", true);
        write_attribute("soft-lieb-radius", params.soft_lieb_radius);
        write_attribute("soft-lieb-sharpness", params.soft_lieb_sharpness);
    } else
        write_attribute("soft-lieb-gas", false);

    if (params.soft_circle) {
        write_attribute("soft-circle", true);
        write_attribute("soft-circle-softness", params.soft_circle_softness);
        write_attribute("soft-circle-eccentricity", params.soft_circle_eccentricity);
        switch (params.soft_circle_type) {
            case ParameterList::SoftCircleType::FERMI_POLE:
                write_attribute("soft-circle-type", std::string("FERMI_POLE"));
                break;
            case ParameterList::SoftCircleType::ERROR_FUNCTION:
                write_attribute("soft-circle-type", std::string("ERROR_FUNCTION"));
                break;
            default:
                std::cerr << "Error: While saving soft-circle-type, not defined." << std::endl;
                break;
        }
    } else {
        write_attribute("soft-circle", false);
    }

    write_attribute("num_particles", params.num_particles);
    write_attribute("B_field", params.B);
    write_attribute("energy", params.energy);
    write_attribute("interaction_strength", params.interaction_strength);
    write_attribute("potential_strength", params.potential_strength);
    write_attribute("ignore_energy", params.ignore_energy);

    write_attribute("Harmonic potential", params.harmonic_potential);
    switch (params.billtype) {
        case ParameterList::BilliardType::BILLIARD:
            write_attribute("BILLTYPE", std::string("BILLIARD"));
            break;

        case ParameterList::BilliardType::MAGNETICBILLIARD:
            write_attribute("BILLTYPE", std::string("MAGNETICBILLIARD"));
            break;

        case ParameterList::BilliardType::INTERACTINGBILLIARD:
            write_attribute("BILLTYPE", std::string("INTERACTINGBILLIARD"));
            break;

        case ParameterList::BilliardType::INTERACTINGMAGNETICBILLIARD:
            write_attribute("BILLTYPE", std::string("INTERACTINGMAGNETICBILLIARD"));
            break;

        default:
            std::cerr << "Error: While saving billtype, not defined." << endl;
            break;
    }

    switch (params.interactiontype) {
        case ParameterList::InteractionType::NONE:
            write_attribute("INTERACTION", std::string("NONE"));
            break;

        case ParameterList::InteractionType::COULOMB:
            write_attribute("INTERACTION", std::string("COULOMB"));
            break;

        default:
            std::cerr << "Error: While saving interactiontype, not defined." << endl;
            break;
    }

    write_attribute("geometry", params.geometry);
    write_attribute("centerpoint", params.centerpoint);
    write_attribute("spawnarea", params.spawnarea);

    write_attribute("bins", params.bins);
    write_attribute("delta_t", params.delta_t);
    write_attribute("min_t", params.min_t);
    write_attribute("max_t", params.max_t);
    write_attribute("sparsesave", params.spsave);

    write_attribute("velocity_histogram_cutoff", params.velocity_histogram_cutoff);
    write_attribute("speed_histogram_cutoff", params.speed_histogram_cutoff);

    write_attribute("save_full_trajectory", params.save_full_trajectory);
    write_attribute("save_velocity", params.save_velocity);

    write_attribute("compression", params.compression);
    write_attribute("config_path", params.config_path);

    write_attribute("initial_positions_path", params.initial_positions_path);
    write_attribute("read_initial_positions_from_a_file", params.read_initial_positions_from_a_file);

    write_attribute("savefilepath", params.savefilepath);
    write_attribute("output_level", params.output_level);
    write_attribute("perturbate_initial_positions", params.perturbate_initial_positions);
    write_attribute("perturbation_std", params.perturbation_std);
    write_attribute("save_bouncemap", params.save_bouncemap);
    write_attribute("max_collisions", params.max_collisions);
    write_attribute("temperature", params.temperature);
}

// Initialize groups, types and pointers
void Datafile::initialize(ParameterList& params) {
    if (not params.buffersizes_calculated) params.calculate_buffersizes();

    output_level = params.output_level;
    params_group = root_group.createGroup("/parameters");
    if (params.save_full_trajectory) traj_group = root_group.createGroup("/trajectories");
    if (params.save_velocity) vel_group = root_group.createGroup("/velocities");
    if (params.save_bouncemap) bmap_group = root_group.createGroup("/bounce maps");

    d_compression = params.compression;

    d_buffersize = params.buffersize_ht;
    if (params.save_histograms) {
        rhist_group = root_group.createGroup("/position_histograms");
        vhist_group = root_group.createGroup("/velocity_histograms");
        shist_group = root_group.createGroup("/speed_histograms");
    }

    hist2d_dims[0] = params.bins;
    hist2d_dims[1] = params.bins;
    hist_dims = params.bins;

    hsize_t range_size[] = {params.bins + 1};
    hsize_t e_maxdims[] = {H5S_UNLIMITED};
    range_space.setExtentSimple(1, range_size);
    if (params.save_energies) {
        create_dataset(
            "/kinetic_energy", 1, 1, &d_buffersize, e_maxdims, &d_buffersize, double_type, true, params.compression
        );
        create_dataset(
            "/potential_energy", 1, 1, &d_buffersize, e_maxdims, &d_buffersize, double_type, true, params.compression
        );
    }
    if (params.save_particlenum)
        create_dataset(
            "/particle_num", 1, 1, &d_buffersize, e_maxdims, &d_buffersize, biguint_type, true, params.compression
        );

    write_parameters(params);
}

void EscapeDatafile::initialize(ParameterList& params) {
    output_level = params.output_level;
    num_simulations = params.simulations;
    if (!escape_file_append) {  // if new file, create 'escape_events' -group
        try {
            escape_group = root_group.createGroup("/escape_events");
            params_group = root_group.createGroup("/parameters");

        } catch (Exception& e) {
            e.printErrorStack();
            exit(1);
        }

    } else {  // else open existing group
        try {
            escape_group = root_group.openGroup("/escape_events");
            params_group = root_group.openGroup("/parameters");

        } catch (Exception& e) {
            e.printErrorStack();
            exit(1);
        }
    }
    file.flush(H5F_SCOPE_GLOBAL);
    if (not escape_file_append) write_parameters(params);
}

void ParallelDatafile::initialize(ParameterList& params) {
    if (not params.buffersizes_calculated) params.calculate_buffersizes();

    output_level = params.output_level;
    params_group = root_group.createGroup("/parameters");
    if (params.save_full_trajectory) traj_group = root_group.createGroup("/trajectories");
    if (params.save_velocity) vel_group = root_group.createGroup("/velocities");
    if (params.save_bouncemap) bmap_group = root_group.createGroup("/bounce maps");

    d_compression = params.compression;

    d_buffersize = params.buffersize_ht;
    if (params.save_histograms) {
        rhist_group = root_group.createGroup("/position_histograms");
        vhist_group = root_group.createGroup("/velocity_histograms");
        shist_group = root_group.createGroup("/speed_histograms");
    }

    hist2d_dims[0] = params.bins;
    hist2d_dims[1] = params.bins;
    hist_dims = params.bins;

    hsize_t range_size[] = {params.bins + 1};
    hsize_t e_maxdims[] = {H5S_UNLIMITED};
    range_space.setExtentSimple(1, range_size);

    if (params.save_particlenum)
        create_dataset(
            "/particle_num", 1, 1, &d_buffersize, e_maxdims, &d_buffersize, biguint_type, true, params.compression
        );

    if (params.calculate_custom_psos) {
        psos_group = root_group.createGroup("custom_psos");
    }
    if (params.save_energies) {
        kinetic_energy_group = root_group.createGroup("kinetic_energy");
        potential_energy_group = root_group.createGroup("potential_energy");
    }

    write_parameters(params);
}

// Write velocity of the particle
void Datafile::write_velocity(const unsigned particleID, const double* buffer, const size_t length, const double time) {
    std::string trajName("/velocities");
    // check if wanted dataset already exists
    bool exists = false;
    stringstream particleName;
    particleName << "particle" << particleID;
    hid_t loc_id = vel_group.getLocId();
    if (H5Lexists(loc_id, particleName.str().c_str(), 0) == true) {
        exists = true;
    }

    hsize_t dims = static_cast<hsize_t>(length);
    hsize_t maxdims = H5S_UNLIMITED;
    // If the dataset doesn't exist, create it
    if (!exists) {
        create_dataset(
            std::string(trajName).append("/").append(particleName.str()),
            1,
            1,
            &dims,
            &maxdims,
            &dims,
            *pairarr,
            true,
            false
        );
    }
    // write
    write_buffer(std::string(trajName).append("/").append(particleName.str()), buffer, length);
    // write spawntime
    if (!exists) {
        DatasetInfo& tmp = fileobjects.at(std::string(trajName).append("/").append(particleName.str()));
        Attribute attr = tmp.dset.createAttribute("spawntime", double_type, scalar_space);
        attr.write(double_type, &time);
    }
}

// Write trajectory of the particle
void Datafile::write_trajectory(
    const unsigned particleID, const double* buffer, const size_t length, const double time
) {
    std::string trajName("/trajectories");
    // check if wanted dataset already exists
    bool exists = false;
    stringstream particleName;
    particleName << "particle" << particleID;
    hid_t loc_id = traj_group.getLocId();
    if (H5Lexists(loc_id, particleName.str().c_str(), 0) == true) {
        exists = true;
    }

    hsize_t dims = static_cast<hsize_t>(length);
    hsize_t maxdims = H5S_UNLIMITED;
    if (!exists) {
        create_dataset(
            std::string(trajName).append("/").append(particleName.str()),
            1,
            1,
            &dims,
            &maxdims,
            &dims,
            *pairarr,
            true,
            false
        );
    }
    write_buffer(std::string(trajName).append("/").append(particleName.str()), buffer, length);
    if (!exists) {
        DatasetInfo& tmp = fileobjects.at(std::string(trajName).append("/").append(particleName.str()));
        Attribute attr = tmp.dset.createAttribute("spawntime", double_type, scalar_space);
        attr.write(double_type, &time);
    }
}

void EscapeDatafile::write_escape_events(const std::string bname, const double* buffer, const size_t length) {
    try {
        hsize_t memdims[1] = {0};
        if (length != 0)
            memdims[0] = {static_cast<hsize_t>(length)};
        else
            memdims[0] = {static_cast<hsize_t>(num_simulations)};
        hsize_t fmaxdims[] = {H5S_UNLIMITED};

        stringstream name;
        name << "/escape_events/" << bname;

        std::string name_str = name.str();

        DataSet dset;
        DataSpace dspace;
        DataSpace memspace = DataSpace(1, memdims);
        hsize_t offset[1];
        hsize_t fdims[1];

        // check if wanted dataset already exists
        bool exists = false;
        if (escape_file_append) {
            hid_t loc_id = escape_group.getLocId();
            if (H5Lexists(loc_id, bname.c_str(), 0) == true) {
                exists = true;
            }
        }

        // if dataset already exists, append
        if (exists) {
            dset = file.openDataSet(name_str);
            dspace = dset.getSpace();
            dspace.getSimpleExtentDims(fdims, nullptr);
            offset[0] = fdims[0];
            fdims[0] += memdims[0];

            dset.extend(fdims);
            dspace = dset.getSpace();  // reload dataspace now with extended dimensions
            dspace.selectHyperslab(H5S_SELECT_SET, memdims, offset);
            dset.write(buffer, double_type, memspace, dspace);
        }  // else create new
        else {
            create_dataset(name_str, 1, 1, memdims, fmaxdims, memdims, double_type, true, true);
            write_buffer(name_str, buffer);
        }

    }

    catch (Exception& e) {
        e.printErrorStack();
        exit(1);
    }
}

ParallelDatafile::ParallelDatafile(ParameterList& plist) : Datafile(plist) {
    // If the ensemble size is large (and thus many datasets are created under a single group), the default metadata
    // cache (MDC) size won't be enough and performance plummets once the in-memory metadata cache is full. The absolute
    // maximum cache size is defined in HDF5 headers, and modifying it means that the whole library needs to be
    // recompiled. However, we can disable cache evictions altogether, which will dramatically increase memory usage
    // while preserving performance. The default maximum cache size is 32 MiB, which by default can be raised to
    // 128 MiB, which is the maximum maximum (not typo) cache size without recompiling the entire library. For more
    // information, see e.g. https://confluence.hdfgroup.org/display/HDF5/Metadata+Caching+in+HDF5
    // The magic threshold numbers for ensemble sizes have been obtained via empirical testing with the HDF5 library.
    if (plist.simulations >= 2500000) {
        H5AC_cache_config_t config;
        H5Fget_mdc_config(file.getId(), &config);
        if (plist.simulations >= 10000000) {
            std::cout << "WARNING: Disabling the HDF5 Metadata cache evictions due to very large ensemble size ("
                      << plist.simulations
                      << " >= 10 000 000). This *will* increase memory usage *significantly* while saving. "
                      << "For example, saving 20M datasets requires about 100 GiB extra memory. "
                      << "You have been warned." << std::endl;
            config.evictions_enabled = false;
            config.incr_mode = H5C_cache_incr_mode::H5C_incr__off;
            config.flash_incr_mode = H5C_cache_flash_incr_mode::H5C_flash_incr__off;
            config.decr_mode = H5C_cache_decr_mode::H5C_decr__off;
        } else if (plist.simulations >= 5500000) {
            config.set_initial_size = true;
            config.initial_size = 128 * 1024 * 1024;
            config.max_size = 128 * 1024 * 1024;
            config.min_size = 128 * 1024 * 1024;
        } else {
            config.set_initial_size = true;
            config.initial_size = 64 * 1024 * 1024;
            config.max_size = 64 * 1024 * 1024;
            config.min_size = 64 * 1024 * 1024;
        }
        H5Fset_mdc_config(file.getId(), &config);
    }
}

void ParallelDatafile::write_initial_positions(const std::vector<std::vector<double>>& initial_positions) {
    write_buffer("/initial_positions", initial_positions, true);
}

void ParallelDatafile::write_squared_differences(const std::vector<std::vector<double>>& squared_differences) {
    write_buffer("/sd", squared_differences, false);
}

void ParallelDatafile::write_psos(const unsigned int index, const CustomPSOS& custom_psos) {
    const auto& data = custom_psos.get_data();
    if (data.size() == 0) {
        return;
    }
    write_buffer("/custom_psos/particle" + std::to_string(index), custom_psos.get_data(), false);
}

void ParallelDatafile::write_energy(
    const unsigned int index, const double* e_kin, const double* e_pot, const size_t length
) {
    std::string kin_name = "/kinetic_energy";
    std::string pot_name = "/potential_energy";
    // check if wanted dataset already exists
    bool exists = false;
    stringstream particleName;
    particleName << "particle" << index;
    hid_t loc_id = kinetic_energy_group.getLocId();
    if (H5Lexists(loc_id, particleName.str().c_str(), 0) > 0) {
        exists = true;
    }

    hsize_t dims = static_cast<hsize_t>(length);
    hsize_t maxdims = H5S_UNLIMITED;
    // If the dataset doesn't exist, create it
    if (!exists) {
        create_dataset(
            (kin_name + "/" + particleName.str()).c_str(), 1, 1, &dims, &maxdims, &dims, double_type, true, false
        );
        create_dataset(
            (pot_name + "/" + particleName.str()).c_str(), 1, 1, &dims, &maxdims, &dims, double_type, true, false
        );
    }
    // write
    write_buffer((kin_name + "/" + particleName.str()).c_str(), e_kin, length);
    write_buffer((pot_name + "/" + particleName.str()).c_str(), e_pot, length);
}

}  // namespace bill2d
