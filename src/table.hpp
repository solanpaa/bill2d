/*
 * table.hpp
 *
 * This file is part of bill2d.
 *
 * The hard-wall billiard tables, open billiard tables, table components,
 * and periodic unit-cells are defined in this file.
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file table.hpp
 * \brief The hard-wall billiard tables, open billiard tables, table components,
 * and periodic unit-cells are defined in this file.
 */

#ifndef _TABLE_HPP_
#define _TABLE_HPP_

#include "bill2d_config.h"

#include "datatypes.hpp"
#include "vec2d.hpp"

#include <boost/math/special_functions/ellint_2.hpp>

#include <cassert>
#include <cmath>
#include <functional>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#ifdef UNITTESTING
#include "gtest/gtest_prod.h"
#endif

using boost::math::ellint_2;

namespace bill2d {

//! Interface class for billiard tables.
class Table {
public:
    //! Returns the name (and relevant parameters) of the billiard table.
    virtual const std::string shape_name() const = 0;

    /*! \brief A method for handling boundary crossings without calculating the collision map.
     * A method for handling boundary crossings without calculating the collision map.
     * Returns true if a collision happens.
     *  \param r position of the particle
     *  \param v velocity of the particle
     *  \param cell unit cell of the particle (relevant only for periodic systems)
     *  \param bmap reference to the bouncing map of the particle
     *  \param time current simulation time
     *  \param hits reference to variable counting the total number of collisions with the boundaries
     */
    inline bool handle_boundary_crossings(
        Vec2d& r, Vec2d& v, pair<int>& cell, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
        bool& hit_flag
    ) const;

    //! A method for checking whether the particle is inside the billiard table (i.e. in the 'allowed' area).
    //! \param r position of the particle
    virtual bool point_is_inside(const Vec2d& r) const = 0;

    virtual ~Table() {}

    //! Return the maximum x and y coordinates of the allowed area inside the billiard table.
    pair<double> getMaxCoords() const { return maxCoords; }

protected:
    //! A parameter for the maximum x and y coordinates of the allowed area inside the billiard table
    pair<double> maxCoords;

    //! Function which handles the collision with the boundary
    virtual bool handle_collisions(
        Vec2d& r, Vec2d& v, pair<int>& cell, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
        bool& hit_flag
    ) const = 0;
};

inline bool Table::handle_boundary_crossings(
    Vec2d& r, Vec2d& v, pair<int>& cell, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
    bool& hit_flag
) const {
    if (point_is_inside(r)) return false;
    handle_collisions(r, v, cell, bmap, time, hits, hit_flag);
    assert(point_is_inside(r));  // Safeguard agains poorly implemented tables
    return true;
}

//! A rectangular table with lower left corner at the origin
class Rectangle : public Table {
public:
    /*!
     * A constructor.
     * \param w width of the rectangle
     * \param h height of the rectangle
     */
    explicit Rectangle(double w = 1.0, double h = 1.0);
    const std::string shape_name() const override { return d_shape_name.str(); }

    inline bool point_is_inside(const Vec2d& r) const override;

protected:
    bool handle_collisions(
        Vec2d& r, Vec2d& v, __attribute__((unused)) pair<int>& cell, std::vector<std::vector<double>>* bmap,
        const double time, unsigned& hits, __attribute__((unused)) bool& hit_flag
    ) const override;
    std::ostringstream d_shape_name;

    //! Width
    double d_w;

    //! Height
    double d_h;
};

inline bool Rectangle::point_is_inside(const Vec2d& r) const {  // this returns true if the point is in the allowed zone
    if (0 <= r[0] and r[0] <= d_w and 0 <= r[1] and r[1] <= d_h)
        return true;
    else
        return false;
}

//! Table without boundaries, i.e., no table!
class NoTable : public Table {
public:
    NoTable(double maxx, double maxy) {
        maxCoords.x = maxx;
        maxCoords.y = maxy;
    }
    NoTable() {
        maxCoords.x = 1.0;
        maxCoords.y = 1.0;
    }
    const std::string shape_name() const override { return "NoTable"; }

    inline bool point_is_inside(__attribute__((unused)) const Vec2d&) const override { return true; }

protected:
    bool
    handle_collisions(Vec2d&, Vec2d&, __attribute__((unused)) pair<int>&, std::vector<std::vector<double>>*, const double, unsigned&, __attribute__((unused)) bool&)
        const override {
        return false;
    };
};

//! Table which is a combination of subtables
class CompoundShape : public Table {
protected:
    class BoundaryShape;

public:
    typedef std::vector<std::unique_ptr<BoundaryShape>> shape_set;

    typedef shape_set::const_iterator shape_iterator;

    const std::string shape_name() const override { return d_shape_name.str(); };

    inline bool point_is_inside(const Vec2d& r) const override;

    virtual ~CompoundShape();

protected:
    bool handle_collisions(
        Vec2d& r, Vec2d& v, __attribute__((unused)) pair<int>& cell, std::vector<std::vector<double>>* bmap,
        const double time, unsigned& hits, bool& hit_flag
    ) const override;

#ifdef UNITTESTING
    friend class compoundshape_tests;
#endif
    //! Shapes that do not contribute to bouncing maps
    shape_set shapes;

    //! Shapes that contribute to bouncing maps
    shape_set bshapes;
    std::ostringstream d_shape_name;

    //! A subclass representing a general boundary curve
    class BoundaryShape {
    public:
        /*!
         * \param arg_s0 zero value of the arc length
         * \param arg_hitFlags reference to the hitflags of the bill2d::CompoundShape
         */
        explicit BoundaryShape(double arg_s0) : d_s0(arg_s0) {}

        /*! \brief Check's if the particle is inside the table with respect to this particular boundary curve.
         *  \param r position of the particle
         */
        virtual bool point_is_inside(const Vec2d& r) const = 0;

        /*! \brief Handles crossing with the bill2d::CompoundShape::BoundaryShape and records bouncing map
         * \param r Position
         * \param v Velocity
         * \param bmap Bounce map
         * \param time Time
         * \param hits Reference to collision counter
         */
        virtual bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            __attribute__((unused)) bool& hit_flag
        ) const = 0;

        virtual ~BoundaryShape() {}

    protected:
        double d_s0;
    };

    //! \brief A derived subclass representing a boundary line \f$Ax+By+C=0\f$
    class BoundaryLine : public BoundaryShape {
    public:
        //! Describes whether the particle is forbidden in the upper or lower section of the line
        enum Direction { ForbidUpper, ForbidLower };

        /*!
         * \param arg_dir Direction, which is forbidden area for the particle
         * \param arg_A \f$Ax+By+C=0\f$
         * \param arg_B \f$Ax+By+C=0\f$
         * \param arg_C \f$Ax+By+C=0\f$
         * \param arg_s0 Value of arc length at position arg_r0
         * \param arg_r0 Position on the line for fixing the arc length
         * \param arg_hitFlags hitflags for informing about certain collisions
         */
        BoundaryLine(enum Direction arg_dir, double arg_A, double arg_B, double arg_C, double arg_s0, Vec2d arg_r0);

        inline bool point_is_inside(const Vec2d& r) const override;
        bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            __attribute__((unused)) bool& hit_flag
        ) const override;

    protected:
        enum Direction dir;
        double A;
        double B;
        double C;
        Vec2d unormal;
        Vec2d utangent;
        double invsqrAB;  // 1/sqrt(A²+B²)
        Vec2d d_r0;
    };

    //! \brief A derived subclass representing a boundary line \f$y=y_0\f$
    class BoundaryHorizontalLine : public BoundaryShape {
    public:
        enum Direction { ForbidUpper, ForbidLower };
        /*!
         * \param arg_dir Direction, which is forbidden area for the particle
         * \param y Vertical position of the line
         * \param arg_s0 Value of arc length at position (arg_x0,y)
         * \param arg_x0 x-coordinate of the line for fixing the arc length
         * \param arg_hitFlags hitflags for informing about certain collisions
         */
        BoundaryHorizontalLine(enum Direction arg_dir, double y, double arg_s0, double arg_x0);
        inline bool point_is_inside(const Vec2d& r) const override;
        bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            __attribute__((unused)) bool& hit_flag
        ) const override;

    protected:
        enum Direction dir;
        double pos_y;
        double beg_x;
    };

    //! \brief A horizontal ray (= half-line)
    class BoundaryHorizontalRay : public BoundaryShape {
    public:
        enum Direction { ForbidUpper, ForbidLower };
        enum InfiniteDirection { Left, Right };

        /*!
         * \param arg_dir Direction, which is forbidden area for the particle
         * \param arg_dir2 Direction to which the line extends to infinity
         * \param x x-coordinate of the beginning/end of the ray
         * \param y Vertical position of the ray
         * \param arg_s0 Value of arc length at position (x,y)
         * \param arg_hitFlags hitflags for informing about certain collisions
         */
        BoundaryHorizontalRay(
            enum Direction arg_dir, enum InfiniteDirection arg_dir2, double x, double y, double arg_s0
        );

        inline bool point_is_inside(const Vec2d& r) const override;
        bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            __attribute__((unused)) bool& hit_flag
        ) const override;

    protected:
        enum Direction dir;
        enum InfiniteDirection dir2;
        double pos_x;
        double pos_y;
    };

    //! \brief A circle
    class BoundaryCircle : public BoundaryShape {
    public:
        enum ArcLengthZero { ADown, ARight };
        enum Direction { ForbidInside, ForbidOutside };
        enum IsHalved { No, Left, Top, Right, Bottom };
        enum IncreasingSDirection { CLOCKWISE, COUNTERCLOCKWISE };
        /*!
         * \param arg_dir Particle allowed either inside or outside the circle
         * \param arg_center Center of the circle
         * \param arg_radius Radius of the circle
         * \param arg_halved How tha circle is halved
         * \param arg_s0 Value of the arc length at the arc length origin set by 'arg_origin'
         * \param arg_hitFlags hitflags for informing about certain collisions
         * \param arg_origin Arc length origin (either right or down)
         * \param arg_sdir Direction to which arc length increases
         */
        BoundaryCircle(
            Direction arg_dir, Vec2d arg_center, double arg_radius, IsHalved arg_halved, double arg_s0,
            ArcLengthZero arg_origin, IncreasingSDirection arg_sdir = COUNTERCLOCKWISE
        );
        inline bool point_is_inside(const Vec2d& r) const override;
        bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            __attribute__((unused)) bool& hit_flag
        ) const override;

    protected:
        enum Direction dir;
        enum IncreasingSDirection sdir;
        enum IsHalved halved;
        Vec2d center;
        double radius;
        double invradsqr;  // radius^-2
        ArcLengthZero origin;
    };

    //! An ellipse
    class BoundaryEllipse : public BoundaryShape {
    public:
        /*!
         * \param arg_a Length of the horizontal semi-axis
         * \param arg_b Length of the vertical semi-axis
         * \param arg_s0 Value of the arc length at (0,b)
         * \param arg_hitFlags hitflags for informing about certain collisions
         */
        BoundaryEllipse(double arg_a, double arg_b, double arg_s0);
        inline bool point_is_inside(const Vec2d& r) const override;
        bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            __attribute__((unused)) bool& hit_flag
        ) const override;

    protected:
        double a, b, e, major_axis, s_integral_fix;
    };

    //! A rectangle (note that this is a bill2d::BoundaryShape, cf. bill2d::Rectangle)
    class BoundaryRectangle : public BoundaryShape {
    public:
        enum Direction { ForbidInside, ForbidOutside };
        // enum IncreasingSDirection { CLOCKWISE, COUNTERCLOCKWISE };
        /*!
         * \param arg_dir Whether to forbid particles inside or outside the rectangle
         * \param arg_minx x-coordinate of the left wall
         * \param arg_maxx x-coordinate of the right wall
         * \param arg_miny y-coordinate of the bottom wall
         * \param arg_maxy y-coordinate of the top wall
         * \param arg_s0 Value of arc length at lower left corner
         * \param arg_hitFlags hitflags for informing about certain collisions
         * \param arg_sdir Direction of increasing arclength
         */
        BoundaryRectangle(
            Direction arg_dir, double arg_minx, double arg_maxx, double arg_miny, double arg_maxy, double arg_s0
        );
        inline bool point_is_inside(const Vec2d& r) const override;
        bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            __attribute__((unused)) bool& hit_flag
        ) const override;

    protected:
        enum Direction dir;
        double minx;
        double maxx;
        double miny;
        double maxy;
        double d_w;
        double d_h;
    };

    //! A rectangle corner forbidding inside points
    class BoundaryRectangleCorner : public BoundaryShape {
    public:
        enum Direction { TopLeft, TopRight, BottomLeft, BottomRight };
        /*!
         * \param arg_dir Which corner (top left, top right, etc.)
         * \param x x-coordinate of the corner
         * \param y y-coordinate of the corner
         * \param arg_s0 Arc length value at the corner
         * \param arg_hitFlags hitflags for informing about certain collisions
         */
        BoundaryRectangleCorner(Direction arg_dir, double x, double y, double arg_s0);
        inline bool point_is_inside(const Vec2d& r) const override;
        virtual bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            __attribute__((unused)) bool& hit_flag
        ) const override;

    protected:
        enum Direction dir;
        double pos_x;
        double pos_y;
    };

    //! Open boundaries, they support multiple evenly spaced holes
    class OpenBoundaryRectangle : public CompoundShape::BoundaryRectangle {
    public:
        /*!
         \param dir BoundaryRectangle::ForbidInside or BoundaryRectangle::ForbidOutside, determines the allowed zone
         is inside or outside the rectangle.
         \param leftX x-coordinate of the left side
         \param rightX x-coordinate of the right side
         \param bottomY y-coordinate of the bottom side
         \param topY y-coordinate of the top side
         \param holeWidth width of the holes
         \param holePeriodicity length of the boundary which contains only one hole (after this length there will be
         another hole, etc.)
         \param lowerLeftBoundaryLengthValue boundary length at the lower left corner, calculated counter-clockwise
         \param collisionFlags vector of boolean values, the first of which is true after first collision with one
         of the holes
         */
        OpenBoundaryRectangle(
            Direction dir, double leftX, double rightX, double bottomY, double topY, double holeWidth,
            double holePeriodicity, double lowerLeftBoundaryLengthValue
        );

        bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            bool& hit_flag
        ) const override;

    private:
        double d_holeWidth, d_holePeriodicity;
    };

    class OpenBoundaryCircle : public CompoundShape::BoundaryCircle {
    public:
        /*!
         * \param arg_dir Particle allowed either inside or outside the circle
         * \param arg_center Center of the circle
         * \param arg_radius Radius of the circle
         * \param arg_halved How tha circle is halved
         * \param arg_l hole width
         * \param arg_L hole periodicity
         * \param arg_s0 Value of the arc length at the arc length origin set by 'arg_origin'
         * \param arg_hitFlags hitflags for informing about certain collisions
         * \param arg_origin Arc length origin (either right or down)
         */
        OpenBoundaryCircle(
            Direction arg_dir, Vec2d arg_center, double arg_radius, IsHalved, double arg_l, double arg_L, double arg_s0,
            ArcLengthZero arg_origin
        );
        bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            bool& hit_flag
        ) const override;

    private:
        double d_l, d_L;
    };

    class OpenBoundaryHorizontalLine : public CompoundShape::BoundaryHorizontalLine {
    public:
        /*!
         * \param arg_dir Direction, which is forbidden area for the particle
         * \param y Vertical position of the line
         * \param arg_s0 Value of arc length at position (arg_x0,y)
         * \param arg_x0 x-coordinate of the line for fixing the arc length
         * \param arg_l hole width
         * \param arg_L hole periodicity
         * \param arg_hitFlags hitflags for informing about certain collisions
         */
        OpenBoundaryHorizontalLine(
            enum Direction arg_dir, double y, double arg_s0, double arg_x0, double arg_l, double arg_L
        );
        bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            bool& hit_flag
        ) const override;

    private:
        double d_l, d_L;
    };

    class OpenBoundaryRectangleCorner : public CompoundShape::BoundaryRectangleCorner {
    public:
        /*!
         * \param arg_dir Which corner (top left, top right, etc.)
         * \param x x-coordinate of the corner
         * \param y y-coordinate of the corner
         * \param arg_s0 Arc length value at the corner
         * \param arg_l hole width
         * \param arg_L hole periodicity
         * \param arg_hitFlags hitflags for informing about certain collisions
         */
        OpenBoundaryRectangleCorner(Direction arg_dir, double x, double y, double arg_s0, double arg_l, double arg_L);
        bool handle_crossing(
            Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
            bool& hit_flag
        ) const override;

    private:
        double d_l, d_L;
    };
};

inline bool CompoundShape::point_is_inside(const Vec2d& r) const {
    for (const auto& shape : shapes)
        if (!shape->point_is_inside(r)) return false;
    for (const auto& bshape : bshapes) {
        if (!bshape->point_is_inside(r)) return false;
    }
    return true;
}

//! \brief Base class for tables of periodic systems.
//! Only parallellogram unit cell currently supported.
class CompoundShapePeriodic : public CompoundShape {
public:
    //! \param unit_cell defines the unit cell. \param s_1 is the arc length at r=(0,0) (lower side) and \param s_2
    // is the arc length for lower right corner (right side).
    CompoundShapePeriodic(triplet<double> unit_cell, double s_1, double s_2);

    inline bool point_is_inside(const Vec2d& r) const override;

protected:
#ifdef UNITTESTING
    friend class compoundshape_tests;
#endif

    //! Checks if moved out of the current unit cell
    inline bool is_inside_cell(const Vec2d& r) const;

    //! Handles change of cell and calculates also the bouncing map
    bool handle_cell(
        Vec2d& r, const Vec2d& v, pair<int>& cell, std::vector<std::vector<double>>* bmap, const double time,
        bool& hit_flag
    ) const;

    bool handle_collisions(
        Vec2d& r, Vec2d& v, pair<int>& cell, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits,
        bool& hit_flag
    ) const override;
    /*!
    These parameters define the parallellogram unit cell. The boundary on the left hand side is: -a_l*x+y > 0
    On right hand side -x+d_Br*y+d_L1 < 0
    d_alpha is the angle of between the right (and left) boundary and x-axis
    \param d_L1 length of the horizontal side
    \param d_L2 length of the vertical side
    \param d_uh height of the parallellogram
    \param d_hd displacement of the parallellogram in horizontal direction
    \param d_s1 is the value of arc length at lower left corner
    \param d_s2 is the value of arc length at lower right corner
    */
    double d_Al, d_Br, d_L1, d_L2, d_uh, d_hd, d_s1, d_s2, d_alpha, d_shelp1, d_shelp2;
    //! Normalized tangent of the boundary on rhs
    Vec2d d_right_boundary_tangent;  // This is normalized

    std::function<bool(const Vec2d&)> over_right_boundary;
    std::function<bool(const Vec2d&)> over_left_boundary;
};

inline bool CompoundShapePeriodic::is_inside_cell(const Vec2d& r) const {
    if (r[1] > d_uh) {
        return false;
    }
    if (r[1] < 0) {  // To lower cell
        return false;
    }
    if (over_right_boundary(r)) {  // To right cell
        return false;
    }
    if (over_left_boundary(r)) {  // To left cell
        return false;
    }
    return true;
}

inline bool CompoundShapePeriodic::point_is_inside(const Vec2d& r) const {
    for (const auto& shape : shapes)
        if (!shape->point_is_inside(r)) return false;
    for (const auto& bshape : bshapes) {
        if (!bshape->point_is_inside(r)) return false;
    }
    if (!is_inside_cell(r)) return false;
    return true;
}

// point_is_inside returns whether the particle is in the allowed zone (true), not whether it's actually inside the
// shape.
inline bool CompoundShape::BoundaryRectangle::point_is_inside(const Vec2d& r) const {
    // If ForbidOutside
    if (dir == ForbidOutside) {
        if (r[0] < minx || r[0] > maxx) return false;
        if (r[1] < miny || r[1] > maxy) return false;
    } else {  // If ForbidInside
        if ((r[0] < maxx) && (r[0] > minx) && (r[1] < maxy) && (r[1] > miny)) return false;
    }
    return true;
}

inline bool CompoundShape::BoundaryCircle::point_is_inside(const Vec2d& r) const {
    const Vec2d d = r - center;
    if (dir == ForbidOutside) {
        if (d.norm() >= radius) {
            if (halved == No or (halved == Left and d[0] <= 0) or (halved == Right and d[0] >= 0) or
                (halved == Top and d[1] >= 0) or (halved == Bottom and d[1] <= 0))
                return false;
        }
    } else if (d.norm() < radius) {
        if (halved == No or (halved == Left and d[0] <= 0) or (halved == Right and d[0] >= 0) or
            (halved == Top and d[1] >= 0) or (halved == Bottom and d[1] <= 0))
            return false;
    }
    return true;
}

inline bool CompoundShape::BoundaryEllipse::point_is_inside(const Vec2d& r) const {
    if (pow(r[0] / a, 2) + pow(r[1] / b, 2) - 1 < 0) return true;

    return false;
}

inline bool CompoundShape::BoundaryHorizontalRay::point_is_inside(const Vec2d& r) const {
    if (dir2 == Left) {
        if (r[0] > pos_x) return true;
    } else if (r[0] < pos_x)
        return true;
    if (dir == ForbidUpper) {
        if (r[1] > pos_y) return false;
    } else if (r[1] < pos_y)
        return false;
    return true;
}

inline bool CompoundShape::BoundaryHorizontalLine::point_is_inside(const Vec2d& r) const {
    if (dir == ForbidUpper) {
        if (r[1] > pos_y) return false;
    } else if (r[1] < pos_y)
        return false;
    return true;
}
inline bool CompoundShape::BoundaryLine::point_is_inside(const Vec2d& r) const {
    if (dir == ForbidUpper) {
        if (A * r[0] + B * r[1] + C > 0)
            return false;
        else
            return true;
    } else {
        if (A * r[0] + B * r[1] + C < 0)
            return false;
        else
            return true;
    }
}

inline bool CompoundShape::BoundaryRectangleCorner::point_is_inside(const Vec2d& r) const {
    switch (dir) {
        case TopLeft:
            if (r[0] > pos_x and r[1] < pos_y) return false;
            break;
        case TopRight:
            if (r[0] < pos_x and r[1] < pos_y) return false;
            break;
        case BottomLeft:
            if (r[0] > pos_x and r[1] > pos_y) return false;
            break;
        case BottomRight:
            if (r[0] < pos_x and r[1] > pos_y) return false;
            break;
    }
    return true;
}

//! A class representing a scaled equilateral triangle
class Triangle : public CompoundShape {
public:
    explicit Triangle(double scale_x = 1.0, double scale_y = 1.0);
};

//! A class representing a circle
class Circle : public CompoundShape {
public:
    explicit Circle(Vec2d arg_center = Vec2d(0.5, 0.5), double arg_radius = 0.5);
};

//! A class representing an ellipse
class Ellipse : public CompoundShape {
public:
    explicit Ellipse(double arg_a = 2.0, double arg_b = 1.0);
};

// Another rectangle, derived from CompoundShape
// class AlternativeRectangle: public CompoundShape {
// public:
//     AlternativeRectangle(double w, double h);
// };

//! The famous Sinai billiard
class Sinai : public CompoundShape {
public:
    explicit Sinai(double side_l = 1.0, double radius = 0.3);
};

//! Two concentric circles
class Ring : public CompoundShape {
public:
    explicit Ring(double inner_r = 0.3, double outer_r = 0.5);
};

//! The Bunimovich stadium
class Stadium : public CompoundShape {
public:
    explicit Stadium(double R = 0.5, double L = 1.0);
};

//! The Bunimovich mushroom
class Mushroom : public CompoundShape {
public:
    explicit Mushroom(double foot_width = 0.2, double foot_height = 0.5, double radius = 0.5);
};

//! Two-container system
class Gate : public CompoundShape {
public:
    explicit Gate(double gate_width = 0.3, double box_side = 1.0);
};

// Tables for escape simulations

//! Open rectangle
class OpenRectangle : public CompoundShape {
public:
    explicit OpenRectangle(double width = 1.0, double height = 1.0, double holeRatio = 1 / 50.);
    explicit OpenRectangle(HoleRatio arg_holeratio) : OpenRectangle(1.0, 1.0, arg_holeratio.value) {}
};
//! Open circle
class OpenCircle : public CompoundShape {
public:
    explicit OpenCircle(Vec2d centerPoint = {0.0, 0.0}, double radius = 1.0, double holeRatio = 1.0 / 50.);
    explicit OpenCircle(HoleRatio arg_holeratio) : OpenCircle({0.0, 0.0}, 1.0, arg_holeratio.value) {}
};

//! Open Bunimovich stadium
class OpenStadium : public CompoundShape {
public:
    explicit OpenStadium(double R = 0.5, double L = 1.0, double arg_holeratio = 1.0 / 50.);
    explicit OpenStadium(HoleRatio arg_holeratio) : OpenStadium(0.5, 1.0, arg_holeratio.value) {}
};

//! Open Sinai arena
class OpenSinai : public CompoundShape {
public:
    explicit OpenSinai(double side_l = 1.0, double radius = 0.3, double arg_holeratio = 1 / 50.);
    explicit OpenSinai(HoleRatio arg_holeratio) : OpenSinai(1.0, 0.3, arg_holeratio.value) {}
};

//! Open Bunimovich mushroom
class OpenMushroom : public CompoundShape {
public:
    explicit OpenMushroom(
        double foot_width = 0.2, double foot_height = 0.5, double radius = 0.5, double arg_holeratio = 1.0 / 50.
    );
    explicit OpenMushroom(HoleRatio arg_holeratio) : OpenMushroom(0.2, 0.5, 0.5, arg_holeratio.value) {}
};

//! Hard wall Lorentz gas
class LorentzGas : public CompoundShapePeriodic {
public:
    explicit LorentzGas(triplet<double> uc_params = triplet<double>(M_PI / 3, 1.0, 1.0), double radius = 0.3);
};

//! Periodic table without hard wall scatterers
class NoTablePeriodic : public CompoundShapePeriodic {
public:
    explicit NoTablePeriodic(triplet<double> uc_params = triplet<double>(M_PI / 3, 1, 1))
        : CompoundShapePeriodic(uc_params, 0, uc_params[1]) {
        maxCoords.x = uc_params[1] + cos(uc_params[0]) * uc_params[2];
        maxCoords.y = sin(uc_params[0]) * uc_params[2];
        d_shape_name << "notableperiodic()";
    }
};

}  // namespace bill2d

#endif  // _TABLE_HPP
