/*
 * potentials.hpp
 *
 * This file is part of bill2d.
 *
 * External potentials are defined in this file.
 *

 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file potentials.hpp
 * \brief External potentials are defined here.
 */

#ifndef _POTENTIALS_HPP_
#define _POTENTIALS_HPP_
#include "bill2d_config.h"
#include "datatypes.hpp"
#include "parameterlist.hpp"
#include "rng.hpp"
#include "vec2d.hpp"

#include <boost/align/aligned_allocator.hpp>
#include <boost/align/aligned_delete.hpp>

#include <list>
#include <vector>

#if defined(HAVE_SSE2) || defined(HAVE_AVX)
#include <stdlib.h>
#include <x86intrin.h>
#endif

#ifdef UNITTESTING
#include "gtest/gtest_prod.h"
#endif
namespace bill2d {

//! A base class for potentials
class Potential {
public:
    //! Force on the particle
    virtual const Vec2d force(const Vec2d& r) const = 0;
    //! Energy of the particle (from the potential)
    virtual double energy(const Vec2d& r) const = 0;
    virtual ~Potential() {}
};

//! \brief Harmonic potential
class HarmonicPotential : public Potential {
public:
    HarmonicPotential(double str, pair<double> arg_centerpoint);
    const Vec2d force(const Vec2d& r) const override;
    double energy(const Vec2d& r) const override;

private:
    //! \f$\omega\f$
    double d_str;
    //! Center of the potential
    Vec2d d_centerpoint;
};

/*! \brief Soft stadium potential
 *
 * Note that with this potential the possible propagation area is restricted by the energy.
 * Remember to make a new preparation method so that the particles will be inside the stadium potential
 */
class SoftStadium : public Potential {
public:
    SoftStadium(double n, double R, double L);
    const Vec2d force(const Vec2d& r) const override;
    double energy(const Vec2d& r) const override;

private:
    inline double d_e(double r) const;
    Vec2d cpl;
    Vec2d cpr;
    double d_n;
    double d_R_p1;
};

inline double SoftStadium::d_e(double r) const { return pow(-r + d_R_p1, -1 * d_n); }

/*! \brief Soft repulsive Lorentz gas
 *
 * The potential is given by a sum over distances \f$r\f$ to all lattice points,
 * \f[ V_{\text{total}} = \sum V(r), \f]
 * where potential extended by a single lattice point is given by \f$V(r)=1/(1+exp((r-radius)/sharpness))\f$.
 */
class SoftLorentzGas : public Potential {
public:
    //! Constructor
    SoftLorentzGas(triplet<double> uc_params, double radius, double sharpness, int N, bool inverted = false);

    //! Delete copy constructor
    SoftLorentzGas(const SoftLorentzGas&) = delete;

    //! Force on the particle
    const Vec2d force(const Vec2d& r) const override;
    //! Energy of the particle (from the potential)
    double energy(const Vec2d& r) const override;

    ~SoftLorentzGas() {
#if (defined(HAVE_ALIGNED_ALLOC) || defined(HAVE_POSIX_MEMALIGN)) && (defined(HAVE_AVX) || defined(HAVE_SSE2))
        free(Rx);
        free(Ry);
#else
        delete[] Rx;
        delete[] Ry;
#endif
    }

private:
    const unsigned d_N;
    const double d_radius, d_sharpness;
    double* Rx;
    double* Ry;
    bool d_inverted;
#if defined(HAVE_ALIGNED_ALLOC) || defined(HAVE_POSIX_MEMALIGN)

#ifdef HAVE_AVX
    __m256d *rad, *sharp, *ONE, *TWO;
    __m256d* Rxs;
    __m256d* Rys;
#elif defined(HAVE_SSE2)
    __m128d *rad, *sharp, *ONE, *TWO;
    __m128d* Rxs;
    __m128d* Rys;
#endif

#endif  // Have aligned alloc
};

//! Class for fermi-poles: \f$V(r)=1/(1+exp((r-radius)/sharpness))$\f
class FermiPole : public Potential {
public:
    //! Constructor
    FermiPole(Vec2d center = Vec2d(0.5, 0.5), double radius = 0.5, double sharpness = 0.1, bool inverted = false)
        : d_radius(radius),
          d_sharpness(sharpness),
          d_c(center),
          d_inverted(inverted) {}

    //! Force on the particle
    const Vec2d force(const Vec2d& r) const override {
        Vec2d d = r - d_c;
        Vec2d force = d / (2 * d.norm() * d_sharpness * (1 + cosh((d_radius - d.norm()) / d_sharpness)));
        if (d_inverted) {
            return -1.0 * force;
        }
        return force;
    }
    //! Energy of the particle (from the potential)
    double energy(const Vec2d& r) const override {
        Vec2d d = d_c - r;
        double energy = 1 / (exp((d.norm() - d_radius) / d_sharpness) + 1);
        if (d_inverted) {
            return -energy;
        }
        return energy;
    }

    ~FermiPole() {}

protected:
    double d_radius, d_sharpness;
    Vec2d d_c;
    bool d_inverted;
};

class SoftCircle : public Potential {
public:
    SoftCircle(ParameterList::SoftCircleType type, double softness, double eccentricity);
    const Vec2d force(const Vec2d& r) const override;
    double energy(const Vec2d& r) const override;

protected:
    ParameterList::SoftCircleType d_type;
    double d_softness;
    double d_eccentricity;
    double d_b_squared;
    double d_force_multiplier;
    FermiPole d_pole;
};

//! Class for a Gaussian "bump" as in itp2d
class GaussianPotential : public Potential {
public:
    GaussianPotential(double amplitude, double width, Vec2d center)
        : d_amplitude{amplitude},
          d_width{width},
          d_center{center} {}

    const Vec2d force(const Vec2d& r) const override {
        const Vec2d d = r - d_center;
        const double exponent = -d.normsqr() / (2 * d_width * d_width);
        return d * (exp(exponent) * d_amplitude / (d_width * d_width));
    }

    double energy(const Vec2d& r) const override {
        const Vec2d dist = d_center - r;
        const double exponent = -dist.normsqr() / (2 * d_width * d_width);
        return d_amplitude * exp(exponent);
    }

protected:
    double d_amplitude;
    double d_width;
    Vec2d d_center;
};

//! Class for multiple Gaussian "bumps" as in itp2d, fast implementation
class GaussianPotentialCluster : public Potential {
public:
    GaussianPotentialCluster(std::string itp2d_filename);

    const Vec2d force(const Vec2d& r) const override;

    double energy(const Vec2d& r) const override;

protected:
    std::string filename;
    std::vector<double, boost::alignment::aligned_allocator<double, 4 * sizeof(double)>> X, Y, amplitudes, widths;
    std::size_t N;  // Number of potentials
#if (defined(HAVE_AVX) || defined(HAVE_SSE2))
    const __m256d* s_x;
    const __m256d* s_y;
    const __m256d* s_amplitudes;
    const __m256d* s_widths;
#endif
};

//! Class which can contain many different potentials and external forces in the system.
class Potential_cont : public Potential {
public:
    inline const Vec2d force(const Vec2d& r) const override;
    inline double energy(const Vec2d& r) const override;
    //! Add a potential to the container
    void add(Potential* arg_int) { d_potentials.push_back(arg_int); }

    ~Potential_cont() {
        for (std::list<Potential*>::const_iterator it = d_potentials.begin(); it != d_potentials.end(); ++it) {
            delete *it;
        }
    }

private:
#ifdef UNITTESTING
    friend class billiardfactory_tests;
    friend class openbilliardfactory_tests;
#endif

    std::list<Potential*> d_potentials;
};

inline const Vec2d Potential_cont::force(const Vec2d& r) const {
    Vec2d res(0.0, 0.0);
    for (const auto& elem : d_potentials) {
        res += (elem)->force(r);
    }
    return res;
}

inline double Potential_cont::energy(const Vec2d& r) const {
    double res = 0;
    for (const auto& elem : d_potentials) {
        res += (elem)->energy(r);
    }
    return res;
}

/*! \brief Class for random gaussian forces
 *
 *\f$P(F_{H,i,\alpha}) = \frac{1}{\sqrt{2\pi T}}\exp(-F_{H,i,\alpha}^2 / (2T))\f$
 */
class GaussianBath : public Potential {
public:
    explicit GaussianBath(double temperature) : rng(), d_temp_sqr(sqrt(temperature)) {}
    const Vec2d force(__attribute__((unused)) const Vec2d& r) const override;
    inline double energy(__attribute__((unused)) const Vec2d& r) const override { return 0; }

private:
    RNG rng;
    double d_temp_sqr;
};

/*class SoftLiebGas : public Potential {
public:
        explicit SoftLiebGas(ARGUMENTS) {

        }

        const Vec2d force(const Vec2d& r) const override;
        double energy(const Vec2d& r) const override;
private:
        SOMETHING HERE
};*/

/*! \brief Soft repulsive Lieb gas
 *
 * The potential is given by a sum over distances \f$r\f$ to all lattice points,
 * \f[ V_{\text{total}} = \sum V(r), \f]
 * where potential extended by a single lattice point is given by \f$V(r)=1/(1+exp((r-radius)/sharpness))\f$.
 */
class SoftLiebGas : public Potential {
public:
    //! Constructor
    SoftLiebGas(triplet<double> uc_params, double radius, double sharpness, int N);

    //! Delete copy constructor
    SoftLiebGas(const SoftLiebGas&) = delete;

    //! Force on the particle
    const Vec2d force(const Vec2d& r) const override;
    //! Energy of the particle (from the potential)
    double energy(const Vec2d& r) const override;

    ~SoftLiebGas() {
#if (defined(HAVE_ALIGNED_ALLOC) || defined(HAVE_POSIX_MEMALIGN)) && (defined(HAVE_AVX) || defined(HAVE_SSE2))
        free(Rx);
        free(Ry);
#else
        delete[] Rx;
        delete[] Ry;
#endif
    }

private:
    const unsigned d_N;
    const double d_radius, d_sharpness;
    double* Rx;
    double* Ry;
#if defined(HAVE_ALIGNED_ALLOC) || defined(HAVE_POSIX_MEMALIGN)

#ifdef HAVE_AVX
    __m256d *rad, *sharp, *ONE, *TWO;
    __m256d* Rxs;
    __m256d* Rys;
#elif defined(HAVE_SSE2)
    __m128d *rad, *sharp, *ONE, *TWO;
    __m128d* Rxs;
    __m128d* Rys;
#endif

#endif  // Have aligned alloc
};

}  // namespace bill2d
#endif
