/*
 * vec.hpp
 *
 * A simple templated class representing a R^N vector (floating point)
 * is defined here. The class has support for basic things such
 * as addition, substraction, and multiplying by any arithmetic type.
 *
 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file vec2d.hpp
 *  \brief Definition of an \f$\mathbb{R}^2\f$ vector
 *  and relating operations.
 */

#ifndef _VEC_HPP_
#define _VEC_HPP_

#include "bill2d_config.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <numeric>
#include <ostream>
#include <type_traits>

#ifdef HAVE_SSE2
#include <x86intrin.h>
#endif

#ifdef UNITTESTING
#include "gtest/gtest_prod.h"
#endif
namespace bill2d {

/*! A class for an \f$\mathbb{R}^N\f$ vector
 *  and relating operations.
 */
template <typename Type, unsigned DIM, typename = typename std::enable_if_t<std::is_floating_point<Type>::value>>
class Vec {
public:
    Vec() { std::fill(d_vec, d_vec + DIM, 0.0); }

    //! Returns a zero vector
    static inline Vec zero() {
        Vec v;
        return v;
    }

    //! Constructor for DIM number of arguments which are all of template type Type
    template <typename... T, typename = typename std::enable_if_t<(sizeof...(T) == DIM)>>
    Vec(const T... e) : d_vec{e...} {}

    //! Constructor from initializer list
    Vec(std::initializer_list<Type> c) {
        assert(c.size() == DIM);
        std::copy(c.begin(), c.end(), d_vec);
    }

    //! Copy constructor
    Vec(const Vec& copythis) { std::copy(copythis.d_vec, copythis.d_vec + DIM, d_vec); }

    //! Element access. Note that to speed up, we do not check that i<DIM.
    //! Hence, here is possibility for bad things if you supply i>=DIM.
    inline Type& operator[](const size_t i) { return d_vec[i]; }

    //! Element access with bound check
    inline Type& at(const size_t i) {
        assert(i < DIM);
        return d_vec[i];
    }

    //! Element access. Note that to speed up, we do not check that i<DIM.
    //! Hence, here is possibility for bad things if you supply i=DIM or greater.
    inline Type const& operator[](const size_t i) const { return d_vec[i]; }

    //! Output to streams
    friend std::ostream& operator<<(std::ostream& stream, const Vec& vec) {
        stream << "(";
        for (unsigned i = 0; i < DIM - 1; i++) stream << vec[i] << ", ";
        stream << vec[DIM - 1];

        return stream << ")";
    }

    //! Addition assignment of another Vec
    inline Vec& operator+=(const Vec& other) {
        for (size_t i = 0; i < DIM; i++) d_vec[i] += other.d_vec[i];
        return *this;
    }

    //! Substraction assignment of another Vec
    inline Vec& operator-=(const Vec& other) {
        for (size_t i = 0; i < DIM; i++) d_vec[i] -= other.d_vec[i];
        return *this;
    }

    //! Addition assignment of an arithmetic type
    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>::type>
    inline Vec& operator+=(const T other) {
        for (auto& e : d_vec) e += other;
        return *this;
    }

    //! Substraction assignment of an arithmetic type
    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>::type>
    inline Vec& operator-=(const T other) {
        for (auto& e : d_vec) e -= other;
        return *this;
    }

    //! Multiplication assignment of an arithmetic type
    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>::type>
    inline Vec& operator*=(const T other) {
        for (auto& e : d_vec) e *= other;
        return *this;
    }

    //! Division assignment of an arithmetic type
    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>::type>
    inline Vec& operator/=(const T other) {
        for (auto& e : d_vec) e /= other;
        return *this;
    }

    //! Assignment operator for initializer list
    inline Vec& operator=(std::initializer_list<Type> c) {
        assert(c.size() == DIM);
        std::copy(c.begin(), c.end(), d_vec);
        return *this;
    }

    //! Assignment operator
    inline Vec& operator=(const Vec& other) {
        for (size_t i = 0; i < DIM; i++) d_vec[i] = other.d_vec[i];
        return *this;
    }

    //! Equality check
    inline bool operator==(const Vec& other) const {
        for (size_t i = 0; i < DIM; i++) {
            if (d_vec[i] != other.d_vec[i]) return false;
        }
        return true;
    }

    //! Dot product
    inline Type dot(const Vec& other) const { return std::inner_product(d_vec, d_vec + DIM, other.d_vec, 0.0); }

    //! Euclidian norm squared
    inline Type normsqr() const { return std::inner_product(d_vec, d_vec + DIM, d_vec, 0.0); }

    //! Euclidian norm
    inline Type norm() const { return std::sqrt(normsqr()); }

    //! Euclidian norm squared
    inline void scale_to_unit() { (*this) /= norm(); }

#ifndef USE_SSE2_VEC2D
    inline Vec<double, 2> rotcw90() const {
        Vec<double, 2> tmpvec;
        tmpvec.d_vec[0] = d_vec[1];
        tmpvec.d_vec[1] = -d_vec[0];
        return tmpvec;
    };
#endif

private:
// In case of unittests, define a few friend tests
#ifdef UNITTESTING
    FRIEND_TEST(Vec2d, constructor);
    FRIEND_TEST(Vec2d, element_access);
#endif

    //! The vector data
    alignas(2 * sizeof(Type)) Type d_vec[DIM];
};

// The free functions that implement basic binary operations with Vec<Type, DIM>

template <typename Type, unsigned DIM, typename = typename std::enable_if<std::is_arithmetic<Type>::value>::type>
inline const Vec<Type, DIM> operator+(const Vec<Type, DIM>& lhand, const Vec<Type, DIM>& rhand) {
    return Vec<Type, DIM>(lhand) += rhand;
}

template <typename Type, unsigned DIM, typename = typename std::enable_if<std::is_arithmetic<Type>::value>::type>
inline const Vec<Type, DIM> operator-(const Vec<Type, DIM>& lhand, const Vec<Type, DIM>& rhand) {
    return Vec<Type, DIM>(lhand) -= rhand;
}

template <typename Type, unsigned DIM, typename = typename std::enable_if<std::is_arithmetic<Type>::value>::type>
inline const Vec<Type, DIM> operator*(const Type& coeff, const Vec<Type, DIM>& vec) {
    return Vec<Type, DIM>(vec) *= coeff;
}

template <typename Type, unsigned DIM, typename = typename std::enable_if<std::is_arithmetic<Type>::value>::type>
inline const Vec<Type, DIM> operator*(const Vec<Type, DIM>& vec, const Type& coeff) {
    return Vec<Type, DIM>(vec) *= coeff;
}

template <typename Type, unsigned DIM, typename = typename std::enable_if<std::is_arithmetic<Type>::value>::type>
inline const Vec<Type, DIM> operator/(const Vec<Type, DIM>& vec, const Type& coeff) {
    return Vec<Type, DIM>(vec) /= coeff;
}

#ifdef USE_SSE2_VEC2D
// If we have SSE2, then let's use that for 2D double vector.
template <>
class Vec<double, 2u> {
public:
    Vec() { std::fill(d_vec.arr, d_vec.arr + 2, 0.0); }

    //! Returns a zero vector
    static inline Vec zero() {
        Vec v;
        return v;
    }

    //! Constructor for DIM number of arguments which are all of type Type
    template <typename... T, typename = typename std::enable_if_t<(sizeof...(T) == 2)>>
    Vec(const T... e) : d_vec{e...} {}

    Vec(std::initializer_list<double> c) {
        assert(c.size() == 2);
        std::copy(c.begin(), c.end(), d_vec.arr);
    }

    //! Copy constructor
    Vec(const Vec<double, 2>& copythis) { std::copy(copythis.d_vec.arr, copythis.d_vec.arr + 2, d_vec.arr); }

    //! Element access. Note that to speed up, we do not check that i<DIM.
    //! Hence, here is possibility for bad things if you supply i=DIM or greater.
    inline double& operator[](const size_t i) { return d_vec.arr[i]; }

    //! Element access with bounds check
    inline double& at(const size_t i) {
        assert(i < 2);
        return d_vec.arr[i];
    }

    //! Element access. Note that to speed up, we do not check that i<DIM.
    //! Hence, here is possibility for bad things if you supply i=DIM or greater.
    inline double const& operator[](size_t i) const { return d_vec.arr[i]; }

    //! Output to stream
    friend std::ostream& operator<<(std::ostream& stream, const Vec<double, 2>& vec) {
        return stream << "(" << vec[0] << ", " << vec[1] << ")";
    }

    //! Addition assignment
    inline Vec<double, 2>& operator+=(const Vec<double, 2>& other) {
        d_vec.sr += other.d_vec.sr;
        return *this;
    }

    //! Substraction assignment
    inline Vec<double, 2>& operator-=(const Vec<double, 2>& other) {
        d_vec.sr -= other.d_vec.sr;
        return *this;
    }

    //! Addition assignment of arithmetic type
    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>::type>
    inline Vec<double, 2>& operator+=(const T other) {
        d_vec.sr += _mm_set1_pd(other);

        return *this;
    }

    //! Substraction assignment of arithmetic type
    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>::type>
    inline Vec<double, 2>& operator-=(const T other) {
        d_vec.sr -= _mm_set1_pd(other);
        return *this;
    }

    //! Multiplication assignment of arithmetic type
    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>::type>
    inline Vec<double, 2>& operator*=(const T other) {
        d_vec.sr *= _mm_set1_pd(other);
        return *this;
    }

    //! Division assignment of arithmetic type
    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>::type>
    inline Vec<double, 2>& operator/=(const T other) {
        d_vec.sr /= _mm_set1_pd(other);
        return *this;
    }

    //! Assignment operator for initializer lists
    inline Vec<double, 2>& operator=(std::initializer_list<double> c) {
        assert(c.size() == 2);
        std::copy(c.begin(), c.end(), d_vec.arr);
        return *this;
    }

    //! Copy assignment operator
    inline Vec<double, 2>& operator=(const Vec<double, 2>& other) {
        d_vec.sr = other.d_vec.sr;
        return *this;
    }

    //! Equality check
    inline bool operator==(const Vec<double, 2>& other) const {
        for (size_t i = 0; i < 2; i++) {
            if (d_vec.arr[i] != other.d_vec.arr[i]) return false;
        }
        return true;
    }

    //! Dot product
    inline double dot(const Vec<double, 2>& other) const {
        return std::inner_product(d_vec.arr, d_vec.arr + 2, other.d_vec.arr, 0.0);
    }

    //! Square of the norm
    inline double normsqr() const { return std::inner_product(d_vec.arr, d_vec.arr + 2, d_vec.arr, 0.0); }

    //! Norm
    inline double norm() const { return std::sqrt(normsqr()); }

    //! Scaling of the vector to unit vector
    inline void scale_to_unit() { (*this) /= norm(); }

    inline Vec<double, 2> rotcw90() const {
        Vec<double, 2> tmpvec;
#ifdef HAVE_AVX
        tmpvec.d_vec.sr = _mm_permute_pd(d_vec.sr, 1);
        tmpvec.d_vec.arr[1] *= -1;
#else
        tmpvec.d_vec.arr[0] = d_vec.arr[1];
        tmpvec.d_vec.arr[1] = -d_vec.arr[0];
#endif
        return tmpvec;
    };

private:
// In case of unittests, define a few friend tests
#ifdef UNITTESTING
    FRIEND_TEST(Vec2d, constructor);
    FRIEND_TEST(Vec2d, element_access);
#endif

    //! The vector data type
    union VT {
        __m128d sr;
        alignas(2 * sizeof(double)) double arr[2];
    };

    //! Vector elements
    VT d_vec;
};
#endif

// Alias for bill2d
using Vec2d = Vec<double, 2>;

}  // namespace bill2d
#endif  // _VEC_HPP_
