/*
 * hist2d.cpp
 *
 * This file is part of bill2d.
 *
 * A wrapper for 2D GSL histograms
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file hist2d.hpp
 * \brief Definition of 2D histograms
 */

#ifndef _HIST_2D_HPP_
#define _HIST_2D_HPP_
#include <cstdlib>
#include <gsl/gsl_histogram2d.h>
namespace bill2d {

//! Wrapper for 2D GSL histograms
class Hist2d {
public:
    Hist2d(std::size_t nx, std::size_t ny, double minx, double maxx, double miny, double maxy);
    //! Override copy constructor
    Hist2d(const Hist2d&);
    //! Increment a certain region
    inline int add(double x, double y);
    //! Set a certain bin to value
    inline int set(double x, double y, double value);
    //! Get value of bin
    inline double get(std::size_t i, std::size_t j) const;
    //! Get xrange
    inline const double* xrange() const;
    //! Get yrange
    inline const double* yrange() const;
    //! Get pointer to histogram (bin) data
    inline const double* data() const;
    //! Calculate sum of all bins
    inline double sum() const;
    ~Hist2d();

private:
    gsl_histogram2d* h;
};

inline int Hist2d::add(double x, double y) { return gsl_histogram2d_increment(h, x, y); }

inline int Hist2d::set(double x, double y, double value) { return gsl_histogram2d_accumulate(h, x, y, value); }

inline double Hist2d::get(std::size_t i, std::size_t j) const { return gsl_histogram2d_get(h, i, j); }

inline const double* Hist2d::xrange() const { return h->xrange; }

inline const double* Hist2d::yrange() const { return h->yrange; }

inline const double* Hist2d::data() const { return h->bin; }

inline double Hist2d::sum() const { return gsl_histogram2d_sum(h); }
}  // namespace bill2d
#endif
