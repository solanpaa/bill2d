/*
 * billiard.cpp
 *
 * This file is part of bill2d.
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file billiard.cpp
 * \brief Propagation, algorithms etc. are implemented here.
 */

#include "billiard.hpp"

#include <signal.h>

namespace bill2d {

// Returns the positions and velocitires of the particles
std::vector<std::vector<double>> Billiard::get_state() {
    std::vector<std::vector<double>> allParticles{};
    allParticles.reserve(N());
    std::vector<double> oneParticle{};
    oneParticle.reserve(4);

    for (Particle& p : d_particles) {
        oneParticle.clear();
        oneParticle.push_back(p.d_r[0]);
        oneParticle.push_back(p.d_r[1]);
        oneParticle.push_back(p.d_v[0]);
        oneParticle.push_back(p.d_v[1]);
        allParticles.push_back((oneParticle));
    }
    return allParticles;
}

// Calculates the phase-space separtion of two systems
double Billiard::get_phase_space_difference(Billiard& compBill) {
    double diffSqr = 0;
    assert(N() == compBill.N());

    for (auto p = d_particles.begin(), p2 = compBill.d_particles.begin(); p != d_particles.end(); ++p, ++p2) {
        diffSqr += pow(p->d_r[0] - p2->d_r[0], 2) + pow(p->d_r[1] - p2->d_r[1], 2) + pow(p->d_v[0] - p2->d_v[0], 2) +
                   pow(p->d_v[1] - p2->d_v[1], 2);
    }

    return sqrt(diffSqr);
}

// Remove a particle
void Billiard::remove_particle(const particle_iterator& p) {
    // First save the buffers
    if (d_file != nullptr) {
        if (d_save_bmap) d_file->write_bmap(p->ID, p->bmap);
        if (d_save_traj) d_file->write_trajectory(p->ID, p->traj, p->saveidx, p->spawntime);
        if (d_save_vel) d_file->write_velocity(p->ID, p->vel, p->saveidx, p->spawntime);
        if (d_save_histograms) {
            d_file->write_rhist(p->ID, p->d_rhist);
            d_file->write_shist(p->ID, p->d_shist);
            d_file->write_vhist(p->ID, p->d_vhist);
        }
    }

    // And erase the particle from the list d_particles
    d_particles.erase(p);
}

// Remove a particle and update the forces after removal
void InteractingBilliard::remove_particle(const particle_iterator& p) {
    Billiard::remove_particle(p);
    update_forces();
}

// Creates a particle
void Billiard::d_create_particle(
    std::pair<Vec2d, Vec2d> (*f)(
        double /*Ekin*/, double /*Epot*/, unsigned /*num_particles*/, const Potential* /*potential*/,
        const Table* /*table*/, const RNG* /*Random number generator*/, ParameterList* /*parameterlist*/
    ),
    const Potential* pot
) {
    // Advance the ID of the last particle in the list
    d_currID++;

    // Create the particle
    Particle p(
        d_currID,
        d_time,
        d_table->getMaxCoords(),
        d_plist.save_full_trajectory,
        d_plist.save_velocity,
        d_plist.save_histograms,
        d_plist.save_bouncemap,
        d_plist.bins,
        d_plist.velocity_histogram_cutoff,
        d_plist.speed_histogram_cutoff,
        d_plist.buffersize
    );

    std::tie(p.d_r, p.d_v) = f(kinetic_energy(), potential_energy(), N(), pot, d_table.get(), d_rng.get(), &d_plist);

    p.d_F = Vec2d::zero();

    d_particles.push_back(std::move(p));

    std::cout << "Created " << p.d_r << std::endl;
}

// Creates a particle

void Billiard::create_particle(std::pair<Vec2d, Vec2d> (*f)(
    double /*Ekin*/, double /*Epot*/, unsigned /*num_particles*/, const Potential* /*potential*/,
    const Table* /*table*/, const RNG* /*Random number generator*/, ParameterList* /*parameterlist*/
)) {
    d_create_particle(f, nullptr);
}

// Same as above, but updates forces after creation of the particle
void InteractingBilliard::create_particle(std::pair<Vec2d, Vec2d> (*f)(
    double /*Ekin*/, double /*Epot*/, unsigned /*num_particles*/, const Potential* /*potential*/,
    const Table* /*table*/, const RNG* /*Random number generator*/, ParameterList* /*parameterlist*/
)) {
    d_create_particle(f, d_pot.get());
    update_forces();
}

/*
 *  Methods of class Billiard
 */

Billiard::Billiard(
    std::shared_ptr<const Table> table, std::unique_ptr<const RNG> rng, ParameterList& plist,
    std::unique_ptr<Datafile> file, std::unique_ptr<CustomPSOS> custom_psos, SelectPropagator should_i_select_propagator
)
    : d_table(std::move(table)),
      d_plist(plist),
      d_file(std::move(file)),
      d_rng(std::move(rng)),
      d_currID{1},
      d_custom_psos(std::move(custom_psos)),
      parallel_(false) {
    assert(d_table);  // Check that a table was given
    // Check if buffersizes have been calculated, and if not, calculate
    if (not d_plist.buffersizes_calculated) d_plist.calculate_buffersizes();

    d_num_buffersaves = 0;
    // Set member pointers and variables
    d_geometry = d_plist.geometry;
    d_periodic = d_plist.periodic;
    // Initialize parameters relating to the parallellogram unit cell if periodic system
    if (d_periodic) {
        d_L1 = d_plist.uc_params[1];
        d_L2 = d_plist.uc_params[2];
        d_Al = tan(d_plist.uc_params[0]);
        d_Br = 1 / d_Al;
        d_uh = sin(d_plist.uc_params[0]) * d_L2;
        d_hd = d_L2 * cos(d_plist.uc_params[0]);
    }

    // Create the particles
    for (unsigned i = 1; i <= plist.num_particles; i++) {
        d_particles.push_back(Particle(
            d_currID,
            d_time,
            d_table->getMaxCoords(),
            d_plist.save_full_trajectory,
            d_plist.save_velocity,
            d_plist.save_histograms,
            d_plist.save_bouncemap,
            d_plist.bins,
            d_plist.velocity_histogram_cutoff,
            d_plist.speed_histogram_cutoff,
            d_plist.buffersize
        ));
        d_currID++;
    }

    // Check that enough particles were created
    assert(d_particles.size() == plist.num_particles);

    // Set variables
    d_Ninitial = d_plist.num_particles;
    d_dt = d_plist.delta_t;
    d_spawnarea = d_plist.spawnarea;
    d_save_bmap = d_plist.save_bouncemap;
    d_save_energies = d_plist.save_energies;
    d_save_traj = d_plist.save_full_trajectory;
    d_save_vel = d_plist.save_velocity;
    d_save_histograms = d_plist.save_histograms;
    d_saveparticlenum = d_plist.save_particlenum;
    d_hits = 0;
    d_hit_flag = false;
    d_time = 0;
    d_saveidx = 0;
    d_iteration_index = 0;
    d_sparsesave = d_plist.spsave;
    d_minimum_number_of_iterations_minus_one = d_plist.minimum_number_of_iterations - 1;

    // Allocate memory for buffers
    if (d_save_energies) {
        d_ekin = new double[d_plist.buffersize];
        d_epot = new double[d_plist.buffersize];
    }

    if (d_saveparticlenum) d_particlenum = new unsigned[d_plist.buffersize];

    if (should_i_select_propagator == SelectPropagator::YES) select_propagator();
}

// Destructor, release memory
Billiard::~Billiard() {
    save_remaining_data();

    if (d_save_energies) {
        delete[] d_ekin;
        delete[] d_epot;
    }
    if (d_saveparticlenum) delete d_particlenum;
}

void Billiard::propagate() {
    if ((d_file || parallel_) && (d_iteration_index % d_sparsesave == 0) && (not d_plist.save_nothing)) {
        save_iteration();  // Save data to buffers
    }

    // When buffers full, save them to file and start filling buffers from the beginning
    if (d_iteration_index == d_minimum_number_of_iterations_minus_one) {
        d_iteration_index = 0;
        if (d_file || parallel_) save_data();
        d_num_buffersaves++;
        d_time = d_num_buffersaves * d_plist.minimum_number_of_iterations * d_dt;  // To avoid some rounding errors
    } else {
        d_iteration_index++;
        d_time += d_dt;
    }

    // Propagate the particles
    (this->*d_propagator)();

    if (d_custom_psos) d_custom_psos->evaluate(d_particles, d_time);

    // Handle boundary crossings
    bool handled = false;
    for (Particle& p : d_particles)
        if (d_table->handle_boundary_crossings(p.d_r, p.d_v, p.d_cell, p.bmap_ptr, d_time, d_hits, d_hit_flag))
            handled = true;

    /*
     * If we, e.g., reflect the particle or it jumps to the other side of the unit cell
     * then the Custom PSOS value might change causing erroneous crossing on next time step.
     * Hence we reset the Custom PSOS value if anything changed.
     */
    if (d_custom_psos and handled) d_custom_psos->reset(d_particles);
}

void Billiard::select_propagator() {
    switch (d_plist.propagator) {
        case ParameterList::PropagatorType::SECOND:
            d_propagator = &Billiard::d_propagate2;
            break;

        default:
            std::cerr << "Error in selecting propagator, not implemented for this type billiard simulation."
                      << std::endl;
            exit(1);
            break;
    }
}

// Save the buffers to file
void Billiard::save_data() {
    if (parallel_) {
        // Assumes that there is one particle
        save_callback_(d_ekin, d_epot, d_particles[0], d_saveidx);
        d_saveidx = 0;
    } else if (d_file != nullptr) {  // If datafile set
        if (d_save_energies) {
            if (d_saveidx > 0) {
                d_file->write_buffer("/kinetic_energy", d_ekin, d_saveidx);
                d_file->write_buffer("/potential_energy", d_epot, d_saveidx);
            }
        }

        for (Particle& p : d_particles) {
            if (p.saveidx > 0) {
                if (d_save_traj) d_file->write_trajectory(p.ID, p.traj, p.saveidx, p.spawntime);
                if (d_save_vel) d_file->write_velocity(p.ID, p.vel, p.saveidx, p.spawntime);
                p.saveidx = 0;
            }
        }

        if (d_saveparticlenum and (d_saveidx > 0)) d_file->write_buffer("/particle_num", d_particlenum, d_saveidx);

        d_saveidx = 0;
    }
}

// Saves rest of the data to file
void Billiard::save_remaining_data() {
    if (parallel_) {
        save_data();
    } else if (d_file != nullptr) {
        save_data();  // Save last iteration

        if (d_save_bmap) {
            for (Particle& p : d_particles) {
                if (p.bmap.size() > 0) d_file->write_bmap(p.ID, p.bmap);
            }
        }

        if (d_save_histograms) {
            for (Particle& p : d_particles) {
                d_file->write_rhist(p.ID, p.d_rhist);
                d_file->write_shist(p.ID, p.d_shist);
                d_file->write_vhist(p.ID, p.d_vhist);
            }
        }

        d_file->write_attribute("collisions", d_hits);
        d_file->write_attribute("time", d_time);
        d_file->write_attribute("iterations", static_cast<unsigned>(ceil(d_time / d_dt)));

        if (d_custom_psos) d_file->write_buffer("custom_psos", d_custom_psos->get_data());
    }
}

// Saves data from the current iteration to databuffers, note that each particle has its own saveidx
void Billiard::save_iteration() {
    for (Particle& p : d_particles) {
        if (d_save_traj) {
            if (d_periodic) {
                // TODO: Do these with SSE2 streaming instructions to avoid cache pollution, might want to save
                // histograms in a separate loop
                p.traj[2 * p.saveidx] = p.d_r[0] + p.d_cell[0] * d_L1 + p.d_cell[1] * d_hd;
                p.traj[2 * p.saveidx + 1] = p.d_r[1] + p.d_cell[1] * d_uh;
            } else {
                p.traj[2 * p.saveidx] = p.d_r[0];
                p.traj[2 * p.saveidx + 1] = p.d_r[1];
            }
        }
        if (d_save_vel) {
            p.vel[2 * p.saveidx] = p.d_v[0];
            p.vel[2 * p.saveidx + 1] = p.d_v[1];
        }

        (p.saveidx)++;
    }
    for (const Particle& p : d_particles) {
        if (d_save_histograms) {
            p.d_rhist->add(p.d_r[0], p.d_r[1]);
            p.d_vhist->add(p.d_v[0], p.d_v[1]);
            p.d_shist->add(hypot(p.d_v[0], p.d_v[1]));
        }
    }
    if (d_save_energies) {
        d_ekin[d_saveidx] = kinetic_energy();
        d_epot[d_saveidx] = potential_energy();
    }
    if (d_saveparticlenum) d_particlenum[d_saveidx] = N();

    d_saveidx++;
}

bool Billiard::prepare_custom(
    std::function<bool(
        particleset&, const RNG& rng, const Table& table, std::function<double()> epot, std::function<double()> ekin
    )>
        randomizer,
    const double total_energy
) {
    // Get the initial state
    bool is_possible = randomizer(
        d_particles,
        *d_rng,
        *d_table,
        std::bind(&Billiard::potential_energy, this),
        std::bind(&Billiard::kinetic_energy, this)
    );
    if (!is_possible) return false;
    // Check that total energy is really close to total_energy
    assert(fabs(total_energy - kinetic_energy() - potential_energy()) / total_energy < 1e-5);

    update_forces();
    return true;
}

// Prepares the system with initial conditions from 'rv'
void Billiard::prepare_manual(
    const std::vector<vector<double>>& rv, double total_energy, bool dont_care_about_energy, bool perturbate, double std
) {
    double maxV = 0;
    double tmpV = 0;
    double eta = 1;
    double ekin_sum_given = 0;
    bool cont = true;
    bool contr = true;
    double theta;
    std::vector<std::vector<double>>::const_iterator it;
    double ekin = 0;
    double epot = 0;
    Vec2d tmp;
    while (cont) {
        if ((SIG_FLAG == SIGINT) or (SIG_FLAG == SIGTERM)) {
            std::cout << "Killed by user." << std::endl;
            throw SignalFinish();
        }
        cont = false;
        while (contr) {
            it = rv.begin();
            contr = false;
            for (Particle& p : d_particles) {
                if (it != rv.end()) {
                    if (perturbate)
                        p.d_r = Vec2d(it->at(0) + d_rng->gaussian_rand(std), it->at(1) + d_rng->gaussian_rand(std));
                    else
                        p.d_r = Vec2d(it->at(0), it->at(1));
                    it++;
                    if (!d_table->point_is_inside(p.d_r)) {
                        std::cout << "Error: Position configuration is not inside the table." << std::endl;
                        exit(1);
                    }

                } else
                    while (true) {
                        p.d_r = Vec2d(
                            d_rng->uniform_rand(d_spawnarea[0], d_spawnarea[2]),
                            d_rng->uniform_rand(d_spawnarea[1], d_spawnarea[3])
                        );
                        if (d_table->point_is_inside(p.d_r)) break;
                    }
                p.d_r0 = p.d_r;
            }
            if (!dont_care_about_energy) {
                if (potential_energy() >= total_energy) contr = true;
                if (not perturbate)
                    if (d_particles.size() == rv.size()) {
                        if (!(potential_energy() < total_energy)) {
                            std::cout << "Error: Position configuration gives too large potential energy." << std::endl;
                            exit(1);
                        }
                    }
            } else
                contr = false;
        }

        it = rv.begin();

        // Normalize speeds so that the energies agree
        if (!dont_care_about_energy) {
            epot = potential_energy();
            ekin_sum_given = 0;
            for (Particle& p : d_particles) {
                if (it < rv.end()) {
                    if (perturbate)
                        p.d_v = Vec2d(it->at(2) + d_rng->gaussian_rand(std), it->at(3) + d_rng->gaussian_rand(std));
                    else
                        p.d_v = Vec2d(it->at(2), it->at(3));
                    tmpV = p.d_v.norm();
                    if (maxV < tmpV) maxV = tmpV;
                    it++;
                } else {
                    theta = d_rng->uniform_rand(0, 2 * M_PI);
                    p.d_v = d_rng->uniform_rand(0, maxV) * Vec2d(cos(theta), sin(theta));
                }
                ekin_sum_given += 0.5 * (p.d_v).norm() * (p.d_v).norm();
            }
            eta = sqrt((total_energy - epot) / ekin_sum_given);

            for (Particle& p : d_particles) {
                p.d_v = p.d_v * eta;
                p.d_v0 = p.d_v;
            }

        }

        else {
            for (Particle& p : d_particles) {
                if (it < rv.end()) {
                    p.d_v = Vec2d(it->at(2), it->at(3));
                    tmpV = p.d_v.norm();
                    if (maxV < tmpV) maxV = tmpV;
                    it++;
                } else {
                    theta = d_rng->uniform_rand(0, 2 * M_PI);
                    p.d_v = d_rng->uniform_rand(0, maxV) * Vec2d(cos(theta), sin(theta));
                }
                p.d_v0 = p.d_v;
            }
        }

        ekin = kinetic_energy();
        if (!(fabs(total_energy - ekin - epot) < 1e-6)) cont = true;
        if (dont_care_about_energy) cont = false;
    }

    update_forces();

    if (d_custom_psos) d_custom_psos->initialize(d_particles);
}

void Billiard::prepare(double energy) {
    bool cont = true;

    for (Particle& p : d_particles) {
        cont = true;
        while (cont) {
            cont = false;
            if ((SIG_FLAG == SIGINT) or (SIG_FLAG == SIGTERM)) {
                throw SignalFinish();
            }
            p.d_r = Vec2d(
                d_rng->uniform_rand(d_spawnarea[0], d_spawnarea[2]), d_rng->uniform_rand(d_spawnarea[1], d_spawnarea[3])
            );
            p.d_r0 = p.d_r;
            if (!d_table->point_is_inside(p.d_r)) cont = true;
        }
    }

    assert(energy > 0);
    double speed = sqrt(2 * energy / static_cast<double>(N()));
    double theta = 0.0;

    for (Particle& p : d_particles) {
        theta = d_rng->uniform_rand(0, 2 * M_PI);
        p.d_v = speed * Vec2d(cos(theta), sin(theta));
        p.d_v0 = p.d_v;
    }

    if (d_custom_psos) d_custom_psos->initialize(d_particles);
}

// Propagation. Euler method:
void Billiard::d_propagate2() {
    for (Particle& p : d_particles) {
        p.d_r += d_dt * p.d_v;
    }
}

// Calculate the kinetic energy
double Billiard::kinetic_energy() const {
    double Ekin = 0;
    double v;

    for (const Particle& p : d_particles) {
        v = p.d_v.norm();
        Ekin += 0.5 * v * v;
    }

    return Ekin;
}

/*
 *  Methods of class MagneticBilliard
 */

MagneticBilliard::MagneticBilliard(
    std::shared_ptr<const Table> table, std::unique_ptr<const RNG> rng, ParameterList& plist,
    std::unique_ptr<Datafile> file, std::unique_ptr<CustomPSOS> custom_psos
)
    : Billiard(
          std::move(table), std::move(rng), plist, std::move(file), std::move(custom_psos),
          Billiard::SelectPropagator::NO
      ) {
    d_B = plist.B;
    invB = 1.0 / d_B;
    invdt = 1.0 / plist.delta_t;
    sinBdt = sin(d_B * plist.delta_t);
    cosBdt = cos(d_B * plist.delta_t);
    cosBdtm1 = cosBdt - 1.0;
    select_propagator();
}

void MagneticBilliard::select_propagator() {
    switch (d_plist.propagator) {
        case ParameterList::PropagatorType::SECOND:
            d_propagator = static_cast<Billiard::propagator_function>(&MagneticBilliard::d_propagate2);
            break;

        default:
            std::cerr << "Error in selecting propagator, not implemented for this type billiard simulation."
                      << std::endl;
            exit(1);
            break;
    }
}

void MagneticBilliard::calculate_propagator_coefficients() {
    invdt = 1.0 / d_dt;
    sinBdt = sin(d_B * d_dt);
    cosBdt = cos(d_B * d_dt);
    cosBdtm1 = cosBdt - 1.0;
}

void MagneticBilliard::d_propagate2() {
    for (Particle& p : d_particles) {
        /* The modifed velocity Verlet algorithm by Spreiter & Walter
         * without interactions */
        Vec2d newv;
        p.d_r[0] += invB * (p.d_v[0] * sinBdt - p.d_v[1] * cosBdtm1);
        p.d_r[1] += invB * (p.d_v[1] * sinBdt + p.d_v[0] * cosBdtm1);
        newv[0] = p.d_v[0] * cosBdt + p.d_v[1] * sinBdt;
        newv[1] = p.d_v[1] * cosBdt - p.d_v[0] * sinBdt;
        p.d_v = newv;
    }
}

// Propagate and calculate the collision map

/*
 * Methods of class InteractingBilliard
 */

InteractingBilliard::InteractingBilliard(
    std::shared_ptr<const Interaction> inter, std::shared_ptr<const Potential> pot, std::shared_ptr<const Table> table,
    std::unique_ptr<const RNG> rng, ParameterList& plist, std::unique_ptr<Datafile> file,
    std::unique_ptr<CustomPSOS> custom_psos, Billiard::SelectPropagator shouldiselectpropagator
)
    : Billiard(
          std::move(table), std::move(rng), plist, std::move(file), std::move(custom_psos),
          Billiard::SelectPropagator::NO
      ),
      d_inter(std::move(inter)),
      d_pot(std::move(pot)),
      d_Ccoeffs{nullptr},
      d_Dcoeffs{nullptr} {
    // assert(arg_N >= 2);
    // d_rng= new RNG();
    d_force = get_force_pointer();
    if (shouldiselectpropagator == Billiard::SelectPropagator::YES) {
        calculate_propagator_coefficients();
        select_propagator();
    }
}

void InteractingBilliard::select_propagator() {
    switch (d_plist.propagator) {
        case ParameterList::PropagatorType::SECOND:
            d_propagator = static_cast<propagator_function>(&InteractingBilliard::d_propagate2);
            break;

        case ParameterList::PropagatorType::SECOND_OPTIMAL:
            d_propagator = static_cast<propagator_function>(&InteractingBilliard::d_propagate2_optimal);
            break;

        case ParameterList::PropagatorType::THIRD:
            d_propagator = static_cast<propagator_function>(&InteractingBilliard::d_propagate3);
            break;

        case ParameterList::PropagatorType::FOURTH:
            d_propagator = static_cast<propagator_function>(&InteractingBilliard::d_propagate4);
            break;

        case ParameterList::PropagatorType::FOURTH_OPTIMAL:
            d_propagator = static_cast<propagator_function>(&InteractingBilliard::d_propagate4_optimal);
            break;

        case ParameterList::PropagatorType::SIXTH:
            d_propagator = static_cast<propagator_function>(&InteractingBilliard::d_propagate6_optimal);
            break;

        default:
            std::cerr << "Error in selecting propagator, not implemented for this type of billiard simulation."
                      << std::endl;
            exit(1);
            break;
    }
}

void InteractingBilliard::calculate_propagator_coefficients() {
    long double y{}, z{};
    switch (d_plist.propagator) {
        case (ParameterList::PropagatorType::SECOND):
            break;

        case (ParameterList::PropagatorType::SECOND_OPTIMAL):
            if (!d_Ccoeffs) d_Ccoeffs = new double[3];
            y = powl(2 * sqrtl(326) - 36, 1. / 3.);
            z = (y * y + 6 * y - 2) / (12 * y);
            d_Ccoeffs[0] = z * d_dt;
            d_Ccoeffs[1] = (1 - 2 * z) * d_dt;
            d_Ccoeffs[2] = d_Ccoeffs[0];

            if (!d_Dcoeffs) d_Dcoeffs = new double[2];
            d_Dcoeffs[0] = 0.5 * d_dt;
            d_Dcoeffs[1] = 0.5 * d_dt;
            d_num_coeff_iters = 2;
            break;

        case (ParameterList::PropagatorType::THIRD):
            if (!d_Ccoeffs) d_Ccoeffs = new double[3];
            d_Ccoeffs[0] = 1 * d_dt;
            d_Ccoeffs[1] = -2. / 3. * d_dt;
            d_Ccoeffs[2] = 2. / 3. * d_dt;

            if (!d_Dcoeffs) d_Dcoeffs = new double[3];
            d_Dcoeffs[0] = -1. / 24. * d_dt;
            d_Dcoeffs[1] = 3. / 4. * d_dt;
            d_Dcoeffs[2] = 7. / 24. * d_dt;
            d_num_coeff_iters = 3;

            break;

        case (ParameterList::PropagatorType::FOURTH):
            if (!d_Ccoeffs) d_Ccoeffs = new double[4];
            d_Ccoeffs[0] = 1. / (2. * (2. - pow(2., 1. / 3.))) * d_dt;
            d_Ccoeffs[1] = (1. - pow(2., 1. / 3.)) / (2. * (2. - pow(2., 1. / 3.))) * d_dt;
            d_Ccoeffs[2] = (1. - pow(2, 1 / 3.)) / (2 * (2 - pow(2, 1 / 3.))) * d_dt;
            d_Ccoeffs[3] = 1. / (2 * (2 - pow(2., 1. / 3.))) * d_dt;

            if (!d_Dcoeffs) d_Dcoeffs = new double[4];

            d_Dcoeffs[0] = 1. / (2 - pow(2, 1. / 3.)) * d_dt;
            d_Dcoeffs[1] = -pow(2, 1 / 3.) / (2 - pow(2, 1 / 3.)) * d_dt;
            d_Dcoeffs[2] = 1. / (2 - pow(2, 1. / 3.)) * d_dt;
            d_Dcoeffs[3] = 0;
            d_num_coeff_iters = 4;

            break;

        case (ParameterList::PropagatorType::FOURTH_OPTIMAL):  // SIAM J. Sci. Comput., 16(1), 151–168.
            if (!d_Ccoeffs) d_Ccoeffs = new double[5];
            z = sqrtl(((long double)7) / ((long double)8)) / ((long double)3);
            d_Ccoeffs[0] = (0.5 - z) * d_dt;
            d_Ccoeffs[1] = (-1. / 3 + z) * d_dt;
            d_Ccoeffs[2] = 2. / 3 * d_dt;
            d_Ccoeffs[3] = d_Ccoeffs[1];
            d_Ccoeffs[4] = d_Ccoeffs[0];

            if (!d_Dcoeffs) d_Dcoeffs = new double[4];
            d_Dcoeffs[0] = d_dt;
            d_Dcoeffs[1] = -0.5 * d_dt;
            d_Dcoeffs[2] = -0.5 * d_dt;
            d_Dcoeffs[3] = d_dt;
            d_num_coeff_iters = 4;
            break;

        case (ParameterList::PropagatorType::SIXTH):  // SIAM J. Sci. Comput., 16(1), 151–168.
            if (!d_Ccoeffs) d_Ccoeffs = new double[8];

            d_Ccoeffs[0] = -1.01308797891717472981l * d_dt;
            d_Ccoeffs[1] = 1.18742957373254270702l * d_dt;
            d_Ccoeffs[2] = -0.01833585209646059034l * d_dt;
            d_Ccoeffs[3] = 0.34399425728109261313l * d_dt;
            d_Ccoeffs[4] = d_Ccoeffs[3];
            d_Ccoeffs[5] = d_Ccoeffs[2];
            d_Ccoeffs[6] = d_Ccoeffs[1];
            d_Ccoeffs[7] = d_Ccoeffs[0];

            if (!d_Dcoeffs) d_Dcoeffs = new double[7];
            d_Dcoeffs[0] = 0.00016600692650009894l * d_dt;
            d_Dcoeffs[1] = -0.37962421426377360608l * d_dt;
            d_Dcoeffs[2] = 0.68913741185181063674l * d_dt;
            d_Dcoeffs[3] = 0.38064159097092574080l * d_dt;
            d_Dcoeffs[4] = d_Dcoeffs[2];
            d_Dcoeffs[5] = d_Dcoeffs[1];
            d_Dcoeffs[6] = d_Dcoeffs[0];
            d_num_coeff_iters = 7;
            break;

        default:
            std::cerr << "Error in calculating coefficients for the interactingbilliard propagator, exiting..."
                      << std::endl;
            exit(1);
            break;
    }
}

Vec2d InteractingBilliard::d_fpot(const Particle& p) const { return d_pot->force(p.d_r); }

Vec2d InteractingBilliard::d_fno_ext(__attribute__((unused)) const Particle&) const { return Vec2d::zero(); }

void InteractingBilliard::prepare(double total_energy) {
    assert(total_energy > 0);
    Vec2d pos_tmp;
    double Epot;

    // Randomize positions until we find an energetically allowed configuration
    do {
        if ((SIG_FLAG == SIGINT) or (SIG_FLAG == SIGTERM)) {
            throw SignalFinish();
        }
        for (Particle& p : d_particles) {
            do {
                pos_tmp = Vec2d(
                    d_rng->uniform_rand(d_spawnarea[0], d_spawnarea[2]),
                    d_rng->uniform_rand(d_spawnarea[1], d_spawnarea[3])
                );

            } while (not d_table->point_is_inside(pos_tmp));
            p.d_r = pos_tmp;
            p.d_r0 = p.d_r;
        }
        Epot = potential_energy();

    } while (Epot > total_energy);

    double Ekin = total_energy - potential_energy();

    // Distribute kinetic energy evenly for particles
    double speed = sqrt(2 * Ekin / static_cast<double>(N()));
    double theta{};  // randomize the velocity angle
    for (Particle& p : d_particles) {
        theta = d_rng->uniform_rand(0, 2 * M_PI);
        p.d_v = Vec2d(speed * cos(theta), speed * sin(theta));
        p.d_v0 = p.d_v;
    }
    assert(fabs(Ekin - kinetic_energy()) < 1e-6);
    update_forces();
    if (d_custom_psos) d_custom_psos->initialize(d_particles);
}

void InteractingBilliard::d_propagate2() {
    // This is pretty much standard velocity Verlet
    // Update particle positions, and also velocities the first time
    for (Particle& p : d_particles) {
        p.d_r += d_dt * (p.d_v + 0.5 * d_dt * p.d_F);
        p.d_v += 0.5 * d_dt * p.d_F;
    }
    // Calculate change in forces and update velocities the second time

    Vec2d ftmp;
    for (Particle& p : d_particles) {
        p.d_F = (this->*d_force)(p);  // External forces
        for (Particle& op : d_particles) {
            if (&p != &op) {
                p.d_F += d_inter->force(p.d_r, op.d_r);  // Particle-particle interaction
            }
        }

        p.d_v += 0.5 * d_dt * p.d_F;
        // Handle boundary
    }
}

void InteractingBilliard::d_propagate3() {
    for (unsigned i = 0; i < d_num_coeff_iters; i++) {
        for (Particle& p : d_particles) {
            p.d_v += d_Dcoeffs[d_num_coeff_iters - 1 - i] * p.d_F;
            p.d_r += d_Ccoeffs[d_num_coeff_iters - 1 - i] * p.d_v;
        }
        update_forces();
    }
}

void InteractingBilliard::d_propagate4() {
    for (unsigned i = 0; i < d_num_coeff_iters; i++) {
        for (Particle& p : d_particles) {
            p.d_v += d_Dcoeffs[d_num_coeff_iters - 1 - i] * p.d_F;
            p.d_r += d_Ccoeffs[d_num_coeff_iters - 1 - i] * p.d_v;
        }
        update_forces();
    }
}

void InteractingBilliard::d_propagate2_optimal() {
    for (unsigned i = 0; i < d_num_coeff_iters; i++) {
        for (Particle& p : d_particles) {
            p.d_r += d_Ccoeffs[i] * p.d_v;
        }
        update_forces();
        for (Particle& p : d_particles) {
            p.d_v += d_Dcoeffs[i] * p.d_F;
        }
    }
    for (Particle& p : d_particles) {
        p.d_r += d_Ccoeffs[d_num_coeff_iters] * p.d_v;
    }
}

void InteractingBilliard::d_propagate4_optimal() {
    for (unsigned i = 0; i < d_num_coeff_iters; i++) {
        for (Particle& p : d_particles) {
            p.d_r += d_Ccoeffs[i] * p.d_v;
        }
        update_forces();
        for (Particle& p : d_particles) {
            p.d_v += d_Dcoeffs[i] * p.d_F;
        }
    }
    for (Particle& p : d_particles) {
        p.d_r += d_Ccoeffs[d_num_coeff_iters] * p.d_v;
    }
}
void InteractingBilliard::d_propagate6_optimal() {
    for (unsigned i = 0; i < d_num_coeff_iters; i++) {
        for (Particle& p : d_particles) {
            p.d_r += d_Ccoeffs[i] * p.d_v;
        }
        update_forces();
        for (Particle& p : d_particles) {
            p.d_v += d_Dcoeffs[i] * p.d_F;
        }
    }
    for (Particle& p : d_particles) {
        p.d_r += d_Ccoeffs[d_num_coeff_iters] * p.d_v;
    }
}

double InteractingBilliard::potential_energy() const {
    double Epot = 0;
    for (const Particle& p : d_particles) {
        if (d_inter) {
            for (const Particle& op : d_particles) {
                if (&p != &op) {
                    Epot += 0.5 * d_inter->energy(p.d_r, op.d_r);
                }
            }
        }
        if (d_pot != nullptr) Epot += d_pot->energy(p.d_r);
    }
    return Epot;
}

InteractingMagneticBilliard::InteractingMagneticBilliard(
    std::shared_ptr<const Interaction> inter, std::shared_ptr<const Potential> pot, std::shared_ptr<const Table> table,
    std::unique_ptr<const RNG> rng, ParameterList& plist, std::unique_ptr<Datafile> file,
    std::unique_ptr<CustomPSOS> custom_psos, Billiard::SelectPropagator shouldiselectpropagator
)
    : InteractingBilliard(
          std::move(inter), std::move(pot), std::move(table), std::move(rng), plist, std::move(file),
          std::move(custom_psos), Billiard::SelectPropagator::NO
      ) {
    assert(plist.B != 0);
    d_B = plist.B;

    if (shouldiselectpropagator == Billiard::SelectPropagator::YES) {
        select_propagator();
        calculate_propagator_coefficients();
    }

    initial_steps_taken = 0;
}

void InteractingMagneticBilliard::select_propagator() {
    switch (d_plist.propagator) {
        case ParameterList::PropagatorType::SECOND:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate2);
            break;

        case ParameterList::PropagatorType::BORIS:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_initial_steps);
            break;

        case ParameterList::PropagatorType::YHE2:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_initial_steps);
            break;

        case ParameterList::PropagatorType::PRK4:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_prk4);
            break;

        case ParameterList::PropagatorType::PRK6:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_prk6);
            break;

        case ParameterList::PropagatorType::CHIN_2B:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_chin2b);
            break;

        case ParameterList::PropagatorType::CHIN_2A:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_chin2a);
            break;

        case ParameterList::PropagatorType::SCOVEL:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_scovel);
            break;

        case ParameterList::PropagatorType::RK4:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_rk4);
            break;

        case ParameterList::PropagatorType::YOSHIDA4:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_yoshida4);
            break;

        case ParameterList::PropagatorType::SUZUKI4:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_suzuki4);
            break;

        case ParameterList::PropagatorType::SS4_5:  // Suzuki
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_SS4_5);
            break;

        case ParameterList::PropagatorType::SS6_7:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_SS6_7);
            break;

        case ParameterList::PropagatorType::SS6_9:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_SS6_9);
            break;

        case ParameterList::PropagatorType::SS8_15:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_SS8_15);
            break;

        case ParameterList::PropagatorType::SS8_17:
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_SS8_17);
            break;

        default:
            std::cerr << "Error in selecting propagator, not implemented for this type of billiard simulation."
                      << std::endl;
            exit(1);
            break;
    }
}

void InteractingMagneticBilliard::calculate_propagator_coefficients() {
    invB = 1.0 / d_B;
    invdt = 1.0 / d_dt;
    Bdt = d_B * d_dt;
    C0 = cos(Bdt);
    S0 = -sin(Bdt);

    C1 = invB * sin(Bdt);
    S1 = invB * (cos(Bdt) - 1);

    C2 = -invB * invB * (cos(Bdt) - 1);
    S2 = invB * invB * (sin(Bdt) - Bdt);

    tC0 = cos(0.5 * Bdt);
    tS0 = -sin(0.5 * Bdt);

    tC1 = invB * sin(0.5 * Bdt);
    tS1 = invB * (cos(0.5 * Bdt) - 1);

    tC2 = -invB * invB * (cos(0.5 * Bdt) - 1);
    tS2 = invB * invB * (sin(0.5 * Bdt) - 0.5 * Bdt);

    m_B2dt2_plus_4 = 4 - Bdt * Bdt;
    B2dt2_plus_4 = Bdt * Bdt + 4.0;
    Bdt_times_4 = Bdt * 4.0;

    Y1 = sin(Bdt);
    Y2 = 1.0 - cos(Bdt);

    yoshida_a1 = 1.0 / (2 - std::cbrt(2.0));
    yoshida_a2 = 1 - 2 * yoshida_a1;

    suzuki_b1 = 1.0 / (4 - std::cbrt(4.0));
    suzuki_b2 = suzuki_b1;
    suzuki_b3 = 1 - 2 * (suzuki_b1 + suzuki_b2);

    prk4_a1 = 0.0792036964311957;
    prk4_a2 = 0.353172906049774;
    prk4_a3 = -0.0420650803577195;
    prk4_a4 = 1 - 2 * (prk4_a1 + prk4_a2 + prk4_a3);

    prk4_b1 = 0.209515106613362;
    prk4_b2 = -0.143851773179818;
    prk4_b3 = 0.5 - prk4_b1 - prk4_b2;

    prk6_a = {0.0502627644003922, 0.413514300428344, 0.0450798897943977, -0.188054853819569, 0.541960678450780, 0};
    prk6_a[5] = 1 - 2 * std::accumulate(prk6_a.begin(), prk6_a.end(), 0.0);

    prk6_b = {0.148816447901042, -0.132385865767784, 0.067307604692185, 0.432666402578175, 0};
    prk6_b[4] = 0.5 - std::accumulate(prk6_b.begin(), prk6_b.end(), 0.0);

    ss6_7 = {0.78451361047755726382, 0.23557321335935813368, -1.17767998417887100695, 0.0};
    ss6_7.back() = 1 - 2 * std::accumulate(ss6_7.begin(), ss6_7.end(), 0.0);
    std::for_each(ss6_7.begin(), ss6_7.end(), [this](double& w) { w *= d_dt; });

    ss6_9 = {0.1867, 0.55549702371247839916, 0.12946694891347535806, -0.84326562338773460855, 0.0};
    ss6_9.back() = 1 - 2 * std::accumulate(ss6_9.begin(), ss6_9.end(), 0.0);
    std::for_each(ss6_9.begin(), ss6_9.end(), [this](double& w) { w *= d_dt; });

    ss8_15 = {
        0.74167036435061295345,
        -0.40910082580003159400,
        0.19075471029623837995,
        -0.57386247111608226666,
        0.29906418130365592384,
        0.33462491824529818378,
        0.31529309239676659663,
        0.0
    };
    ss8_15.back() = 1 - 2 * std::accumulate(ss8_15.begin(), ss8_15.end(), 0.0);
    std::for_each(ss8_15.begin(), ss8_15.end(), [this](double& w) { w *= d_dt; });

    ss8_17 = {
        25. / 194.,
        0.58151408710525096243,
        -0.41017537146985013753,
        0.18514693571658773265,
        -0.40955234342085141934,
        0.14440594108001204106,
        0.27833550039367965131,
        0.31495668391629485789,
        0.0
    };
    ss8_17.back() = 1 - 2 * std::accumulate(ss8_17.begin(), ss8_17.end(), 0.0);
    std::for_each(ss8_17.begin(), ss8_17.end(), [this](double& w) { w *= d_dt; });

    ss4_5 = {1.0 / (4 - std::cbrt(4.0)), 1.0 / (4 - std::cbrt(4.0)), 0.0};
    ss4_5.back() = 1 - 2 * std::accumulate(ss4_5.begin(), ss4_5.end(), 0.0);
    std::for_each(ss4_5.begin(), ss4_5.end(), [this](double& w) { w *= d_dt; });
}

void InteractingMagneticBilliard::d_propagate_chin2b() {
    // exp dt/2 T
    for (Particle& p : d_particles) {
        p.d_r += 0.5 * d_dt * p.d_v;
    }
    update_forces();
    // exp dt Vbf
    for (Particle& p : d_particles) {
        Vec2d newv{C0 * p.d_v[0] - S0 * p.d_v[1] - S1 * p.d_F[1], S0 * p.d_v[0] + C0 * p.d_v[1] + S1 * p.d_F[0]};
        p.d_v = newv + C1 * p.d_F;
    }

    // exp dt/2 T
    for (Particle& p : d_particles) {
        p.d_r += d_dt / 2.0 * p.d_v;
    }
}

void InteractingMagneticBilliard::d_propagate_rk4() {  // From wikipedia
    std::vector<Vec2d> k1r{d_particles.size(), Vec2d{0.0, 0.0}};
    std::vector<Vec2d> k1v{d_particles.size(), Vec2d{0.0, 0.0}};
    std::vector<Vec2d> k2r{d_particles.size(), Vec2d{0.0, 0.0}};
    std::vector<Vec2d> k2v{d_particles.size(), Vec2d{0.0, 0.0}};
    std::vector<Vec2d> k3r{d_particles.size(), Vec2d{0.0, 0.0}};
    std::vector<Vec2d> k3v{d_particles.size(), Vec2d{0.0, 0.0}};
    std::vector<Vec2d> k4r{d_particles.size(), Vec2d{0.0, 0.0}};
    std::vector<Vec2d> k4v{d_particles.size(), Vec2d{0.0, 0.0}};

    std::vector<Vec2d> r0{d_particles.size(), Vec2d{0.0, 0.0}};
    std::vector<Vec2d> v0{d_particles.size(), Vec2d{0.0, 0.0}};
    // k1
    for (unsigned i = 0; i < d_particles.size(); i++) {
        auto& p = d_particles.at(i);
        r0.at(i) = p.d_r;
        v0.at(i) = p.d_v;
        k1r.at(i) = p.d_v;
        k1v.at(i) = p.d_F;
        k1v.at(i)[0] += p.d_v[1] * d_B;
        k1v.at(i)[1] -= p.d_v[0] * d_B;

        p.d_r = r0.at(i) + 0.5 * d_dt * k1r.at(i);
        p.d_v = v0.at(i) + 0.5 * d_dt * k1v.at(i);
    }
    update_forces();

    // k2
    for (unsigned i = 0; i < d_particles.size(); i++) {
        auto& p = d_particles.at(i);

        k2r.at(i) = p.d_v;
        k2v.at(i) = p.d_F;
        k2v.at(i)[0] += p.d_v[1] * d_B;
        k2v.at(i)[1] -= p.d_v[0] * d_B;

        p.d_r = r0.at(i) + 0.5 * d_dt * k2r.at(i);
        p.d_v = v0.at(i) + 0.5 * d_dt * k2v.at(i);
    }

    update_forces();

    // k3
    for (unsigned i = 0; i < d_particles.size(); i++) {
        auto& p = d_particles.at(i);
        k3r.at(i) = p.d_v;
        k3v.at(i) = p.d_F;
        k3v.at(i)[0] += p.d_v[1] * d_B;
        k3v.at(i)[1] -= p.d_v[0] * d_B;

        p.d_r = r0.at(i) + d_dt * k3r.at(i);
        p.d_v = v0.at(i) + d_dt * k3v.at(i);
    }

    update_forces();

    // k4
    for (unsigned i = 0; i < d_particles.size(); i++) {
        auto& p = d_particles.at(i);

        k4r.at(i) = p.d_v;
        k4v.at(i) = p.d_F;
        k4v.at(i)[0] += p.d_v[1] * d_B;
        k4v.at(i)[1] -= p.d_v[0] * d_B;

        p.d_r = r0.at(i) + d_dt / 6.0 * (k1r.at(i) + 2.0 * k2r.at(i) + 2.0 * k3r.at(i) + k4r.at(i));
        p.d_v = v0.at(i) + d_dt / 6.0 * (k1v.at(i) + 2.0 * k2v.at(i) + 2.0 * k3v.at(i) + k4v.at(i));
    }
    update_forces();
}

void InteractingMagneticBilliard::d_propagate_scovel() {  // From the book
    for (Particle& p : d_particles) {
        p.d_v += 0.5 * d_dt * p.d_F;
    }

    for (Particle& p : d_particles) {
        p.d_r += (d_dt + invB * S2) * p.d_v;
        p.d_r[0] -= C2 * p.d_v[1];
        p.d_r[1] += C2 * p.d_v[0];
    }

    update_forces();

    for (Particle& p : d_particles) {
        Vec2d newv;
        newv[0] = p.d_v[0] - C1 * p.d_v[1] - 2 * tC1 * tC1 * p.d_v[0];
        newv[1] = p.d_v[0] + C1 * p.d_v[0] - 2 * tC1 * tC1 * p.d_v[0];
        p.d_v = newv + 0.5 * d_dt * p.d_F;
    }
}

void InteractingMagneticBilliard::d_propagate_chin2a() {
    // exp 0.5dt Vbf
    for (Particle& p : d_particles) {
        Vec2d newv{tC0 * p.d_v[0] - tS0 * p.d_v[1] - tS1 * p.d_F[1], tS0 * p.d_v[0] + tC0 * p.d_v[1] + tS1 * p.d_F[0]};
        p.d_v = newv + tC1 * p.d_F;
    }

    // exp dt T
    for (Particle& p : d_particles) {
        p.d_r += d_dt * p.d_v;
    }
    update_forces();

    // exp 0.5dt Vbf
    for (Particle& p : d_particles) {
        Vec2d newv{tC0 * p.d_v[0] - tS0 * p.d_v[1] - tS1 * p.d_F[1], tS0 * p.d_v[0] + tC0 * p.d_v[1] + tS1 * p.d_F[0]};
        p.d_v = newv + tC1 * p.d_F;
    }
}

void InteractingMagneticBilliard::d_propagate2() {
    Vec2d newv;
    Vec2d deltaF;
    /* The modifed velocity Verlet algorithm by Spreiter & Walter with
     * interactions */
    // Update particle positions, and also velocities the first time
    for (Particle& p : d_particles) {
        p.d_r[0] += C1 * p.d_v[0] - S1 * p.d_v[1] + C2 * p.d_F[0] - S2 * p.d_F[1];

        p.d_r[1] += S1 * p.d_v[0] + C1 * p.d_v[1] + S2 * p.d_F[0] + C2 * p.d_F[1];
    }
    // Calculate change in forces and update velocities the second time
    // TODO: Check if this can be optimized more (this counts each distance twice
    // but avoids extra loops.
    for (Particle& p : d_particles) {
        deltaF = -1.0 * p.d_F + (this->*d_force)(p);
        for (Particle& op : d_particles) {
            if (&p != &op) {
                deltaF += d_inter->force(p.d_r, op.d_r);
            }
        }
        newv[0] = C0 * p.d_v[0] - S0 * p.d_v[1] + C1 * p.d_F[0] - S1 * p.d_F[1] + C2 * deltaF[0] * invdt -
                  S2 * deltaF[1] * invdt;

        newv[1] = S0 * p.d_v[0] + C0 * p.d_v[1] + S1 * p.d_F[0] + C1 * p.d_F[1] + S2 * deltaF[0] * invdt +
                  C2 * deltaF[1] * invdt;

        p.d_v = newv;
        p.d_F += deltaF;
    }
}

void InteractingMagneticBilliard::d_propagate_initial_steps() {
    if (d_plist.propagator == ParameterList::PropagatorType::BORIS or
        d_plist.propagator == ParameterList::PropagatorType::YHE2) {
        // During first step of boris integration we step the position
        // forward by dt with 2nd order Spreiter & Walter scheme
        // and velocity forward by dt/2 by the 2nd order Spreiter & Walter scheme
        const double dt = d_dt / 2.0;

        std::vector<Vec2d> pos(d_particles.size(), Vec2d{0.0, 0.0});
        Vec2d deltaF, newv;

        for (unsigned i = 0; i < d_particles.size(); i++) {
            auto& p = d_particles[i];
            pos[i] = p.d_r;
            pos[i][0] += tC1 * p.d_v[0] - tS1 * p.d_v[1] + tC2 * p.d_F[0] - tS2 * p.d_F[1];
            pos[i][1] += tS1 * p.d_v[0] + tC1 * p.d_v[1] + tS2 * p.d_F[0] + tC2 * p.d_F[1];
            p.d_r[0] += C1 * p.d_v[0] - S1 * p.d_v[1] + C2 * p.d_F[0] - S2 * p.d_F[1];
            p.d_r[1] += S1 * p.d_v[0] + C1 * p.d_v[1] + S2 * p.d_F[0] + C2 * p.d_F[1];
        }

        for (unsigned i = 0; i < d_particles.size(); i++) {
            auto& p = d_particles[i];
            deltaF = -1.0 * p.d_F + (this->*d_force)(p);
            for (unsigned j = 0; j < d_particles.size(); j++) {
                if (j != i) {
                    deltaF += d_inter->force(pos[i], pos[j]);
                }
            }
            newv[0] = tC0 * p.d_v[0] - tS0 * p.d_v[1] + tC1 * p.d_F[0] - tS1 * p.d_F[1] + tC2 * deltaF[0] / dt -
                      tS2 * deltaF[1] / dt;

            newv[1] = tS0 * p.d_v[0] + tC0 * p.d_v[1] + tS1 * p.d_F[0] + tC1 * p.d_F[1] + tS2 * deltaF[0] / dt +
                      tC2 * deltaF[1] / dt;
            p.d_v = newv;
        }
        if (d_plist.propagator == ParameterList::PropagatorType::BORIS)
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_boris);
        else if (d_plist.propagator == ParameterList::PropagatorType::YHE2)
            d_propagator = static_cast<propagator_function>(&InteractingMagneticBilliard::d_propagate_yhe2);
    } else {
        std::cerr << "Accidentally handling initial steps separately. This shouldn't happen. Exiting.." << std::endl;
        exit(1);
    }
}

void InteractingMagneticBilliard::d_propagate_boris() {
    for (Particle& p : d_particles) {
        Vec2d vm = p.d_v + p.d_F * d_dt / 2.0;

        Vec2d vp = {0., 0.};
        vp[0] = m_B2dt2_plus_4 * vm[0] + Bdt_times_4 * vm[1];
        vp[1] = m_B2dt2_plus_4 * vm[1] - Bdt_times_4 * vm[0];
        vp /= B2dt2_plus_4;

        p.d_v = vp + p.d_F * d_dt / 2.0;

        p.d_r += p.d_v * d_dt;
    }

    update_forces();
}

void InteractingMagneticBilliard::d_propagate_yhe2() {
    for (Particle& p : d_particles) {
        Vec2d vr = p.d_v + 0.5 * d_dt * p.d_F;
        p.d_v += d_dt * p.d_F;
        p.d_v[0] += Y1 * vr[1] - Y2 * vr[0];
        p.d_v[1] += -Y1 * vr[0] - Y2 * vr[1];
    }

    for (Particle& p : d_particles) {
        p.d_r += p.d_v * d_dt;
    }

    update_forces();
}

void InteractingMagneticBilliard::d_propagate_yoshida4() {
    G2(yoshida_a1 * d_dt);

    G2(yoshida_a2 * d_dt);

    G2(yoshida_a1 * d_dt);
}

void InteractingMagneticBilliard::d_propagate_suzuki4() {
    G2(suzuki_b1 * d_dt);

    G2(suzuki_b2 * d_dt);

    G2(suzuki_b3 * d_dt);

    G2(suzuki_b2 * d_dt);

    G2(suzuki_b1 * d_dt);
}

void InteractingMagneticBilliard::d_propagate_prk4() {
    for (Particle& p : d_particles) {
        p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, prk4_a1 * d_dt);
    }
    update_forces();

    for (Particle& p : d_particles) {
        p.d_v += phi2_v(p.d_r, p.d_v, p.d_F, prk4_b1 * d_dt);
        p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, prk4_a2 * d_dt);
    }
    update_forces();

    for (Particle& p : d_particles) {
        p.d_v += phi2_v(p.d_r, p.d_v, p.d_F, prk4_b2 * d_dt);
        p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, prk4_a3 * d_dt);
    }
    update_forces();

    for (Particle& p : d_particles) {
        p.d_v += phi2_v(p.d_r, p.d_v, p.d_F, prk4_b3 * d_dt);
        p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, prk4_a4 * d_dt);
    }
    update_forces();

    for (Particle& p : d_particles) {
        p.d_v += phi2_v(p.d_r, p.d_v, p.d_F, prk4_b3 * d_dt);
        p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, prk4_a3 * d_dt);
    }

    update_forces();

    for (Particle& p : d_particles) {
        p.d_v += phi2_v(p.d_r, p.d_v, p.d_F, prk4_b2 * d_dt);
        p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, prk4_a2 * d_dt);
    }
    update_forces();

    for (Particle& p : d_particles) {
        p.d_v += phi2_v(p.d_r, p.d_v, p.d_F, prk4_b1 * d_dt);
        p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, prk4_a1 * d_dt);
    }
}

void InteractingMagneticBilliard::d_propagate_prk6() {
    for (Particle& p : d_particles) {
        p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, prk6_a[0] * d_dt);
    }
    update_forces();
    for (unsigned i = 0; i < prk6_b.size(); i++) {
        for (Particle& p : d_particles) {
            p.d_v += phi2_v(p.d_r, p.d_v, p.d_F, prk6_b[i] * d_dt);
            p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, prk6_a[i + 1] * d_dt);
        }
        update_forces();
    }

    for (int i = prk6_b.size() - 1; i >= 0; i--) {
        for (Particle& p : d_particles) {
            p.d_v += phi2_v(p.d_r, p.d_v, p.d_F, prk6_b[i] * d_dt);
            p.d_r += phi1_x(p.d_r, p.d_v, p.d_F, prk6_a[i] * d_dt);
        }
        update_forces();
    }
}

void InteractingMagneticBilliard::d_propagate_SS6_7() {
    for (unsigned i = 0; i < ss6_7.size(); i++) {
        G2(ss6_7[i]);
    }
    for (int i = ss6_7.size() - 2; i >= 0; i--) {
        G2(ss6_7[i]);
    }
}

void InteractingMagneticBilliard::d_propagate_SS6_9() {
    for (unsigned i = 0; i < ss6_9.size(); i++) {
        G2(ss6_9[i]);
    }
    for (int i = ss6_9.size() - 2; i >= 0; i--) {
        G2(ss6_9[i]);
    }
}

void InteractingMagneticBilliard::d_propagate_SS8_15() {
    for (unsigned i = 0; i < ss8_15.size(); i++) {
        G2(ss8_15[i]);
    }
    for (int i = ss8_15.size() - 2; i >= 0; i--) {
        G2(ss8_15[i]);
    }
}

void InteractingMagneticBilliard::d_propagate_SS8_17() {
    for (unsigned i = 0; i < ss8_17.size(); i++) {
        G2(ss8_17[i]);
    }
    for (int i = ss8_17.size() - 2; i >= 0; i--) {
        G2(ss8_17[i]);
    }
}

void InteractingMagneticBilliard::d_propagate_SS4_5() {
    for (unsigned i = 0; i < ss4_5.size(); i++) {
        G2(ss4_5[i]);
    }
    for (int i = ss4_5.size() - 2; i >= 0; i--) {
        G2(ss4_5[i]);
    }
}

}  // namespace bill2d
