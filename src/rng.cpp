/*
 * rng.cpp
 *
 * This file is part of bill2d.
 *


 *
 * Licensed under GNU General Public License 3.0 or later.
 * See COPYING or <http://www.gnu.org/licenses/> for the license.
 */

/*! \file rng.cpp
 * \brief The random number generator is implemented here.
 */

#include "rng.hpp"

namespace bill2d {

/*
 *  Methods of class RNG
 */

RNG::RNG(unsigned long int seed) { d_rng = new std::mt19937(seed); }

RNG::RNG() {
    std::random_device rd;  // This uses system's random number generator
    d_rng = new std::mt19937(rd());
}

RNG::~RNG() { delete d_rng; }

double RNG::uniform_rand(double min, double max) const {
    return std::uniform_real_distribution<double>(min, max)(*d_rng);
}

double RNG::gaussian_rand(double sigma) const { return std::normal_distribution<double>(0, sigma)(*d_rng); }
}  // namespace bill2d
