\documentclass[a4paper,12pt]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{mathtools}
\usepackage[pdftex]{graphicx}
\usepackage{pxfonts}
\usepackage{float}
\linespread{1.03}
\usepackage{cite}
\usepackage{microtype}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{titlesec}
\usepackage[hmargin={30mm,20mm},vmargin={18mm,18mm}]{geometry}
\parskip 7.2pt
\parindent 0pt
\usepackage{xspace}
\usepackage{soul}
\usepackage{hyperref}

\definecolor{mygray}{rgb}{0.4,0.4,0.4}
\definecolor{stdblue}{rgb}{0,0.1098,0.74117}
\definecolor{myorange}{rgb}{1.0,0.4,0}
\lstset{
language=C++,
commentstyle=\color{mygray},
frame=single,
numbers=none,
keywordstyle=\color{stdblue},
showspaces=false,
showstringspaces=false,
stringstyle=\color{myorange},
tabsize=4,
backgroundcolor=\color{black!5}, % set backgroundcolor
basicstyle=\footnotesize,% basic font setting
escapeinside={(*@}{@*)},
}

\newcommand\Billiard{\textbf{Billiard}\xspace}


\titleclass{\subsubsubsection}{straight}[\subsection]
\newcounter{subsubsubsection}[subsubsection]
\renewcommand\thesubsubsubsection{\thesubsubsection.\arabic{subsubsubsection}}

\titleformat{\subsubsubsection}
  {\normalfont\normalsize\bfseries}{\thesubsubsubsection}{1em}{}
\titlespacing*{\subsubsubsection}
{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}

\makeatletter
\def\toclevel@subsubsubsection{4}
\def\l@subsubsubsection{\@dottedtocline{4}{7em}{4em}}
\makeatother

\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{4}

\title{Bill2d: developer manual}
\author{Janne Solanpää}

\begin{document}
\maketitle

\tableofcontents

\section{Reading this guide and developing Bill2d}

In this guide we illustrate the basic structure of the \emph{Bill2d} code.
All the major parts of the code are explained -- at least to some extent --
and we give concrete examples on how to implement new features.

Before you begin, you should be familiar with C$++$ and computational physics/mathematics.
If you are a beginner, we suggest reading the book \emph{C$++$ Primer} by S. Lippman et. al.,
and in order to understand computational physics, have a look at book \emph{A First Course in Computational Physics
and Object-Oriented Programming with C$++$} by D. Yevick.

Finally, keep in mind that we recently got the new C$++$14 standard, which brings many long-needed
features for a computational physicist (such as move schematics). We suggest you get acquainted with the new standard at some point
as \emph{Bill2d} is already using some C$++$14 features.

\section{Overall description the program}
\emph{Bill2d} is a software package designed for molecular dynamics simulation of
classical 2D systems of several point-like particles. The simulation part of the package is written in C$++$,
and the visualization scripts are written in Python.

A single simulation is handled by several classes -- each designed for one spesific task. The following list briefly describes
the purpose of each major class:
\begin{description}
\item[Billiard] The billiards classes (Billiard, MagneticBilliard, InteractingBilliard,
                and InteractingMagneticBilliard) handle the initialization and time-propagation of the particles.
                At every time-step the Billiard class calls an instance of one of the Table classes
                to (i) handle collisions with hard wall boundaries, (ii) handle periodic boundaries,
                and possibly (iii) to check crossings with the Poincar\'e section.
                Whenever the buffers get full, the Billiard class also calls an instance of the Datafile
                to handle saving those buffers to disk.

\item[Table]    The Table classes (i) handle the collisions with hard wall boundaries, (ii) check crossings with
                the chosen Poincar\'e section, and (iii) handle periodic boundaries.

\item[Datafile] The Datafile class handle saving buffers to file. The class actually just
                wraps HDF5 using its C$++$ api.

\item[Interaction] Interaction classes (optional) define different types of particle-particle interactions.

\item[Potential] Potential classes (optional) define external (single-particle) potentials.
\end{description}

In addition, the classes \textbf{Parser} and \textbf{ParameterList} are used to provide the command- and config-file-interfaces for \emph{Bill2d}.
The Parser-class is, however, completely optional when building your own binaries.
\clearpage
A minimal working example could be something along the lines:

\begin{lstlisting}
// testprogram.cxx
#include "datafile.hpp"
#include "billiard.hpp"
#include "rng.hpp"
#include "parameterlist.hpp"
#include "interactions.hpp"

using namespace bill2d;

int main(){
    RNG rng; // Random number generator

    ParameterList plist; // Create the parametelist with default values

    // Change some values of the parameterlist
    plist.min_t = 3;         // Buffers can hold T = 3 long simulations
                             // before they need to be written to a file
    plist.max_t = 5;         // Simulate up to T = 5
    plist.delta_t = 1e-3;    // Time-step
    plist.num_particles = 3; // Number of particles

    Rectangle table(1.5 /* width */, 3.2 /* height */); // A finite table

    Datafile file(plist); // Create the savefile from the parameters

    // Write the table_shape string
    file.write_attribute("table_shape", table.shape_name());



    CoulombInteraction interaction(1.0 /* interaction strenght */);

    InteractingBilliard bill(&interaction, nullptr /* no potential */,
                             &table, &rng, plist, &file);

    bill.prepare(15); // Randomize the initial positions
                      // with total energy = 15

    // Here is the actual propagation

    double& t=bill.get_time();  //Reference to current simulation time

    while(t < plist.max_t){
        bill.propagate();
    }

    bill.save_remaining_data(); // Let's inform that we finished
                                // the simulation and rest of buffers
                                // should be saved to file

    return 0;
}
\end{lstlisting}

This can be compiled with
\begin{lstlisting}
  $ compiler testprogram.cxx -std=c++11 -lbill2d -lhdf5 -lhdf5_cpp\
    -lboost_program_options -lboost_filesystem -lboost_system\
    -lgsl -lgslcblas -I$INCLUDE_PREFIX/bill2d -L$LIB_PREFIX\
    -Wall -Wextra
\end{lstlisting}
\clearpage

\section{class Billiard -- main simulation frame and algorithms}
\newcommand\Particle{\textbf{Particle}\xspace}
\newcommand\Table{\textbf{Table}\xspace}
The \Billiard class holds the particles (defined by the class \Particle), and handles the time-evolution.
After every time-step it calls an instance of \Table to handle possible collisions with the hard-wall boundaries.

Most important functions:
\begin{description}
\item[prepare] This function can be used to set the initial positions of the particles to random values.
\item[prepare\_manual] You can also set desired initial positions for the particles.
\item[propagate] This function evolves the system one time-step forward.
\item[d\_propagator2 etc.] These functions implement the time-propagation algorithms.
\item[save\_remaining\_data] This should always be called after the simulation to save the remaining buffers to file.
\item[create\_particle] This function creates a particle with initial state given by the supplied function. This is meant to be used mid-simulation.
\item[remove\_particle] This function removes all particles that satisfy the condition given by the argument function. This is meant to be used mid-simulation.
\end{description}

In the next few pages we will describe in detail the implementation of a few of the above functions. For rest, please consult the source code in the file \emph{src/billiard.cpp}.


\clearpage
Let's have a closer look at the function \textbf{Billiard::prepare}, which gives random initial positions to the particles.

\begin{lstlisting}
void Billiard::prepare(double energy) {
    // (1)
    double theta=0.0;
    bool cont=true;

    // (2)
    double speed = sqrt(2*energy/static_cast<double>(N()));

    // (3)
    while(cont) {
        cont=false;

        // (4)
        if ((SIG_FLAG == SIGINT) or (SIG_FLAG == SIGTERM)) {
            std::cout << "Killed by user." << std::endl;
            exit(1);
        }

        // (5)
        for (Particle& p : d_particles) {
            p.d_r = Vec2d(d_rng->uniform_rand(d_spawnarea[0],d_spawnarea[2]),
                        d_rng->uniform_rand(d_spawnarea[1],d_spawnarea[3]));
            p.d_r0=p.d_r;
        }

        // (6)
        for (Particle& p : d_particles) {
            theta = d_rng->uniform_rand(0, 2*M_PI);
            p.d_v = speed*Vec2d(cos(theta), sin(theta));
            p.d_v0=p.d_v;
            if (!d_table->point_is_inside(p.d_r))
                cont=true;
        }
    }
}
\end{lstlisting}
\begin{enumerate}
\item Initialize a few local variables.

\item All particles will have equal speed (this is easy to change if you want). As \textbf{Billiard} class calculates only non-interacting systems, all the energy is kinetic energy.

\item The main loop inside which we pick the random initial state for the particles. The loop continues until all particles have a position inside the table.

\item Check for kill/interrupt signal from the user. This is here in case the \emph{d\_spawnarea} is too large compared to the billiard table.

\item Pick random positions for each particle. Each coordinate is picked from a uniform distribution.

\item Randomize the direction of the velocity. Speed was already given earlier. If the particle is not inside the table, run the \emph{while}-loop again.

\end{enumerate}





\clearpage
Let's have a closer look at the function \textbf{Billiard::propagate}, which evolves the system one time-step forward.
\begin{lstlisting}
void Billiard::propagate(){
    // (1)
    if(d_iteration_index%d_sparsesave == 0 and (not d_plist.save_nothing))
        save_iteration();

    // (2)
    if(d_iteration_index==d_minimum_number_of_iterations_minus_one){
        d_iteration_index=0;
        save_data();
        d_num_buffersaves++;
        d_time=d_num_buffersaves*d_plist.minimum_number_of_iterations*d_dt;
    }else{
        d_iteration_index++;
        d_time+=d_dt;
    }

    // (3)
    (this->*d_propagator)();

    // (4)
    for(Particle& p : d_particles)
        d_table->handle_boundary_collisions(p.d_r, p.d_v, p.d_cell,
                                        p.bmap_ptr, d_time, d_hits);
}
\end{lstlisting}


\begin{enumerate}
\item Save data from the \Particle \!s to buffers at every \emph{d\_sparsesave}th time-step. Unless, of course, we
do not wish to save anything (d\_plist.save\_nothing = true).

\item \textbf{If}: When the iteration number reaches the buffer size (given by \\ \emph{d\_minimum\_numbers\_of\_iteration}), we
save the buffers to file by calling \textbf{Billiard::save\_data()}. At the same time (in the if-clause) we also calculate the number of times we have done this, as we can get the simulated time from that.

\textbf{Else}: If the buffers are not full, increase \emph{d\_time} by one time-step. This causes rounding errors, which are more or less corrected by the if-part every now and then.

\item In the constructor of \Billiard, a pointer-to-member-function (of \Billiard) -- named here \emph{d\_propagator} -- was set to point to the correct time-evolution algorithm. Here we just call that.

\item Here we call the \Table to handle collisions with hard wall boundaries. Bouncing map is calculated if \emph{p.bmap\_ptr} != \emph{nullptr} (initialized at the constructor of \Particle).

\end{enumerate}

\clearpage

Finally, let's have a look at the implementation of the velocity Verlet algorithm for noninteracting system (no forces, i.e., the algorithm reduces to the Euler method), i.e., \textbf{Billiard::d\_propagator2}. The code should be pretty self-explanatory.

\begin{lstlisting}
void Billiard::d_propagate2() {
    for(Particle& p : d_particles){
        p.d_r+=d_dt*p.d_v;
    }
}
\end{lstlisting}


For a look at the implementation of the velocity Verlet algorithm with forces, have a look at Sec. \ref{sec:interacting_billiard}.

\subsection{class MagneticBilliard}
The class \textbf{MagneticBilliard} is derived from \textbf{Billiard}. Their functionality is identical, only that \textbf{MagneticBilliard}
implements propagation algorithm(s) for a free (i.e., no external or particle-particle forces) particle in a uniform magnetic field. The algorithm is described in Ref. \cite{spreiterwalter}.

\subsection{class InteractingBilliard}
\label{sec:interacting_billiard}

The class \textbf{InteractingBilliard} is derived from \textbf{Billiard}. Their functionality is identical, only that \textbf{InteractingBilliard} implements propagation algorithm(s) for a system with external potentials and particle-particle interactions. Also the initial positions are randomized while taking into account potential energy contributions.

There are several different propagation algorithms implemented. If looking for a list, please consult the user guide. Let's have a brief look at, say, \textbf{InteractingBilliard::d\_propagate3}.
\begin{lstlisting}
void InteractingBilliard::d_propagate3(){
    for(unsigned i=0;i<d_num_coeff_iters;i++){
        for (Particle& p : d_particles) {
            p.d_v+= d_Dcoeffs[d_num_coeff_iters-1-i]*p.d_F;
            p.d_r+= d_Ccoeffs[d_num_coeff_iters-1-i]*p.d_v;
        }
        update_forces();
    }
}
\end{lstlisting}
This implements a symplectic propagator of type
\[
\exp\left( \Delta t D_H \right) = \prod\limits_{i=1}^3 \exp(d_i \Delta t D_V) \exp(c_i \Delta t D_T),
\]
where the coefficients $d_i$ and $c_i$ (in the code: \emph{d\_Dcoeffs} and \emph{d\_Ccoeffs}) have
been set in the constructor of \textbf{InteractingBilliard}. Here $D_V=\{\cdot, V(x)\}$ and $D_T=\{\cdot, \frac{1}{2}p^2\}$.

\subsection{class InteractingMagneticBilliard}
The class \textbf{InteractingMagneticBilliard} is derived from \textbf{InteractingBilliard}. It adds a uniform magnetic field to the system in addition to the interactions.

\clearpage
\section{Datafile}
\subsection{Overview}
The \textbf{Datafile} class serves as an interface for saving data to a HDF5-file. Note that due to HDF5 and it not being thread-safe, also the Datafile class is not thread safe. Also, it does not seem to work properly even if each thread has its own Datafile. So, don't save trajectories when doing things in parallel.

The standard constructor, \textbf{Datafile::Datafile}( std::string ), takes the path as argument.
There exists also a constructor for escape simulations (for appending data), \textbf{Datafile::Datafile}( std::string, bool).

The \textbf{Datafile}-class (and its child, \textbf{EscapeDatafile}, which is used to save data from bill2d\_escape) uses RAII, i.e., it initializes all its variables when being constructed. Likewise it releases memory upon destruction.

There are essentially two kinds of methods for writing data to file, \textbf{Datafile::write\_buffer} and \textbf{Datafile::write\_attribute}.
The attributes are written to a dataset named 'parameters', whereas the buffers are saved to whatever dataset name given to them.
Currently all datasets must be created before trying to write to them. Otherwise the methods throw an error and exit the program.

\subsection{Saving}
Let's have a closer look at what happens when we are writing a buffer of type \emph{double*}. First, in the initializer methods,
we create the corresponding dataset with the following method
\begin{lstlisting}
void Datafile::create_dataset(std::string & dataset_name, unsigned rank,
                              unsigned memrank, hsize_t dims[],
                              hsize_t maxdims[], hsize_t memdims[],
                              DataType &type, bool chuncked,
                              bool compression) {
    // (1)
    std::pair<Datasets::iterator, bool> res =
         fileobjects.insert(std::make_pair(dataset_name,
            DatasetInfo(file,dataset_name, rank, memrank, dims,
              maxdims, memdims, type, chuncked,compression)));
    // (2)
    if (!res.second){
        throw Exception("bill2d: create_dataset",
                        "A dataset with that name already exists");
    }
}
\end{lstlisting}

The \textbf{Datafile::create\_dataset} method takes as an argument the dataset name, rank (how many dimensions) of the dataset in disk, rank of the data in memory,
dimensions (i.e., [xlength, ylength, \dots]) on file,
maximum dimensions on disk, dimensions of the data in memory, datatype, and whether the dataset should be chuncked and compressed. Only chuncked datasets can have unlimited dimensions.


The implementation does the following:
\begin{enumerate}
\item First, we create a pair of the dataset name and all the HDF5-objects of the dataset (contained within DatasetInfo).
The pair is inserted to a map named \emph{fileobjects}.
\item Finally, we check whether the insertion failed (in which case there is already a dataset with the same name).
\end{enumerate}\clearpage


Let's consider what happens when creating an instance of \textbf{DatasetInfo}:
\begin{lstlisting}
DatasetInfo::DatasetInfo(H5File& file,std::string name, unsigned arank,
         unsigned arank_mem, hsize_t arg_dims[], hsize_t arg_maxdims[],
         hsize_t arg_memdims[], DataType arg_type, bool arg_chuncked,
         bool arg_compression) {
    // (1)
    file_ptr=&file;
    d_name=name;
    compression=arg_compression;
    rank=arank;
    rank_mem=arank_mem;
    offset=new hsize_t[rank];
    chuncksize=new hsize_t[rank];
    maxdims=new hsize_t[rank];
    chuncked=arg_chuncked;
    bool isinf=false;
    initialized=true;
    dims=new hsize_t[rank];
    memdims=new hsize_t[rank_mem];
    type=arg_type;

    // (2)
    if(chuncked){
        for(unsigned i=0;i<rank;i++){
            if(arg_maxdims[i]==H5S_UNLIMITED){
                isinf=true;
            }
        }
        if(not isinf)
            throw Exception("DatasetInfo",
            "Set chuncked saving even if no dimension is unlimited.");
    }

    // (3)
    for(unsigned i=0;i<rank;i++) {
        offset[i]=0;
        chuncksize[i]=arg_dims[i];
        dims[i]=arg_dims[i];
        maxdims[i]=arg_maxdims[i];
    }
    for(unsigned i=0;i<rank_mem;i++) {
        memdims[i]=arg_memdims[i];
    }


    try{
        // (4)
        fspace=DataSpace(rank,dims,maxdims);
        memspace=DataSpace(rank_mem,memdims);

        // (5)
        if (chuncked) {
            plist.setChunk(rank,chuncksize);
            if (compression)
                plist.setDeflate(9);
        }

        // (6)
        dset=file.createDataSet(name,type,fspace,plist);

    }

    catch(Exception& e) {
        e.printError();
        exit(1);
    }
}
\end{lstlisting}
\begin{enumerate}
\item First, we initialize the required variables (these are member variables).
\item Check chunckedness.
\item Set the dimensions, offsets etc.
\item Create the dataspaces corresponding to the data on disk (\emph{fspace}) and in memory (\emph{memspace}).
\item Set chunckedness and compression.
\item Create the dataset with the method \textbf{H5File::createDataSet}.
\end{enumerate}

\clearpage

As a final note, let's see how we write to the dataset.
\begin{lstlisting}
void Datafile::write_buffer(std::string& dataset_name,
                            const double * buffer, size_t nbr_points) {
    try{
        // (1)
        DatasetInfo& tmp=fileobjects.at(dataset_name);

        // (2)
        if((tmp.maxdims[0]<=(tmp.offset[0]
                             +static_cast<hsize_t>(nbr_points))))
            throw Exception("write_buffer: double", "Trying to write
                     larger buffer than maximum length of the dataset");

        // (3)
        if (tmp.chuncked) {
                for(unsigned i=0;i<tmp.rank;i++) {
                    if ((nbr_points!=0)and(i==0))
                        tmp.dims[0]=tmp.offset[0]
                                    +static_cast<hsize_t>(nbr_points);
                    else
                        tmp.dims[i]=tmp.offset[i]+tmp.chuncksize[i];
                }
            tmp.dset.extend(tmp.dims);
        }

        // (4)
        hsize_t tmp2=tmp.chuncksize[0];
        if (nbr_points!=0)
            tmp.chuncksize[0]=nbr_points;

        // (5)
        DataSpace tempspace=tmp.dset.getSpace();
        hsize_t tmp3=tmp.memdims[0];
        if (nbr_points!=0) {
            tmp.memdims[0]=nbr_points;
            tmp.memspace=DataSpace(tmp.rank_mem,tmp.memdims);
        }

        // (6)
        tempspace.selectHyperslab(H5S_SELECT_SET,tmp.chuncksize,tmp.offset);
        tmp.dset.write(buffer, tmp.type,tmp.memspace,tempspace);

        // (7)
        if (tmp.chuncked) {
            for(unsigned i=0;i<tmp.rank;i++) {
                if (tmp.maxdims[i]==H5S_UNLIMITED) {
                    tmp.offset[i]+=tmp.chuncksize[i];
                }
            }
        }

        // (8)
        if (nbr_points!=0) {
            tmp.chuncksize[0]=tmp2;
            tmp.memdims[0]=tmp3;
        }

    }catch(Exception& e) {
        e.printError();
        exit(1);
    }
}
\end{lstlisting}
\begin{enumerate}
\item First, get the DatasetInfo from \emph{fileobjects}. This actually throws if
      the dataset does not exists. Thus, remember to create the dataset first.

\item Here we check if the data we are trying to write is larger than the max. length of the dataset on file.
      This actually is not very accurate. We assume here that \emph{nbr\_points} refers to
      the first dimension. All in all, this works only for rank 1 datasets.

\item In case of  achuncked dataset (i.e., maximum dimension unknown at time of creation),
      we take the dimensions on file as sum of \emph{offset} and \emph{chuncksize} unless
      \emph{nbr\_points} is given, in which case it refers to the first dimensions of the data in memory.

\item If \emph{nbr\_points} given (i.e., not zero), the first chunck dimension is that.

\item Get the dataspace corresponding to file. If \emph{nbr\_points} not zero, then we use that to create
      a new dataspace for the data in memory.

\item Finally, we select the hyperslap from the dataspace on file, and write the data.

\item If chuncked dataset, change the offset to next saving point.

\item Set the modified variables back to what they were.
\end{enumerate}

\subsection{New datasets}
Essentially, one just has to modify \textbf{Datafile::initialize} routine to create the new dataset,
and to call \textbf{Datafile::write\_buffer} somewhere in order to save the data.


\section{Particle-particle interactions}
The interface for defining particle-particle interactions is the following:
\begin{lstlisting}
class Interaction {
  public:
    virtual const Vec2d force(const Vec2d& x, const Vec2d& y) const = 0;
    virtual double energy(const Vec2d& x, const Vec2d& y) const = 0;
    virtual ~Interaction() {}
};
\end{lstlisting}
Here one has essentially two functions, the \textbf{force} and \textbf{energy}. Both
must be implemented for each interaction type.

\section{Potentials}
The interface for potentials is
\begin{lstlisting}
class Potential {
 public:
    virtual const Vec2d force(const Vec2d& r) const = 0;
    virtual double energy(const Vec2d& r) const = 0;
    virtual ~Potential() {}
};
\end{lstlisting}
So, one must always implement the calculation of forces and potential energy.

There's also a class \textbf{Potential\_cont}, which can hold several
different potentials, if you wish to generate new potentials as a sum of the old implmementations.
Note that this is somewhat slow.

\subsection{Checklist for adding a new potential}

These steps outline the process to implement a new potential class:

\begin{enumerate}
    \item Subclass the potential from \texttt{Potential} in \texttt{potentials.hpp}, and implement the \texttt{force} and \texttt{energy} methods in \texttt{potentials.cpp}
    \item Create tests for the \texttt{force} and \texttt{energy} methods in \texttt{test/potentials\_unittest.cpp}
    \item Add the required parameters to \texttt{ParameterList} in \texttt{parameterlist.hpp}, set their default values in the constructor and copy them properly in the copy constructor
    \begin{itemize}
        \item These parameters can be e.g. booleans to enable the potential and control parameters for the potential shape
    \end{itemize}
    \item Add CLI arguments in \texttt{parser.cpp} (constructor), linking the parameters to these
    \item Instantiate the potential in \texttt{BaseBilliardFactory::create\_Potential} (located in \texttt{billiard\_factory.cpp})
    \item Save the parameters to the datafile in \texttt{Datafile::write\_parameters} (located in \texttt{datafile.cpp})
\end{enumerate}

Note that if you are not using the provided executables but you create your own, you just need to implement the potential and then instantiate it in your code.

\section{class ParameterList}
This class holds all essential parameters for knowing what kind of simulation to run.
It can be automatically created by using the \textbf{Parser} class.

\section{Command-line arguments and parser}
Command-line (and configuration file) interface is implemented using the
\textbf{boost::program\_options} library. Adding new options is simple:
\begin{enumerate}
\item First, create the type of parameter you wish to \textbf{ParameterList}.
\item Implement the corresponding command line option to constructor of \textbf{Parser}.
\end{enumerate}



\section{class BilliardFactory}
You know, generating the correct table, interactions, potentials, and \textbf{Billiard}-class can be a pain if done manually.
Hence, we introduce the \textbf{BilliardFactory}, which -- upon construction -- creates the correct methods automatically from the
parameters (\textbf{ParameterList}) supplied to it.

When not saving trajectories, BilliardFactory is safe to use in several threads (it's protected from simultaneous generation of the Billiard-instance).

Have a look how it's used, e.g., in \emph{bill2d\_escape.cpp} or \emph{bill2d\_diffusion\_coefficient.cpp}.

\clearpage
\section{Billiard tables}

\subsection{General}
The interface for tables is relatively simple. The method \textbf{Table::handle\_boundary\_crossings}
takes care of handling collisions with the boundaries, handles crossings of the unit cell for periodic systems, calculates
Poincar\'e section (bmap) if desired, and handles possible collisions with holes in the boundary for open tables.

\begin{lstlisting}
class Table {
public:
    virtual const std::string shape_name() const = 0;

    inline bool handle_boundary_crossings(Vec2d& r, Vec2d& v, pair<int>& cell,
                 std::vector < std::vector < double> >* bmap, const double time,
                 unsigned& hits, bool& hit_flag) const;

    virtual bool point_is_inside(const Vec2d& r) const = 0;

    virtual ~Table() {}

    pair<double> getMaxCoords() const { return maxCoords; }

protected:
   virtual bool handle_collisions(Vec2d& r, Vec2d& v, pair<int>& cell,
                std::vector< std::vector<double>>* bmap, const double time,
                unsigned& hits, bool& hit_flag) const =0;

    pair<double> maxCoords;
};
\end{lstlisting}

Calculation of the Poincar\'e map is done only if \emph{bmap} supplied to \textbf{Table::handle\_boundary\_crossings}
is not \emph{nullptr}.

How this all works is that after each time-step in \textbf{Billiard::propagate}, one calls \textbf{handle\_boundary\_crossings}.
It's implemented as follows

\begin{lstlisting}
inline bool Table::handle_boundary_crossings( ARGUMENTS ) const {
    if (point_is_inside(r))
            return false;
    bool collided = handle_collisions( ARGUMENTS );
    assert(point_is_inside(r)); // Safeguard agains badly implemented tables
    return collided;
}
\end{lstlisting}

So, first we check whether the particle is inside allowed area of the table. If so, the function returns \emph{false}.

Only if the particle is found outside the table boundaries, we handle the collisions with \textbf{handle\_collisions},
which should be implemented for each new child of \textbf{Table} separately. In the end we check that the particle is indeed inside the table (the assert), and return whether collisions to hard-body objects were detected.

Notice that \textbf{handle\_collisions}
should be implemented so that it checks at the end that the particle is inside the table. Implementation could be something like:
\begin{lstlisting}
bool MyTable::handle_collisions(...) const{
       // Collision handling here.

       if(!point_is_inside(r)) handle_collisions(...);
       return true;
}
\end{lstlisting}

\subsection{CompoundTables BoundaryShapes}
It's possible to use previously defined shapes such as circular arcs, lines, and boxes to define new billiard tables.
Such a class combined from several parts is defined as \textbf{CompoundShape} -- the periodic cousin begin \textbf{CompoundShapePeriodic}.
\text{BoundaryShape}s, the elementary parts, are protected members of \textbf{CompoundShape}.

New \textbf{CompoundShape}s can be easily created just by filling the member variables
\emph{shapes} (those that do not contribute to Poincar\'e maps) and \emph{bshapes} (those that contribute to Poincar\'e maps) with \textbf{BoundaryShapes}.
We also have to fill the \emph{d\_shape\_name} and \emph{maxCoords} (basically optional) variables.

Let's have a look how one could define an infinite tube using two horizontal lines.

\subsubsection{Creating a BoundaryShape}
The first thing would be to define the horizontal lines, but this is already implemented as
\textbf{BoundaryHorizontalLine}:
\begin{lstlisting}
class BoundaryHorizontalLine : public BoundaryShape {
public:
    enum Direction { ForbidUpper, ForbidLower };
    BoundaryHorizontalLine(enum Direction arg_dir, double y, double arg_s0,
                           double arg_x0);
    virtual bool point_is_inside(const Vec2d& r) const;
    virtual bool handle_crossing(Vec2d&r, Vec2d& v,
         std::vector < std::vector < double> >* bmap, const double time,
         unsigned& hits, __attribute__((unused)) bool& hit_flag) const;

protected:
    enum Direction dir;
    double pos_y;
    double beg_x;
};
\end{lstlisting}

In all \textbf{BoundaryShape}s, you must implement (i) the \textbf{point\_is\_inside} and (ii)
\textbf{handle\_crossing} functions, which (i) tell whether the particle is inside allowed area of the shape
and (ii) handle the crossing with the boundary, respectively. The method \textbf{handle\_crossing} also
defines how to calculate the Poincar\'e section.

\subsubsection{Creating a CompoundShape}
Let's now use the class \textbf{BoundaryHorizontalLine} to create the infinite tube called
\textbf{InfiniteTube} so that only the collisions with the upper line contribute to the Poincar\'e section:

\begin{lstlisting}
class InfiniteTube : public CompoundShape {
public:
    explicit InfiniteTube(double tube_width);
};
\end{lstlisting}

The constructor could be implemented as
\begin{lstlisting}
InfiniteTube::InfiniteTube(double tube_width){
    // (1)
    d_shape_name << "InfiniteTube(" << tube_width << ")";

    // (2)
    shapes.push_back(new BoundaryHorizontalLine(BoundaryHorizontalLine::ForbidLower,
                      -tube_width/2., 0, 0) );

    // (3)
    bshapes.push_back(new BoundaryHorizontalLine(BoundaryHorizontalLine::ForbidUpper,
                      tube_width/2., 0, 0) );
}
\end{lstlisting}
Here's what happens:
\begin{enumerate}
\item We give our new table a name, which will be saved to the HDF5-file along with the parameter \emph{tube\_width}. This can be when plotting trajectories and the table.
\item Here we create the lower line (\emph{ForbidLower}) at $y=-\text{\emph{tube\_width}}/2$. The two last arguments here do not matter as
we put this to \emph{shapes} container, the members of which do not contribute to Poincar\'e maps.
\item Here we create the upper line (\emph{ForbidUpper}) at $y=\text{\emph{tube\_width}}/2$. As the upper line should contribute to the Poincar\'e section,
it's pushed to \emph{bshapes}. The last two arguments tell that at $(x,y)=(0,\text{\emph{tube\_width}}/2)$ the arc length parameter of the Poincar\'e section gets the value 0.
\end{enumerate}

\subsection{Open tables}
Open tables can be created as \textbf{CompoundShape}s too. Only exception is that
one must define new \textbf{BoundaryShape}s that set the \emph{hit\_flag} argument of
\begin{lstlisting}
virtual bool handle_crossing(Vec2d&r, Vec2d& v,
         std::vector < std::vector < double> >* bmap, const double time,
         unsigned& hits, __attribute__((unused)) bool& hit_flag) const;
\end{lstlisting}
to true, when the particle hits a hole.

\subsection{Periodic systems}
Periodic \textbf{CompoundShape}s are defined in the class \textbf{CompoundShapePeriodic}.
Where a regular \textbf{CompoundShape} checks collisions only with the \textbf{BoundaryShape}s,
the \textbf{CompoundShapePeriodic} also checks passing of the unit cell boundary and handles
it appropriately.

In periodic system, the particle coordinate is always inside the unit cell (which is always a parallellogram)
and the cell number of the particle is changed upon passing from one cell to another. When saving trajectories to file,
the true trajectory is calculated.

\subsection{Poincar\'e sections}
To briefly show how Poincar\'e section can be implemented, we review the implementation of
\textbf{BoundaryHorizontalLine::handle\_crossing} (only the case \emph{ForbidUpper}:
\begin{lstlisting}
// (1)
bool CompoundShape::BoundaryHorizontalLine::handle_crossing(Vec2d& r, Vec2d& v, std::vector<std::vector<double>>* bmap, const double time, unsigned& hits, bool& hit_flag) const {
    // (2)
    std::vector<double> hit(4, 0);

    if (dir == ForbidUpper) {
        if (r[1] > pos_y) {

            // (3)
            if (bmap) {
                // (4)
                hit.at(0) = time;
                hit.at(1) = d_s0+beg_x-r[0];
                hit.at(2) = -v[0];
                hit.at(3) = -atan(v[0]/v[1]);

                // (5)
                bmap->push_back(hit);
            }


            r[1] = 2*pos_y - r[1];
            v[1] *= -1;
            hits++;


            return true;
        }
    }

    /*
     * SNIP
     */
}
\end{lstlisting}
\begin{enumerate}
\item When calling the method, \emph{bmap} is the variable that will hold the information on all collisions of the current particle with the boundaries.
      If the Poincar\'e section is not to be saved, then \emph{bmap} should be set to \emph{nullptr} (done automatically in the class \textbf{Billiard::Particle}).
\item The variable that will record the information on this (possible) particular collision.
\item Calculate Poincar\'e section only if \emph{bmap} is not \emph{nullptr}.
\item Calculate the collision information: time, arc length parameter, tangential velocity, collision angle.
\item Save the collision information.
\end{enumerate}

\section{class Vec2d}
The \emph{Vec2d}-class implements vectors of $\mathbb{R}^2$, and common operations on them such as multiplication/division by scalar, dot product, addition/substraction of two vectors.

\begin{thebibliography}{3}
\bibitem{spreiterwalter} Q. Spreiter and M. Walter, \emph{Classical molecular dynamics simulation with the Velocity Verlet algorithm at strong external magnetic field}, J. Comp. Phys \textbf{152}, 102--119 (1999).
\end{thebibliography}

\end{document}
