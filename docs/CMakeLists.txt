find_package(Doxygen)
find_program(PDFLATEX_EXECUTABLE pdflatex REQUIRED)
find_program(EPSTOPDF_EXECUTABLE epstopdf REQUIRED)
find_program(PANDOC_EXECUTABLE pandoc REQUIRED)
find_program(MD2MAN_EXECUTABLE go-md2man REQUIRED)
find_program(GZIP_EXECUTABLE gzip REQUIRED)

# TODO: are these individual checks really needed? The REQUIRED specifier is supported in Cmake
# find_program since version 3.18, so bumping the minimum version and getting rid of individual
# ifs could be possible. Alternatively, we could drop the REQUIRED specifiers and keep the ifs.
if( ( DOXYGEN_FOUND OR PANDOC_EXECUTABLE ) AND PDFLATEX_EXECUTABLE AND EPSTOPDF_EXECUTABLE )
    # Target api-docs
    if(DOXYGEN_FOUND)
        message(STATUS "Found Doxygen, installing api docs")
        add_custom_command(
                OUTPUT ${CMAKE_BINARY_DIR}/latex/refman.pdf
                COMMAND cd ${CMAKE_BINARY_DIR} && cp ${PROJECT_SOURCE_DIR}/docs/doxy_config ${CMAKE_BINARY_DIR} && ${DOXYGEN_EXECUTABLE} doxy_config && cd latex && make)
        add_custom_target(api-docs ALL DEPENDS ${CMAKE_BINARY_DIR}/latex/refman.pdf)
        install(FILES ${CMAKE_BINARY_DIR}/latex/refman.pdf
                DESTINATION ${CMAKE_INSTALL_PREFIX}/share/doc/bill2d
                RENAME api.pdf)
    endif()

    # Target dev-manual
    message(STATUS "Found pdflatex, compiling and installing developer manual.")


    add_custom_command(
        OUTPUT ${CMAKE_BINARY_DIR}/developer_manual/developer_manual.pdf
        COMMAND mkdir -p ${CMAKE_BINARY_DIR}/developer_manual && ${PDFLATEX_EXECUTABLE} --output-directory ${CMAKE_BINARY_DIR}/developer_manual/ ${PROJECT_SOURCE_DIR}/docs/developer_manual.tex && ${PDFLATEX_EXECUTABLE} --output-directory ${CMAKE_BINARY_DIR}/developer_manual/ ${PROJECT_SOURCE_DIR}/docs/developer_manual.tex)
    add_custom_target(dev-docs ALL DEPENDS ${CMAKE_BINARY_DIR}/developer_manual/developer_manual.pdf)
    install(FILES ${CMAKE_BINARY_DIR}/developer_manual/developer_manual.pdf
            DESTINATION ${CMAKE_INSTALL_PREFIX}/share/doc/bill2d
            )

    if( PANDOC_EXECUTABLE)
        message(STATUS "Found pandoc, compiling and installing userguide.pdf")
        add_custom_command(
               OUTPUT ${CMAKE_BINARY_DIR}/userguide.pdf
               COMMAND pandoc -f markdown -t latex --toc -N -o ${CMAKE_BINARY_DIR}/userguide.pdf ${PROJECT_SOURCE_DIR}/USERGUIDE.md)
        add_custom_target(userguidepdf ALL DEPENDS ${CMAKE_BINARY_DIR}/userguide.pdf)
        install(FILES ${CMAKE_BINARY_DIR}/userguide.pdf
                DESTINATION ${CMAKE_INSTALL_PREFIX}/share/doc/bill2d
        )
    endif()
endif()

if( MD2MAN_EXECUTABLE AND GZIP_EXECUTABLE )
    message(STATUS "Found go-md2man and gzip, generating and installing manpages")
    set(DOCS_TO_BUILD, "$DOCS_TO_BUILD manpages")
    add_custom_command(
        OUTPUT ${CMAKE_BINARY_DIR}/bill2d.1.gz
        COMMAND go-md2man -in ${PROJECT_SOURCE_DIR}/USERGUIDE.md -out ${CMAKE_BINARY_DIR}/bill2d.1 && gzip -f ${CMAKE_BINARY_DIR}/bill2d.1)
    add_custom_target(manpages ALL DEPENDS ${CMAKE_BINARY_DIR}/bill2d.1.gz)
    install(FILES ${CMAKE_BINARY_DIR}/bill2d.1.gz
        DESTINATION ${CMAKE_INSTALL_PREFIX}/share/man/man1/
        )
endif()
